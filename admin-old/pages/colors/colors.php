<?php
// Helper::pre($_SESSION);
$o = '';

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'edit':
      require_once 'edit_color.php';
      break;
    case 'add':
      require_once 'add_color.php';
      break;
    case 'details':
      require_once 'details_color.php';
      break;
    case 'delete':
      $id = isset($_GET['id']) ? $_GET['id'] : null;
      if ( $id != null ) {
        //  return data for artikal id
        if ( Database::num_rows(Database::returnWhereQuery('colors', array('id'=>$id))) > 0 ) {
          // delete from database table artikal
          $query = "DELETE FROM `colors` WHERE id = '{$id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            // delete database table for artikal details table
            Url::header_status(ADMINURL.'?page=color');
          }

        } else {
          // return error
        }
      }
      break;
    default:
      # code...
      break;
  }

} else {
  if ( $color_list = @Database::fetchData('colors') ) {

    // Helper::pre($komesa_list);

    // $o .= '<h2 class="text-center alert alert-success">Colors</h2>';
    $o .= '<h2 class="sub-header">Colors</h2>';
    $o .= '<a href="'.ADMINURL.'?page=colors&options=add" class="btn btn-info add_btn"><i class="fa fa-plus"></i> <span class="show-text">Add color</span></a>';
    $o .= '<div class="clear"></div>';


    $colors_data = Database::fetchData('colors');
    $count = count(Database::fetchData('colors'));

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>Color ID</th>';
    $o .= '      <th>Color Name</th>';
    $o .= '      <th>Color Value</th>';
    $o .= '      <th>Color Date</th>';
    $o .= '      <th>#</th>';
    $o .= '      <th>#</th>';
    $o .= '      <th>#</th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';


    foreach ($colors_data as $item ) {
      $o .= '<tr>';
      $o .= '<td>'.$item['id'].'</td>';
      $o .= '<td>'.$item['color_name'].'</td>';
      $o .= '<td style="background-color:#'.$item['color_value'].'">'.$item['color_value'].'</td>';
      $o .= '<td>'.$item['color_date'].'</td>';
      $o .= '<td>#</td>';
      $o .= '<td>#</td>';
      $o .= '<td>#</td>';
      $o .= '</tr>';
      $o .= '</a>';
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);


  } else {
    $o .= '<div class="wrap wrap-top alert alert-warning text-center">';
    $o .= '<p class="wrap-bottom wrap-10">Trenutno nema boje. Dodajte novu boju</p>';
    $o .= '<a class="btn btn-success" href="'.ADMINURL.'?page=colors&options=add"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add color </a>';
    $o .= '</div>';
  }
}
if ( is_array($o) ) {
  Helper::pre($o);
} else {
  echo $o;
}
