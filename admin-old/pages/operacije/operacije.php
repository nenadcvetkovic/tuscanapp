<?php
// Helper::pre($_SESSION);
$o = '';

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'edit':
      require_once 'edit_operacija.php';
      break;
    case 'add':
      require_once 'add_operacija.php';
      break;
    case 'delete':
      $id = isset($_GET['id']) ? $_GET['id'] : null;
      if ( $id != null ) {
        //  return data for artikal id
        if ( $operacija_data = Database::whereQuery('operacije', array('id'=>$id) ) ) {
          // delete from database table artikal
          $query = "DELETE FROM `operacije` WHERE id = '{$id}'";
          if ( Database::query($query) ) {
            // izbrisati operaciju iz artikla
            $artikal_lista = Database::fetchData('artikal');
            foreach ($artikal_lista as $artikal) {
              if ( Database::num_rows(Database::returnWhereQuery('artikal_data_'.$artikal['id'], array('operacija_id'=>$operacija_data[0]['operacija_id']))) > 0 ) {
                $query = "DELETE FROM `artikal_data_".$artikal['id']."` WHERE operacija_id = '{".$operacija_data[0]['operacija_id']."}'";
                if ( Database::query($query) ) {
                  // izbrisati operaciju iz masina
                  $masine_lista = Database::fetchData('masine');
                  foreach ($masine_lista as $masina) {
                    if ( Database::num_rows(Database::returnWhereQuery('masina_data_'.$masina['id'], array('operacija_id'=>$operacija_data[0]['operacija_id']))) > 0 ) {
                      $query = "DELETE FROM `masina_data_".$masina['id']."` WHERE operacija_id = '{".$operacija_data[0]['operacija_id']."}'";
                      if ( Database::query($query) ) {
                        // izbrisati operaciju iz masina
                        Url::header_status(ADMINURL.'?page=operacije&msg=uspesno');
                      }
                    }
                  }
                }
              }
            }

            // izbrisati oepraciju iz ..


            $o .= 'Uspelo je';
            // delete database table for artikal details table
            $_SESSION['info_page_msg'] = "Operacija sa ID {$id} je uspesno obrisana";

            Url::header_status(ADMINURL.'?page=operacije');
          }

        } else {
          // return error
        }
      }
      break;
    default:
      # code...
      break;
  }

} else {
  if ( $operacije_list = @Database::fetchData('operacije') ) {

    $o .= '<div class="clear"></div>';
    $o .= '<h2 class="sub-header input_padding text-left">Operacije</h2>';
    $o .= '<a href="'.ADMINURL.'?page=operacije&options=add" class="btn btn-info add_btn"><i class="fa fa-plus"></i> <span class="show-text">Dodaj operaciju</span></a>';

    $o .= '<div class="clearfix"></div>';

    $o .= '<input type="text" id="filter_operacija_val" class="col-xs-6">';
    $o .= '<div class="wrap-top btn btn-danger wrap-left reset_operacija">Reset</div>';
    $o .= '<div class="clearfix"></div>';

    $o .= Messages::infoErrorMsg($_SESSION);

    // $limit = $start.",".$item_per_page;
    // $operacije_list = Database::fetchPagData('operacije',$start,$item_per_page);
    // $count = Database::num_rows(Database::returnFetchData('operacije'));

    $operacije_list = Database::fetchData('operacije');

    // $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $table_head = array('ID','Operacija ID', 'Operacija Name', 'Operacija Description', 'Tempo operacije/h', 'Datum Dodavanja');
    $url_array = array(
      'Edit' => ADMINURL.'?page=operacije&options=edit',
      'Delete'  => ADMINURL.'?page=operacije&options=delete'
    );
    $o .= Table::returnListDataTable( $table_head ,$operacije_list,$url_array );

    // $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $o .= '<hr/>';

  } else {
    $o .= '<div class="clear"></div>';
    $o .= '<h2 class="sub-header input_padding text-left">Operacije</h2>';
    $o .= '<a href="'.ADMINURL.'?page=operacije&options=add" class="btn btn-info add_btn"><i class="fa fa-plus"></i> <span class="show-text">Dodaj operaciju</span></a>';
    $o .= '<hr class="delimiter">';
    $o .= '<div class="alert alert-warning">';
    $o .= 'Trenutno nema komese. Dodajte novu komesu <a href="'.ADMINURL.'?page=operacije&options=add"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>';
    $o .= '</div>';
  }
  echo $o;
}
