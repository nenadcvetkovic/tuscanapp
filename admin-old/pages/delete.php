<?php
// start session
session_start();

// requires files
require_once '../../system/libs/Database.php';
require_once '../../system/libs/Url.php';
require_once '../../system/libs/Helper.php';
require_once '../../system/libs/Ip.php';

require_once '../../system/config/config.php';

if ( isset($_POST) ) {

  if ( isset($_POST['action']) ) {

    $action = $_POST['action'];

    if ( $action == 'delete_operaciju_from_artikal') {
      if ( isset($_POST['table_name']) && isset($_POST['operacija_id']) ) {
        $table_name = $_POST['table_name'];
        $operacija_id = $_POST['operacija_id'];

        // $sql = "DELETE FROM `$table_name` WHERE operacija_id='".$operacija_id."'";
        // if ( Database::query($sql) ) {
        if ( Database::deleteData($table_name, array('id'=>$operacija_id) ) ) {
          echo 1;
          // Helper::pre(Database::returnDeleteData($table_name, array('id'=>$operacija_id) ));
        } else {
          echo 0;
        }
      }
    }  else {
      echo 0;
    }

  }

}
