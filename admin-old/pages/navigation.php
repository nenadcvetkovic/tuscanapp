<?php

$link_list = array(
  'Website' => array(
    'name' => 'Site',
    'icon' => 'home',
    'url' => BASEURL,
    'sublinks' => '',
    'visible' => true
  ),
  'Dashboard' => array(
    'name' => 'Dashboard',
    'icon' => 'dashboard',
    'url' => '',
    'sublinks' => '',
    'visible' => true
  ),
  'artikal' => array(
    'name' => 'Artikal',
    'icon' => 'folder-open',
    'url' => 'artikal',
    'sublinks' => array(
      array(
        'url'  => 'add_artikal',
        'name' => 'Add Artikal',
        'icon' => 'plus-sign'
      )
    ),
    'visible' => true
  ),
  'komesa' => array(
    'name' => 'Komese',
    'icon' => 'files-o',
    'url' => 'komesa',
    'sublinks' => array(
      array(
        'url'  => 'add_komesa',
        'name' => 'Add komesa',
        'icon' => 'plus-sign'
      )
    ),
    'visible' => true
  ),
  'operacija' => array(
    'name' => 'Operacije',
    'icon' => 'wrench',
    'url' => 'operacije',
    'sublinks' => array(
      array(
        // 'url'  => 'add_komesa',
        // 'name' => 'Add komesa',
        // 'icon' => 'plus-sign'
      )
    ),
    'visible' => true
  ),
  'set_jobs_for_line' => array(
    'name' => 'Set Jobs For Line',
    'icon' => 'dashboard',
    'url' => 'set_jobs_for_line',
    'sublinks' => '',
    'visible' => false
  ),
  'tablet_ip_settings' => array(
    'name' => 'Set Tablet Ip',
    'icon' => 'dashboard',
    'url' => 'tablet_ip_settings',
    'sublinks' => '',
    'visible' => false
  ),
  'Set artikal data' => array(
    'name' => 'Set artikal data',
    'icon' => 'wrench',
    'url' => 'set_artikal',
    'sublinks' => '',
    'visible' => false
  ),
  'radnici' => array(
    'name' => 'Radnici',
    'icon' => 'user',
    'url' => 'radnici',
    'sublinks' => '',
    'visible' => true
  ),
  'stat' => array(
    'name' => 'Statistika',
    'icon' => 'signal',
    'url' => 'stat',
    'sublinks' => '',
    'visible' => true
  ),
  'linije' => array(
    'name' => 'Linije',
    'icon' => 'align-justify',
    'url' => 'linije',
    'sublinks' => '',
    'visible' => true
  ),
  'analiza' => array(
    'name' => 'Analiza Svih podataka',
    'icon' => 'line-chart',
    'url' => 'analiza',
    'sublinks' => '',
    'visible' => true
  ),
  'analiza po danu' => array(
    'name' => 'Analiza podataka po logu',
    'icon' => 'line-chart',
    'url' => 'analiza&x=po_logovanju',
    'sublinks' => '',
    'visible' => false
  ),
  'zadaci' => array(
    'name' => 'Podesavanja zadataka',
    'icon' => 'cog',
    'url' => 'zadaci',
    'sublinks' => '',
    'visible' => true
  ),
  'ip settings' => array(
    'name' => 'Podesavanja IP',
    'icon' => 'wifi',
    'url' => 'ip_settings',
    'sublinks' => '',
    'visible' => true
  ),
  'options' => array(
    'name' => 'Options',
    'icon' => 'cog',
    'url' => 'options',
    'sublinks' => '',
    'visible' => true
  ),
  'masine' => array(
    'name' => 'Masine',
    'icon' => 'cog',
    'url' => 'masine',
    'sublinks' => '',
    'visible' => true
  ),

);

if ( false ) {

  $o = '';
  // foreach ($link_list as $key => $value) {
  //   $o .= '<a class="wrap btn btn-primary" href="'.BASEURL.'admin/index.php?page='.$value['url'].'">'. $key .'</a>';
  // }
  $o .= '
  <div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Dashboard
  <span class="caret"></span></button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">HTML</a></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">CSS</a></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">JavaScript</a></li>
  <li role="presentation" class="divider"></li>
  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">About Us</a></li>
  </ul>
  </div>
  ';
  foreach ($link_list as $item) {
    if ( $item['visible'] == true ) {
      if ( !preg_match("~^(?:f|ht)tps?://~i", $item['url'] ) ) {
        $url = ADMINURL.'?page='.$item['url'];
      } else {
        $url = $item['url'];
      }
      if ( $item['url'] == '' ) {
        $url = ADMINURL;
      }
      $sublinks = $item['sublinks'];
      $o .= '<a class="wrap btn btn-primary" href="'.$url.'"> ';
      if ( $item['icon'] != '' ) {
        $o .= '<span class="glyphicon glyphicon-'.$item['icon'].'" aria-hidden="true"></span> ';
      }
      $o .= ' '.$item['name'] .'</a>';
      // $o .= '<div class="subnav-container">';
      // foreach ($item['sublinks'] as $key => $value) {
      //   $o .= '<a class="wrap btn btn-primary" href="'.ADMINURL.'?page='.$value.'">'. $key .'</a>';
      // }

      // $o .= '</div>';
    }
  }
  $o .= '<br/>';

  $page = isset( $_GET['page'] ) ? $_GET['page'] : '';
  if ( isset( $link_list[$page]['sublinks']) && $link_list[$page]['sublinks']!='' ) {
    $sublinks = $link_list[$page]['sublinks'];
    $num_sublinks = count($sublinks);
    if ( $num_sublinks > 0 ) {

      foreach ($sublinks as $item) {
        $o .= '<a class="wrap btn btn-primary" href="'.ADMINURL.'?page='.$item['url'].'"><span class="glyphicon glyphicon-'.$item['icon'].'" aria-hidden="true"></span> '. $item['name'] .'</a>';
      }

    } else {
    }
  }

  echo $o;
}
