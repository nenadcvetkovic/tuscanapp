<?php

/*
id
tablet_id
tablet_name
linija_id
operacija_id
operacija_name
operacija_desc
tablet_ip
converted_tablet_ip
*/

/* post Database
tablet_id
linija_id
operacija_id
tablet_ip
*/

$o = '';

// Helper::pre(Database::returnWhereQuery('tablet_ip_settings',array('id'=> $_GET['id'])));
// die();

if ( isset($_GET['id']) && ($_GET['id'] != 0) && ($_GET['id'] != '') && (Database::num_rows(Database::returnWhereQuery('masine',array('id'=>(int)$_GET['id']))) > 0) ) {

// check if isset id from get

$id = (int)$_GET['id'];

if ( !empty($_POST) && isset($_POST) && $_POST!='' ) {
  // Helper::pre($_POST);

  $post = $_POST;
  $array_data = array(
    'masina_name'=>$_POST['masina_name'],
    'datum_dodavanja' => date('Y-m-d H-i-s')
  );


  if ( Database::updateData('masine', $array_data, array('id'=>$id) ) ) {
    $o .= "<script>alert('masina ".$id." successfuly updated');</script>";
  }
}

// continue with edit page

  $masina_details = Database::whereQuery('masine',array('id'=>$id));
  // Helper::pre($tablet_details);

  $o .= '<h1 class="sub-header">Edit masinu </h1>';

  $o .= Messages::infoErrorMsg($_SESSION);

  $o .= '<a class="btn btn-primary" href="'.ADMINURL.'?page=masine" class=""><i class="fa fa-double-angle"></i> Povratak na sve masine </a>';

  $o .= '<form class="add_masinu" action="'.ADMINURL.'?page=masine&options=edit&id='.$id.'" method="post">';

  // tablet id
  $o .= '  <div class="col-md-3 text-right input_padding wrap-10">Masina Name</div>';
  $o .= '   <input id="masina_name" class="col-xs-12 col-sm-12 col-md-6 col-lg-6" type="text" name="masina_name" value="'.$masina_details[0]['masina_name'].'" placeholder="Masina Name">';

  $o .= '  <div class="clear"></div>';
  // $o .= '  <button id="add_tablet" class="text-center btn btn-warning col-xs-12 col-sm-6 col-md-3 col-lg-3 input-style-btn wrap-top pull-right" type="submit" name="submit" value="1">Update tablet</button>';
  $o .= '  <input id="add_tablet" class="text-center btn btn-warning col-xs-12 col-sm-6 col-md-3 col-lg-3 input-style-btn wrap-top pull-right" type="submit" name="submit" value="Update masinu">';
  $o .= '</form>';


  $o .= '<div class="clear"></div>';


} elseif( isset($_GET['masina_id']) ) {
  require_once 'set_masina_data.php';

}else {
  $_SESSION['error_page_msg'] = 'Nema tableta sa ovim id-om';
  Url::header_status(ADMINURL.'?page=masine');
}
