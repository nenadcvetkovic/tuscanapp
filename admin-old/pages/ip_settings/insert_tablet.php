<?php
//
// Helper::pre($_POST);
// die();

/*
id
tablet_id
tablet_name
linija_id
operacija_id
operacija_name
operacija_desc
tablet_ip
converted_tablet_ip
*/
if ( isset($_POST) ) {

  if ( $_POST['tablet_ip']=='192.168.1.' || $_POST['tablet_id']=='' || $_POST['masina_id']=='' || !isset($_POST['masina_id']) ) {
    echo 'Greska';
    $_SESSION['errors']['error_page_msg'] = 'Greska u podacima tableta';
    Url::header_status(ADMINURL.'?page=ip_settings');
  }

  // Helper::pre($_POST);

  // die();

  $array_data = $_POST;

  $array_data['converted_tablet_ip'] = Ip::ReturnConvertedIp($_POST['tablet_ip']);

  $insert_data_array = array(
    'tablet_id' => $_POST['tablet_id'],
    'masina' => $_POST['masina_id'],
    'tablet_ip' => $_POST['tablet_ip'],
    'converted_tablet_ip' => $array_data['converted_tablet_ip']
  );

  // Helper::pre($insert_data_array);
  // Helper::pre(Database::num_rows(Database::returnInsertData('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip']))));
  // Helper::pre(Database::num_rows(Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip']))));

  //if ( Database::num_rows( Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$array_data['converted_tablet_ip'])) ) == 0 ) {

  if ( true ) {
    // die('nema takvih ovde');
    if ( $last_id = Database::insert_data('tablet_ip_settings', $insert_data_array) ) {

      // create new tablet db table
      $sql = "CREATE TABLE IF NOT EXISTS tablet_data_".$last_id."( ".
      "id INT NOT NULL AUTO_INCREMENT, ".
      "operacija_id VARCHAR(11) NULL, ".
      "operacija_datum_dodavanja DATETIME NULL , ".
      "PRIMARY KEY ( id )); ";
      if ( Database::query($sql) ) {
        Url::header_status(ADMINURL.'?page=ip_settings&options=edit&tablet_id='.$last_id);
      }

    } else {
      $_SESSION['error_page_msg'] = '';
      unset($_SESSION['error_page_msg']);
      $_SESSION['error_page_msg'] = 'Error inserting new ip into database!';
    }
    Url::header_status(ADMINURL.'?page=ip_settings');

  } else {
    // die('Ima takvih');
    $_SESSION['error_page_msg'] = '';
    unset($_SESSION['error_page_msg']);
    $_SESSION['error_page_msg'] = 'Postoji Tablet sa ovom Ip adresom';
    // Helper::pre(Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip'])));
    // die('Ima ove ip adrese');
    Url::header_status(ADMINURL.'?page=ip_settings');

  }


}
