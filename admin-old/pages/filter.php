<?php

$o .= '<div class="filter-container alert-info">';

$o .= '<form action="<?php echo ADMINURL; ?>" method="get">';
$o .= '<input name="page" value="analiza" hidden="hidden"/>';
$o .= '    <div class="container">';
$o .= '      <div class="row">';
$o .= '        <!-- <span class=""> Izaberi datum </span></br> -->';
$o .= '        <div class="col-xs-12">';
$o .= '          <div class="filter-box">';
$o .= '            <input class="col-xs-12 col-sm-3 col-md-3 col-lg-2" type="text" name="date" id="datepicker-1" value="';
  $o .= (isset($_GET['date']) ? $_GET['date'] : '' );
  $o .= '" placeholder="Izaberite datum">';
$o .= '            <!-- Vreme OD -->';
$o .= '            <!-- <span  class="">Vreme od</span> -->';
$o .= '            <select class="col-xs-6 col-sm-3 col-md-3 col-lg-2"  id="start_time" name="start_time">';
$o .= '              <option value="0"';
if ( isset($_GET['start_time']) && $_GET['start_time'] == 7 ) {
  $o .= 'selected="selected"';
}
$o .= '>Vreme OD</option>';

$o .= '              <option value="00">00</option>';
              for ($i=1; $i < 24; $i++) {
                  $o .= '<option class="" value="'.$i.'"';
                if ( isset($_GET['start_time']) && $_GET['start_time'] == $i ) {
                  $o .= 'selected="selected"';
                }
                  $o .= '>'.$i.'</option>';
              }
$o .= '            </select>';
$o .= '            <!-- Vreme DO -->';
$o .= '            <!-- <span  class="">Vreme do</span> -->';
$o .= '            <select class="col-xs-6 col-sm-3 col-md col-lg-2" name="end_time">';
$o .= '              <option value="0"';
if ( isset($_GET['end_time']) && $_GET['end_time'] == 15 ) {
  $o .= 'selected="selected"';
}
$o .= '>Vreme DO</option>';
$o .= '              <option value="00">00</option>';
              for ($i=1; $i < 24; $i++) {
                  $o .= '<option class="" value="'.$i.'"';
                if ( isset($_GET['end_time']) && $_GET['end_time'] == $i ) {
                  $o .= 'selected="selected"';
                }
                  $o .= '>'.$i.'</option>';
              }
$o .= '            </select>';
$o .= '          </div>';
          if ( true )  { // if isset $_GET['komesa']
              $o .= '<select name="komesa" class="select-komesa col-xs-9 col-sm-6 col-md-3 col-lg-2">';
            if( isset($_GET['komesa']) && $_GET['komesa'] != '0' ) {
                $o .= '<option ';
                $o .= 'selected="selected"';
                $o .= ' value="'.$_GET['komesa'].'">'.$_GET['komesa'].'</option>';
            } else {
                $o .= '<option name="komesa"> Izaberite datum </option>';
            }
              $o .= '</select>';
          }
$o .= '        <div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-3 col-sm-3 col-md-3 col-lg-2">Change komesa</div>';

$o .= '          <!-- Radnici -->';
 $radnici = Database::fetchData('radnici_lista');
$o .= '          <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="radnik">';
$o .= '            <option value="0"';

if ( isset($_GET['radnik']) && $_GET['radnik'] == 0 )  {
  $o .= 'selected="selected"';
}
$o .= '>Izaberi radnika</option>';
            foreach ($radnici as $radnik) {
                $o .= '<option value="'.$radnik['id'].'"';
              if ( isset($_GET['radnik']) && $_GET['radnik'] == $radnik['id'] ) {
                  $o .= 'selected="selected"';
              }
                $o .= '>ID:'.$radnik['id'].' | '.$radnik['radnik_name'].'</option>';
            }
$o .= '          </select>';

$o .= '          <!-- Linija -->';
          $sve_linije = Database::fetchData('linija');
$o .= '          <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="linija">';
$o .= '            <option value="0"';
if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) {
$o .= 'selected="selected"';
}
$o .= '          >Izaberi liniju</option>';
            foreach ($sve_linije as $linija) {
                $o .= '<option value="'.$linija['id'].'"';
              if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
                  $o .= 'selected="selected"';
              }
                $o .= '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
            }
$o .= '          </select>';
$o .= '          <div class="clear"></div>';

$o .= '          <hr class="delimiter">';

$o .= '          <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />';
$o .= '          <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->';
$o .= '          <a class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=analiza">Reset Filter</a>';
$o .= '        </div>';
$o .= '      </div>';
$o .= '    </div>';
$o .= '  </form>';

$o .= '</div> <!-- filter-box -->';
$o .= '<div class="filter-btn-visible btn btn-success wrap">Show filter options</div>';
