<?php

// Helper::pre($_POST);

//  id, radnik_id, radnik_name, radnik_lifetime_from, linija, sifra
$o = '';

if (  (isset($_POST['radnik_name']) && $_POST['radnik_name']!='' ) &&
      (isset($_POST['linija']) && $_POST['linija']!='' ) &&
      (isset($_POST['sifra']) && $_POST['sifra']!='' )
) {
  if ( isset($_POST)) {

    $radnik_id = $_POST['sifra'];
    $radnik_name = $_POST['radnik_name'];
    $radnik_lifetime_from = isset($_POST['radnik_lifetime_from']) ? $_POST['radnik_lifetime_from'] : date("Y-m-d H:i:s"); // ?
    $linija = $_POST['linija'];
    $sifra = $_POST['sifra'];

    $table_name = 'radnici_lista';

    $key_value_data = array(
      'radnik_id' => $radnik_id,
      'radnik_name' => strtoupper($radnik_name),
      'radnik_lifetime_from' => $radnik_lifetime_from,
      'linija' => $linija,
      'sifra' => $sifra
    );

    if ( Database::whereQuery('radnici_lista', array('radnik_id'=>$radnik_id) ) == 0 ) {
      // nema takve komese

      if ( Database::insert_data($table_name, $key_value_data) ) {
        $_SESSION['info_page_msg'] = 'Uspesno dodAT radnik';
        // die('dodata');
        Url::header_status(ADMINURL.'?page=radnici');
      } else {
        $_SESSION['error_page_msg'] = 'Radnik nije dodat';
        Helper::pre(Database::returnInsertData($table_name, $key_value_data));
        // die('nije');
        // Url::header_status(ADMINURL.'?page=add_komesa&i=000');
      }

    } else {
      // ima takve komese
      $o .= '<div class="alert alert-danger"> Ima radnika sa ovim id-em </div>';
      // $_SESSION['error_page_msg'] = 'Ima takve radnice';
      // Url::header_status(ADMINURL.'?page=radnici&options=add_radnik');
    }
  }

} else {
  $o .= '<div class="alert alert-danger">No data inserted into form</div>';
}
$o .= '';


?>
<div class="clear"></div>

<div class="col-xs-12">

  <h2 class="sub-header">Dodaj novog radnika</h2>

  <div class="breadcrumb">
    <a href="<?php echo ADMINURL; ?>?page=radnici" class=""><i class="fa fa-angle-double-left"></i> Povratak na sve radnike</a>
    <div class="clear"></div>
  </div>

  <!-- $radnik_id = $_POST['radnik_id'];
  $radnik_name = $_POST['radnik_name'];
  $radnik_lifetime_from = isset($_POST['radnik_lifetime_from']) ? $_POST['radnik_lifetime_from'] : date("Y-m-d H:i:s"); // ?
  $linija = $_POST['linija'];
  $sifra = $_POST['sifra']; -->

  <div id="message_box" class="text-center"></div>
  <form class="add_radnika" action="<?php echo ADMINURL . '?page=radnici&options=add_radnik'; ?>" method="post">
    <input id="radnik_name" class="col-xs-12 col-sm-12 col-md-6 col-lg-6" type="text" name="radnik_name" value="" placeholder="Radnik Ime/Prezime">
    <!-- <input type="text" name="artikal_id" value="" placeholder="Artikal ID"><br/> -->
    <select id="select_liniju" class="col-xs-12" class="select_liniju" name="linija">
      <optgroup label="linija_group">
        <option value="default" disabled selected="selected">Select Liniju</option>
        <?php
        $linija_data = Database::fetchData('linija');
        foreach ($linija_data as $item) {
          # code...
          $o .= '<option value="'.$item['id'].'">'.$item['linija_name'].'</option>';
        }
        echo $o;
        ?>
      </optgroup>
    </select>
    <input id="datepicker" class="radnik_lifetime_from col-xs-12" type="text" name="radnik_lifetime_from" value="" placeholder="Datum rada | primer: 2016-07-18">
    <input id="sifra" class="col-xs-12" type="text" name="sifra" value="" placeholder="Sifra radnika">
    <hr class="delimiter">
    <button id="add_radnika" class="text-center btn btn-success" type="submit" name="submit" value="1">Add Radnika</button>
  </form>

</div>

</div>
