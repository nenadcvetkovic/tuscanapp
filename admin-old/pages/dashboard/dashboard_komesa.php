<?php

// Helper::pre($_GET);


$o .= '<a class="btn btn-info" href="'.ADMINURL.'"> Back to dashboard </a>';

$o .= '<h2 class="sub-header text-left input_padding">Komese</h2>';

if ( Database::num_rows("SELECT * FROM `komesa`") > 0 ) {
  $komesa_data = Database::fetchData('komesa');

  $o .= '<div class="table-responsive">';
  $o .= '<table class="table table-striped">';
  $o .= '  <thead>';
  $o .= '    <tr>';
  $o .= '      <th>Komesa ID</th>';
  $o .= '      <th>Komesa Name</th>';
  $o .= '      <th class="text-center">Komada po komesi</th>';
  $o .= '      <th class="text-left">Operacije po komesi</th>';
  $o .= '      <th class="text-center">Ukupno uradjenih</th>';
  $o .= '      <th class="text-center">Danasnji komadi</th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '    </tr>';
  $o .= '  </thead>';
  $o .= '  <tbody>';


  foreach ($komesa_data as $item ) {
    $komesa_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$item['id'])));
    $today_komesa_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('work_dat'=>date('Y-m-d'), 'komesa_id'=>$item['id'])));
    $komada_po_komesi = $item['artikal_komada_per_komesa'];


    $o .= '<tr>';
    $o .= '<td>'.$item['komesa_id'].'</td>';
    $o .= '<td>'.$item['komesa_name'].'</td>';
 
    $komesa_items = Database::whereQuery('komesa', array('artikal_id'=>$item['id']));
    $komada_po_komesi = $item['artikal_komada_per_komesa'];

    $o .= '<td class="text-center"> '.$komada_po_komesi.' </td>';

    if ( Database::num_rows(Database::returnWhereQuery('artikal', array('id'=>$item['artikal_id']))) > 0 ) {
      $artikal_data = Database::whereQuery('artikal', array('id'=>$item['artikal_id']));
      // Helper::pre($artikal_data);

      if ( Database::num_rows(Database::returnFetchData('artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'])) > 0 ) {
        $artikal_item = Database::fetchData('artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id']);
        // Helper::pre($artikal_item);

        $o .= '<td>';
        foreach ($artikal_item as $artikal_operacija) {
          $o .= '<div><a href="#">'.$artikal_operacija['operacija_name'].'</a></div>';
        }
        $o .= '</td>';

        $o .= '<td class="text-center">';
        foreach ($artikal_item as $artikal_operacija) {
          // Helper::pre($artikal_operacija);
          $op = Database::whereQuery('operacije', array('operacija_id'=>$artikal_operacija['operacija_id']));
          $op_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$item['id'] ,'operacija_id'=>$op[0]['id'])));
          $o .= '<div style="border-bottom:thin solid #000"';
          if ($komada_po_komesi * $artikal_operacija['komada_po_artiklu'] <= $op_data_count ) {
            $o .= ' class="text-center alert-danger" ';
          } else {
            $o .= ' class="text-center alert-success" ';
          }
          $o .= '>'.$komada_po_komesi * $artikal_operacija['komada_po_artiklu'] . ' / ' . $op_data_count.'</div>';

        }
        $o .= '</td>';

        $o .= '<td class="text-center">';
        foreach ($artikal_item as $artikal_operacija) {
          $op = Database::whereQuery('operacije', array('operacija_id'=>$artikal_operacija['operacija_id']));
          $op_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('artikal_id'=>$item['id'], 'working_day'=>date('Y-m-d') ,'operacija_id'=>$op[0]['id'])));
          $o .= '<div>'.$op_data_count.'</div>';
        }
        $o .= '</td>';

        $o .= '</tr>';
      }


    }


  }
  $o .= '  </tbody>';
  $o .= '</table>';
  $o .= '</div>';
} else {
  // Nema artikla
  $o .= '<div class="alert alert-lightred text-center input_padding">';
  $o .= '<h3> Nema artikla trenutno </h3>';
  $o .= '<h5><a href="'.ADMINURL.'?page=artikal&options=add_artikal"> Dodajte novi artikal </a></h5>';
  $o .= '</div>';
}
