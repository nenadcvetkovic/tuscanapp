<?php

$o = '';


$o = '<div class="container-fluid">';


  $o .= '<a class="btn btn-info wrap-right" href="'.ADMINURL.'?page=dashboard&options=linija"> Detalji linija </a>';
  $o .= '<a class="btn btn-info wrap-right" href="'.ADMINURL.'?page=dashboard&options=artikal"> Detalji artikla </a>';
  $o .= '<a class="btn btn-info wrap-right" href="'.ADMINURL.'?page=dashboard&options=komesa"> Detalji komesa </a>';
  $o .= '<a class="btn btn-info wrap-right" href="'.ADMINURL.'?page=dashboard&options=operacija"> Detalji operacija </a>';

  $o .= '<hr class="delimiter">';

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'komesa':
      require_once 'dashboard_komesa.php';
      break;
    case 'linija':
      require_once 'dashboard_linija.php';
      break;
      case 'artikal':
        require_once 'dashboard_artikal.php';
        break;
      case 'operacija':
        require_once 'dashboard_operacija.php';
        break;
    default:
      # code...
      break;
  }

} else {

  $o .= '<div class="col-xs-12 alert alert-white">';
  $linija_data = Database::fetchData('linija');
  foreach ($linija_data as $linija ) {

    if ( $linija['linija_id'] == '0') {
      continue;
    }
    $success_data = Database::whereQuery('working_day_session_details',array('linija_id'=>$linija['linija_id']));
    $counter = Database::num_rows(Database::returnWhereQuery('working_day_session_details',array('linija_id'=>$linija['linija_id'])));
    $o .= '<div class="alert alert-info col-xs-4">';
    $o .= '<h4 class="sub-header">LINIJA '.$linija['linija_id'].'</h4>';
    $o .= '<h6>Uradjenih komada '.$counter.'</h6>';
    $o .= '</div>';
  }

  $o .= '</div>';


  // $o .= '<div class="col-xs-12 alert alert-info wrap">';
  // $o .= '<h1> Linije <h1>';
  // $linije_data = Database::fetchData('linija');
  //
  // $o .= '<div id="carousel-example-generic" class="carousel slide text-center" data-ride="carousel">';
  // $o .= '<ol class="carousel-indicators">';
  // $o .= '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
  // $o .= '<li data-target="#carousel-example-generic" data-slide-to="1"></li>';
  // $o .= '<li data-target="#carousel-example-generic" data-slide-to="2"></li>';
  // $o .= '</ol>';
  // $o .= '<div class="carousel-inner" role="listbox">';
  //   foreach ($linije_data as $linija ) {
  //     $o .= '<div class="item';
  //     if ( $linija === reset($linije_data) ) {
  //       // We are at the first element
  //       $o .= ' active';
  //     }
  //     $o .= '">';
  //     $o .= '<h4>'.$linija['linija_name'].'</h4>';
  //     $o .= '</div>';
  //   }
  // $o .= '  </div>';
  // $o .= '  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">';
  // $o .= '    <span class="icon-prev" aria-hidden="true"></span>';
  // $o .= '    <span class="sr-only">Previous</span>';
  // $o .= '  </a>';
  // $o .= '  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">';
  // $o .= '    <span class="icon-next" aria-hidden="true"></span>';
  // $o .= '     <i class="fa fa-angle-double-right" aria-hidden="true"></i>';
  // $o .= '    <span class="sr-only">Next</span>';
  // $o .= '  </a>';
  // $o .= '</div>';
  //
  // $o .= '</div>';

  $o .= '<div class="col-xs-12">';

  $o .= '<div class="col-xs-6 wrap alert alert-default">';
  $o .= '<div class="panel panel-default">';
  $o .= '  <div class="panel-heading text-center">';
  $o .= '    <h3 class="panel-title"> Komese </h3>';
  $o .= '  </div>';
  $o .= '  <div class="panel-body">';
  $o .= '    <h4>Aktivne Komese za radni dan '. date('d.m.Y') .'</h4>';
  $o .= '     <ol>';

    if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')))) > 0 ) {
      $komesa_array = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')));
      // Helper::pre($komesa_array);
      foreach ($komesa_array as $kitem) {
        $komesa_data = Database::whereQuery('komesa', array('id'=>$kitem['komesa_id']));
        $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));

        $work_data = Database::whereQuery('working_day_session', array('work_day'=>date('Y-m-d'), 'komesa'=>$komesa_data[0]['id']));

        $work_count = 0;
        if ( $work_data != '' || $work_data != 0 ) {
          foreach ($work_data as $item) {
            $work_count += $item['operacija_hint'];
          }
        }
        $o .= '<li>'.$komesa_data[0]['komesa_id'].' :: ARTIKAL '.$artikal_data[0]['artikal_id'].' URADJENO = '.$work_count.'</li>';
      }
    }

  $o .= '      </ol>';
  $o .= '  </div>';
  $o .= '</div>';
  $o .= '</div>';

  $o .= '<div class="col-xs-6 wrap alert alert-default">';
  $o .= '<div class="panel panel-default">';
  $o .= '  <div class="panel-heading">';
  $o .= '    <h3 class="panel-title"> Radnici </h3>';
  $o .= '  </div>';
  $o .= '  <div class="panel-body">';
  $o .= '     <h4> Ukupan broj radnika po linija je: </h4>';
  $o .= '     <ul>';
  if ( Database::num_rows(Database::returnFetchData('linije')) > 0 ) {
    $linije = Database::fetchData('linije');
    foreach ($linije as $linija ) {
      if ( $linija == '1' ) {
        continue;
      }
      if ( Database::num_rows(Database::returnWhereQuery('radnici_lista')) > 0 ) {
        $o .= '      <li> Linija 1: 54 od 56</li>';
      }
    }
  }

  $o .= '     </ul>';
  $o .= '  </div>';
  $o .= '   <div class="panel-footer">Panel footer ';
  $o .= '     <a href="#" class="pull-right">';
  $o .= '       Detalji';
  $o .=   Helper::returnIcon('menu-right');
  $o .= '     </a>';
  $o .= '     <div class="clear"></div>';
  $o .= '   </div>';
  $o .= '</div>';
  $o .= '</div>';


  $o .= '</div>';

  $o .= '<div class="col-xs-12">';

  $o .= '<div class="col-xs-6 wrap alert alert-default">';
  $o .= '<div class="panel panel-default">';
  $o .= '  <div class="panel-heading">';
  $o .= '    <h3 class="panel-title"> Operacije </h3>';
  $o .= '  </div>';
  $o .= '  <div class="panel-body">';


  $o .= '    <h4>Ukupno operacija u radu je: x operacija</h4>';
  $o .= '  </div>';
  $o .= '</div>';

  $o .= '</div>';
  // $o .=  date('s', mktime(1, 1, 22, 0, 0, 0));
  // $o .= Helper::returnSecondsFromTimeString('00:10:20');



  $o .= '</div>';

}

$o .= '</div>';


echo $o;
