<?php
$o = '';

$o .= '<h2 class="sub-header text-left">Add Artikal</h1>';
$o .= '<a class="btn btn-primary wrap-btn" href="'.ADMINURL.'?page=artikal"><i class="fa fa-angle-double-left"></i> Back</a>';

$o .= Messages::infoErrorMsg($_SESSION);

if ( (isset($_POST['artikal_id']) && $_POST['artikal_id']!='' ) &&
      ( isset($_POST['artikal_name']) && $_POST['artikal_name']!='' ) &&
      ( isset($_POST['artikal_desc']) && $_POST['artikal_desc']!='' ) &&
      ( isset($_POST['linija_rada']) && $_POST['linija_rada']!='' )
) {

  if ( isset($_POST) && $_POST != null ) {
    Helper::pre($_POST);
  }

  // Add details into artikal list table
  $key_value_data = array(
    'artikal_id'          => $_POST['artikal_id'],
    'artikal_name'        => strtoupper($_POST['artikal_name']),
    'artikal_desc'        => $_POST['artikal_desc'],
    'artikal_datum_unosa' => date("Y-m-d H:i:s"),
    'artikal_status'      => '0',
    'linija_rada'         => $_POST['linija_rada']
  );

  $artikal_name = $key_value_data['artikal_id'];
  if ( Database::num_rows("SELECT id FROM artikal WHERE artikal_id='$artikal_name'") > 0 ) {
    $o .= 'There are some artikal with these name';
  } else {
    if ( $last_id = Database::insert_data('artikal', $key_value_data) ){
      $o .= '<p>NEW Artikal successfully inserted into db</p>';

      $new_artikal = $key_value_data['artikal_id'].'_'.$last_id;

      $replace_char = array('/','=');
      $new_artikal = str_replace($replace_char, '_', $new_artikal);

      // Create db table for new artikal
      $sql = "CREATE TABLE IF NOT EXISTS artikal_data_".$new_artikal."( ".
      "id INT NOT NULL AUTO_INCREMENT, ".
      "operacija_id VARCHAR(11) NULL, ".
      "opis_operacije VARCHAR(100) NULL, ".
      "operacija_name VARCHAR(100) NULL, ".
      "tempo_operacije INT(3) NULL, ".
      "komada_po_artiklu INT(5) NULL DEFAULT '1' , ".
      "komada_po_operaciji INT(2) NULL DEFAULT '1' , ".
      "operacija_datum_dodavanja DATETIME NULL , ".
      "PRIMARY KEY ( id )); ";

      if ( Database::query($sql) ) {
        $o .= 'Creating db '.$new_artikal.'_artikal_data successfuly';
        //Url::header_status(ADMINURL.'?page=artikal&options=edit&id='.$last_id);

        $color_sql = "CREATE TABLE IF NOT EXISTS artikal_data_".$new_artikal.'_color'."( ".
        "id INT NOT NULL AUTO_INCREMENT, ".
        "color_id INT(3) NULL, ".
        "color_datum_dodavanja DATETIME NULL , ".
        "color_status INT(1) NULL , ".
        "PRIMARY KEY ( id )); ";
        if ( Database::query($color_sql) ) {
          $o .= 'Creating db artikal_data_'.$new_artikal.'_color'.' successfuly';

          // insert all colors
          $colors = Database::fetchData('colors');
          $sql = "INSERT INTO `artikal_data_".$new_artikal."_color` (color_id, color_datum_dodavanja, color_status) VALUES";
          $date = date('Y-m-d');
          foreach ($colors as $item) {
            // Helper::pre($item);
            $sql .= " ( '".$item['id']."','".$date."', '0'),";
          }
          $sql = rtrim($sql, ',');
          $_SESSION['sdfsdgf']['2222'] = $sql;
          Database::query($sql);

          $size_sql = "CREATE TABLE IF NOT EXISTS artikal_data_".$new_artikal.'_size'."( ".
          "id INT NOT NULL AUTO_INCREMENT, ".
          "size_id int(3) NULL, ".
          "size_datum_dodavanja DATETIME NULL , ".
          "size_status INT(1) NULL , ".
          "PRIMARY KEY ( id )); ";
          if ( Database::query($size_sql) ) {
            $o .= 'Creating db artikal_data_'.$new_artikal.'_size'.' successfuly';

            // insert all colors
            $sizes = Database::fetchData('sizes');
            $sql = "INSERT INTO `artikal_data_".$new_artikal."_size` (size_id, size_datum_dodavanja, size_status) VALUES";
            foreach ($sizes as $item) {
              // Helper::pre($item);
              $sql .= " ( '".$item['id']."','".date('Y-m-d')."', '0'),";
            }
            $sql = rtrim($sql, ',');
            Database::query($sql);
            Url::header_status(ADMINURL.'?page=artikal&options=edit&id='.$last_id);
          }


        }

      } else {
        $o .= 'Creating db '.$new_artikal.'_artikal_data failed';
        $o .= $sql;
      }

      // Insert data into new created db tatble for new artikal



    } else {
      $o .= '<div class="alert alert-danger">';
      $o .= '<p class="panel-body">Error inserting "ARTIKAL" data into db `artikal`!!!</p>';
      $o .= Database::returnInsertData('artikal', $key_value_data);
      $o .= '</div>';
    }

  }


} else { // If not set post data from post form
  // $o .= 'There are some error in your form.';
}

echo $o;
$o = '';

?>
<!-- START Add new artikal form  -->
<form class="add_artikal" action="<?php echo ADMINURL.'?page=artikal&options=add_artikal'; ?>" method="post">
  <div class="alert alert-default col-xs-12 grey-box">
    <br/>
    <div class="col-xs-12">
      <div class="col-xs-3 text-right input_padding wrap"> Artikal ID </div><input class="col-xs-8" type="text" name="artikal_id" value="" placeholder="Artikal ID">
    </div>
    <div class="col-xs-12">
      <div class="col-xs-3 text-right input_padding wrap"> Artikal Name </div><input class="col-xs-8" type="text" name="artikal_name" value="" placeholder="Ime Artikla">
    </div>
    <div class="col-xs-12">
      <div class="col-xs-3 text-right input_padding wrap"> Artikal Description </div><input class="col-xs-8" type="text" name="artikal_desc" value="" placeholder="Opis Artikla">
    </div>
    <div class="col-xs-12">
      <div class="col-xs-3 text-right input_padding wrap"> Linija rada </div>
      <!-- <input class="col-xs-8" type="text" name="linija_rada" value="" placeholder="Linija rada"> -->
      <select id="select_linija" class="col-xs-8" class="select_linija" name="linija_rada">
        <optgroup label="Sve Linije">
          <option value="default" disabled selected="selected">Select linija</option>
          <?php
          $o = '';
          $linija_list = Database::fetchData('linija');
          foreach ($linija_list as $item) {
            # code...
            $o .= '<option value="'.$item['id'].'"';
            if ( false ) {
              $o .= ' selected="selected"';
            }
            $o .= '>'.$item['id']."-".$item['linija_id']."-".$item['linija_name'].'</option>';
          }
          echo $o;
          $o = '';
          ?>
        </optgroup>
      </select>
    </div>
    <button class="btn btn-primary pull-right wrap-10" type="submit" name="submit" value="1">Add Artikal</button>
  </div>
</form>
<!--  END of New Artikal Form -->
<div class="clear"></div>
