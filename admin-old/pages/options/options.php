<?php

if ( !empty($_GET['options']) ) {
  switch ($_GET['options']) {
    case 'backupdb':
      require_once 'backupdb.php';
      break;
    case 'masine':
      require_once 'masine.php';
      break;
    case 'neradni-dani':
      require_once 'neradni_dani.php';
      break;
    case 'db_table_set':
      require 'db_table_set.php';
      break;
    default:
      # code...
      break;
  }
}  else {
  $o = '';




  if ( !empty($_POST) ) {
    // Helper::pre($_POST);

    $tuscan_family_options = $_POST['tuscan_family_options'];

    foreach ($tuscan_family_options as $key => $value) {
      $sql = "UPDATE `tuscan_family_options` SET `option_value`='".$value."' WHERE `option_name`='".$key."'";
      Database::query($sql);
    }

  }

    $o .= '<a href="'.ADMINURL.'?page=options&options=masine" class="btn btn-success"> PODESAANJE MASINA </a>';
    $o .= '<a href="'.ADMINURL.'?page=options&options=db_table_set" class="btn btn-danger wrap-left"> Podesavnje baze </a>';
    $o .= '<a href="'.ADMINURL.'?page=options&options=neradni-dani" class="btn btn-danger wrap-left"> Podesavnje radnih dana </a>';
    $o .= '<hr class="delimiter">';

  if ( Database::num_rows(Database::returnFetchData('tuscan_family_options')) == 0 ) {
    // nema optionsa
    $o .= '<h3 class="alert alert-lightred text-center"> Nema opcija trenutno </h3>';

  } else {

      $o .= '<h3 class="sub-header">Podesavanja</h3>';

        $o .= '<form name="options" action="'.ADMINURL.'?page=options" method="post">';

        $options = Database::fetchData('tuscan_family_options');

        foreach ($options as $option ) {
          // Helper::pre($option);
          $o .= '<label class="col-xs-6 input_padding">';
          $o .= $option['option_title'];
          $o .= '<p><i><small>'.$option['option_desc'].'</small></i></p>';
          $o .= '</label>';
          $o .= '<input class="col-xs-5" type="text" name="tuscan_family_options['.$option['option_name'].']" value="'.$option['option_value'].'"/>';
        }

        $o .= '<input type="submit" name="submit" value="Sacuva podesavanja">';
        $o .= '</form>';
    $o .= '</div>';
  }

  echo $o;
}
