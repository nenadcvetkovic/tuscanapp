<?php
// CREATE TABLE `neardni_dany` IF TABEL NOT EXIST
// month,year,day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,day16,day17,day18,day19,day20,day21,day22,day23,day24,day25,day26,day27,day28,day29,day30,day31

// check if row exist for updating
if ( Database::num_rows("SHOW TABLES LIKE 'neradni_dani'") == 0 ) {
  // row doesnt exist
  // create table 
  $table_sql = "CREATE TABLE IF NOT EXISTS `neradni_dani` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `radnik_id` int(11) DEFAULT NULL,
    `month` varchar(2) DEFAULT NULL,
    `year` varchar(4) DEFAULT NULL,
    `work_stat` json DEFAULT NULL,
    `day1` int(3) DEFAULT '0',
    `day2` int(3) DEFAULT '0',
    `day3` int(3) NOT NULL DEFAULT '0',
    `day4` int(3) NOT NULL DEFAULT '0',
    `day5` int(3) DEFAULT '0',
    `day6` int(3) NOT NULL DEFAULT '0',
    `day7` int(3) NOT NULL DEFAULT '0',
    `day8` int(3) NOT NULL DEFAULT '0',
    `day9` int(3) NOT NULL DEFAULT '0',
    `day10` int(3) NOT NULL DEFAULT '0',
    `day11` int(3) NOT NULL DEFAULT '0',
    `day12` int(3) NOT NULL DEFAULT '0',
    `day13` int(3) NOT NULL DEFAULT '0',
    `day14` int(3) NOT NULL DEFAULT '0',
    `day15` int(3) NOT NULL DEFAULT '0',
    `day16` int(3) NOT NULL DEFAULT '0',
    `day17` int(3) NOT NULL DEFAULT '0',
    `day18` int(3) NOT NULL DEFAULT '0',
    `day19` int(3) NOT NULL DEFAULT '0',
    `day20` int(3) NOT NULL DEFAULT '0',
    `day21` int(3) NOT NULL DEFAULT '0',
    `day22` int(3) NOT NULL DEFAULT '0',
    `day23` int(3) NOT NULL DEFAULT '0',
    `day24` int(3) NOT NULL DEFAULT '0',
    `day25` int(3) NOT NULL DEFAULT '0',
    `day26` int(3) NOT NULL DEFAULT '0',
    `day27` int(3) NOT NULL DEFAULT '0',
    `day28` int(3) NOT NULL DEFAULT '0',
    `day29` int(3) DEFAULT '0',
    `day30` int(3) NOT NULL DEFAULT '0',
    `day31` int(3) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

  Database::query($table_sql);
}

// $ar = array('2016', '2017');

// foreach ($ar as $year) {

//   for ( $z=1; $z<=12; $z++ ) {

//         $month = $z;
//         $year = $year;
        
//         $dana_u_mesecu = get_days_for_this_month($month,$year);
//         $radni_dani_u_mesecu = work_day_for_month($year,$month);

//           $insert_what = array(
//             'month'=>$month,
//             'year'=>$year
//           );
//           for ( $i=1; $i<=$dana_u_mesecu; $i++ ) {
//             $monthday = explode(',', date('t,N', strtotime($year.'-'.$month.'-'.$i)));
//             $day = 'day'.$i;
//             if ( $monthday[1]=='6' || $monthday[1]=='7' ) {
//               $insert_what[$day] = '0';
//             } else {
//               $insert_what[$day] = '1';
//             }
//           }

//           // Helper::pre(Database::returnInsertData('neradni_dani', $insert_what));
//           // 
//           // 
//           if ( Database::num_rows(Database::returnWhereQuery('neradni_dani', array('month'=>$month, 'year'=>$year))) == 0 ) {
//             Database::insert_data('neradni_dani', $insert_what);
//           }


//   }

// }

// die();
// 

  $o = '';

  $o .= '<a href="'.ADMINURL.'?page=options" class="btn btn-success"> Options </a>';
  $o .= '<hr class="delimiter">';
  
  $o .= '<h1>Radni dani</h1>';
  $o .= '<hr class="delimiter">';

  $o .= '<form action="http://localhost/tuscan/admin/?page=options&options=neradni-dani" method="post" class="wrap-bottom">';
  $o .= '<input name="month" id="datepicker-month">';
  $o .= '<input id="neradni_dani_submit_btn" class="btn btn-succes wrap-bottom" type="submit" name="submit-date" value="Izaberi datum">';

  $o .= '</form>';
  $o .= '<hr class="delimiter">';

  // Obrada podataka
  if ( !empty($_POST) ) {
    // Helper::pre($_POST);
    if ( isset($_POST['radni_dani']) ) {
      // Update radne dane
      $radni_dani = $_POST['radni_dani'];

      $res = explode('-', $_POST['month']); // 2017-03
      $month = $res[1];
      $year = $res[0];
      
      $dana_u_mesecu = get_days_for_this_month($month,$year);
      $where_data = array(
        'month'=>$month,
        'year'=>$year
      );
      $insert_data = array();
      for ( $x=1; $x<=$dana_u_mesecu; $x++ ) {
        if ( in_array($x, $radni_dani) ) {
          $insert_what['day'.$x] = '1';
        } else {
          $insert_what['day'.$x] = '0';
        }
      }

      if ( Database::updateData('neradni_dani', $insert_what) ) {
        $o .= '<script>alert("Uspesno odradjena operacija");</script>';
      } else {
        $o .= '<script>alert("Nije uspesno odradjena operacija");</script>';
      }

      // UPDATE NEW WORK DAY

    }



    // AKO NISU PODESENI DANI ZA IZABRANI DATUM
      $o .= '<h2> Radni dani za mesec '.$_POST['month'];

      $res = explode('-', $_POST['month']); // 2017-03
      $month = $res[1];
      $year = $res[0];
      
      $dana_u_mesecu = get_days_for_this_month($month,$year);
      $radni_dani_u_mesecu = work_day_for_month($year,$month);

      if ( Database::num_rows(Database::returnWhereQuery('neradni_dani', array('month'=>$res[1], 'year'=>$res[0]))) == 0 ) {
        $insert_what = array(
          'month'=>$month,
          'year'=>$year
        );
        for ( $i=1; $i<=$dana_u_mesecu; $i++ ) {
          $monthday = explode(',', date('t,N', strtotime($year.'-'.$month.'-'.$i)));
          $day = 'day'.$i;
          if ( $monthday[1]=='6' || $monthday[1]=='7' ) {
            $insert_what[$day] = '0';
          } else {
            $insert_what[$day] = '1';
          }
        }

        // Helper::pre(Database::returnInsertData('neradni_dani', $insert_what));
        // die();

        if ( Database::insert_data('neradni_dani', $insert_what) ) {
          $o .= '<div class="clearfix"></div>';
          $o .= '<hr class="delimiter">';
          $o .= '<a class="reload btn btn-danger btn-large">Refresh page</a>';
        } else {
          $o .= '<div class="clearfix"></div>';
          $o .= '<hr class="delimiter">';
          $o .= '<h1>Nema podesavanje za ovaj mesec</h1>';
          $o .= '<hr class="delimiter">';
        }

      } else {
        

  $o .= '<form action="'.ADMINURL.'?page=options&options=neradni-dani" method="post" class="wrap-bottom">';

        $o .= '<input type="hidden" name="month" value="'.$_POST['month'].'">';
        $o .= '<div class="neradni_dani col-xs-12 wrap-top btn-groupp" data-toggle="buttons">';
        $o .= '<h4 class="sub-header">Izaberite radne dane</h4>';

            for ( $i=1; $i<=$dana_u_mesecu; $i++ ) {

              $monthday = explode(',', date('t,N', strtotime($year.'-'.$month.'-'.$i)));
              if ( $monthday[1]=='1' ) {
                $o .= '<div class="clearfix"></div>';
                $o .= '<hr class="delimiter">';
              }

              $o .= '<label class="col-xs-1 btn';

              if ( in_array($i, $radni_dani_u_mesecu) && Database::num_rows(Database::returnWhereQuery('neradni_dani', array('month'=>$month, 'year'=>$year, 'day'.$i=>'1'))) == 1) {
                $o .= ' alert-success';
              } else {
                $o .= ' alert-danger';
              }

              if (  Database::num_rows(Database::returnWhereQuery('neradni_dani', array('month'=>$month, 'year'=>$year, 'day'.$i=>'1'))) == 1 ) {
                $o .= ' active';
              }
              $o .= '" style="height:80px;">';
                $o .= '<input type="checkbox" name="radni_dani['.$i.']" id="day'.$i.'" autocomplete="on"';
              if ( Database::num_rows(Database::returnWhereQuery('neradni_dani', array('month'=>$month, 'year'=>$year, 'day'.$i=>'1'))) == 1 ) {
                $o .= ' checked';
              }
                $o .= ' value="'.$i.'">';
                $o .= '<span class="glyphicon glyphicon-ok"></span>';
                $o .= '<h5>'.$i.'</h5>';
              $o .= '</label>';
            }

        $o .= '</div>';

        $o .='<div class="clearfix"></div>';
        $o .= '<input class="btn btn-success" type="submit" name="submit" value="Podesi radne dane">';
  $o .= '</form>';

      }

  }


  echo $o;