<?php

if ( isset($_SESSION['error_page_msg']) && $_SESSION['error_page_msg'] != '' ) {
  $error_page_msg = $_SESSION['error_page_msg'];
  $o .= '<div class="alert alert-danger">';
  $o .= $error_page_msg;
  $o .= '</div>';
  unset($_SESSION['error_page_msg']);
} elseif ( isset($_SESSION['info_page_msg']) && $_SESSION['info_page_msg'] != '' ) {
  $info_page_msg = $_SESSION['info_page_msg'];
  $o .= '<div class="alert alert-info">';
  $o .= $info_page_msg;
  $o .= '</div>';
  unset($_SESSION['info_page_msg']);
} else {
  // $o .= '<div class="alert alert-info">';
  // $o .= 'Info msg box';
  // $o .= '</div>';
}
