<?php
// Helper::pre($_SESSION);

$o = '';

if ( isset($_GET['options']) && $_GET['options']!='') {
  // options = edit
  if ($_GET['options'] == 'edit') {
    if ( isset($_GET['options']) && (!isset($_GET['id']) || $_GET['id']=='') ) {
      $_SESSION['error_page_msg'] = 'Neeeee';
      Url::header_status(ADMIN_ERROR_PAGE);
      die('<div class="alert alert-danger">Greska u proveri parametara</div>');
    } else {
      $zadatak_id = isset($_GET['id']) ? $_GET['id'] : '';
    }
    // Helper::pre($zadatak_id);
    require_once 'edit_zadatak.php';

  } elseif ( $_GET['options'] == 'add_zadatak' ) {
    require_once 'add_zadatak.php';
  } elseif ( $_GET['options'] == 'delete' ) {

    require_once 'delete_zadatak.php';
  } elseif ( $_GET['options'] == 'today' ) {

    if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings',array('working_day'=>date('Y-m-d'))) ) > 0) {

      $o .= '<h2 class="sub-header">Danasnji zadaci</h2>';
      $o .= '<div class="breadcrumb">';
      $o .= '<a href="'.ADMINURL.'?page=zadaci"><i class="fa fa-angle-double-left"></i> Svi zadaci</a>';
      $o .= '</div>';

      $limit = $start.",".$item_per_page;
      $komesa_linija_list = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')) , ' ORDER BY id DESC', $limit);
      $count = Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d'))));

      if ( $count > $item_per_page ) $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

      $table_head = array('ID', 'Working day', 'linija ID', 'Komesa ID', '');
      $url_array = array(
        'Edit' => ADMINURL.'?page=zadaci&options=edit',
        'Delete'  => ADMINURL.'?page=zadaci&options=delete'
      );
      $o .= Table::returnListDataTable( $table_head ,$komesa_linija_list,$url_array );

      if ( $count > $item_per_page ) $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    } else {
      $o .= '<div class="text-center input-padding alert alert-warning"> Nema podataka za date parametre </div>';
    }

  }

} else {

  $o .= '<h2 class="sub-header text-left input_padding"> Podesavanje dnevnih zadataka </h2>';
  $o .= Messages::infoErrorMsg($_SESSION);

  echo $o;
?>
  <div class="filter-container alert-info">
    <div class="filter-btn-visible btn btn-success input_padding wrap wrap-left">Show filter options</div>

    <form action="<?php echo ADMINURL; ?>" method="get">
      <input name="page" value="zadaci" hidden="hidden"/>
        <div class="row">
          <!-- <span class=""> Izaberi datum </span></br> -->
          <div class="col-xs-12">
            <div class="filter-box">
              <input class="datum_zadatka col-xs-12 col-sm-3 col-md-3 col-lg-2" type="text" name="date" id="datepicker-1" value="<?php echo (isset($_GET['date']) ? $_GET['date'] : '' ); ?>" placeholder="Izaberite datum">
            </div>
            <select id="komesa" name="komesa" class="select-komesa col-xs-9 col-sm-6 col-md-3 col-lg-2">
            <?php
            if ( isset($_GET['komesa']) && $_GET['komesa'] != '' )  { // if isset $_GET['komesa']
              if( $_GET['komesa'] != '0' ) {
                echo '<option ';
                echo 'selected="selected"';
                echo ' value="'.$_GET['komesa'].'">'.$_GET['komesa'].'</option>';
              } else {
                echo '<option value="default"> Izaberite datum </option>';
              }
            } else {
              echo '<option value="default"> Izaberite datum </option>';
            }
            ?>
            </select>
            <div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-3 col-sm-3 col-md-3 col-lg-2">Change komesa</div>

            <!-- Linija -->
            <?php $sve_linije = Database::fetchData('linija'); ?>
            <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="linija">
              <option value="0"<?php if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) echo 'selected="selected"'; ?>>Izaberi liniju</option>
              <?php
              foreach ($sve_linije as $linija) {
                echo '<option value="'.$linija['id'].'"';
                if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
                  echo 'selected="selected"';
                }
                echo '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
              }
              ?>
            </select>
            <div class="clear"></div>

            <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />
            <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->
            <a class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=zadaci">Reset Filter</a>
          </div>
        </div>
    </form>

  </div> <!-- filter-box -->
  <a href="<?php echo ADMINURL; ?>?page=zadaci&options=add_zadatak" class="col-xs-3 col-lg-2 btn btn-success input_padding wrap"><i class="fa fa-plus"></i> Dodaj novi zadatak </a>
  <div class="filter-btn-visible  col-xs-3 col-lg-2 btn btn-success input_padding wrap wrap-left">Show filter options</div>
  <a href="<?php echo ADMINURL; ?>?page=zadaci&options=today" class="filter-btn-visible  col-xs-3 col-lg-2 btn btn-warning input_padding wrap wrap-left"> Danasnji zadaci </a>
  <div class="clear"></div>

<?php
  // Helper::pre($_GET);

  $o = '';

  $where_array =array();

  if ( isset($_GET) ) {
    // working_day, linija_id, komesa_id
    if ( isset( $_GET['date']) && $_GET['date']!='' ) {
      $where_array =array_merge( $where_array, array('working_day'=>$_GET['date']) );
    }
    if ( isset( $_GEt['komesa']) && $_GET['komesa'] != 'default' && $_GET['komesa']!='' ) {
      $where_array = array_merge( $where_array, array('komesa_id'=>$_GET['komesa']) );
    }
  }

  // Helper::pre(Database::returnWhereQuery('komesa_line_settings',$where_array));

  if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings',$where_array) ) > 0) {

    $limit = $start.",".$item_per_page;
    $komesa_linija_list = Database::whereQuery('komesa_line_settings', $where_array, ' ORDER BY id DESC', $limit);
    $count = Database::num_rows(Database::returnFetchData('komesa_line_settings'));

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $table_head = array('ID', 'Working day', 'linija ID', 'Komesa ID', '');
    $url_array = array(
      'Edit' => ADMINURL.'?page=zadaci&options=edit',
      'Delete'  => ADMINURL.'?page=zadaci&options=delete'
    );
    $o .= Table::returnListDataTable( $table_head ,$komesa_linija_list,$url_array );

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

  } else {
    $o .= '<div class="text-center input-padding alert alert-warning"> Nema podataka za date parametre </div>';
  }

}


echo $o;
