<?php
// Helper::pre($_POST);
  // update data
  if ( (isset($_POST['zadatak_id']) && $_POST['zadatak_id']!= '' ) &&
        (isset($_POST['komesa_datum_rada']) && $_POST['komesa_datum_rada']!= '' ) &&
        (isset($_POST['select_linija']) && $_POST['select_linija']!= '' ) &&
        (isset($_POST['select_komesa']) && $_POST['select_komesa']!= '' ) &&
        (isset($_POST['submit']) && $_POST['submit']==1 )
  ) {
    $working_day = $_POST['komesa_datum_rada'];
    $linija_id = $_POST['select_linija'];
    $komesa_id = $_POST['select_komesa'];
    $zadatak_id = $_POST['zadatak_id'];

    $table_name = 'komesa_line_settings';
    $key_value_data = array(
      'working_day' => $working_day,
      'linija_id' => $linija_id,
      'komesa_id' => $komesa_id
    );
    $where_array = array(
      'id' => $zadatak_id
    );

    if ( Database::updateData($table_name, $key_value_data, $where_array)){
      $_SESSION['info_page_msg'] = 'Uspesno updateovan zadatka';
      // Url::header_status(ADMINURL.'?page=zadaci&options=edit&id='.$zadatak_id);
    } else {
      $_SESSION['error_page_msg'] = 'Neuspesno updateovan zadatka';
      // Url::header_status(ADMINURL.'?page=zadaci&options=edit&id='.$zadatak_id);
    }
  }

  if ( isset($zadatak_id) ) {
    $zadatak_res = Database::whereQuery('komesa_line_settings', array('id'=>$zadatak_id) );
    // Helper::pre($zadatak_res);
    ?>

    <h2 class="sub-header">Edit This Zadatak</h2>

    <a href="<?php echo ADMINURL.'?page=zadaci'; ?>" class="btn btn-info wrap-bottom"><i class="fa fa-angle-double-left"></i> Povratak na sve zadatke</a>

    <div id="message_box" class="text-center"></div>
    <div class="col-xs-12 alert alert-info">
      <form class="edit_zadatak" action="?page=zadaci&options=edit&id=<?php echo $zadatak_id; ?>" method="post">
        <input id="komesa_id" class="hidden" type="text" name="zadatak_id" value="<?php echo isset($zadatak_res[0]['id']) ? $zadatak_res[0]['id'] : ''; ?>" placeholder="Komesa ID">
        <div class="col-xs-12 col-sm-3 col-lg-3 text-left input_padding wrap-10">Datum rada</div>
        <input id="datepicker" class="datum_rada col-xs-12 col-sm-8 col-md-6 col-lg-6" type="text" name="komesa_datum_rada" value="<?php echo isset($zadatak_res[0]['id']) ? $zadatak_res[0]['working_day'] : ''; ?>" placeholder="Datum rada | primer: 2016-07-18">
        <!-- <input type="text" name="artikal_id" value="" placeholder="Artikal ID"><br/> -->
        <div class="col-xs-12 col-sm-3 col-lg-3 text-left input_padding wrap-10">Linija</div>
        <select id="select_linija" class="col-xs-12 col-sm-8 col-md-6 col-lg-6" class="select_linija" name="select_linija">
          <optgroup label="Sve Linije">
            <option value="default" disabled >Select linija</option>
            <?php
            $o = '';
            $linija_list = Database::fetchData('linija');
            foreach ($linija_list as $item) {
              # code...
              $o .= '<option value="'.$item['id'].'"';
              if ( isset($zadatak_res[0]['linija_id']) && $zadatak_res[0]['linija_id'] == $item['linija_id']) {
                $o .= ' selected="selected"';
              }
              $o .= '>'.$item['id']."-".$item['linija_id']."-".$item['linija_name'].'</option>';
            }
            echo $o;
            ?>
          </optgroup>
        </select>
        <div class="col-sm-3 col-lg-3 text-left input_padding wrap-10"> Komesa </div>
        <select id="select_komesa" class="col-xs-12 col-sm-8 col-md-6 col-lg-6" class="select_komesa" name="select_komesa">
          <optgroup label="Sve Komese">
            <option value="default" disabled>Select Komesu</option>
            <?php
            $o = '';
            $komesa_list = Database::fetchData('komesa');
            // Helper::pre($komesa_list);
            foreach ($komesa_list as $item) {
              $o .= '<option value="'.$item['id'].'"';
              if ( isset($zadatak_res[0]['komesa_id']) && $zadatak_res[0]['komesa_id'] == $item['id'] ) {
                $o .= ' selected="selected"';
              }
              $o .= '>'.$item['id']."-".$item['komesa_id']."-".$item['komesa_name'].'</option>';
            }
            echo $o;
            $o ='';
            ?>
          </optgroup>
        </select>
        <div class="clear"></div>
        <div class="text-center col-xs-10">
          <button id="add_komesa" class="pull-right text-center btn btn-warning col-xs-12 col-sm-6 col-md-2 col-lg-2 input-style-btn wrap-top" type="submit" name="submit" value="1">Update Zadatak</button>
        </div>
      </form>
    </div>

  </div>

    <?php
  } else {
    $o = '';
    // Not isset id
    $o = '<div class="alert alert-danger">Id is not set</div>';
  }

?>
