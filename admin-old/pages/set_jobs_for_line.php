<?php

$line_data = Database::fetchData('linija');
// Helper::pre($line_data);

$o = '';

$o .= '<h4>Detalji linija i zadataka po linije</h4>';
$return_url = '<a class="btn btn-primary wrap-10" href="'.ADMINURL.'?page=set_jobs_for_line"><i class="fa fa-angle-left" aria-hidden="true"></i>
 Back </a>';

if ( isset($_GET['options']) && $_GET['options']=='details' ) {
  $o .= $return_url;

} elseif ( isset($_GET['options']) && $_GET['options']=='edit' ) {
  $o .= $return_url;

} elseif ( isset($_GET['options']) && $_GET['options']=='delete' ) {
  $o .= $return_url;

} else  {

  $table_head = array();
  $url_array = array(
    'Details' => ADMINURL.'?page=set_jobs_for_line&options=details',
    'Edit' => ADMINURL.'?page=set_jobs_for_line&options=edit',
    'Delete' => ADMINURL.'?page=set_jobs_for_line&options=delete'
  );
  $o .= Table::returnListDataTable($table_head ,$line_data, $url_array );

}

echo $o;
