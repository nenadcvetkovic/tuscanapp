<?php
// check if row exist for updating
if ( Database::num_rows("SHOW TABLES LIKE 'work_stat'") == 0 ) {
  // row doesnt exist
  // create table 
  $table_sql = "CREATE TABLE IF NOT EXISTS `work_stat` (
    `id` int(11) NOT NULL,
    `radnik_id` int(11) DEFAULT NULL,
    `month` varchar(2) DEFAULT NULL,
    `year` varchar(4) DEFAULT NULL,
    `work_stat` json DEFAULT NULL,
    `day1` int(3) DEFAULT '0',
    `day2` int(3) DEFAULT '0',
    `day3` int(3) NOT NULL DEFAULT '0',
    `day4` int(3) NOT NULL DEFAULT '0',
    `day5` int(3) DEFAULT '0',
    `day6` int(3) NOT NULL DEFAULT '0',
    `day7` int(3) NOT NULL DEFAULT '0',
    `day8` int(3) NOT NULL DEFAULT '0',
    `day9` int(3) NOT NULL DEFAULT '0',
    `day10` int(3) NOT NULL DEFAULT '0',
    `day11` int(3) NOT NULL DEFAULT '0',
    `day12` int(3) NOT NULL DEFAULT '0',
    `day13` int(3) NOT NULL DEFAULT '0',
    `day14` int(3) NOT NULL DEFAULT '0',
    `day15` int(3) NOT NULL DEFAULT '0',
    `day16` int(3) NOT NULL DEFAULT '0',
    `day17` int(3) NOT NULL DEFAULT '0',
    `day18` int(3) NOT NULL DEFAULT '0',
    `day19` int(3) NOT NULL DEFAULT '0',
    `day20` int(3) NOT NULL DEFAULT '0',
    `day21` int(3) NOT NULL DEFAULT '0',
    `day22` int(3) NOT NULL DEFAULT '0',
    `day23` int(3) NOT NULL DEFAULT '0',
    `day24` int(3) NOT NULL DEFAULT '0',
    `day25` int(3) NOT NULL DEFAULT '0',
    `day26` int(3) NOT NULL DEFAULT '0',
    `day27` int(3) NOT NULL DEFAULT '0',
    `day28` int(3) NOT NULL DEFAULT '0',
    `day29` int(3) DEFAULT '0',
    `day30` int(3) NOT NULL DEFAULT '0',
    `day31` int(3) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

  Database::query($table_sql);
}

if ( isset($_GET['options']) && $_GET['options']=='count-stat') {
  require_once 'count_stat.php';
}

// die();

$o = '';

if ( isset($_GET['year']) ) {
  $o .= '<a href="'.ADMINURL.'?page=stat" class="btn btn-info"> Biranje godine </a>';
}
if ( isset($_GET['month']) ) {
  $o .= '<a href="'.ADMINURL.'?page=stat&year='.$_GET['year'].'" class="btn btn-info wrap-left"> Biranje meseca </a>';
}
  $o .= '<a href="'.ADMINURL.'?page=options&options=neradni-dani" class="btn btn-info wrap-left"> Podesavanje radnih dana </a>';

$o .= '<hr class="delimiter">';
echo $o;
// CHECK STATUS FOR DATABSE

if ( isset($_GET['options']) && $_GET['options']=='count-work-stat' ) {
  $o = '';
    // settup db table for
    // 

    $sql = "UPDATE `work_stat` SET `work_stat` = JSON_SET(`work_stat`, '$.d".date('d')."', 'todaj') WHERE radnik_id = '1984' AND month=".date('m')." AND year='".date('Y')."'";
    $sql2 = "UPDATE `e_store`.`products` SET `attributes` = JSON_SET( `attributes` ,'$.body_color' ,'red') WHERE `category_id` = '1'";

    $o .= 'Count work stat';
    Helper::pre($sql);
    if ( Database::query($sql) ) {
      $o .= 'Heeeeeeee';
    } else {
      $o .= 'Noooooooo';
    }

    echo $o;
} else {

    // 

    $day = date('d');
    $month = date('m');
    $year = date('Y');

    if ( isset($_GET['year']) && !isset($_GET['month']) && !isset($_GET['day']) ) {
        $o = '';
        // year
        $year = $_GET['year'];

        $month_num = 12;

        if ( date('Y') == $year) {
          $month_num  = date('n');
        }

        for ( $i=1; $i<=$month_num; $i++ ) {
            if ( $i<10 ) {
                $i = '0'.$i;
            }
            $monthNum  = $i;
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F'); // March

            $o .= '<a href="'.ADMINURL.'?page=stat&year='.$year.'&month='.$i.'" class="col-xs-2 btn btn-danger wrap wrap-top wrap-left wrap-right wrap-bottom input-padding">';
            $o .= '<div class="col-xs-8 col-xs-offset-2">'.$year.'</div>';
            $o .= '<div class="col-xs-8 col-xs-offset-2">'.$monthName.'</div>';
            $o .= '</a>';
        }

        echo $o;

    } elseif ( isset($_GET['year']) && $_GET['year']!='' && $_GET['year']!=0 &&
                isset($_GET['month']) && $_GET['month']!='' && $_GET['month']!=0 &&
                !isset($_GET['day'])
    ) {
        $o = '';
        // year and month
        $year = $_GET['year'];
        $month = $_GET['month'];

        $table_name = 'working_day_session_details_'.$month.'_'.$year;


        $day_in_month = get_days_for_this_month ($month, $year);

          $o .= '<div class="table-responsive" style="height:70vh;">';
          $o .= '<table class="table table-striped">';
          $o .= '  <thead>';
          $o .= '    <tr>';
          $o .= '      <th>Radnik Name</th>';

          for ( $i=1; $i<=$day_in_month; $i++ ) {
              $o .= '      <th> ' .$i. '</th>';
          }
          $o .= '      <th> Mesecna efikasnost  </th>';
          $o .= '    </tr>';
          $o .= '  </thead>';
          $o .= '  <tbody>';

          $radnici_data = Database::fetchData('radnici_lista');
          foreach ($radnici_data as $item ) {
            $o .= '<tr>';
            $o .= '<td>'.$item['radnik_name'].'</td>';

            $procenat_ucinka = 0;
            $index = 0;

            if ( Database::num_rows(Database::returnWhereQuery('work_stat', array('radnik_id'=>$item['radnik_id'], 'year'=>$year, 'month'=>$month))) > 0 ) {

              $radnik_day_data = Database::whereQuery('work_stat', array('radnik_id'=>$item['radnik_id'], 'year'=>$year, 'month'=>$month));
              $user_data = json_decode($radnik_day_data[0]['work_stat']);
              // Helper::pre($radnik_day_data);
              // Helper::pre($user_data);
              // 
              $neradni_dani = Database::whereQuery('neradni_dani', array('month'=>$month, 'year'=>$year));
              // Helper::pre($neradni_dani);
              for ( $i=1; $i<=$day_in_month; $i++ ) {

                if ( $neradni_dani[0]['day'.$i] == '0' ) {
                  $o .= '<td>';
                    $o .= '<span class="btn btn-primary '.return_class_eff(0).' disabled">X</span>';
                  $o .= '</td>';
                } else {

                    if ( Database::num_rows(Database::returnWhereQuery('work_stat', array('radnik_id'=>$item['radnik_id'], 'year'=>$year, 'month'=>$month))) > 0 ) {
                      // $radnik_day_data = Database::query($efect_sql);
                      $mdata = 'm'.$i;

                      if ( isset($radnik_day_data[0]['day'.$i]) ) {
                        $o .= '<td>';
                        $eff = $radnik_day_data[0]['day'.$i];
                        $o .= '<span class="btn alert-'.return_class_eff($eff).'" title="Radnik: '.$item['radnik_name'].' / Datum: '.$i.'.'.$_GET['month'].'.'.$_GET['year'].'">'.$radnik_day_data[0]['day'.$i].'</span>';
                        $o .= '</td>';
                        $procenat_ucinka += $radnik_day_data[0]['day'.$i];
                        $index++;
                      } else {
                        $o .= '<td>';
                        $o .= '<span class="btn alert-'.return_class_eff(0).' disabled">xs</span>';
                        $o .= '</td>';                    
                      }
     
                    } else {
                      $o .= '<td>';
                        $o .= '<span class="btn alert-'.return_class_eff(0).' disabled">xxs</span>';
                      $o .= '</td>';

                    }


                }
                
              }

            } else {
              $o .= '<td>Nema podataka</td>';
            }
            if ( $index == 0 ) {
              $ukupno = 0;
            } else {
              $ukupno = floor($procenat_ucinka / $index);            

              $o .= '<td>';
              $o .= '<span class="btn alert-'.return_class_eff($ukupno).'" style="width:100%;"> '.$ukupno.' </span>';
              $o .= '</td>';                    
            }

            $o .= '</tr>';
          }
          $o .= '  </tbody>';
          $o .= '</table>';
          $o .= '</div>';


        echo $o;

    } elseif ( isset($_GET['year']) && $_GET['year']!='' && $_GET['year']!=0 &&
                isset($_GET['month']) && $_GET['month']!='' && $_GET['month']!=0 &&
                isset($_GET['day']) && $_GET['day']!='' && $_GET['day']!=0
    ) {
        // year, month and day
        echo 'year, month and day';
        Helper::pre($_GET);

    } else {
        $o = '';

        $from_yaer = 2016;
        $to_year = date('Y');

        for ( $i=$from_yaer; $i<=$to_year; $i++ ) {
            $o .= '<a href="'.ADMINURL.'?page=stat&year='.$i.'" class="col-xs-3 btn btn-danger wrap wrap-top wrap-left wrap-right wrap-bottom input-padding">';
            $o .= $i;
            $o .= '</a>';

        }

        echo $o;
    }

}
