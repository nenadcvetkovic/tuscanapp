<?php


// require_once '../system/function.php';

require_once '../system/config/config.php';
require_once '../system/function.php';

require_once '../system/libs/Database.php';
require_once '../system/libs/Url.php';
require_once '../system/libs/Helper.php';
require_once '../system/libs/Table.php';
require_once '../system/libs/Pagination.php';
require_once '../system/libs/Messages.php';
require_once '../system/libs/Ip.php';
require_once '../system/libs/Colors.php';


// $class = array('Database','Url','Helper','Pagination','Messages','Colors','Ip','Table');
//
// __autoload($class);
//

// Set errro visible when developing site
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

session_start();


// Backup sql database
// shell_exec('mysqldump -u '.DB_USER.' -p '.DB_PASS.' '.DB_NAME.' > db/backdb/'.date('Y_d_m_H').'_backup.sql');

// if ( date("H") == '18' ||
//       date("H") == '20' ||
//       date("H") == '22' ||
//       date("H") == '00' ) {
//   // echo http_response_code();
//   if ( Database::backUpDb() ){
//     // echo 'Database table saccessfuly backuped';
//   } else {
//     // mysqldump -u username -p database_to_backup > backup_name.sql
//     shell_exec('mysqldump -u root -p root tuskan_family > db/backdb/'.date('Y_d_m_H').'_backup.sql');
//   }
// }

// CRETE DB TABLE FOR NEXT MONTH

$month = date('n') + 1;
$year = date('Y');

if ( Database::num_rows("SHOW TABLES LIKE 'working_day_session_details_".$month."_".$year."'") == 0 ) {
    // CRETE TABLE working_day_session_details FOR NEXT MONTH IF NOT EXIST
    // 
    $sql = "CREATE TABLE IF NOT EXISTS `working_day_session_details_".$month."_".$year."` (

      `id` int(11) NOT NULL auto_increment,   
      `working_day_session_id` varchar(50) NOT NULL,
      `working_day` date DEFAULT NULL,
      `komesa_id` int(11) DEFAULT NULL,
      `radnik_id` int(11) DEFAULT NULL,
      `linija_id` int(11) DEFAULT NULL,
      `artikal_id` int(11) DEFAULT NULL,
      `operacija_id` int(11) DEFAULT NULL,
      `color` varchar(20) DEFAULT NULL,
      `size` varchar(20) DEFAULT NULL,
      `vreme_operacije` datetime DEFAULT NULL,
      `razlika_u_vremenu` time DEFAULT NULL,
      `procenat_ucinka` int(3) DEFAULT NULL,
      `hint_code` varchar(4) DEFAULT NULL,
       PRIMARY KEY  (`id`)

    )";
    if (Database::query($sql)) {
    // if (true) {
        echo '<script>alert("Uspesno kreirana tabela working_day_session_details_'.$month.'_'.$year.'");</script>';
    }

}

//pagination data
$item_per_page=10;
if(!isset($_GET['ppage']) || $_GET['ppage']==1){
  $ppage=1;
  $start=0;
}else{
  $ppage=$_GET['ppage'];
  $start=($ppage-1)*$item_per_page;
}
$next=$ppage+1;
$prev=$ppage-1;
$limit=$start.','.$item_per_page;
$current_page = isset($_GET['ppage']) ? $_GET['ppage'] : $current_page=1;
$data['pagination']=array(
  'item_per_page'=>$item_per_page,
  'ppage'=>$ppage,
  'start'=>$start,
  'next'=>$next,
  'prev'=>$prev,
  'limit'=>$limit,
  'current_page'=>$current_page,
  'url' => ADMINURL.'?page=radnici&option=radnik&radnik_id=1'
);

$ip_address = array('192.168.1.200', '127.0.0.1', '192.168.1.255', '192.168.1.254');
// check if ip is in Database
if ( !in_array(Ip::getUserRealIpAdress(), $ip_address )) {

  Url::header_status(BASEURL.'404.php');
  die('Nemate dozvolu da pristupite serveru. Molimo vas da se konsultujete sa administratormom. Hvala');

} else {

  require_once 'template/header.php';

  require_once 'main.php';

  require_once 'template/footer.php';
}
