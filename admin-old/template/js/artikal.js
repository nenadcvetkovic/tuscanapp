$(document).ready(function(){
  // var base_url = 'http://localhost:8888/projekti/svn-projekti/tuskan-family/';
  // var base_url =  'http://localhost:8888/projekti/svn-projekti/tuscan/';
  var base_url = 'http://192.168.1.200/tuscan/';
  // var base_url = 'http://localhost/tuscan/';
  // 


    $('.change_kolicina_po_artiklu').on('click', function(e){
      if ( $('.kolicina_po_operaciji').length == 0) {
        $(this).after(
          '<input class="kolicina_po_operaciji col-xs-6" type="number" name="kolicina_po_operaciji" placeholder="Unesite kolicini po artiklu" />'+
          '<div class="clear"></div>'
        );
      }
      e.preventDefault();
    });
    $('#set_artikal_data_options').on('click', function(e){

      var number_of_input_filds = $('.input_options_artikal').length;
      var redni_id = number_of_input_filds + 1;
      var operacije = '';

      $.ajax({
        url: base_url + "api/data.php?action=return-data",
        method: "POST",
        data: {
          options: "operacija"
        },
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        }
      })
      .done(function( data ) {
        if ( console && console.log ) {
          console.log( "Sample of data:", data.slice( 0, 100 ) );
        }
        // $('#set_artikal_data_options').before(
        $('.select-box-container').append(
          '<div class="clear">'+
          // '<input class="col-md-3 col-xs-3 col-sm-12 btn-primary input_options_artikal" type="text" name="artikal_id[]" value="" placeholder="Artikal ID : artikal_id_'+redni_id+'" >'+
          // '<input class="col-md-3 col-xs-3 col-sm-12  input_options_artikal" type="text" name="artikal_id[]" value="" placeholder="Artikal ID : artikal_id_'+redni_id+'" >'+
          '<select class="col-xs-6 btn-default input_options_artikal" id="operacija" name="operacija[]">'+
          data+
          '</select>'+
          '<input class="col-xs-2  tempo_operacije" type="text" name="tempo_operacije[]" value="" placeholder="Tempo" >'+
          '<input class="col-xs-2  komada_po_operaciji" type="text" name="komada_po_operaciji[]" value="" placeholder="kom/operacija" >'+
          '<input class="col-xs-2  komada_po_artiklu" type="text" name="komada_po_artiklu[]" value="" placeholder="kom/artikal" >'+
          '<a class="pull-right col-xs-1 col-md-offset-1 btn btn-lg btn-danger remove_artikal_row wrap"> X </a>'+
          '<span class="clear"></span>' +
          '</div>'
        );
      });

      e.preventDefault();
    });

    $('#set_artikal_data_options').parent().on('click', '.remove_artikal_row', function(e){
      $(this).parent().remove();
      e.preventDefault();
    });

    $('.remove_artikal_row').parent().on('click', '.remove_artikal_row', function(e){
      // alert();
      $(this).parent().remove();
      e.preventDefault();
    });

    // remove operaciju from artikal
    $('.remove_operaciju').on('click', function(e){
      if ( confirm('Potvrdite brisanje operacije?!') ){
        var operacija = $(this);

        // return false;
        // alert();
        $.ajax({
          url: base_url + "admin/pages/delete.php",
          method: "POST",
          data: {
            action      : 'delete_operaciju_from_artikal',
            table_name  : operacija.data('tablename'),
            operacija_id: operacija.attr('value')
          },
          error: function(data, xhr, status, error) {
            // alert( 'Error ' + data );
          },
          beforeSend: function( xhr ) {
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
          }
        })
        .done(function( data ) {
          // alert(data);
          if ( data == 1 ) {
            operacija.parent().remove();
          } else {
            // alert(data);
          }
        });
      } else {
        // Odbijeno brisanje operacije
      }


      e.preventDefault();
    });

    $('.add_color_box').on('click',function(e){
      $(this).before('<input name="color_value colorpicker" value="hsv(0, 24%, 82%)">');
    });

    // // edit operaciju
    // $('.edit_operacija').on('click',function(e){
    //   $(this).after('<div class="alert alert-danger">Addded</div>');
    //   e.preventDefault();
    // });

// =============================================== //
// ============== FILTER SEARCH ================== //
// =============================================== //

    $('#filter_artikal_val').on('input', function(){
        var input_text = $(this).val();
        var divs = $('tr.table_row');

        $.each(divs, function() {
          $(this).hide();
        });

          $.each(divs, function() {
              var operacija_id = $(this).find('td').eq('0').html();
              if ( operacija_id.indexOf(input_text) > -1 ) {
                  $(this).show();
                  console.log(operacija_id);
              }   
          });

          
          $.each(divs, function() {

              var operacija_name = $(this).find('td').eq('1').html().toUpperCase();
              var upper_text = input_text.toUpperCase();

              if ( operacija_name.indexOf(upper_text) > -1 ) {
                  $(this).show();
                  console.log(operacija_name);
                  console.log(upper_text);
              }   
          });

 

    });

    $('.reset_artikal').on('click', function(e){
        e.preventDefault();

        $('#filter_artikal_val').val('');

        var divs = $('tr.table_row');

        $.each(divs, function( index, value ) {
          $(this).show();
        });

    });

}); // Doucument ready end of
