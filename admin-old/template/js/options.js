jQuery(document).ready(function($) {
        
  $('#neradni_dani_submit_btn').on('click', function(e){

    if ( $('#datepicker-month').val() === '' ) {
      return false; 
    } else {
      return true;
    }

  });

  $("#datepicker-month").datepicker({ 
    dateFormat: 'mm-yy',
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,

    onClose: function(dateText, inst) {  
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
    }
  });

  $("#datepicker-month").focus(function () {
    $(".ui-datepicker-calendar").hide();
    $("#ui-datepicker-div").position({
        my: "center top",
        at: "center bottom",
        of: $(this)
    });    
    $("table#ui-datepicker-calendar").hide();

  });

  $('.reload').on('click', function(e) {
    location.reload();
  });
  
});