$(document).ready(function(){

  // var base_url = 'http://localhost:8888/projekti/svn-projekti/tuscan/';
  // var base_url = 'http://localhost/tuscan/';
  var base_url = 'http://192.168.1.200/tuscan/';
// e.preventDefault();

  $('#add_tablet').on('click',function(e){

    var tablet_id = $('#tablet_id').val();
    var linija_id = $('#linija_id').val();
    var operacija_id = $('#operacija_id').val();

    var error = 0;

    if ( tablet_id == '' ) {
      $('#tablet_id').css('border','thin solid red');
      error++;
    }
    if ( linija_id == '' || linija_id == null ) {
      $('#linija_id').css('border','thin solid red');
      error++;
    }

    if ( operacija_id == '' || operacija_id == null ) {
      $('#operacija_id').css('border','thin solid red');
      error++;
    }

    if ( error > 0 ){
      e.preventDefault();
    }

  });

  $('#add_operations_to_tablet').on('click', function(e){
    // alert();

    var number_of_input_filds = $('.input_options_operation').length;
    var redni_id = number_of_input_filds + 1;
    var operacije = '';

    $.ajax({
      url: base_url + "api/data.php?action=return-data",
      method: "POST",
      data: {
        options: "operacija"
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      }
    })
    .done(function( data ) {
      if ( console && console.log ) {
        console.log( "Sample of data:", data.slice( 0, 100 ) );
      }
      // $('#set_artikal_data_options').before(
      $('.select-box-container').append(
        '<div class="clear">'+
        // '<input class="col-md-3 col-xs-3 col-sm-12 btn-primary input_options_artikal" type="text" name="artikal_id[]" value="" placeholder="Artikal ID : artikal_id_'+redni_id+'" >'+
        // '<input class="col-md-3 col-xs-3 col-sm-12  input_options_artikal" type="text" name="artikal_id[]" value="" placeholder="Artikal ID : artikal_id_'+redni_id+'" >'+
        '<select class="col-md-10 col-xs-10 col-sm-10 btn-default input_options_operation" id="operacija" name="operacija[]">'+
        data+
        '</select>'+
        '<a class="pull-right col-xs-1 col-md-offset-1 btn btn-lg btn-danger remove_artikal_row wrap"> X </a>'+
        '<span class="clear"></span>' +
        '</div>'
      );
    });

    e.preventDefault();
  });


// ===================================================== //
// ===================================================== //
// ===================================================== //

  $('.filter_ip_val').on('input', function() { 
      var input_val = $(this).val(); // get the current value of the input field.
      var divs = $('.filter_ip');

    $.each(divs, function( index, value ) {
      $(this).hide();
    });

    console.log(divs);

    $.each(divs, function( index, value ) {

      if ( divs.find('.radnik_ip_id')[index].html().indexOf(upper_val) >= -1 ) {
        $(this).show();
      }

    });

    console.log(input_val + ' = ' + divs.find('.radnik_ip_id').html());

  });

  $('.reset_input_filter').on('click', function(e){
    e.preventDefault();

    $('.filter_ip_val').val('');

      var divs = $('.filter_ip');

      $.each(divs, function( index, value ) {
      $(this).show();
    });

  });


}); // Doucument ready end of
