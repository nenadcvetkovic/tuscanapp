$(document).ready(function(){

  $('#add_komesa').on('click', function(e){

    // alert('pocetak validacije');

    if ( check_vailidation() ) {
      alert('true');
    } else {
      return false;
    }

  });

  var check_vailidation = function(){
    var error = 0;
    var error_msg = '';


    var komesa_id = $('#komesa_id').val();
    var select_artikal = $('#select_artikal option:selected').val();
    var artikal_komada_po_komesi = $('#artikal_komada_po_komesi').val();
    var datum_rada = $('.datum_rada').val();

    if ( komesa_id == '' ) {
      error++;
      error_msg = error_msg + '<li>Komesa ID</li>';
    }

    if ( select_artikal == '' || select_artikal == 'default' ) {
      error++;
      error_msg = error_msg + '<li>Artikal</li>';
    }
    if ( artikal_komada_po_komesi == '' ) {
      error++;
      error_msg = error_msg + '<li>Komada po komesi</li>';
    }

    if ( error > 0 ) {
      $('#message_box').addClass('alert alert-danger').html(
        'Niste ubacili sve podatke za novu komesu. Molimo vas da popravite podatke.'+
        error_msg
      );
      error_msg = '';
      return false;
      // e.preventDefault();
    } else {
      //
      return true;
    }
  }


  // filter box
  var hint_btn_visible = 0;
  $('.filter-btn-visible').on('click', function(){
      $('.filter-container').toggle('slideDown');
      if ( hint_btn_visible == 0 ) {
        hint_btn_visible = 1;
        $('.filter-btn-visible').html('Hide filter options');
      } else {
        hint_btn_visible = 0;
        $('.filter-btn-visible').html('Show filter options');
      }
  });

  $('.filter-btn').on('click', function(e){
    // alert();
    // $('.filter-box').slideDown();

    if ( $('.filter-box').length == 0 ) {
      $('.filter-btn').after(
        '<div class=" filter-box alert alert-danger">Working</div>'
      );
    } else {
      alert('Vec ima');
    }

    e.preventDefault();
  });

  // add btn
    $('.add_btn').mouseover(function(e){
      $('span.show-text').toggle();
    });

    $('.add_btn').mouseout(function(e){
      $('span.show-text').toggle();
    });

    // filter form
    $('#reset_filter_form').on('click', function(e){
      document.getElementById("filter_form").reset();
      e.preventDefault();
    });


}); // Doucument ready end of
