<html>
<head>
<script type="text/javascript">
var c=0;
var t;
var timer_is_on=0;
var x=1;


function timedCount()
{
document.getElementById('txt').value=c;
c=c+1;
t=setTimeout("timedCount()",1000);
}

function doTimer()
{
if (!timer_is_on)
  {
  timer_is_on=1;
  timedCount();
  }
}

function stopCount()
{
clearTimeout(t);
timer_is_on=0;
}

function saveSeconds()
{
   var seconds = document.getElementById('txt').value;
   if (!timer_is_on && seconds != '')
   {
        document.getElementById('save').value = document.getElementById('save').value + x++ + ") " + seconds + '\n';
        c=0;
        document.getElementById('txt').value = '';
   }
}



</script>
</head>

<body>
<form>
<input type="button" value="Start count!" onClick="doTimer()">
<input type="text" id="txt">
<input type="button" value="Stop count!" onClick="stopCount()">
<input type="button" value="Save" onClick="saveSeconds()">
<br>
<textarea id="save" name="save" cols="10" rows="10"></textarea>
</form>
</body>
</html>
