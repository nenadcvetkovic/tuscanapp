<?php session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once 'system/function.php';
//
require_once 'system/config/config.php';

// require_once 'system/libs/Database.php';
// require_once 'system/libs/Url.php';
// require_once 'system/libs/Helper.php';
// require_once 'system/libs/Ip.php';
// require_once 'system/libs/Table.php';

// Include classes here
$class = array('Database','Url','Helper','Ip','Table');

__autoload($class);

?>

<?php
// Insert taempalte part 

include('template/header.php');

if ( (Ip::getUserRealIpAdress() == BASEIP) || (Ip::getUserRealIpAdress() == '192.168.1.255') || (Ip::getUserRealIpAdress() == '192.168.1.254') ) { // Povera da li adresa tacna za admin panel
  Url::header_status(ADMINURL);
} elseif ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')) )) == 0 ) { //Povera da li ima zadataka za danasji dan
  Url::header_status(BASEURL.'404.php');
  die('Nemate dozvolu da pristupite serveru. Molimo vas da se konsultujete sa administratormom. Hvala');
} elseif ( Ip::checkIpAddressValidation() ) { // Provera da li ima adrese u db

  Url::header_status(BASEURL.'404.php');
  die('Nemate dozvolu da pristupite serveru. Molimo vas da se konsultujete sa administratormom. Hvala');

} elseif ( false ) { // Provera inactivity 
  // You need to do this!!!!
} else {

  if ( ( isset($_SESSION['working-session']) ) &&
  ( isset($_SESSION['working-session']['action']) &&  ($_SESSION['working-session']['action'] == 'start') ) &&
  ( isset($_SESSION['working-session']['login-data']) && ($_SESSION['working-session']['login-data'] == 'user-loged') ) &&
  ( isset($_SESSION['working-session']['job-selected'])  && ($_SESSION['working-session']['job-selected'] == 'job-selected') ) &&
  // ( isset($_SESSION['working-session']['linija']) && ($_SESSION['working-session']['linija'] != '') ) &&
  ( isset($_SESSION['working-session']['radnik']) && ($_SESSION['working-session']['radnik'] != '') ) &&
  ( isset($_SESSION['working-session']['operacija']) &&  ($_SESSION['working-session']['operacija'] != '') ) &&
  ( isset($_SESSION['working-session']['komesa']) && ($_SESSION['working-session']['komesa'] != '') ) &&
  ( isset($_SESSION['working-session']['artikal']) &&  ($_SESSION['working-session']['artikal'] != '') )
  ) {
    $_SESSION['working_page'] = true;
    // require_once('working-page-new.php');
    require_once('require_files/app/check_work_inactivity.php');
    require_once('require_files/app/working-page.php');

  } elseif ( ( isset($_SESSION['working-session']) ) &&
  ( isset($_SESSION['working-session']['action']) &&  $_SESSION['working-session']['action'] == 'start' ) &&
  ( isset($_SESSION['working-session']['login-data']) && $_SESSION['working-session']['login-data'] == 'user-loged' ) &&
  // ( isset($_SESSION['working-session']['linija']) && $_SESSION['working-session']['linija'] != '' ) &&
  ( isset($_SESSION['working-session']['radnik']) && $_SESSION['working-session']['radnik'] != '' ) &&
  ( !isset($_SESSION['working-session']['job-selected']) ) &&
  ( !isset($_SESSION['working-session']['operation']) )
  ) {

    $o = '';

    // $operacija = Database::whereQuery('operacije', array('id'=>$data[0]['operacija_id']));
    // Helper::pre($operacija);

    // if ( Database::num_rows(Database::returnWhereQuery('tablet_ip_settings', array('converted_tablet_ip'=>Ip::ReturnConvertedIp()))) == 1 ) {
    //   $o .= '<div class="col-md-12 alert-lightgreen text-center wrap-bottom">';
    //   $o .= '<h3>Naziv operacije: '.$operacija[0]['opis_operacije'].'</h3>';
    //   $o .= '<h3>Sifra operacije: '.$operacija[0]['operacija_id'].'</h3>';
    //   $o .= '<h3>Linija: Linija '.$data[0]['linija_id'].'</h3>';
    //   $o .= '</div>';
    // }

    echo $o;
    
    require_once('require_files/app/check_work_inactivity.php');
    require_once('require_files/app/job-option.php');

  } elseif( ( !isset($_SESSION['working-session']) ) &&
  ( !isset($_SESSION['working-session']['action']) ) &&
  ( !isset($_SESSION['working-session']['login-data']) ) &&
  ( !isset($_SESSION['working-session']['operation']) ) &&
  ( !isset($_SESSION['working-session']['linija']) ) &&
  ( !isset($_SESSION['working-session']['radnik']) )
  ) {

    // check if job is set for this line and operation
    // $operacija = Database::whereQuery('operacije', array('id'=>$data[0]['operacija_id']));
    // $linija = $data[0]['linija_id'];
    // $operacija_id = $operacija[0]['operacija_id'];

    // Proveriti da li je podesen zadatak za liniju za radni dan
    // Ako nije prijaviti greesku i preci na else deo

    // if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d') ))) == 0 ) {
    //   $o = '';
    //
    //   $o .= '<div class="col-md-12 alert-lightred text-center wrap-bottom input_paddding">';
    //   $o .= '<h2 class="wrap-top wrap-bottom wrap">Trenutno nema zadataka za liniju '.$linija.' </h2>';
    //   $o .= '<h2 class="wrap-top wrap-bottom wrap">za radni dan '. date('d.m.Y') .' </h2>';
    //   $o .= '<h4> Kontaktirajte administratora o nastalom problemu </h4>';
    //   $o .= '<br/>';
    //   $o .= '</div>';
    //
    //   echo $o;
    //
    // } else {
    //   // Helper::pre($operacija);
    //   $o = '';
    //   // if ( Database::num_rows(Database::returnWhereQuery('tablet_ip_settings', array('converted_tablet_ip'=>Ip::ReturnConvertedIp()))) == 1 ) {
    //   if ( false ) {
    //     $o .= '<div class="col-md-12 alert-lightgreen text-center wrap-bottom">';
    //     $o .= '<h3>Naziv operacije: '.$operacija[0]['opis_operacije'].'</h3>';
    //     $o .= '<h3>Sifra operacije: '.$operacija_id.'</h3>';
    //     $o .= '<h3>Linija: Linija '.$linija.'</h3>';
    //     $o .= '</div>';
    //
    //   } else {
    //     // ima vise od jedne operacije preskoci ovaj deo
    //     $o .= '<p class="col-xs-12 text-center alert alert-warning">Nema izabrane operacije</p>';
    //   }
    //   echo $o;
    //
    //
    // }
    require_once('require_files/app/login-new.php');


  } else {

    // Url::header_status('404.php');
    echo '<h1 class="text-center alert alert-lightred"> Problem sa kolacicima </h1>';
    echo '<a class="btn btn-info" href="'.BASEURL.'logout.php"> Logout </a>';
    Url::header_status(BASEURL."logout.php");
  }


}



?>


<?php require_once('template/footer.php'); ?>
