<?php

// cahce sifru
$sifra_radnika = $_POST['sifra'];
$_SESSION['sifra_radnika'] = $sifra_radnika;
// return user from data base for manipulation with data
$_SESSION['q'] = Database::returnWhereQuery('radnici_lista', array('radnik_id'=>$sifra_radnika));
if ( $radnik = Database::whereQuery('radnici_lista', array('radnik_id'=>$sifra_radnika)) ){

  // return ip address from mashin and convert ip fow comparing data
  $realipaddress = Ip::getUserRealIpAdress();
  $converted_ip = Ip::Dot2LongIP($realipaddress);

    $o = array(
      'sifra_radnika'   => $sifra_radnika,
      'ime_radnika'     => $radnik[0]['radnik_name'],
      'pocetak_rada'    => $radnik[0]['radnik_lifetime_from'],
      'ip'              => $realipaddress,
      'converted_ip'    => $converted_ip
    );

    $tablet_place_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>$o['converted_ip']));
    if ( !empty($_POST['linija_id']) && isset($_POST['linija_id']) && $_POST['linija_id']!='' ) {

    } else {
      $linija_id =  $tablet_place_data[0]['linija_id'];
    }

    $working_day_data = array(
      'work_day'  => date("Y-m-d"),
      'radnik_id' => $sifra_radnika,
      // 'linija'    => $radnik[0]['linija'],
      'login_time'=> date("Y-m-d H:i:s")
      // 'operacija_id' => $operacija_id
    );

    if ( count($tablet_place_data) == 1 ) {
      $operacija_id = $tablet_place_data[0]['operacija_id'];
      // $o = array_merge($o, array('linija_id' =>$linija_id));
      $o = array_merge($o, 'operacija_id'=>$operacija_id));
      $working_day_data = array_merge($working_day_data, array('operacija_id' => $operacija_id) );
      $_SESSION['working-session']['operacija'] = $operacija_id;
    } elseif ( count($tablet_place_data) > 1 ) {
      // $o = array_merge($o, array('linija_id' =>$linija_id) );
    }


    // check if user is loged
    $query = 'SELECT id FROM `working_day_session` WHERE radnik_id = '.$sifra_radnika.'  AND logout_time IS NULL';
    $_SESSION['testing'] = $query;
    $check_user = Database::query( $query );

    if ( isset($check_user) && $check_user->num_rows > 0 ) {
      $_SESSION['error'] = 'There are some user loged with this password';
      $o = array(
        'status'     => 'error',
        'error_msg'  => "There are some user loged with this password"
      );
      header('Content-Type: application/json');
      echo json_encode($o);
    } else {
      if ( $last_id = Database::insert_data( 'working_day_session', $working_day_data ) ) {
        $_SESSION['working-session']['working-session-id'] = $last_id;
        // $query = "";
        $_SESSION['working-session']['action'] = 'start';
        $_SESSION['working-session']['first-login-time'] = date("Y-m-d H:i:s");
        $_SESSION['working-session']['first-login-timestamp'] = time();
        $_SESSION['working-session']['login-data'] = 'user-loged';
        // $_SESSION['working-session']['linija'] = $linija_id;
        // $_SESSION['working-session']['name_linija'] = $_POST['name_linija'];
        $_SESSION['working-session']['radnik'] = $sifra_radnika;
        $_SESSION['working-session']['name_radnik'] = $o['ime_radnika'];
        // $_SESSION['working-session']['sifra'] = $_POST['sifra'];
        // if ( count($operacija_id) > 1 ) {
        //   $_SESSION['working-session']['operacija'] = $operacija_id;
        // }

        // Set status for this page
        $_SESSION['working-session']['login_staus'] = 'success';

        // return data to page
        header('Content-Type: application/json');
        echo json_encode($o);
      } else {
        $_SESSION['ne'] = Database::returnInsertData( 'working_day_session', $working_day_data );
        $_SESSION['error'] = 'Error inserting data to database';
        $o = array(
          'status'     => 'error',
          'error_msg'  => "Not inserted data into database"
        );
        header('Content-Type: application/json');
        echo json_encode($o);
      }
    }






} else {
  $o = array(
    'status'     => 'error',
    'error_msg'  => "Pogresna sifra radnika.\n Pokusajte opet"
  );
  header('Content-Type: application/json');
  echo json_encode($o);
}
