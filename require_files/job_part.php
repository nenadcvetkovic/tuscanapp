<?php
  $mysqli = Database::connect();

  if ( isset($_SESSION['working-session']['operacija']) && $_SESSION['working-session']['operacija']!='') {
    $operacija_id = $_SESSION['working-session']['operacija'];
  } else {
    $operacija_id = $_POST['operacija'];
  }
  $operacija_data = Database::whereQuery('operacije',array('operacija_id'=>$operacija_id));

  $artikal_id = $_POST['artikal'];
  $artikal_data = Database::whereQuery('artikal',array('id'=>$artikal_id));

  $komesa = $_POST['komesa'];
  $komesa_data = Database::whereQuery('komesa', array('id'=>$komesa));

  // choose linija id from komesa line settings
  $linija = Database::whereQuery('komesa_line_settings',array('working_day'=>date('Y-m-d'), 'komesa_id'=>$komesa));

  $linija_id = $linija[0]['linija_id'];
  $_SESSION['working-session']['linija'] = $linija_id;

  $_SESSION['working-session']['color'] = $_POST['color'];
  $_SESSION['working-session']['size'] = $_POST['size'];

  $color = $_POST['color'];
  $size  = $_POST['size'];


if ( isset($_SESSION['working-session']['radnik']) && isset($_SESSION['working-session']['radnik2']) ) {
  $radnik_id = $_SESSION['working-session']['radnik'];
  $radnik_id_2 = $_SESSION['working-session']['radnik2'];

  $array_radnika = array(
    $radnik_id=>$_SESSION['working-session']['working-session-id'],
    $radnik_id_2 =>$_SESSION['working-session']['working-session-id-2']
  );

  foreach ($array_radnika as $radnik=>$working_session_id) {
    
    $working_day_session_id = $working_session_id.'-'.$radnik.'-'.$operacija_id.'-'.$artikal_id.'-'.$komesa.'-'.$color.'-'.$size;

    $query = "UPDATE working_day_session ";
    $query .= " SET working_day_session_id = '".$working_day_session_id."' ,linija='".$linija_id."', operacija_id = '".$operacija_id."', artikal_id = '".$artikal_id."', komesa = '".$komesa."' ";
    $query .= " WHERE id = '".$working_session_id."' AND radnik_id ='".$radnik_id."' ";

    if ( $result = $mysqli->query( $query ) ) {

      $where_array =  array(
        'working_day_session_id'=>$working_day_session_id,
        'radnik_id'             => $radnik,
        // 'linija_id'             => $linija_id,
        'komesa_id'             => $komesa,
        'artikal_id'            => $artikal_id,
        'operacija_id'          => $operacija_data[0]['id'],
        'color'                 => $color,
        'size'                  => $size
      );

      $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija_id));

      if ( $last_inserted_details_id = Database::insert_data('working_day_session_details', $where_array) ) {
        $_SESSION['working-session']['action'] = 'start';
        $_SESSION['working-session']['login-data'] = 'user-loged';
        $_SESSION['working-session']['job-selected'] = 'job-selected';
        $_SESSION['working-session']['operacija'] = $operacija_id;
        $_SESSION['working-session']['name_operacija'] = $operacija_data[0]['opis_operacije'];
        $_SESSION['working-session']['komesa'] = $_POST['komesa'];
        $_SESSION['working-session']['name_komesa'] = $komesa_data[0]['komesa_name'];
        $_SESSION['working-session']['artikal'] = $_POST['artikal'];
        $_SESSION['working-session']['name_artikal'] = $artikal_data[0]['artikal_id'];
        if ( !isset($_SESSION['working-session']['working_day_session_id']) ) {
          $_SESSION['working-session']['working_day_session_id'] = $working_day_session_id;
        } else {
          $_SESSION['working-session']['working_day_session_id_2'] = $working_day_session_id;
        } 

        $operacija_id = $_SESSION['working-session']['operacija'];
        $operacija_data = Database::whereQuery('operacije', array('id'=>$operacija_id) );
        $new_artikal_table_name = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$_SESSION['working-session']['artikal'];
        $new_artikal_data = Database::whereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
        $operacija_data_vreme_po_operaciji = $new_artikal_data[0]['tempo_operacije'];
        $_SESSION['working-session']['vreme_operacije'] = $operacija_data_vreme_po_operaciji;

        $_SESSION['working-session']['job_options_staus'] = 'success';

        echo json_encode( array('status'=>'success') );

      } else {
        $_SESSION['fgdbfbdfxvfx']= 'Error';
        $_SESSION['error']['insert_to_working_day_session_details'] = Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id));
        $_SESSION['error']['insert_to_working_day_session_details2'] = Database::returnInsertData('working_day_session_details', $where_array);
        // echo json_encode( array($_POST,$query,$mysqli->error) );
        echo json_encode(
          array(
            'status'=>'error',
            'insert_data' => Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id)),
            'insert_data_2' => Database::returnInsertData('working_day_session_details', $where_array)

          )
        );
      }

    }    


  }

} else {

  $radnik_id = $_SESSION['working-session']['radnik'];

  $working_session_id = $_SESSION['working-session']['working-session-id'];
  $working_day_session_id = $working_session_id.'-'.$radnik_id.'-'.$operacija_id.'-'.$artikal_id.'-'.$komesa.'-'.$color.'-'.$size;

  $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija_id) );

  $query = "UPDATE working_day_session ";
  $query .= " SET working_day_session_id = '".$working_day_session_id."' ,linija='".$linija_id."', operacija_id = '".$operacija_data[0]['id']."', artikal_id = '".$artikal_id."', komesa = '".$komesa."' ";
  $query .= " WHERE id = '".$working_session_id."' AND radnik_id ='".$radnik_id."' ";

  if ( $result = $mysqli->query( $query ) ) {
    // $last_inserted_details_id = Database::insert_data('working_day_session_details', array('working_day_session_id'=>$working_day_session_id));
    // $_SESSION['er_d2'] = $last_inserted_details_id;
    // $_SESSION['er_d1'] = Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id));

    $where_array =  array(
      'working_day_session_id'=>$working_day_session_id,
      'radnik_id'             => $radnik_id,
      // 'linija_id'             => $linija_id,
      'komesa_id'             => $komesa,
      'artikal_id'            => $artikal_id,
      'operacija_id'          => $operacija_data[0]['id'],
      'color'                 => $color,
      'size'                  => $size
    );

    $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija_id));

    if ( $last_inserted_details_id = Database::insert_data('working_day_session_details', $where_array) ) {
      $_SESSION['working-session']['action'] = 'start';
      $_SESSION['working-session']['login-data'] = 'user-loged';
      $_SESSION['working-session']['job-selected'] = 'job-selected';
      $_SESSION['working-session']['operacija'] = $operacija_id;
      $_SESSION['working-session']['name_operacija'] = $operacija_data[0]['opis_operacije'];
      $_SESSION['working-session']['komesa'] = $_POST['komesa'];
      $_SESSION['working-session']['name_komesa'] = $komesa_data[0]['komesa_name'];
      $_SESSION['working-session']['artikal'] = $_POST['artikal'];
      $_SESSION['working-session']['name_artikal'] = $artikal_data[0]['artikal_id'];
      $_SESSION['working-session']['working_day_session_id'] = $working_day_session_id;

      $operacija_id = $_SESSION['working-session']['operacija'];
      $operacija_data = Database::whereQuery('operacije', array('id'=>$operacija_id) );
      $new_artikal_table_name = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$_SESSION['working-session']['artikal'];
      $artikal_data = Database::whereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
      $operacija_data_vreme_po_operaciji = $artikal_data[0]['tempo_operacije'];
      $_SESSION['working-session']['vreme_operacije'] = $operacija_data_vreme_po_operaciji;

      $_SESSION['working-session']['job_options_staus'] = 'success';

      echo json_encode( array('status'=>'success') );

    } else {
      $_SESSION['fgdbfbdfxvfx']= 'Error';
      $_SESSION['error']['insert_to_working_day_session_details'] = Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id));
      $_SESSION['error']['insert_to_working_day_session_details2'] = Database::returnInsertData('working_day_session_details', $where_array);
      // echo json_encode( array($_POST,$query,$mysqli->error) );
      echo json_encode(
        array(
          'status'=>'error',
          'insert_data' => Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id)),
          'insert_data_2' => Database::returnInsertData('working_day_session_details', $where_array)

        )
      );
    }
  } else {
    $_SESSION['dfgdgdffd'] = 'Error 2';
    $_SESSION['dfgdgddfdfdffd'] = 'Error 2: ' . $query;

    $_SESSION['x_error'] = Database::returnInsertData('working_day_session_details', array('working_day_session_id'=>$working_day_session_id));
    // echo json_encode( array($_POST,$query,$mysqli->error.'111') );
    echo json_encode( array('status'=>'Error 2') );
  }


}