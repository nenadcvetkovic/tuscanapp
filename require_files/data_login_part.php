<?php

// return ip address from mashin and convert ip fow comparing data
$realipaddress = Ip::getUserRealIpAdress();
$converted_ip = Ip::Dot2LongIP($realipaddress);

$o = array(
  'ip'              => $realipaddress,
  'converted_ip'    => $converted_ip
);

$working_day_data = array(
  'work_day'  => date("Y-m-d"),
  'login_time'=> date("Y-m-d H:i:s")
);





if ( isset($_POST['sifra']) && $_POST['sifra']!='' && $_POST['sifra'] != 0 &&
      isset($_POST['sifra2']) && $_POST['sifra2']!='' && $_POST['sifra2']!= 0 ) {
        // double login
  require_once('double_login.php');

} elseif( isset($_POST['sifra']) && $_POST['sifra']!='' && $_POST['sifra']!= 0
          && !isset($_POST['sifra2'])
) {
  // cache color and so=ize
  $size = $_POST['size'];
  $color = $_POST['color'];
  $o = array_merge($o, array('color'=> $color, 'size' => $size));
  $working_day_data = array_merge($working_day_data, array('color'=> $color, 'size' => $size));

  // cache sifru
  $sifra_radnika = $_POST['sifra'];
  $o = array_merge($o, array('sifra_radnika'=> $sifra_radnika));
  $working_day_data = array_merge($working_day_data, array('radnik_id'=> $sifra_radnika));

  $tablet_place_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>$o['converted_ip']));
  if ( !empty($_POST['linija_id']) && isset($_POST['linija_id']) && $_POST['linija_id']!='' ) {
    $linija_id = $_POST['linija_id'];
  } else {
    $linija_id =  $tablet_place_data[0]['linija_id'];
  }

  if ( count($tablet_place_data) == 1 ) {
    $operacija_id = $tablet_place_data[0]['operacija_id'];
    $_SESSION['working-session']['operacija'] = $operacija_id;

    $o = array_merge($o,
      array('linija_id' =>$linija_id, 'operacija_id'=>$operacija_id)
    );
    // $o = array_merge($o, array('linija_id' =>$linija_id));
    // $o = array_merge($o, 'operacija_id'=>$operacija_id));


    // $working_day_data = array_merge($working_day_data,
    //   array('operacija_id' => $operacija_id)
    // );

  } elseif ( count($tablet_place_data) > 1 ) {

    $o = array_merge($o,
      array('linija_id' =>$linija_id)
    );

  }

  // return user from data base for manipulation with data
  if ( $radnik = Database::whereQuery('radnici_lista', array('radnik_id'=>$sifra_radnika)) ){

    $o = array_merge($o,
      array(
        'ime_radnika'     => $radnik[0]['radnik_name'],
        'pocetak_rada'    => $radnik[0]['radnik_lifetime_from']
      )
    );

    $_SESSION['working-session']['name_radnik'] = $radnik[0]['radnik_name'];

    // $working_day_data = array_merge(
    //   $working_day_data,
    //   array(
    //     'linija'    => $radnik[0]['linija']
    //   )
    // );

    // check if user is loged
    $query = 'SELECT id FROM `working_day_session` WHERE radnik_id = '.$sifra_radnika.'  AND logout_time IS NULL';
    $check_user = Database::query( $query );

    if ( isset($check_user) && $check_user->num_rows > 0 ) {

      $_SESSION['error'] = 'There are some user loged with this password';
      $error_status = array(
          'status'      => 'error',
          'status_msg'  => 'Cheking user status.......<br/> Error!!!',
          'error_msg'   => "There are some user loged with this password",
        );
        header('Content-Type: application/json');
        echo json_encode($error_status);

      } else {
        if ( $last_id = Database::insert_data( 'working_day_session', $working_day_data ) ) {

          $_SESSION['sifra_radnika'] = $sifra_radnika;
          $_SESSION['working-session']['working-session-id'] = $last_id;
          $_SESSION['working-session']['action'] = 'start';
          $_SESSION['working-session']['first-login-time'] = date("Y-m-d H:i:s");
          $_SESSION['working-session']['first-login-timestamp'] = time();
          $_SESSION['working-session']['login-data'] = 'user-loged';
          // $_SESSION['working-session']['linija'] = $linija_id;

          $_SESSION['working-session']['color'] = $color;
          $_SESSION['working-session']['size'] = $size;

          // $_SESSION['working-session']['name_linija'] = $_POST['name_linija'];
          $_SESSION['working-session']['radnik'] = $sifra_radnika;
          // $_SESSION['working-session']['sifra'] = $_POST['sifra'];
          // if ( count($operacija_id) > 1 ) {
          //   $_SESSION['working-session']['operacija'] = $operacija_id;
          // }

          // return data to page
          $success_status = array(
          'status'      => 'Success',
          'status_msg'  => 'Cheking user status.......<br/> Success!!!',
          'error_msg'   => "Idemo dalje",
          );
          header('Content-Type: application/json');
          echo json_encode($success_status);

        } else {
          $_SESSION['ne'] = Database::returnInsertData( 'working_day_session', $working_day_data );
          $_SESSION['error'] = 'Error inserting data to database';
          $o = array(
          'status'     => 'error-inside',
          'error_msg'  => "Not inserted data into database",
          'sql' => $_SESSION['ne'] = Database::returnInsertData( 'working_day_session', $working_day_data )
          );
          header('Content-Type: application/json');
          echo json_encode($o);
        }
      }

    } else {
      $o = array(
      'status'     => 'error',
      'error_msg'  => "Ne postoji radnik sa ovom sifrom. Pokusajte ponovo"
      );
      header('Content-Type: application/json');
      echo json_encode($o);
    }


} else {
  //
  $o = array(
  'status'     => 'errorrrrr',
  'error_msg'  => "Problem sa aplikacijom. Molimo vas da pokusate ponovo",
  'sql' => Database::returnWhereQuery('radnici_lista', array('radnik_id'=>$sifra_radnika))
  );
  header('Content-Type: application/json');
  echo json_encode($o);
}
