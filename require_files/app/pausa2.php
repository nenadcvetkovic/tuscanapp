<?php

  $new_table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

  if ( Database::num_rows(Database::returnWhereQuery($new_table_name,array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'working_day'=>date('Y-m-d'),'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P003'))) > 0  && Database::num_rows(Database::returnWhereQuery($new_table_name,array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'working_day'=>date('Y-m-d'),'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P004'))) > 0) {

    Url::header_status(BASEURL);

  }
  if ( $_GET['options']=='greska' ) {
    echo '<div id="options" class="hidden">'. $_GET['options'].'</div>';
  }

  if ( isset($_SESSION['working-session']['radnik']) && $_SESSION['working-session']['radnik']!='' ) {
    echo '<div id="radnik_id" class="hidden">'.$_SESSION['working-session']['radnik'].'</div>';
  }


  // Helper::pre($_SESSION);
  echo '<div class="alert alert-success text-center">';

  echo '<h1> Pausa 2</h1>';
  echo '<h2> Uspesno krenula pauza </h2>';

  // Helper::pre(Database::num_rows(Database::returnWhereQuery($new_table_name,array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'working_day'=>date('Y-m-d'),'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P001'))));


  if ( Database::num_rows(Database::returnWhereQuery($new_table_name,array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'working_day'=>date('Y-m-d'),'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P003'))) == 0 ) {
    // echo 'Nema inputa';
    $sql = "INSERT INTO `".$new_table_name."`";
    $sql .= "(working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
    $sql .= "VALUES";
    $sql .= "('".$_SESSION['working-session']['working_day_session_id']."','".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','P003')";
    // Helper::pre($sql);
    if ( Database::query($sql) ) {
      echo '<p class="alert alert-lightblue">Uspesmno zapisana pausa</p>';
      $update_sql = "UPDATE `working_day_session`";
      $update_sql .= " SET pausa_2_start_time='".date('Y-m-d H:i:s')."' ";
      $update_sql .= " WHERE working_day_session_id = '".$_SESSION['working-session']['working_day_session_id']."' AND radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";
      // Helper::pre($update_sql);

      if ( Database::query($update_sql) ){
        // Helper::pre('<p class="alert alert-lightblue">Updatevana tabela working_day_session</p>');
        $_SESSION['pausa_data']['update_sql_status'] = 'updejtovana sql';
      } else {
        // Helper::pre('<p class="alert alert-alert">Nije updatevoana tabela</p>');
        $_SESSION['pausa_data']['update_sql'] = $update_sql;
      }
    }
  } else {
    // echo 'Ima inputa';
    // Url::header_status(BASEURL);
  }
  echo '<a class="end_pausa_2 btn alert alert-warning wrap-bottom" style="padding:30px;" href="'.BASEURL.'index.php?options=pausa2"> Kraj pause 2 </a>';
  echo '<div class="clear"></div>';

  echo '</div>';

  // Helper::pre(date('Y-m-d'));
  // Helper::pre(Database::num_rows(Database::returnWhereQuery($new_table_name,array('working_day'=>date('Y-m-d'), 'hint_code'=>'P001'))));
  // Helper::pre(Database::returnWhereQuery($new_table_name,array('working_day'=>date('Y-m-d'), 'hint_code'=>'P001')));


