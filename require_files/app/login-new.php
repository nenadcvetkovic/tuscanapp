<!-- Page Content -->
<div id="login-page" class="container">
  <div class="clear"></div>
  <div id="error_msg" class="text-center wrap-top alert"></div>

    <div id="login-data" class="wrap-bottom wrap-bottom">
        <div class="text-center">
        <form action="index.php" method="post">

          <div>
            <input id="input_sifra" type="number" name="sifra-radnika" class="col-xs-12" placeholder="Sifra radnika" />
          </div>
          <!-- <div class="change_line col-xs-12 alert alert-success">Pomena linije</div>
          <input id="input_linija" type="number" name="linija-radnika" class="col-md-12 col-xs-12 col-ms-12" placeholder="Linija radnika" /> -->

          <input id="login-new-btn" type="submit" name="submit-form" class="col-xs-5 btn btn-lg btn-primary" value="Prijavi se" />
          <input id="reset-btn" type="reset" name="reset-form" class="col-xs-3 col-xs-offset-4 btn btn-lg btn-danger" value="Reset" />
          <hr class="delimiter">



        </form>
        <div class="clear"></div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<br/>
<div class="container wrap">
  <div class="text-center" id="data"></div>

  <button id="double-login" class="btn btn-danger">Double user</button>
  <button id="double-login-remove" class="hidden btn btn-danger">Remove user input</button>

</div>
