<!-- Page Content -->
<div id="login-page" class="container">

    <div id="login-data">
        <div class="text-center">
        <form action="index.php" method="post">
            <div class="x">
              <div id="error_msg" class="alert"></div>
            </div>
            <h4 class="hiddenn">Linija</h4>
            <select id="linija" class="col-md-12 col-xs-12 col-ms-12">
              <optgroup label="Izaberite liniju">
                <!-- <option selected disabled value="default">Izaberite liniju</option> -->
              </optgroup>
            </select>

            <h4 class="hidden">Radnik</h4>
            <select id="radnik" class="col-md-12 col-xs-12 col-sm-12">
              <optgroup label="Radnik">
               <option selected disabled value="default">Izaberite radnika</option>
              </optgroup>
            </select>

          <h4 class="hidden">Sifra</h4>
          <div><input id="input_sifra" type="password" name="sifra-radnika" class="col-md-12 col-xs-12 col-ms-12" placeholder="Sifra radnika" /></div>
          <input id="login-1-btn" type="submit" name="submit-form" class="col-md-5 col-xs-5 col-sm-6 btn btn-lg btn-primary" value="Prijavi se" />
          <input id="reset-btn" type="reset" name="reset-form" class="col-md-5 col-xs-5 col-xs-offset-1 btn btn-lg btn-primary" value="OPonisti" />
        </form>
        </div>
    </div>
    <!-- /.row -->
    <div class="x" id="data">data</div>

</div>
<!-- /.container -->
