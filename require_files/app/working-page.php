<?php

// Helper::pre($_SESSION);

  $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

if ( empty($_GET) ) {

  // 
  if ( Database::num_rows(Database::returnWhereQuery($table_name, array('work_day'=>date('Y-m-d'), 'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'G001'))) > 0 && Database::num_rows(Database::returnWhereQuery($table_name, array('hint_code'=>'G002'))) == 0 ) {
    Url::header_status(BASEURL.'index.php?options=greska');
  } elseif ( Database::num_rows(Database::returnWhereQuery($table_name, array('work_day'=>date('Y-m-d'), 'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P001'))) > 0 && Database::num_rows(Database::returnWhereQuery($table_name, array('hint_code'=>'P002'))) == 0 ) {
    Url::header_status(BASEURL.'index.php?options=pausa2');
  }  elseif ( Database::num_rows(Database::returnWhereQuery($table_name, array('work_day'=>date('Y-m-d'), 'radnik_id'=>$_SESSION['working-session']['radnik'], 'hint_code'=>'P003'))) > 0 && Database::num_rows(Database::returnWhereQuery($table_name, array('hint_code'=>'P004'))) == 0 ) {
    Url::header_status(BASEURL.'index.php?options=pausa2');
  }

} 

if ( !isset($_SESSION['working-session']['login_staus']) && empty($_SESSION['working-session']['login_staus']) ) {
  $_SESSION['working-session']['login_staus'] = 'success';
}


$sql = "INSERT INTO `".$table_name."`";
$sql .= "(working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
$sql .= "VALUES";
$sql .= "('".$_SESSION['working-session']['working_day_session_id']."','".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s"')."','P001')";
$update_sql = "UPDATE `working_day_session`";
$update_sql .= "SET pausa_1_start_time='".date('Y-m-d H:i:s"')."' ";
$update_sql .= "WHERE radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";

// Helper::pre($update_sql);
// Helper::pre(get_options('siteurl'));
if ( !empty($_GET['options']) && $_GET['options']=='change_job' ) {
 require_once 'change_job_data.php';

} elseif ( !empty($_GET['options']) && $_GET['options']=='greska' ) {
  // GRESKA
  require_once 'greska.php';

} elseif ( !empty($_GET['options']) && $_GET['options']=='pausa' ) {
  // PAUSA 1
  require_once 'pausa.php';

} elseif ( !empty($_GET['options']) && $_GET['options']=='pausa2' ) {
  // PAUSA 2
  require_once 'pausa2.php';

} elseif ( !empty($_GET['options']) && $_GET['options']=='nesto' ) {
  // PAUSA 2
  require_once 'nesto.php';

} else {
  // WORKING PAGE

  require_once 'app.php';
}



$o = '';

$o .= '<!-- Modal -->';
$o .= '<div id="myModal" class="modal fade" role="dialog" style="display:none;">';
$o .= '  <div class="modal-dialog">';

$o .= '    <!-- Modal content-->';
$o .= '    <div class="modal-content alert-danger text-center">';
$o .= '      <div class="modal-header">';
$o .= '        <button type="button" class="close" data-dismiss="modal">&times;</button>';
$o .= '        <h4 class="modal-title">Problem sa wifi vezom!!!!</h4>';
$o .= '      </div>';
$o .= '      <div class="modal-body" style="height:80vh">';
$o .= '        <p>Pokusajte da uspostavite vezu sa ruterom i pokusajte ponovo.</p>';
$o .= '        <p>U slucaju da ne mozete da uspostavite vezu ili postoji neki drugi problem,</p>';
$o .= '        <p>konsultujte se sa administratorom</p>';

$o .= '         <button id="no_wifi_btn" type="button" class="btn-danger" data-dismiss="modall" style="height:200px;font-size:100px;cursor:pointer;">Try me</button>';
$o .= '      </div>';
$o .= '      <div class="modal-footer">';
// $o .= '        ';
$o .= '      </div>';
$o .= '    </div>';

$o .= '  </div>';
$o .= '</div>';

echo $o;