<?php

if ( isset( $_SESSION['working-session']['radnik'] ) && isset( $_SESSION['working-session']['working-session-id'] ) ) {
  if ( true ) {

  }

}

// Helper::pre($_SESSION);

if ( !empty($_POST) ) {

  $op_id = Database::whereQuery('operacije', array('operacija_id'=>$_POST['new_operacija']));
  $komesa_linija_data = Database::whereQuery('komesa_line_settings', array('komesa_id'=>$_POST['komesa'], 'working_day'=>date('Y-m-d')));
  // Helper::pre($komesa_linija_data);

 $radnik_id   = $_SESSION['working-session']['radnik'];
 $linija_id   = $komesa_linija_data[0]['linija_id'];
 $operacija_id= $op_id[0]['operacija_id'];
 $artikal_id  = $_POST['artikal'];
 $komesa_id   = $_POST['komesa'];
 $color       = 0;
 $size        = 0;

 $working_day_data  = array(
  // 'working_day_session_id' =>,
  'work_day' => date('Y-m-d'),
  'radnik_id' => $radnik_id,
  'linija' => $linija_id,
  'login_time'  => date('Y-m-d H:i:m'),
  // 'logout_time'  =>,
  // 'pausa_1_start_time'  =>,
  // 'pausa_1_end_time'  =>,
  // 'pausa_2_start_time'  =>,
  // 'pausa_2_end_time'  =>,
  'komesa' => $komesa_id,
  'artikal_id' => $artikal_id,
  'color' => '0',
  'size' => '0',
  'operacija_id' => $op_id[0]['id'],
  'operacija_hint' => '0'
  );

 if ( $working_session_id = Database::insert_data( 'working_day_session', $working_day_data ) ){

  $working_day_session_id = $working_session_id.'-'.$radnik_id.'-'.$operacija_id.'-'.$artikal_id.'-'.$komesa_id.'-'.$color.'-'.$size;

  $key_value_data = array(
   'working_day_session_id' => $working_day_session_id
  );
  $where_array = array(
   'id' => $working_session_id
  );

   if ( Database::updateData('working_day_session', $key_value_data, $where_array) ){
    // Helper::pre('Uradjeno');
    $artikal_data = Database::whereQuery('artikal', array('id'=>$artikal_id));
    $artikal_op_data = Database::whereQuery('artikal_data_'.$artikal_data[0]['artikal_id'].'_'. $artikal_data[0]['id'], array('operacija_id'=>$operacija_id));
   // Helper::pre($artikal_op_data);
   $komesa_data = Database::whereQuery('komesa', array('id'=>$komesa_id));
   $radnik_data = Database::whereQuery('radnici_lista', array('radnik_id'=>$radnik_id));
   // Helper::pre($radnik_data);

   $_SESSION['working-session']['operacija'] = $artikal_op_data[0]['operacija_id'];
   $_SESSION['working-session']['vreme_operacije'] = $artikal_op_data[0]['tempo_operacije'];
   $_SESSION['working-session']['name_operacija'] = $artikal_op_data[0]['operacija_name'];

   $_SESSION['working-session']['radnik'] = $radnik_id;
   $_SESSION['working-session']['name_radnik'] = $radnik_data[0]['radnik_name'];

   $_SESSION['working-session']['working-session-id'] = $working_session_id;

   $_SESSION['working-session']['first-login-time'] = date('Y-m-d H:i:m');
   $_SESSION['working-session']['first-login-timestamp'] = '1482600461';

   $_SESSION['working-session']['color'] = $color;
   $_SESSION['working-session']['size'] = $size;

   $_SESSION['working-session']['linija'] = $linija_id;
   $_SESSION['working-session']['job-selected'] = 'job-selected';

   $_SESSION['working-session']['komesa'] = $komesa_id;
   $_SESSION['working-session']['name_komesa'] = $komesa_data[0]['komesa_name'];

   $_SESSION['working-session']['artikal'] = $artikal_id;
   $_SESSION['working-session']['name_artikal'] = $artikal_data[0]['artikal_id'];

   $_SESSION['working-session']['working_day_session_id'] = $working_day_session_id;
   $_SESSION['working-session']['job_options_staus'] = 'success';
   $_SESSION['working-session']['login_staus'] = 'success';
   $_SESSION['working-session']['login-data'] = 'user-loged';
   $_SESSION['working-session']['action'] = 'start';

   Url::header_status(BASEURL.'index.php');

   } else {
    Helper::pre('Nije uradjeno');
   }
 } else {
  Helper::pre('Ne vraca');
 }


} else {


if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d') ))) == 0 ) {
  $o = '';

  $o .= '<div class="col-md-12 alert-lightred text-center wrap-bottom input_paddding">';
  $o .= '<h2 class="wrap-top wrap-bottom wrap">Trenutno nema zadataka </h2>';
  $o .= '<h2 class="wrap-top wrap-bottom wrap">za radni dan '. date('d.m.Y') .' </h2>';
  $o .= '<h4> Kontaktirajte administratora o nastalom problemu </h4>';
  $o .= '<hr class="delimiter">';

  $o .= '<a class="wrap-top input_padding btn btn-info wrap-bottom" href="'.BASEURL.'"><i class="fa fa-angle-double-left"></i> Provera zadatka </a>';
  $o .= '<div class="clear">';
  $o .= '<br/>';
  $o .= '</div>';

  echo $o;

} else {
  ?>
  <div class="container" id="job-page">
  <h1 class="alert alert-info text-center"> Promena posla </h1>
  <!-- Page Content -->
  <div id="job-page">
    <div id="error_msg" class="alert alert-lightred text-center input_paddding hidden">Error here</div>

      <div id="job-data">
          <div class="text-center">
          <form action="<?php echo BASEURL; ?>index.php?options=change_job" method="post" class="form-change_job-selected-2">
            <?php if ( isset($_SESSION['working-session']['operacija']) && $_SESSION['working-session']['operacija']!='' ) { ?>
            <input name="operacija" class="operacija_value hidden" type="text" value="<?php echo $_SESSION['working-session']['operacija']; ?>">
            <?php } ?>
        
            <?php if ( isset($_SESSION['working-session']['radnik']) && $_SESSION['working-session']['radnik']!='' ) { ?>
            <div id="radnik_id" class="hiddenn"><?php echo $_SESSION['working-session']['radnik']; ?></div>
            <?php } ?>
        
            <?php if ( isset($_SESSION['working-session']['working-session-id']) && $_SESSION['working-session']['working-session-id']!='' ) { ?>
            <div id="working_session_id" class="hiddenn"><?php echo $_SESSION['working-session']['working-session-id']; ?></div>
            <?php } ?>
        
            <h4 class="hidden">Artikal</h4>
            <select id="artikal" name="artikal" class="col-md-12 col-xs-12 col-ms-12">
              <optgroup label="">
                <option selected disabled value="default">Izaberite artikal</option>
                <?php
                $all_today_komesa = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')));
                $komessa_array = array();

                foreach ($all_today_komesa as $item ) {
                    $komesa_data = Database::whereQuery('komesa', array('id'=>$item['komesa_id']));

                    $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));

                    if ( in_array($komesa_data[0]['artikal_id'], $komessa_array) ) {
                      // echo '<option value="0"> Ima </option>';
                      // ima artikla u nizu
                      // preskace se ovaj artikal
                    } else {

                      echo '<li style="padding:20px;">';
                      echo '<option value="'.$komesa_data[0]['artikal_id'].'">ARTIKAL NAME:  '.$artikal_data[0]['artikal_name'].' | ARTIKAL ID '.$artikal_data[0]['artikal_id'].' </option>';
                      echo '</li>';
                      
                      // add komesa to array
                      array_push($komessa_array,$komesa_data[0]['artikal_id']);
                    }
                }
                Helper::pre($komessa_array);
                // $artikal_data_list = Database::fetchData('artikal');
                // foreach ($artikal_data_list as $artikal) {
                //   echo '<li style="padding:20px;">';
                //   echo '<option value="'.$artikal['id'].'">[ '.$artikal['id'].' ] [ '.$artikal['artikal_id'].' ]</option>';
                //   echo '</li>';
                // }
                ?>
              </optgroup>
            </select>

            <h4 class="hidden">Komesa</h4>
              <select id="komesa" name="komesa" class="col-md-12 col-xs-12 col-ms-12">
                <optgroup label="">
                  <option selected disabled value="default">Izaberite komesu</option>
                  <?php
                  $linija_id = $_SESSION['working-session']['linija'];
                  $komesa_array = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')));
                  foreach ($komesa_array as $komesa) {
                    $komesa_data = Database::whereQuery('komesa',array('id'=>$komesa['komesa_id']));
                    Helper::pre(Database::returnWhereQuery('komesa',array('komesa_id'=>$komesa['komesa_id'])));
                    Helper::pre($komesa_data);
                    if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d'),'komesa_id'=>$komesa_data[0]['id'])) ) > 0 ) {
                      echo '<option value="'.$komesa['komesa_id'].'"> [ Ime komese '.$komesa_data[0]['komesa_name']. ' ] [ Komesa ID '.$komesa_data[0]['komesa_id'] . ' ] </option>';
                    }
                  }
                  ?>
                </optgroup>
              </select>
            <?php

            // Helper::pre($komesa_array);

            ?>
              <style>
              .btn span.glyphicon {
                opacity: 0;
              }
              .btn.active span.glyphicon {
                opacity: 1;
              }
              label{
                height:50px;
                margin: 5px;
              }
              .color_options{
                  margin-top: 20px;
                  padding: 0;
              }
              .size_options{
                  margin-top: 20px;
                  padding: 0;
              }
              .size_options label{
                border: thin solid #333;
              }
              h2{
                margin-top: -15px;
              }
              select {
               -webkit-appearance: none;
               -moz-appearance: none;
               appearance: none;
               padding: 25px 10px;
              }
              optgroup,option{
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                padding: 25px 10px;
              }
              </style>


              <div class="artikal_color text-center"></div>

              <?php
                if ( !isset($_SESSION['working-session']['operacija']) ) {
              ?>
                <h4 class="text-center hidden">Operacija</h4>
                  <select id="operacija" name="new_operacija" class="hidden col-md-12 col-xs-12 col-ms-12">
                    <optgroup label="">
                      <option selected disabled value="default">Izaberite operaciju</option>
                      <?php
                      $user_converted_ip_adress = Ip::ReturnConvertedIp();
                      $tablet_place_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>$user_converted_ip_adress));

                      foreach ($tablet_place_data as $item) {
                        $op_data = Database::whereQuery('operacije', array('id'=>$item['operacija_id']));
                        echo '<option value="'.$op_data[0]['id'].'">'.$op_data[0]['operacija_id'].' [ '.$op_data[0]['operacija_name'].' ] [ '.$op_data[0]['opis_operacije'].' ] </option>';
                      }
                      ?>
                    </optgroup>
                  </select>
              <?php
              } else {
                // echo count($_SESSION['working-session']['operacija']);
               ?>
                <h4 class="text-center hidden">Operacija</h4>
                  <select id="operacija" name="new_operacija" class="hidden col-md-12 col-xs-12 col-ms-12">
                    <optgroup label="">
                      <option selected disabled value="default">Izaberite operaciju</option>
                     </optgroup>
                  </select>
                  <?php
              }
              ?>
              <hr class="delimiter">
            <input id="change_job-selected-2-btn" type="submit" name="submit-form" class="input_padding col-xs-5 btn btn-lg btn-primary" value="Prijavi se" />
            <input id="change_reset-job-btn" type="reset" name="reset-form" class="input_padding col-xs-5 col-xs-offset-2 btn btn-lg btn-danger" value="Reset" />
          </form>
          </div>
      </div>
      <!-- /.row -->
      <!-- <div class="x" id="data">data</div> -->

      <!-- coloro and siez from choosen artikal  -->

  </div>
  <!-- /.container -->

  <hr class="delimiter">
  <div class="clear"></div>
  <br/><br/><br/>
  <a href="<?php echo BASEURL; ?>logout.php" class="text-center btn-danger col-xs-5 full-width-btn btn-primary">Odjavi se</a>

  <a href="<?php echo BASEURL; ?>" class="text-center btn-danger col-xs-offset-2 col-xs-5 full-width-btn"> PONISTI </a>
  </div>
  <!--  working page container -->

<?php

}


}