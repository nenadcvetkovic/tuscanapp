  <?php
  // Helper::pre($_SESSION);

  $data = $_SESSION['working-session'];

    if ( isset($_SESSION['working-session']['working-session-id']) ) {
      echo '<div id="working_session_id" class="hidden">'.$_SESSION['working-session']['working-session-id'].'</div>';
    }
    if ( isset($_SESSION['working-session']['working_day_session_id']) ) {
      echo '<div id="working_day_session_id" class="hidden">'.$_SESSION['working-session']['working_day_session_id'].'</div>';
    }
    if ( isset($_SESSION['working-session']['operacija']) ) {
      echo '<div id="operacija_id" class="hidden">'.$_SESSION['working-session']['operacija'].'</div>';
    }
    if ( isset($_SESSION['working-session']['name_artikal']) ) {
      echo '<div id="name_artikal" class="hidden">'.$_SESSION['working-session']['name_artikal'].'</div>';
    }
    if ( isset($_SESSION['working-session']['artikal']) ) {
      echo '<div id="artikal" class="hidden">'.$_SESSION['working-session']['artikal'].'</div>';
    }
    if ( isset($_SESSION['working-session']['radnik']) ) {
      echo '<div id="radnik" class="hidden">'.$_SESSION['working-session']['radnik'].'</div>';
    }
    if ( isset($_SESSION['working-session']['linija']) ) {
      echo '<div id="linija" class="hidden">'.$_SESSION['working-session']['linija'].'</div>';
    }
    if ( isset($_SESSION['working-session']['komesa']) ) {
      echo '<div id="komesa" class="hidden">'.$_SESSION['working-session']['komesa'].'</div>';
    }



    $res = Database::whereQuery('tablet_ip_settings', array('tablet_ip'=>Ip::getUserRealIpAdress()));
      echo '<div id="masina_id" class="hidden">'.$res[0]['masina'].'</div>';
  ?>
  <div class="container-fluid" id="working-page">
    <div class="row">
      <div class="container col-xs-9">
      <!-- ------------ TOP MENU - USER DETAILS --------------- -->

        <div class="alert alert-white user-card-info">
        <?php if ( isset($data['radnik2']) && $data['radnik2']!='') { ?>
          <h6><span class="col-xs-5">Radnik:</span> <?php echo '<span id="radnik_id">'.$data['radnik'] .'</span> - '.$data['name_radnik']; ?></h6>
          <h6><span class="col-xs-5">Radnik2:</span> <?php echo '<span id="radnik_id_2">'.$data['radnik2'] .'</span> - '.$data['name_radnik_2']; ?></h6>
        <?php } else { ?>
          <h6><span class="col-xs-5">Radnik:</span> <?php echo '<span id="radnik_id">'.$data['radnik'] .'</span> - '.$data['name_radnik']; ?></h6>
        <?php } ?>

          <h6><span class="col-xs-5">Radni Broj/Komesa:</span> <?php echo $data['komesa'].' - '.$data['name_komesa']; ?></h6>
          <h6><span class="col-xs-5">Artikal:</span> <?php echo $data['artikal'].' - '.$data['name_artikal']; ?></h6>
          <h6><span class="col-xs-5">Operacija:</span> <?php echo $data['operacija'].' - '.$data['name_operacija']; ?></h6>
        </div>

        <!-- ------------ END TOP MENU ------------ -->
      </div>

      <!-- ------------- CLOCK AND LOGOUT ----------- -->
      <div class="col-xs-3">
        <div class="clock-style btn btn-lg btn-primary col-xs-11" id="clock">Clock</div>
        <a class="logout-btn btn btn-lg btn-primary col-xs-11" href="<?php echo BASEURL; ?>logout.php">Logout</a>
      </div>
      <!-- ------------- END CLOCK AND LOGOUT ----------- -->

      <div class="clear"></div>

      <div class="col-md-12">
        <?php

        $time = Database::whereQuery($table_name,array( 'working_day_session_id'=>$_SESSION['working-session']['working_day_session_id']), 'ORDER BY id DESC', '1');
        $_SESSION['e'] = Database::returnWhereQuery($table_name,array( 'working_day_session_id'=>$_SESSION['working-session']['working_day_session_id']), 'ORDER BY id DESC', '1');
        ?>

        <!-- ------------- HINT BTN ----------- -->

        <div id="operation-hint-btn" class="hint-btn col-xs-6">Hint IT</div>

        <!-- ------------- END HINT BTN ----------- -->


        <!-- ------------- HINT DETAILS ----------- -->
        <div class="col-xs-6 operation-hint-data">
        <?php

        $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

          $artikal_id = $_SESSION['working-session']['artikal'];
          $artikal_name = $_SESSION['working-session']['name_artikal'];
          $artikal_table_name = 'artikal_data_'.$artikal_name.'_'.$artikal_id;

          $working_data         = Database::whereQuery('working_day_session', array('id'=>$_SESSION['working-session']['working-session-id'], ) );
          $operacija_data       = Database::whereQuery('operacije', array('operacija_id'=>$_SESSION['working-session']['operacija']) );
          $artikal_table_data   = Database::whereQuery($artikal_table_name, array('operacija_id'=>$operacija_data[0]['operacija_id']) );

          $operacije_table_data   = Database::whereQuery('operacije', array('operacija_id'=>$operacija_data[0]['operacija_id']) );
          $tempo_operacije = $artikal_table_data[0]['tempo_operacije'];
          $vreme_po_operaciji = $tempo_operacije * $artikal_table_data[0]['komada_po_operaciji'];

          $last_hint_data       = Database::whereQuery($table_name, array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'working_day'=>date('Y-m-d')), 'ORDER BY id DESC LIMIT 1' );

          $hints = $working_data[0]['operacija_hint'] * $artikal_table_data[0]['komada_po_operaciji'];
          $hourly_rate = $tempo_operacije;
          $hourly_percent = floor(( 100 * $hints ) / $hourly_rate);

          $hourly_rate_percent  = Helper::returnCountThisHourHints($_SESSION['working-session']['working_day_session_id']);
          $working_hour_percent_success = ceil(( 100 * $hourly_rate_percent ) / $hourly_rate);

          $radnik_id = $_SESSION['working-session']['radnik'];
          // SELECT * FROM `working_day_session_details` WHERE radnik_id='101'
          if ( Database::num_rows(Database::returnWhereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001'))) > 0 ) {

            $index = 1;
            $efikasnost = 0;
            $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

            $sva_dnevna_efikasnost = Database::whereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001'));

            foreach ($sva_dnevna_efikasnost as $item) {
              $efikasnost += $item['procenat_ucinka'];
              $index++;
            }
            
            $counted_num = Database::num_rows(Database::returnWhereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001')));

            $dnevni_ucinak = ceil($efikasnost / $counted_num);

          } else {
            $counted_num = 0;
            $dnevni_ucinak = 0;
          }


          echo '<span class="hints-pre-operations hidden">'.$artikal_table_data[0]['komada_po_operaciji'].'</span>';
          echo '<div id="all_day_data" class="text-center alert alert-black">';
          echo '  <p> Ukupno danas: <span id="all_day_count">'.$counted_num.'</span></p>';
          echo '  Procenat dnevnog rada <span class="all_day_percent">'.floor($dnevni_ucinak).' </span>%';
          echo '</div>';

          $working_day_details = Database::whereQuery($table_name, array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'operacija_id'=>$operacije_table_data[0]['id'], 'radnik_id'=>$data['radnik'],'working_day'=>date('Y-m-d'),'hint_code'=>'F001'));
          // echo '<span class="col-xs-12 alert alert-grey">U ovom satu ima <span class="uradjenih_operacija">'.Helper::returnCountThisHourHints($_SESSION['working-session']['working_day_session_id']).'</span> uradjenih operacija</span>';
          // // echo '<hr class="delimiter">';

          $ukuppno_operacija =  Database::num_rows(Database::returnWhereQuery($table_name, array('working_day_session_id'=>$_SESSION['working-session']['working_day_session_id'], 'operacija_id'=>$operacije_table_data[0]['id'], 'radnik_id'=>$data['radnik'],'working_day'=>date('Y-m-d'),'hint_code'=>'F001')));

        ?>
          <div class="operation-hint-data-body">
            <span class="text-label col-xs-8 btn btn-primary">Ukupno operacija: </span>
              <span class=" col-xs-4 btn btn-info"><span class="operation-hints"><?php echo $ukuppno_operacija; ?></span></span>
             <span class="text-label col-xs-8 btn btn-primary">Komada po operaciji </span>
              <span class=" col-xs-4 btn btn-info"><span class=""><?php echo $artikal_table_data[0]['komada_po_operaciji']; ?></span></span>
            <span class="text-label col-xs-8 btn btn-primary">Vreme operacije </span>
              <span class="col-xs-4 btn btn-info">
                <span id="time_hint" class="time_hint_val">
                <?php if ( is_null($last_hint_data) ) { echo date('H:i:s',strtotime($last_hint_data[0]['vreme_operacije'])); } else { echo '00:00:00'; } ?>
                </span>
              </span>
            </span>
              
              <span id="razlika" class="text-label col-xs-12 btn btn-warning"> Razlika: <?php echo $last_hint_data[0]['razlika_u_vremenu']; ?></span>
              <span class="razlika_u_sekundama text-label col-xs-12 btn btn-primary"></span><br/>

            <span id="vreme_po_operaciji">
              <span class="text-label col-xs-8 btn btn-primary">Vreme po operaciji: </span>
                <span class="value col-xs-4 btn btn-info"><?php echo floor(3600 / $tempo_operacije) * $artikal_table_data[0]['komada_po_operaciji']; ?> (<?php echo floor(3600 / $tempo_operacije); ?>) s</span>
            </span>
            <span class="text-label col-xs-8 btn btn-primary">Komada po satu </span>
              <span class="col-xs-4 btn btn-info"><span class="hourly-rate"><?php echo $hourly_rate; ?></span><span> kom/h </span> </span>
            <span class="text-label col-xs-8 btn btn-primary">Procenat rada po satu </span>
              <span class=" col-xs-4 btn btn-info"><span class="working_hour_percent_success"><?php echo $working_hour_percent_success; ?></span><span>%</span> </span>
            <div class="clear"></div>
          </div>
          <p id="uspesnost" class="btn">
            <span class="text-label">Procenat zadnje operacije </span><span class="work-percent"><?php echo $last_hint_data[0]['procenat_ucinka']; ?></span> <span> % </span> <br/>
          </p>
        </div> <!--  END div.operation-hint-data -->

        <!-- ------------- END HINT DETAILS ----------- -->

        <!-- START Komesa Info  -->
        <?php
        // $komada_po_artiklu_array = Database::fetch("SELECT artikal_komada_per_komesa FROM `komesa` WHERE id='".$data['komesa']."'");

        $uradjenih_komada_po_artiklu_array = 'Napravi request for thhisss!!!!!';

        ?>
      </div>
    </div>
    <div>

    <hr class="delimiter">

    <?php 
      // =================================== //
      // ========== OPTION BUTTONS ========= //
      // =================================== //

      if ( Database::num_rows(Database::returnWhereQuery($table_name,array('working_day'=>date('Y-m-d'),'radnik_id'=>$data['radnik'],'hint_code'=>'P001'))) == 0 ) { ?>
      <a id="pausa-hint-btn2" class="menu_btn alert-warning" href="<?php echo BASEURL; ?>index.php?options=pausa">PAUSA CLICK</a>
      <?php } elseif ( Database::num_rows(Database::returnWhereQuery($table_name,array('working_day'=>date('Y-m-d'),'radnik_id'=>$data['radnik'],'hint_code'=>'P003'))) == 0 ) { ?>
      <a id="pausa-hint-btn2" class="menu_btn btn alert-warning col-xs-4" href="<?php echo BASEURL; ?>index.php?options=pausa2">PAUSA 2 CLICK</a>
      <?php } ?>
      <!-- <a id="2" class="menu_btn alert-warning" href="<?php echo BASEURL; ?>index.php?options=nesto"> Nesto </a> -->
      <a id="3" class="menu_btn alert-success col-xs-4 font-normal" href="<?php echo BASEURL; ?>index.php?options=greska"> Greska </a>
      <a id="4" class="menu_btn alert-danger col-xs-4 font-normal" href="<?php echo BASEURL; ?>index.php?options=change_job"> Promena Posla</a>


    </div>
  </div>
  <!--  working page container -->