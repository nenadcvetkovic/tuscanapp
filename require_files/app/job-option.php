<?php

// Helper::pre($_SESSION);

// $artikal_id = 60;
//
// $artikal_data = Database::whereQuery('artikal',array('id'=>$artikal_id));
// $artikal_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];
// $operacije_list = Database::fetchData($artikal_table);
//
// $tablet_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>Ip::ReturnConvertedIp()));
//
// $masina_table = 'masina_data_'.$tablet_data[0]['masina'];
// $operacije = Database::fetchData($masina_table);
//  // Helper::pre($operacije);
//
//
// echo $o;


if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d') ))) == 0 ) {
  $o = '';

  $o .= '<div class="col-md-12 alert-lightred text-center wrap-bottom input_paddding">';
  $o .= '<h2 class="wrap-top wrap-bottom wrap">Trenutno nema zadataka </h2>';
  $o .= '<h2 class="wrap-top wrap-bottom wrap">za radni dan '. date('d.m.Y') .' </h2>';
  $o .= '<h4> Kontaktirajte administratora o nastalom problemu </h4>';
  $o .= '<hr class="delimiter">';

  $o .= '<a class="wrap-top input_padding btn btn-info wrap-bottom" href="'.BASEURL.'"><i class="fa fa-angle-double-left"></i> Provera zadatka </a>';
  $o .= '<div class="clear">';
  $o .= '<br/>';
  $o .= '</div>';

  echo $o;

} else {
  ?>
  <div class="container" id="job-page">
  <hr class="delimiter">
  <h2 class="text-center">Izaberite posao</h2>
  <!-- Page Content -->
  <?php
    if ( isset($_SESSION['working-session']['radnik']) ) {
      echo '<div id="radnik_id" class="hidden">'.$_SESSION['working-session']['radnik'].'</div>';
    }
    if ( isset($_SESSION['working-session']['working-session-id']) ) {
      echo '<div id="working_session_id" class="hidden">'.$_SESSION['working-session']['working-session-id'].'</div>';
    }
  ?>
  <div id="job-page" class="container">
    <div id="error_msg" class="alert alert-lightred text-center input_paddding hidden">Error here</div>

      <div id="job-data">
          <div class="text-center">
          <form action="index.php" method="post" class="form-job-selected-2">
            <?php if ( isset($_SESSION['working-session']['operacija']) && $_SESSION['working-session']['operacija']!='' ) { ?>
            <input name="operacija" class="operacija_value hidden" type="text" value="<?php echo $_SESSION['working-session']['operacija']; ?>">
            <?php } ?>

            <h4 class="hidden">Artikal</h4>
            <select id="artikal" class="col-md-12 col-xs-12 col-ms-12">
              <optgroup label="">
                <option selected disabled value="default">Izaberite artikal</option>
                <?php
                $all_today_komesa = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')));
                $komessa_array = array();

                foreach ($all_today_komesa as $item ) {
                    $komesa_data = Database::whereQuery('komesa', array('id'=>$item['komesa_id']));

                    $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));

                    if ( in_array($komesa_data[0]['artikal_id'], $komessa_array) ) {
                      // echo '<option value="0"> Ima </option>';
                      // ima artikla u nizu
                      // preskace se ovaj artikal
                    } else {

                      echo '<li style="padding:20px;">';
                      echo '<option value="'.$komesa_data[0]['artikal_id'].'">ARTIKAL NAME:  '.$artikal_data[0]['artikal_name'].' | ARTIKAL ID '.$artikal_data[0]['artikal_id'].' </option>';
                      echo '</li>';
                      
                      // add komesa to array
                      array_push($komessa_array,$komesa_data[0]['artikal_id']);
                    }
                }
                Helper::pre($komessa_array);
                // $artikal_data_list = Database::fetchData('artikal');
                // foreach ($artikal_data_list as $artikal) {
                //   echo '<li style="padding:20px;">';
                //   echo '<option value="'.$artikal['id'].'">[ '.$artikal['id'].' ] [ '.$artikal['artikal_id'].' ]</option>';
                //   echo '</li>';
                // }
                ?>
              </optgroup>
            </select>

            <h4 class="hidden">Komesa</h4>
              <select id="komesa" class="col-md-12 col-xs-12 col-ms-12">
                <optgroup label="">
                  <option selected disabled value="default">Izaberite komesu</option>
                  <?php
                  $linija_id = $_SESSION['working-session']['linija'];
                  $komesa_array = Database::whereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')));
                  foreach ($komesa_array as $komesa) {
                    $komesa_data = Database::whereQuery('komesa',array('id'=>$komesa['komesa_id']));
                    Helper::pre(Database::returnWhereQuery('komesa',array('komesa_id'=>$komesa['komesa_id'])));
                    Helper::pre($komesa_data);
                    if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d'),'komesa_id'=>$komesa_data[0]['id'])) ) > 0 ) {
                      echo '<option value="'.$komesa['komesa_id'].'"> [ Ime komese '.$komesa_data[0]['komesa_name']. ' ] [ Komesa ID '.$komesa_data[0]['komesa_id'] . ' ] </option>';
                    }
                  }
                  ?>
                </optgroup>
              </select>
            <?php

            // Helper::pre($komesa_array);

            ?>
              <style>
              .btn span.glyphicon {
                opacity: 0;
              }
              .btn.active span.glyphicon {
                opacity: 1;
              }
              label{
                height:50px;
                margin: 5px;
              }
              .color_options{
                  margin-top: 20px;
                  padding: 0;
              }
              .size_options{
                  margin-top: 20px;
                  padding: 0;
              }
              .size_options label{
                border: thin solid #333;
              }
              h2{
                margin-top: -15px;
              }
              select {
               -webkit-appearance: none;
               -moz-appearance: none;
               appearance: none;
               padding: 25px 10px;
              }
              optgroup,option{
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                padding: 25px 10px;
              }
              </style>


              <div class="artikal_color text-center"></div>

              <?php
                if ( !isset($_SESSION['working-session']['operacija']) ) {
              ?>
                <h4 class="text-center hidden">Operacija</h4>
                  <select id="operacija" class="hidden col-md-12 col-xs-12 col-ms-12">
                    <optgroup label="">
                      <option selected disabled value="default">Izaberite operaciju</option>
                      <?php
                      $user_converted_ip_adress = Ip::ReturnConvertedIp();
                      $tablet_place_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>$user_converted_ip_adress));

                      foreach ($tablet_place_data as $item) {
                        $op_data = Database::whereQuery('operacije', array('id'=>$item['operacija_id']));
                        echo '<option value="'.$op_data[0]['id'].'">'.$op_data[0]['operacija_id'].' [ '.$op_data[0]['operacija_name'].' ] [ '.$op_data[0]['opis_operacije'].' ] </option>';
                      }
                      ?>
                    </optgroup>
                  </select>
              <?php
              } else {
                // echo count($_SESSION['working-session']['operacija']);
              }
              ?>

              <hr class="delimiter">
            <input id="job-selected-2-btn" type="submit" name="submit-form" class="input_padding col-xs-6 btn btn-lg btn-primary" value="Prijavi se" />
            <input id="reset-job-btn" type="reset" name="reset-form" class="input_padding col-xs-3 col-xs-offset-3 btn btn-lg btn-danger" value="Reset" />
          </form>
          </div>
      </div>
      <!-- /.row -->
      <!-- <div class="x" id="data">data</div> -->

      <!-- coloro and siez from choosen artikal  -->

  </div>
  <!-- /.container -->

  <hr class="delimiter">
  <a href="<?php echo BASEURL; ?>logout.php" class="btn btn-lg btn-primary">Odjavi se</a>
  </div>
  <!--  working page container -->
<?php

}
