-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Dec 04, 2016 at 09:36 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tuskan_family`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikal`
--

CREATE TABLE `artikal` (
  `id` int(11) NOT NULL,
  `artikal_id` varchar(50) DEFAULT NULL,
  `artikal_name` varchar(50) DEFAULT NULL,
  `artikal_desc` text,
  `artikal_datum_unosa` datetime DEFAULT NULL,
  `artikal_status` varchar(10) DEFAULT NULL,
  `linija_rada` int(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal`
--

INSERT INTO `artikal` (`id`, `artikal_id`, `artikal_name`, `artikal_desc`, `artikal_datum_unosa`, `artikal_status`, `linija_rada`) VALUES
(59, '1122888', 'TEST PROBA ARTIKAL,SIZE,COLOR 2', 'Test Proba artikal,size,color 2', '2016-11-03 23:04:02', '1', NULL),
(60, 'dfgh', 'BRVBDFCV', 'fdbcvncgb', '2016-11-08 15:45:23', '1', NULL),
(61, '112233erfr', 'TEST PROBA ARTIKAL,SIZE,COLOR 2', 'json probas', '2016-11-24 16:10:27', '0', 2);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61`
--

CREATE TABLE `artikal_data_112233erfr_61` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61`
--

INSERT INTO `artikal_data_112233erfr_61` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(1, '01.014', 'MANICHE', 'TAGLIACUCI UNIONE', 211, NULL, '2016-10-25 22:20:26'),
(2, '01.008', 'SEP. TELO SCHIENA + MANICA', 'TAGLIO A MANO', 101, NULL, '2016-10-25 22:18:32'),
(3, '01.012', 'F. COLLO', 'RIMAGLIO', 101, NULL, '2016-10-25 22:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61_color`
--

CREATE TABLE `artikal_data_112233erfr_61_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61_color`
--

INSERT INTO `artikal_data_112233erfr_61_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '2016-11-24 00:00:00', 1),
(2, 2, '2016-11-24 00:00:00', 1),
(3, 3, '2016-11-24 00:00:00', 0),
(4, 4, '2016-11-24 00:00:00', 1),
(5, 5, '2016-11-24 00:00:00', 1),
(6, 6, '2016-11-24 00:00:00', 0),
(7, 7, '2016-11-24 00:00:00', 0),
(8, 8, '2016-11-24 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61_size`
--

CREATE TABLE `artikal_data_112233erfr_61_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61_size`
--

INSERT INTO `artikal_data_112233erfr_61_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '2016-11-24 00:00:00', 1),
(2, 2, '2016-11-24 00:00:00', 1),
(3, 3, '2016-11-24 00:00:00', 1),
(4, 4, '2016-11-24 00:00:00', 1),
(5, 5, '2016-11-24 00:00:00', 0),
(6, 6, '2016-11-24 00:00:00', 0),
(7, 7, '2016-11-24 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59`
--

CREATE TABLE `artikal_data_1122888_59` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59`
--

INSERT INTO `artikal_data_1122888_59` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(5, '01.008', 'SEP. TELO SCHIENA + MANICA', 'TAGLIO A MANO', 334, NULL, '2016-10-25 22:18:32'),
(6, '01.011', 'COLLO SING. C 205 1X1---> C 221 1X1', 'RIMAGLIO', 31, NULL, '2016-10-25 22:19:31'),
(7, '123456', 'test brisajnja', 'Test brisanja', 11122, NULL, '2016-11-29 00:45:25');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59_color`
--

CREATE TABLE `artikal_data_1122888_59_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59_color`
--

INSERT INTO `artikal_data_1122888_59_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 1),
(3, 3, '0000-00-00 00:00:00', 1),
(4, 4, '0000-00-00 00:00:00', 1),
(5, 5, '0000-00-00 00:00:00', 0),
(6, 6, '0000-00-00 00:00:00', 1),
(7, 7, '0000-00-00 00:00:00', 0),
(8, 8, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59_size`
--

CREATE TABLE `artikal_data_1122888_59_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59_size`
--

INSERT INTO `artikal_data_1122888_59_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 1),
(3, 3, '0000-00-00 00:00:00', 0),
(4, 4, '0000-00-00 00:00:00', 1),
(5, 5, '0000-00-00 00:00:00', 1),
(6, 6, '0000-00-00 00:00:00', 1),
(7, 7, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60`
--

CREATE TABLE `artikal_data_dfgh_60` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60`
--

INSERT INTO `artikal_data_dfgh_60` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(1, '01.011', 'COLLO SING. C 205 1X1---> C 221 1X1', 'RIMAGLIO', 31, NULL, '2016-10-25 22:19:31'),
(2, '01.017', '1', 'TRAVETTA', 1254, NULL, '2016-10-25 22:21:05'),
(3, '01.013', '2A SPALLA (STRETTA)', 'TAGLIACUCI UNIONE', 50, NULL, '2016-10-25 22:20:08'),
(4, '01.014', 'MANICHE', 'TAGLIACUCI UNIONE', 105, NULL, '2016-10-25 22:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60_color`
--

CREATE TABLE `artikal_data_dfgh_60_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60_color`
--

INSERT INTO `artikal_data_dfgh_60_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 0),
(3, 3, '0000-00-00 00:00:00', 0),
(4, 4, '0000-00-00 00:00:00', 0),
(5, 5, '0000-00-00 00:00:00', 1),
(6, 6, '0000-00-00 00:00:00', 0),
(7, 7, '0000-00-00 00:00:00', 0),
(8, 8, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60_size`
--

CREATE TABLE `artikal_data_dfgh_60_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60_size`
--

INSERT INTO `artikal_data_dfgh_60_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 0),
(3, 3, '0000-00-00 00:00:00', 0),
(4, 4, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(2) NOT NULL,
  `color_name` varchar(40) DEFAULT NULL,
  `color_value` varchar(6) DEFAULT NULL,
  `color_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `color_name`, `color_value`, `color_date`) VALUES
(1, 'Sucmurasta', 'AB2567', '2016-10-22 00:00:00'),
(2, 'Bezi', '0C07AB', '2016-10-22 00:00:00'),
(3, 'Neka tamo', '0A15AB', '2016-10-22 00:00:00'),
(4, 'Nedefenisina', '43grdf', '2016-10-22 00:00:00'),
(5, 'Blue', '598AAB', '2016-10-22 00:00:00'),
(6, 'Nedefenisina', '3EAB1D', '2016-10-23 00:00:00'),
(7, 'Nedefenisina', 'BD2972', '2016-10-23 00:00:00'),
(8, 'Nedefenisina', 'ABAB60', '2016-10-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `komesa`
--

CREATE TABLE `komesa` (
  `id` int(11) NOT NULL,
  `komesa_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `komesa_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `komesa_datum_unosa` date NOT NULL,
  `komesa_datum_rada` date NOT NULL,
  `artikal_id` int(11) NOT NULL,
  `artikal_komada_per_komesa` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `komesa`
--

INSERT INTO `komesa` (`id`, `komesa_id`, `komesa_name`, `komesa_datum_unosa`, `komesa_datum_rada`, `artikal_id`, `artikal_komada_per_komesa`) VALUES
(6, '7', ';DSKJFLKEAJ', '2016-11-18', '2016-11-06', 60, 21),
(7, '7', ';DSKJFLKEAJ', '2016-11-18', '2016-11-06', 60, 21),
(8, '7', ';DSKJFLKEAJ', '2016-11-18', '2016-11-06', 60, 21),
(9, 'esdfedasf', 'KMFCKMSCKM', '2016-11-18', '2016-11-18', 60, 25415),
(10, '562656', 'TES 2', '2016-11-18', '2016-11-18', 59, 525),
(11, 'edkflnklasnm', 'KMFCKMSCKM', '2016-11-27', '2016-11-27', 59, 25425);

-- --------------------------------------------------------

--
-- Table structure for table `komesa_line_settings`
--

CREATE TABLE `komesa_line_settings` (
  `id` int(11) NOT NULL,
  `working_day` date NOT NULL,
  `linija_id` int(11) NOT NULL,
  `komesa_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komesa_line_settings`
--

INSERT INTO `komesa_line_settings` (`id`, `working_day`, `linija_id`, `komesa_id`) VALUES
(5, '2016-08-17', 1, 5),
(6, '2016-10-09', 1, 5),
(7, '2016-10-10', 1, 5),
(8, '2016-10-10', 1, 5),
(9, '2016-10-11', 1, 5),
(10, '2016-10-12', 1, 5),
(11, '2016-10-13', 1, 5),
(12, '2016-10-16', 1, 5),
(13, '2016-10-18', 1, 5),
(14, '2016-10-18', 1, 5),
(15, '2016-10-22', 1, 5),
(16, '2016-10-24', 1, 5),
(17, '2016-10-25', 1, 5),
(18, '2016-10-26', 1, 5),
(20, '2016-10-27', 1, 5),
(21, '2016-10-30', 1, 5),
(22, '2016-11-01', 1, 5),
(23, '2016-11-03', 1, 5),
(24, '2016-11-04', 1, 5),
(25, '2016-11-05', 1, 5),
(26, '2016-11-06', 1, 6),
(27, '2016-11-07', 1, 5),
(28, '2016-11-08', 1, 5),
(29, '2016-11-08', 2, 6),
(30, '2016-11-17', 1, 5),
(33, '2016-11-18', 1, 5),
(34, '2016-11-24', 1, 6),
(35, '2016-11-27', 1, 7),
(37, '2016-11-28', 1, 6),
(38, '2016-11-28', 1, 11),
(39, '2016-11-28', 1, 8),
(40, '2016-11-29', 2, 11),
(41, '2016-11-29', 2, 11),
(42, '2016-11-29', 3, 8),
(43, '2016-12-01', 1, 6),
(44, '2016-12-01', 2, 9),
(45, '2016-12-01', 3, 11);

-- --------------------------------------------------------

--
-- Table structure for table `linija`
--

CREATE TABLE `linija` (
  `id` int(3) NOT NULL,
  `linija_id` int(3) NOT NULL,
  `linija_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linija`
--

INSERT INTO `linija` (`id`, `linija_id`, `linija_name`) VALUES
(1, 1, 'Linija 001'),
(2, 2, 'Linija 002'),
(3, 3, 'Linija 003');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_3`
--

CREATE TABLE `masina_data_3` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_3`
--

INSERT INTO `masina_data_3` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.007', '2016-11-28 18:56:00'),
(2, '01.010', '2016-11-28 19:20:35'),
(3, '01.013', '2016-11-28 19:20:39'),
(4, '01.015', '2016-11-28 19:20:43'),
(5, '01.017', '2016-11-28 19:20:48'),
(6, '123456', '2016-11-29 00:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_4`
--

CREATE TABLE `masina_data_4` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_4`
--

INSERT INTO `masina_data_4` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '534513', '2016-11-28 19:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_5`
--

CREATE TABLE `masina_data_5` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_6`
--

CREATE TABLE `masina_data_6` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_6`
--

INSERT INTO `masina_data_6` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.019', '2016-11-28 21:22:37'),
(2, '01.014', '2016-11-28 23:30:06'),
(3, '01.011', '2016-11-29 00:31:26'),
(4, '01.017', '2016-11-29 00:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_7`
--

CREATE TABLE `masina_data_7` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_8`
--

CREATE TABLE `masina_data_8` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_8`
--

INSERT INTO `masina_data_8` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.007', '2016-11-28 21:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_9`
--

CREATE TABLE `masina_data_9` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_10`
--

CREATE TABLE `masina_data_10` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_10`
--

INSERT INTO `masina_data_10` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '123456', '2016-11-29 00:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `masine`
--

CREATE TABLE `masine` (
  `id` int(11) NOT NULL,
  `masina_name` varchar(50) DEFAULT NULL,
  `datum_dodavanja` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masine`
--

INSERT INTO `masine` (`id`, `masina_name`, `datum_dodavanja`) VALUES
(3, 'OV', '2016-11-28 18:52:11'),
(4, 'T/C', '2016-11-28 19:11:11'),
(5, 'Rimaglio', '2016-11-28 19:12:11'),
(6, 'Lineare', '2016-11-28 19:12:11'),
(7, 'Travetta', '2016-11-28 19:12:11'),
(8, 'Manichino', '2016-11-28 19:12:11'),
(9, 'Taglio Fin.', '2016-11-28 19:13:11'),
(10, 'Bordatrice', '2016-11-28 19:13:11');

-- --------------------------------------------------------

--
-- Table structure for table `operacije`
--

CREATE TABLE `operacije` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(10) DEFAULT NULL,
  `operacija_name` varchar(50) NOT NULL,
  `opis_operacije` varchar(50) DEFAULT NULL,
  `tempo_operacije` int(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operacije`
--

INSERT INTO `operacije` (`id`, `operacija_id`, `operacija_name`, `opis_operacije`, `tempo_operacije`, `operacija_datum_dodavanja`) VALUES
(7, '534513', 'SFILACCIATURA', 'COLLO', 100, '2016-10-25 22:16:36'),
(8, '01.007', 'TAGLIO A MANO', 'SEP. TELO DAVANTI + MANICA', 80, '2016-10-25 22:17:46'),
(9, '01.008', 'TAGLIO A MANO', 'SEP. TELO SCHIENA + MANICA', 334, '2016-10-25 22:18:32'),
(10, '01.019', 'TAGLIAC. SURFILATURA', 'SAG. 1 F. B. E M. + GIROM. B. E M. (NO SPALLE) + S', 36, '2016-10-25 22:18:54'),
(11, '01.010', 'TAGLIACUCI UNIONE', '1A SPALLA (STRETTA) +ET.SEGN.TG.', 88, '2016-10-25 22:19:11'),
(12, '01.011', 'RIMAGLIO', 'COLLO SING. C 205 1X1---> C 221 1X1', 31, '2016-10-25 22:19:31'),
(13, '01.012', 'RIMAGLIO', 'F. COLLO', 150, '2016-10-25 22:19:46'),
(14, '01.013', 'TAGLIACUCI UNIONE', '2A SPALLA (STRETTA)', 50, '2016-10-25 22:20:08'),
(15, '01.014', 'TAGLIACUCI UNIONE', 'MANICHE', 70, '2016-10-25 22:20:26'),
(16, '01.015', 'TAGLIACUCI UNIONE', 'FIANCHI R.C.', 47, '2016-10-25 22:20:49'),
(17, '01.017', 'TRAVETTA', '1', 258, '2016-10-25 22:21:05'),
(18, '01.018', 'RIFINITURA', '!', 73, '2016-10-25 22:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `radnici_lista`
--

CREATE TABLE `radnici_lista` (
  `id` int(11) NOT NULL,
  `radnik_id` int(11) NOT NULL,
  `radnik_name` varchar(50) NOT NULL,
  `radnik_lifetime_from` datetime NOT NULL,
  `linija` int(3) NOT NULL,
  `sifra` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `radnici_lista`
--

INSERT INTO `radnici_lista` (`id`, `radnik_id`, `radnik_name`, `radnik_lifetime_from`, `linija`, `sifra`) VALUES
(1, 101, 'Mirjana Stamenkovic', '2014-11-03 00:00:00', 3, 0),
(2, 222222, 'Mira Cekic', '2015-05-13 00:00:00', 1, 0),
(3, 3, 'Mirka Vasiljevic', '2015-05-13 00:00:00', 3, 0),
(4, 4, 'Usnija Redzepova', '2015-05-13 00:00:00', 1, 0),
(5, 5, 'Dusanka Maric', '2014-09-09 00:00:00', 3, 0),
(6, 6, 'Duska Jaksic', '2015-05-13 00:00:00', 3, 0),
(7, 111, 'ILIJESKI MARTINA', '2016-11-24 00:00:00', 2, 111),
(8, 10, 'Ilijeski Martina', '2014-01-15 00:00:00', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(2) NOT NULL,
  `size_name` varchar(40) DEFAULT NULL,
  `size_value` varchar(6) DEFAULT NULL,
  `size_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size_name`, `size_value`, `size_date`) VALUES
(1, 'xs', 'xs', '2016-11-03 00:00:00'),
(2, 's', 's', '2016-11-03 00:00:00'),
(3, 'm', 'm', '2016-11-03 00:00:00'),
(4, 'l', 'l', '2016-11-03 00:00:00'),
(5, 'xl', 'xl', '2016-11-03 00:00:00'),
(6, '2xl', '2xl', '2016-11-03 00:00:00'),
(7, '3xl', '3xl', '2016-11-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tablet_ip_settings`
--

CREATE TABLE `tablet_ip_settings` (
  `id` int(3) NOT NULL,
  `tablet_id` varchar(50) NOT NULL,
  `masina` varchar(50) DEFAULT NULL,
  `tablet_ip` varchar(50) NOT NULL,
  `converted_tablet_ip` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tablet_ip_settings`
--

INSERT INTO `tablet_ip_settings` (`id`, `tablet_id`, `masina`, `tablet_ip`, `converted_tablet_ip`) VALUES
(28, '127.0.0.1', '6', '127.0.0.1', '2130706433'),
(29, '26', '5', '192.168.1.26', '3232235802'),
(30, '29', '9', '192.168.1.29', '3232235805');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `json_data` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `json_data`) VALUES
(1, '{\r\n"id":"0001",\r\n"value":"1"\r\n}'),
(2, '{\r\n"id":"0002",\r\n"value":"2"\r\n}');

-- --------------------------------------------------------

--
-- Table structure for table `tuscan_family_options`
--

CREATE TABLE `tuscan_family_options` (
  `option_id` int(5) NOT NULL,
  `option_name` varchar(50) NOT NULL,
  `option_value` longtext NOT NULL,
  `option_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuscan_family_options`
--

INSERT INTO `tuscan_family_options` (`option_id`, `option_name`, `option_value`, `option_status`) VALUES
(1, 'siteurl', 'http://localhost:8888/projekti/svn-projekti/tuscan/', '1'),
(2, 'btn_disable_time', '75', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials_tbl`
--

CREATE TABLE `tutorials_tbl` (
  `tutorial_id` int(11) NOT NULL,
  `tutorial_title` varchar(100) NOT NULL,
  `tutorial_author` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `working_day_session`
--

CREATE TABLE `working_day_session` (
  `id` int(11) NOT NULL,
  `working_day_session_id` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `work_day` date DEFAULT NULL,
  `radnik_id` int(11) DEFAULT NULL,
  `linija` int(3) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `pausa_1_start_time` datetime DEFAULT NULL,
  `pausa_1_end_time` datetime DEFAULT NULL,
  `pausa_2_start_time` datetime DEFAULT NULL,
  `pausa_2_end_time` datetime DEFAULT NULL,
  `komesa` int(11) DEFAULT NULL,
  `artikal_id` int(11) DEFAULT NULL,
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operacija_id` int(11) DEFAULT NULL,
  `operacija_hint` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `working_day_session`
--

INSERT INTO `working_day_session` (`id`, `working_day_session_id`, `work_day`, `radnik_id`, `linija`, `login_time`, `logout_time`, `pausa_1_start_time`, `pausa_1_end_time`, `pausa_2_start_time`, `pausa_2_end_time`, `komesa`, `artikal_id`, `color`, `size`, `operacija_id`, `operacija_hint`) VALUES
(1, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 101, 2, '2016-11-08 04:08:53', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 3),
(2, '2-101-8-59-6-Sucmurasta-m', '2016-11-08', 101, 2, '2016-11-08 04:18:51', '2016-11-29 20:52:55', '0000-00-00 00:00:00', '2016-11-08 00:00:00', NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 1),
(4, NULL, '2016-11-08', 101, NULL, '2016-11-08 04:34:48', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(6, '6-4-8-59-6-Sucmurasta-s', '2016-11-08', 4, 2, '2016-11-08 04:43:38', '2016-11-29 20:52:56', '2016-11-08 04:46:38', NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 0),
(7, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 4, 2, '2016-11-08 04:48:51', '2016-11-29 20:52:56', '2016-11-08 04:49:09', '2016-11-08 04:54:32', NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 7),
(8, NULL, '2016-11-08', 3, NULL, '2016-11-08 04:59:32', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(9, '9-5-11-59-6-Sucmurasta-s', '2016-11-08', 5, 2, '2016-11-08 05:02:10', '2016-11-18 01:15:51', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 11, 1),
(10, '10-101-8-59-5-Sucmurasta-s', '2016-11-08', 101, 1, '2016-11-08 15:32:26', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 1),
(11, '11-5-11-59-6-Sucmurasta-s', '2016-11-08', 5, 2, '2016-11-08 15:41:35', '2016-11-18 01:15:51', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 11, 0),
(12, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 5, 2, '2016-11-08 15:43:31', '2016-11-18 01:15:51', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 5),
(13, NULL, '2016-11-08', 5, NULL, '2016-11-08 16:21:44', '2016-11-18 01:15:51', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(14, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 16:43:57', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 11, 3),
(15, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 2, 1, '2016-11-17 16:49:13', '2016-11-18 23:03:36', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 11, 21),
(16, '16-101-8-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 23:55:34', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(17, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 23:56:18', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 10),
(18, '18-101-8-59-5-Sucmurasta-s', '2016-11-18', 101, 1, '2016-11-18 00:58:57', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(19, '19-101-8-59-5-Sucmurasta-s', '2016-11-18', 101, 1, '2016-11-18 01:15:55', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(20, '20-6-8-59-5-Sucmurasta-s', '2016-11-18', 6, 1, '2016-11-18 01:16:15', '2016-11-18 01:16:25', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(21, NULL, '2016-11-18', 2, NULL, '2016-11-18 01:36:17', '2016-11-18 23:03:36', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(22, NULL, '2016-11-18', 101, NULL, '2016-11-18 16:15:10', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(23, NULL, '2016-11-18', 101, NULL, '2016-11-18 16:17:58', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(24, '24-101-11-60-6-Sucmurasta-xs', '2016-11-24', 101, 1, '2016-11-24 16:57:15', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 6, 60, 'NULL', 'NULL', 11, 0),
(25, NULL, '2016-11-24', 101, NULL, '2016-11-24 17:14:09', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(26, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 101, 1, '2016-11-27 14:48:17', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 7, 60, 'NULL', 'NULL', 1, 5),
(27, '27-101-01.011-59-11-Nedefenisina-xs', '2016-11-28', 101, 1, '2016-11-28 15:45:58', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 0),
(28, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 3, 1, '2016-11-28 20:46:12', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 12),
(29, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 3, 3, '2016-11-29 00:19:32', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 17),
(30, '30-3-01.014-60-8-Blue-l', '2016-11-29', 3, 3, '2016-11-29 00:32:06', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 19),
(31, NULL, '2016-11-29', 3, NULL, '2016-11-29 00:55:52', '2016-11-29 20:52:55', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(32, NULL, '2016-11-29', 4, NULL, '2016-11-29 20:52:48', '2016-11-29 20:53:02', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(33, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 4, 2, '2016-11-29 20:55:18', '2016-11-29 21:06:30', NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 4),
(34, NULL, '2016-12-01', 101, NULL, '2016-12-01 15:34:56', '2016-12-01 15:35:22', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(35, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 101, 1, '2016-12-01 15:35:50', '2016-12-01 15:42:13', NULL, NULL, NULL, NULL, 6, 60, 'NULL', 'NULL', 1, 2),
(36, '36-101-01.011-59-11-Sucmurasta-xs', '2016-12-01', 101, 3, '2016-12-01 15:42:19', NULL, NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `working_day_session_details`
--

CREATE TABLE `working_day_session_details` (
  `id` int(11) NOT NULL,
  `working_day_session_id` varchar(50) NOT NULL,
  `working_day` date DEFAULT NULL,
  `komesa_id` int(11) DEFAULT NULL,
  `radnik_id` int(11) DEFAULT NULL,
  `linija_id` int(11) DEFAULT NULL,
  `artikal_id` int(11) DEFAULT NULL,
  `operacija_id` int(11) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `size` varchar(4) DEFAULT NULL,
  `vreme_operacije` datetime DEFAULT NULL,
  `razlika_u_vremenu` time DEFAULT NULL,
  `procenat_ucinka` int(3) DEFAULT NULL,
  `hint_code` varchar(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `working_day_session_details`
--

INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(1, '1-101-8-59-6-Sucmurasta-s', NULL, 6, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(3, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 04:16:54', '00:04:54', 17, 'F001'),
(4, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 04:18:36', '00:01:42', 49, 'F001'),
(5, '2-101-8-59-6-Sucmurasta-m', NULL, 6, 101, NULL, 59, 8, 'Sucmurasta', 'm', NULL, NULL, NULL, NULL),
(6, '2-101-8-59-6-Sucmurasta-m', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 04:20:07', '00:00:00', 116, 'F001'),
(8, '3-5-8-59-6-Nedefenisina-s', NULL, 6, 5, NULL, 59, 8, 'Nedefenisi', 's', NULL, NULL, NULL, NULL),
(9, '3-5-8-59-6-Nedefenisina-s', '2016-11-08', 6, 5, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:29:05', '00:00:00', 39, 'F001'),
(10, '3-5-8-59-6-Nedefenisina-s', '2016-11-08', NULL, 5, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:29:30', NULL, NULL, 'P001'),
(11, '5-4-11-59-6-Nedefenisina-xl', NULL, 6, 4, NULL, 59, 11, 'Nedefenisi', 'xl', NULL, NULL, NULL, NULL),
(12, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:36:26', '00:00:00', 131, 'F001'),
(13, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:37:48', '00:01:22', 123, 'F001'),
(15, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:41:33', '00:01:35', 106, 'F001'),
(16, '6-4-8-59-6-Sucmurasta-s', NULL, 6, 4, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(19, '7-4-8-59-6-Nedefenisina-s', NULL, 6, 4, NULL, 59, 8, 'Nedefenisi', 's', NULL, NULL, NULL, NULL),
(20, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', NULL, 4, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:49:09', NULL, NULL, 'P001'),
(21, '7-4-8-59-6-Nedefenisina-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:53:55', NULL, NULL, ''),
(22, '7-4-8-59-6-Nedefenisina-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:54:32', NULL, NULL, ''),
(23, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:55:27', '00:00:55', 90, 'F001'),
(24, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:56:22', '00:00:55', 90, 'F001'),
(25, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:57:01', '00:00:39', 128, 'F001'),
(26, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:59:23', '00:02:22', 35, 'F001'),
(27, '9-5-11-59-6-Sucmurasta-s', NULL, 6, 5, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(28, '9-5-11-59-6-Sucmurasta-s', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 05:05:06', '00:00:00', 109, 'F001'),
(29, '10-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(30, '10-101-8-59-5-Sucmurasta-s', '2016-11-08', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-08 15:41:16', '00:00:00', 9, 'F001'),
(31, '11-5-11-59-6-Sucmurasta-s', NULL, 6, 5, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(32, '12-5-8-59-6-Sucmurasta-m', NULL, 6, 5, NULL, 59, 8, 'Sucmurasta', 'm', NULL, NULL, NULL, NULL),
(33, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:46:42', '00:00:00', 33, 'F001'),
(34, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:48:07', '00:01:25', 58, 'F001'),
(35, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:50:05', '00:01:58', 42, 'F001'),
(36, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:51:17', '00:01:12', 111, 'F001'),
(37, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:51:58', '00:00:41', 195, 'F001'),
(38, '14-101-11-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(39, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:45:39', '00:00:00', 104, 'F001'),
(40, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:47:57', '00:02:18', 29, 'F001'),
(41, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:48:35', '00:00:38', 107, 'F001'),
(42, '15-2-11-59-5-Sucmurasta-s', NULL, 5, 2, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(43, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:50:11', '00:00:00', 95, 'F001'),
(44, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:50:44', '00:00:33', 123, 'F001'),
(45, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:51:23', '00:00:39', 104, 'F001'),
(46, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:52:11', '00:00:48', 85, 'F001'),
(47, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:52:43', '00:00:32', 127, 'F001'),
(48, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:55:30', '00:02:47', 24, 'F001'),
(49, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:56:01', '00:00:31', 131, 'F001'),
(50, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:56:32', '00:00:31', 131, 'F001'),
(51, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:57:06', '00:00:34', 120, 'F001'),
(52, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:57:42', '00:00:36', 113, 'F001'),
(53, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:58:28', '00:00:46', 88, 'F001'),
(54, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:59:41', '00:01:13', 56, 'F001'),
(55, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:04:24', '00:04:43', 14, 'F001'),
(56, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:05:48', '00:01:24', 48, 'F001'),
(57, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:06:42', '00:00:54', 75, 'F001'),
(58, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:17:25', '00:10:43', 6, 'F001'),
(59, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:20:38', '00:03:13', 21, 'F001'),
(60, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:23:59', '00:03:21', 20, 'F001'),
(61, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:30:13', '00:06:14', 10, 'F001'),
(62, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:31:11', '00:00:58', 70, 'F001'),
(63, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:32:29', '00:01:18', 52, 'F001'),
(64, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:33:37', '00:01:08', 60, 'F001'),
(65, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:34:12', '00:00:35', 116, 'F001'),
(66, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:34:56', '00:00:44', 92, 'F001'),
(67, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:35:36', '00:00:40', 102, 'F001'),
(68, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:36:17', '00:00:41', 99, 'F001'),
(69, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:36:50', '00:00:33', 123, 'F001'),
(70, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:37:31', '00:00:41', 99, 'F001'),
(71, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:38:27', '00:00:56', 73, 'F001'),
(72, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:39:04', '00:00:37', 110, 'F001'),
(73, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:39:47', '00:00:43', 95, 'F001'),
(74, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:41:18', '00:01:31', 44, 'F001'),
(75, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:42:06', '00:00:48', 85, 'F001'),
(76, '16-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(77, '17-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(78, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 23:57:03', '00:00:00', 125, 'F001'),
(79, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 23:57:38', '00:00:35', 128, 'F001'),
(80, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:33:48', '00:36:10', 2, 'F001'),
(81, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:44:24', '00:10:36', 7, 'F001'),
(82, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:45:22', '00:00:58', 77, 'F001'),
(83, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:47:37', '00:02:15', 33, 'F001'),
(84, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:49:27', '00:01:50', 40, 'F001'),
(85, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:50:13', '00:00:46', 97, 'F001'),
(86, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:50:49', '00:00:36', 125, 'F001'),
(87, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', NULL, 101, NULL, NULL, NULL, NULL, NULL, '2016-11-18 00:51:06', NULL, NULL, 'P001'),
(88, '17-101-8-59-5-Sucmurasta-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-18 00:51:11', NULL, NULL, 'P002'),
(89, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:52:34', '00:01:23', 54, 'F001'),
(90, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:53:20', '00:00:46', 97, 'F001'),
(91, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:54:05', '00:00:45', 100, 'F001'),
(92, '18-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(93, '19-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(94, '20-6-8-59-5-Sucmurasta-s', NULL, 5, 6, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(95, '24-101-11-60-6-Sucmurasta-xs', NULL, 6, 101, NULL, 60, 11, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(96, '26-101-01.017-60-7-Blue-xs', NULL, 7, 101, NULL, 60, 1, 'Blue', 'xs', NULL, NULL, NULL, NULL),
(97, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:09:52', '00:00:00', 0, 'F001'),
(98, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:10:42', '00:00:50', 0, 'F001'),
(99, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:11:00', '00:00:18', 0, 'F001'),
(100, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:11:51', '00:00:51', 0, 'F001'),
(101, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:12:38', '00:00:47', 0, 'F001'),
(102, '27-101-01.011-59-11-Nedefenisina-xs', NULL, 11, 101, NULL, 59, 1, 'Nedefenisi', 'xs', NULL, NULL, NULL, NULL),
(103, '28-3-01.014-60-8-Sucmurasta-xs', NULL, 8, 3, NULL, 60, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(104, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-28 23:57:39', '00:00:00', 0, 'F001'),
(105, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-28 23:58:26', '00:00:47', 0, 'F001'),
(106, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:01:19', '00:02:53', 0, 'F001'),
(107, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:03:57', '00:02:38', 0, 'F001'),
(108, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:04:36', '00:00:39', 0, 'F001'),
(109, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:06:31', '00:01:55', 0, 'F001'),
(110, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:07:37', '00:01:06', 0, 'F001'),
(111, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:08:03', '00:00:26', 0, 'F001'),
(112, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:09:27', '00:01:24', 0, 'F001'),
(113, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:12:18', '00:02:51', 20, 'F001'),
(114, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:12:46', '00:00:28', 122, 'F001'),
(115, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:13:11', '00:00:25', 137, 'F001'),
(116, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:14:53', '00:01:42', 33, 'F001'),
(117, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:19:22', '00:04:29', 12, 'F001'),
(118, '29-3-01.014-60-8-Blue-xs', NULL, 8, 3, NULL, 60, 1, 'Blue', 'xs', NULL, NULL, NULL, NULL),
(119, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:26:52', '00:00:00', 126, 'F001'),
(120, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:29:35', '00:02:43', 21, 'F001'),
(121, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:30:10', '00:00:35', 97, 'F001'),
(122, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:31:33', '00:01:23', 41, 'F001'),
(123, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:31:59', '00:00:26', 131, 'F001'),
(124, '30-3-01.014-60-8-Blue-l', NULL, 8, 3, NULL, 60, 1, 'Blue', 'l', NULL, NULL, NULL, NULL),
(125, '30-3-01.014-60-8-Blue-l', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'l', '2016-11-29 00:50:09', '00:00:00', 20, 'F001'),
(126, '30-3-01.014-60-8-Blue-l', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'l', '2016-11-29 00:55:26', '00:05:17', 10, 'F001'),
(127, '33-4-01.011-59-11-Sucmurasta-xs', NULL, 11, 4, NULL, 59, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(128, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 20:57:58', '00:00:00', 136, 'F001'),
(129, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 20:59:41', '00:01:43', 112, 'F001'),
(130, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 21:04:17', '00:04:36', 42, 'F001'),
(131, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 21:06:27', '00:02:10', 89, 'F001'),
(132, '35-101-01.011-60-6-Sucmurasta-xs', NULL, 6, 101, NULL, 60, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(133, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 6, 101, 1, 60, 6, 'Sucmurasta', 'xs', '2016-12-01 15:39:04', '00:00:00', 138, 'F001'),
(134, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 6, 101, 1, 60, 6, 'Sucmurasta', 'xs', '2016-12-01 15:42:12', '00:03:08', 61, 'F001'),
(135, '36-101-01.011-59-11-Sucmurasta-xs', NULL, 11, 101, NULL, 59, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(136, '36-101-01.011-59-11-Sucmurasta-xs', '2016-12-01', 11, 101, 3, 59, 11, 'Sucmurasta', 'xs', '2016-12-01 16:15:04', '00:00:00', 117, 'F001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikal`
--
ALTER TABLE `artikal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61`
--
ALTER TABLE `artikal_data_112233erfr_61`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61_color`
--
ALTER TABLE `artikal_data_112233erfr_61_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61_size`
--
ALTER TABLE `artikal_data_112233erfr_61_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59`
--
ALTER TABLE `artikal_data_1122888_59`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59_color`
--
ALTER TABLE `artikal_data_1122888_59_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59_size`
--
ALTER TABLE `artikal_data_1122888_59_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60`
--
ALTER TABLE `artikal_data_dfgh_60`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60_color`
--
ALTER TABLE `artikal_data_dfgh_60_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60_size`
--
ALTER TABLE `artikal_data_dfgh_60_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komesa`
--
ALTER TABLE `komesa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komesa_line_settings`
--
ALTER TABLE `komesa_line_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linija`
--
ALTER TABLE `linija`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_3`
--
ALTER TABLE `masina_data_3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_4`
--
ALTER TABLE `masina_data_4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_5`
--
ALTER TABLE `masina_data_5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_6`
--
ALTER TABLE `masina_data_6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_7`
--
ALTER TABLE `masina_data_7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_8`
--
ALTER TABLE `masina_data_8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_9`
--
ALTER TABLE `masina_data_9`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_10`
--
ALTER TABLE `masina_data_10`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masine`
--
ALTER TABLE `masine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operacije`
--
ALTER TABLE `operacije`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radnici_lista`
--
ALTER TABLE `radnici_lista`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_ip_settings`
--
ALTER TABLE `tablet_ip_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuscan_family_options`
--
ALTER TABLE `tuscan_family_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `tutorials_tbl`
--
ALTER TABLE `tutorials_tbl`
  ADD PRIMARY KEY (`tutorial_id`);

--
-- Indexes for table `working_day_session`
--
ALTER TABLE `working_day_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `working_day_session_details`
--
ALTER TABLE `working_day_session_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikal`
--
ALTER TABLE `artikal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61`
--
ALTER TABLE `artikal_data_112233erfr_61`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61_color`
--
ALTER TABLE `artikal_data_112233erfr_61_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61_size`
--
ALTER TABLE `artikal_data_112233erfr_61_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59`
--
ALTER TABLE `artikal_data_1122888_59`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59_color`
--
ALTER TABLE `artikal_data_1122888_59_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59_size`
--
ALTER TABLE `artikal_data_1122888_59_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60`
--
ALTER TABLE `artikal_data_dfgh_60`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60_color`
--
ALTER TABLE `artikal_data_dfgh_60_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60_size`
--
ALTER TABLE `artikal_data_dfgh_60_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `komesa`
--
ALTER TABLE `komesa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `komesa_line_settings`
--
ALTER TABLE `komesa_line_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `linija`
--
ALTER TABLE `linija`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `masina_data_3`
--
ALTER TABLE `masina_data_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `masina_data_4`
--
ALTER TABLE `masina_data_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masina_data_5`
--
ALTER TABLE `masina_data_5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `masina_data_6`
--
ALTER TABLE `masina_data_6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `masina_data_7`
--
ALTER TABLE `masina_data_7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `masina_data_8`
--
ALTER TABLE `masina_data_8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masina_data_9`
--
ALTER TABLE `masina_data_9`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `masina_data_10`
--
ALTER TABLE `masina_data_10`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masine`
--
ALTER TABLE `masine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `operacije`
--
ALTER TABLE `operacije`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `radnici_lista`
--
ALTER TABLE `radnici_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tablet_ip_settings`
--
ALTER TABLE `tablet_ip_settings`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tuscan_family_options`
--
ALTER TABLE `tuscan_family_options`
  MODIFY `option_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tutorials_tbl`
--
ALTER TABLE `tutorials_tbl`
  MODIFY `tutorial_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `working_day_session`
--
ALTER TABLE `working_day_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `working_day_session_details`
--
ALTER TABLE `working_day_session_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
