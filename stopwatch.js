var s = 0,
  ms = 0,
  sEl = document.getElementById('s'),
  msEl = document.getElementById('ms'),
  play = false;
stopwatch = setInterval(function() {
  if (!play) return;
  if (ms === 99) {
    s += 1;
    ms = 0;
  } else {
    ms += 1;
  }
  update();

}, 1);

function update() {

  sEl.innerText = s;
  msEl.innerText = ms;
}

function toggle() {
  if (!play) {
    s = 0, ms = 0;
    update();
  }
  play = !play;
}
