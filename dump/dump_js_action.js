$('#login-1-btn').click(function(e) {
  $('#loader-wrapper').removeClass('hidden').removeClass('loaded');

  var line = $( "#linija option:selected" ).val();

  // if field are empty
  if ($( "#linija option:selected" ).val() == 'default' || $('#radnik option:selected').val() == 'default' || $('#input_sifra').val() == '' ){
    return false;
  }

    var input_linija = $('#linija option:selected').val();
    var name_linija = $( "#linija option:selected" ).attr('name');
    var input_name = $( "#radnik option:selected" ).val();
    var name_radnik = $( "#radnik option:selected" ).attr('name');
    var input_sifra = $('#input_sifra').val();

  // alert( input_name+' '+input_sifra );

  if ( input_sifra=='' || input_name=='' ) {
    $('#error_msg').text('You have errors in your login data');
  } else {
    var new_text = input_name+' '+input_sifra;
    $('#data').text(new_text);

    $.ajax({
      url: "http://localhost:8888/projekti/svn-projekti/tuskan-family/data.php?action=login-data",
      method: "POST",
      data: {
        linija: input_linija,
        name_linija: name_linija,
        radnik: input_name,
        name_radnik: name_radnik,
        sifra: input_sifra
      },
      dataType: "json",
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      success: function(data, status, xhr) {
          //  alert(data[0]);
          $('#loader-wrapper').addClass('hidden').addClass('loaded');
      },
      error: function(xhr, status, error) {
          //  alert(status);
      }
    })
      .done(function( data ) {
        if ( data['status'] == 'error' ) {
          $('#error_msg').addClass('alert alert-danger').html(data['error-msg']);
        } else {
          window.location = 'http://localhost:8888/projekti/svn-projekti/tuskan-family/index.php';
        }
        // if ( console && console.log ) {
        //   console.log( "Sample of data:", data.slice( 0, 100 ) );
        // }
        // $('#data').text(data[0]);
        //
        // if ( data == 'Nenad Cvetkovic' ) {
        //   $('#login-data').hide();
        // } else {
        //   $('#error_msg').text('Bad login data');
        // }
        // alert(data);
        // $('#login-page').fadeOut("slow");
        // var logout_link = '<a href="http://localhost:8888/projekti/svn-projekti/tuskan-family/libs/logout.php" class="btn btn-lg btn-primary">Logout</a>';
        // setTimeout(function() {
        //   $('#working-page').html('<h1>Working Page</h1>'+logout_link);
        // }, 1000);
      });


  }


  e.preventDefault();
});
