<?php
// start session
session_start();

// requires files
require_once 'system/libs/Database.php';
require_once 'system/libs/Url.php';
require_once 'system/libs/Helper.php';
require_once 'system/libs/Ip.php';

require_once 'system/config/config.php';

if (!Database::connect()) {
  echo json_encode(array('1','nemad'));
}

$table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

if ( isset($_GET) ) {
  foreach ($_GET as $key => $value) {
    // echo $key . ' = ' . $value . '<br/>';
  }
  $wherewhat = array( 'working_day_session_id'=>$_SESSION['working-session']['working_day_session_id']);
  $time = Database::whereQuery($table_name, $wherewhat, 'ORDER BY id DESC', '1');
  // echo Helper::DateTimeDiff( $time[0]['vreme_operacije'], date('H:i:s'));
  echo json_encode(
    array(
      'full_time' => $time[0]['vreme_operacije'],
      'diff_time' => strtotime(Helper::DateTimeDiff( $time[0]['vreme_operacije'], date('H:i:s'))),
      'time' => strtotime($time[0]['vreme_operacije']),
      'time_seconds' => '',
      'vreme'=> strtotime($time[0]['vreme_operacije']),
      'x' => ''
    )
  );
  // echo json_encode($time);
} else {
  echo '<h1>Nema nista</h1>';
}


// var t;
// var new_time = 0;
// $.get("http://localhost:8888/projekti/svn-projekti/tuskan-family/time_data.php?time=1", function(data, status){
//   t = JSON.parse(data);
//   var x = t.time.split(':');
//   var sum = parseInt(x[0])*60*60 + parseInt(x[1])*60 + parseInt(x[2]);
//   new_time = t.vreme;
// });
