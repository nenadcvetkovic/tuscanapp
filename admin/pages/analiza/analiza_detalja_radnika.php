<?php

$rid = $_GET['radnik'];

$full_date = date('Y-m-d');

$new_date = date('m').'_'.date('Y');

$date = date('Y-m-d');
if ( isset($_GET['date']) && $_GET['date']!='' && !empty($_GET['date']) ) {
  $full_date = $_GET['date'];
  $old_date = explode('-', $full_date);
  $new_date = ltrim($old_date[1], '0').'_'.$old_date[0];
}

$new_table_name = 'working_day_session_details_'.$new_date;


if ( Database::num_rows(Database::returnWhereQuery('radnici_lista',array('radnik_id'=>$rid))) > 0 ) {
  $radnik_data = Database::whereQuery('radnici_lista',array('radnik_id'=>$rid));
  if (Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'working_day'=>date('Y-m-d'), 'hint_code'=>'F001'))) > 0) {
    $danasnji_komadi = Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'working_day'=>date('Y-m-d'), 'hint_code'=>'F001')));
  } else {
    $danasnji_komadi = 0;
  }
  if (Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001'))) > 0) {
    $ukupni_komadi = Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001')));
  } else {
    $ukupni_komadi = 0;
  }
  $efikasnost_data = Database::whereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001'));
  $efikasnost = 0;
  foreach ( $efikasnost_data as $data_item ) {
    $efikasnost += $data_item['procenat_ucinka'];
  }


// Helper::pre(Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'working_day'=>$full_date, 'hint_code'=>'F001')));

  // ================================ //
  //          DISPLAY DETALJE
  // ================================ //
  $o .= '<div class="row">';

  $o .= '<div class="alert alert-info col-xs-5">';
  $o .= '<div class="col-xs-6"> Ime i prezime radnika </div>';
  $o .= '<div class="col-xs-6"> ' . $radnik_data[0]['radnik_name'] . '</div>';
  $o .= '<div class="col-xs-6"> Linija </div>';
  $o .= '<div class="col-xs-6"> ' . $radnik_data[0]['linija'] . '</div>';
  $o .= '<div class="col-xs-6"> Ukupno komada </div> <div class="col-xs-6"> ' . $ukupni_komadi . '</div>';
  // $o .= '<div class="col-xs-6"> Efikasnos radnika </div> <div class="col-xs-6"> ' . floor($efikasnost/$ukupni_komadi) . ' % </div>';
  $o .= '<div class="col-xs-6"> Broj danasnjih komada </div> <div class="col-xs-6"> ' . $danasnji_komadi . '</div>';
  $o .= '<div class="clear"></div>';

  $o .= '</div><!-- END ROW -->';



  // $mesecna_efikasnost = 0;
  // $dnevna_efikasnost = 0;

  // $efect_sql = "SELECT * FROM `working_day_session_details` WHERE radnik_id='".$radnik_data[0]['radnik_id']."' AND YEAR(`working_day`) = '".date('Y')."' AND MONTH(`working_day`) = '".date('m')."' AND `hint_code`='F001'";
  // // Helper::pre($efect_sql);
  // if ( Database::num_rows( $efect_sql ) > 0 ) {
  //   $mesecna_data = Database::query($efect_sql);
  //   $index = 0;
  //   $mprocenat = 0;

  //   foreach ( $mesecna_data as $mdata ) {
  //     $mprocenat += $mdata['procenat_ucinka'];
  //     $index++;
  //   }
  //   $mesecna_efikasnost = floor($mprocenat/$index);
    
  // }

    // OVAJ MESEC
  $working_days = work_day_for_month(date('Y'), date('n'), date('j'));

  $data_array = explode('-', $full_date);

  $neradni_dani = Database::whereQuery('neradni_dani', array('month'=>date('m'), 'year'=>date('Y')));
  $day_in_month = get_days_for_this_month ($data_array[1], $data_array[0]);

  // Helper::pre(get_days_for_this_month ($data_array[1], $data_array[0]));

  if ( isset($_GET['date']) && $_GET['date']!='' && !empty($_GET['date']) ) {
    $data_array = explode('-', $_GET['date']);
  } else {
    $data_array = explode('-', $full_date);
  }

  $radnik_day_data = Database::whereQuery('work_stat', array('radnik_id'=>$rid, 'year'=>$data_array[0], 'month'=>$data_array[1]));

  // Helper::pre($radnik_day_data);

  $mesecna_efikasnost = 0;
  $mesecna_suma = 0;
  $index = 0;

  for ($i=1; $i <= $day_in_month ; $i++) { 
    if ( $i <= date('d') && $data_array[2]==date('n`') ) {
      if ( $neradni_dani[0]['day'.$i] == '1' ) {
        $mesecna_suma += $radnik_day_data[0]['day'.$i];
        $index++;
      }
    } else {
      $mesecna_suma += $radnik_day_data[0]['day'.$i];
      $index++;
    }
  }

  $mesecna_efikasnost = floor($mesecna_suma / $index);

  // foreach ( $working_days as $day ) {
  //   $efect_sql = "SELECT * FROM `$new_table_name` WHERE radnik_id='".$radnik_data[0]['radnik_id']."' AND YEAR(`working_day`) = '".date('Y')."' AND MONTH(`working_day`) = '".$day."' AND `hint_code`='F001'";

  //     if ( Database::num_rows( $efect_sql ) > 0 ) {
  //       $mesecna_data = Database::query($efect_sql);
  //       $mprocenat = 0;

  //       foreach ( $mesecna_data as $mdata ) {
  //         $mprocenat += $mdata['procenat_ucinka'];
  //       }
        
  //     }
  // }
  // 

  
  
  // if ( Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001')) ) > 0 ) {
  //   $mesecna_data = Database::whereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001'));
  //   $mprocenat = 0;
  //   $index = 1;
  //   foreach ( $mesecna_data as $mdata ) {
  //     $mprocenat += $mdata['procenat_ucinka'];
  //     $index++;
  //   }
  //   $mesecna_efikasnost = floor($mprocenat/$index);
  // } else {
  //   $mesecna_efikasnost = 0;
  // }

  // Helper::pre(Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'], 'hint_code'=>'F001')));
  // Helper::pre($mprocenat);
  // Helper::pre($count_working_days);
  // Helper::pre(floor($mprocenat/$count_working_days));


  if ( Database::num_rows( Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'],'working_day'=>$full_date,'hint_code'=>'F001'))) > 0 ) {

    $index = 1;
    $efikasnost = 0;
    $table_name = 'working_day_session_details_'.$new_date;

    $sva_dnevna_efikasnost = Database::whereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'],'working_day'=>$full_date,'hint_code'=>'F001'));

    foreach ($sva_dnevna_efikasnost as $item) {
      $efikasnost += $item['procenat_ucinka'];
      $index++;
    }
    
    $counted_num = Database::num_rows(Database::returnWhereQuery($new_table_name, array('radnik_id'=>$radnik_data[0]['radnik_id'],'working_day'=>$full_date,'hint_code'=>'F001')));

    $dnevna_efikasnost = ceil($efikasnost / $counted_num);

  } else {
    $counted_num = 0;
    $dnevni_ucinak = 0;
  }

  $o .= '<div class="row">';

  $o .= '<div class="col-xs-2 btn-info mesecna efikasnost text-center wrap-left">';
    $o .= '<h5>Mesecna Efikasnost <br>'.$data_array[1].'.'.$data_array[0].'</h5>';
    $o .= '<h2> '.$mesecna_efikasnost.' % </h2>';
  $o .= '</div>';

  $o .= '<div class="col-xs-2 btn-danger dnevna efikasnost text-center wrap-left">';
    $o .= '<h5>Dnevna Efikasnost <br>'.display_date($full_date,'.').'</h5>';
    $o .= '<h2> '.$dnevna_efikasnost.' % </h2>';
  $o .= '</div>';

  $o .= '<div class="col-xs-2 btn-default dnevna efikasnost text-center wrap-left" data-toggle="modal" data-target="#myModal">';
    $o .= '<i style="font-size:8em" class="fa fa-info-circle fa-5" aria-hidden="true"></i>';
  $o .= '</div>';
?>
<!-- <html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Meseci', 'Efikasnost'],
          ['Januar', 50],
          ['Februar', 70],
          ['Mart', 66],
          ['April', 22],
          ['Maj', 55],
          ['Jun', 12],
          ['Jul', 88],
          ['Avgust', 90],
          ['Septembar', 24],
          ['Oktobar', 56],
          ['Novembar', 66],
          ['Decembar', 99]
        ]);

        var options = {
          chart: {
            title: 'Uspesnost radnika po mesecima',
            subtitle: 'Uspesnost radnika po mesecima za 2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="columnchart_material" style="width: 100%; height: 500px;"></div>
  </body>
</html>
 -->

<?php

  $o .= '</div>';


  $o .= '</div><!-- END ROW -->';

  $o .= '<div class="row wrap-top wrap-bottom">';
  // Helper::pre(Database::returnWhereQuery('working_day_session', array('radnik_id'=>$rid, 'work_day'=>$date) ) );

  if ( Database::num_rows(Database::returnWhereQuery('working_day_session', array('radnik_id'=>$rid, 'work_day'=>$date) ) ) > 0 ) {
    $working_day_session_details = Database::whereQuery('working_day_session', array('radnik_id'=>$rid, 'work_day'=>$date) );
  
    // MODAL HEAD PART
    $o .= '  <!-- Modal -->';
    $o .= '<div id="myModal" class="modal fade" role="dialog">';
    $o .= '  <div class="modal-dialogg">';

    $o .= '    <!-- Modal content-->';
    $o .= '    <div class="modal-content">';
    $o .= '      <div class="modal-header">';
     $o .= '       <button type="button" class="close" data-dismiss="modal">&times;</button>';
    $o .= '        <h4 class="modal-title"> DETALJI RADNIKA </h4>';
    $o .= '      </div>';
    $o .= '      <div class="modal-body">';

    $o .= '<div class="alert alert-grey col-xs-12">';

    $index = 1;
    $radno_vreme_all = 0;

      foreach ($working_day_session_details as $detail_item) {
        // Helper::pre($detail_item);

        $o .= '<h3 class="sub-header"> Log <i>'.$index.'</i></h3>';

        // LOGIN TIME
          if ( $detail_item['login_time'] == '' ) { $login_time='0'; } else { $login_time=date( 'H:i:s', strtotime($detail_item['login_time'])); }
        $o .= '<div class="col-xs-2 btn btn-warning text-left"> Loged Time: </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$login_time.'</div>';

        // LOGOUT TIME
          if ( $detail_item['logout_time'] == '' ) { $logout_time='0'; } else { $logout_time=date( 'H:i:s', strtotime($detail_item['logout_time'])); }
        $o .= '<div class="col-xs-2 btn btn-warning"> Logout Time: </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$logout_time.'</div>';

        // WORKING TIME
          if ( $login_time == '0' || $logout_time == '0' ) { $work_time=Helper::DateTimeDiff($login_time, date('H:i:s')); } else { $work_time=Helper::DateTimeDiff($login_time, $logout_time); }
        $o .= '<div class="col-xs-2 btn btn-warning"> Radno vreme </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$work_time.'</div>';

        @$radno_vreme_all += Helper::returnSecondsFromTimeString($work_time);

        // PAUSA 1
          if ( $detail_item['pausa_1_start_time'] == '' ) { $pausa_1_start_time='0'; } else { $pausa_1_start_time=date( 'H:i:s', strtotime($detail_item['pausa_1_start_time'])); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Pausa 1 Start Time: </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_1_start_time.'</div>';
 
          if ( $detail_item['pausa_1_end_time'] == '' ) { $pausa_1_end_time='0'; } else { $pausa_1_end_time=date( 'H:i:s', strtotime($detail_item['pausa_1_end_time'])); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Pausa 1 End Time:</div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_1_end_time.'</div>';

        // UKUPNO VREME PAUSA 1
          if ( $pausa_1_start_time == '0' || $pausa_1_end_time == '0' ) { $pausa_1_time='0'; } else { $pausa_1_time=Helper::DateTimeDiff($pausa_1_start_time, $pausa_1_end_time); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Ukupno PAUSA 1 </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_1_time.'</div>';
 
        @$radno_vreme_all += Helper::returnSecondsFromTimeString($pausa_1_time);

        // PAUSA 2
          if ( $detail_item['pausa_2_start_time'] == '' ) { $pausa_2_start_time='0'; } else { $pausa_2_start_time=date( 'H:i:s', strtotime($detail_item['pausa_2_start_time'])); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Pausa 2 Start Time: </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_2_start_time.'</div>';

          if ( $detail_item['pausa_2_end_time'] == '' ) { $pausa_2_end_time='0'; } else { $pausa_2_end_time=date( 'H:i:s', strtotime($detail_item['pausa_2_end_time'])); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Pausa 2 End Time:</div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_2_end_time.'</div>';
        
        // UKUPNO VREME PAUSA 2
          if ( $pausa_2_start_time == '0' || $pausa_2_end_time == '0' ) { $pausa_2_time='0'; } else { $pausa_2_time=Helper::DateTimeDiff($pausa_2_start_time, $pausa_2_end_time); }
        $o .= '<div class="col-xs-2 btn btn-info text-left"> Ukupno PAUSA 2 </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$pausa_2_time.'</div>';

        @$radno_vreme_all += Helper::returnSecondsFromTimeString($pausa_2_time);

          // KOMESA
        $komesa_data = Database::whereQuery('komesa', array('id'=>$detail_item['komesa']));
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Komesa : </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$komesa_data[0]['komesa_id'].'</div><br/>';
        
        // ARTIKAL
        $artikal_data = Database::whereQuery('artikal', array('id'=>$detail_item['artikal_id']));
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Artikal : </div>              <div class="col-xs-2 btn btn-default">'.$artikal_data[0]['artikal_id'].'</div><br/>';
        
        // COLOR
        if ( $detail_item['color']=='' ) { $color = 'Univerzanla'; } else { $color = 'Universalna'; }
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Color : </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$color.'</div><br/>';
        
        // SIZE
        if ( $detail_item['size']=='' ) { $size = 'Univerzanla'; } else { $size = 'Univerzalna'; }
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Size : </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$size.'</div><br/>';
        
        // OPERACIJA

        $operacija_data = Database::whereQuery('operacije', array('id'=>$detail_item['operacija_id']));
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Operacija :</div>';
          $o .= ' <div class="col-xs-2 btn btn-default">'.$operacija_data[0]['operacija_name'].'</div><br/>';
        
        // KOMADA
        $o .= '<div class="col-xs-2 btn btn-success text-left"> Kom : </div>';
          $o .= '<div class="col-xs-2 btn btn-default">'.$detail_item['operacija_hint'].'</div><br/>';

        $o .= '<div class="clear"></div>';
        $o .= '<hr class="delimiter">';

        if ( Database::num_rows(Database::returnWhereQuery($new_table_name, array('working_day_session_id'=>$detail_item['working_day_session_id'], 'hint_code'=>'G001') )) > 0 ) {

            $start_greske = Database::whereQuery($new_table_name, array('working_day_session_id'=>$detail_item['working_day_session_id'], 'hint_code'=>'G001') );
            $end_greske = Database::whereQuery($new_table_name, array('working_day_session_id'=>$detail_item['working_day_session_id'], 'hint_code'=>'G002') );

            $end_greska_array = array();

              foreach ($end_greske as $egreska) {
                array_push($end_greska_array, array($egreska['vreme_operacije']));
              }
            // Helper::pre($end_greska_array);

            $o .= '<h2 class="sub-header text-left"> Greske </h2>';

            $i = 0;
            foreach ($start_greske as $greska) {
                $o .= '<div class="col-xs-2 btn btn-default">Start: '.date( 'H:i:s', strtotime($greska['vreme_operacije'])).'</div>';
                if ( isset($end_greska_array[$i][0]) ) {
                  $end_greska_time = $end_greska_array[$i][0];
                  $o .= '<div class="col-xs-2 btn btn-default"> End: '.date( 'H:i:s', strtotime($end_greska_time)).'</div>';
                } else {
                  $end_greska_time = '0';
                  $o .= '<div class="col-xs-2 btn btn-default">'.$end_greska_time.'</div>';
                }

                $ukupno_greska = '';
                if ( $greska['vreme_operacije'] == '0' || $end_greska_time == '0' ) { $ukupno_greska='0'; } else { $ukupno_greska=Helper::DateTimeDiff($greska['vreme_operacije'], $end_greska_time); }

                $o .= '<div class="col-xs-2 btn btn-danger"> Ukupno: '.$ukupno_greska.'</div>';
                
                $o .= '<div class="clear"></div>';

              
                @$radno_vreme_all += Helper::returnSecondsFromTimeString($ukupno_greska);


              $i++;
            }
                $count_greska = Database::num_rows(Database::returnWhereQuery($new_table_name, array('working_day_session_id'=>$detail_item['working_day_session_id'], 'hint_code'=>'CG01')));
                $o .= '<div class="col-xs-4 btn btn-success"> UKUNPNO KOMADA </div>';
                $o .= '<div class="col-xs-2 btn btn-success"> '.$count_greska.'</div>';


            $o .= '<hr class="delimiter">';

            $index++;


        }

      }

      $o .= '<div class="col-xs-6 btn alert-white"> UKUPNO RADNO VREME ZA DANAS</div>';
      $o .= '<div class="col-xs-6 btn alert-danger">';
        $o .= Helper::hoursMinutesSeconds($radno_vreme_all);
      $o .= '</div>';

      // MODAL DOWN PART
      $o .= '      </div>';
      $o .= '      <div class="modal-footer">';
      $o .= '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
      $o .= '      </div>';
      $o .= '    </div>';

      $o .= '  </div>';
      $o .= '</div>';

    $o .= '</div>';

  } else {
    $o .= '<div class="btn btn-warning col-xs-12"> Nema podataka za radnika na dan '.$date.'</div>';
  }


  $o .= '</div> <!-- END ROW -->';


} else {
  echo 'nema';
}
