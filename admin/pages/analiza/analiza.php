<?php

// Helper::pre($_POST);
// Helper::pre($_SESSION);
// Helper::pre($_GET);


if ( isset($_GET['x']) && $_GET['x']!='' ) {

  if ( $_GET['x'] == 'po_logovanju') {
    if ( file_exists('pages/'.$array['page'].'/analiza_po_logovanju.php') ) {
      // echo '<h1>',$array['page'],'</h1>';
      require_once 'pages/'.$array['page'].'/analiza_po_logovanju.php';
    } else {
      require_once 'pages/404.php';
    }
  } else {

  }
} else {

  ?>
  <h2 class="sub-header text-left input_padding"> Analiza ucinka </h2>
  <?php
    if ( isset($_GET) && count($_GET) == 1 ) {
      $o = '';

      $o .= '<div class="sub-header input_padding col-xs-12 text-center">';
      $o .= '<p> Svi podaci </p>';
      $o .= '</div>';

      echo $o;
    }
  ?>

  <div class="filter-container alert-primary">

    <form id="filter_form" action="<?php echo ADMINURL; ?>" method="get">
        <input name="page" value="analiza" hidden="hidden"/>
        <div class="row">
          <!-- <span class=""> Izaberi datum </span></br> -->
          <div class="col-xs-12">
            <div class="filter-box">
              <div class="col-xs-12">
                <input class="col-xs-3 wrap-right" type="text" name="date" id="datepicker-1" value="<?php echo (isset($_GET['date']) ? $_GET['date'] : '' ); ?>" placeholder="Izaberite datum">
                <!-- Vreme -->
                <select class="col-xs-2"  id="start_time" name="start_time">
                  <option value="0"<?php if ( isset($_GET['start_time']) && $_GET['start_time'] == 7 ) echo 'selected="selected"'; ?>>Vreme OD</option>
                  <option value="00">00</option>
                  <?php
                  for ($i=1; $i < 24; $i++) {
                    echo '<option class="" value="'.$i.'"';
                    if ( isset($_GET['start_time']) && $_GET['start_time'] == $i ) echo 'selected="selected"';
                    // elseif ( $i == 7 ) echo 'selected="selected"';
                    echo '>'.$i.'</option>';
                  }
                  ?>
                </select>
                <select class="col-xs-2" name="end_time">
                  <option value="0"<?php if ( isset($_GET['end_time']) && $_GET['end_time'] == 15 ) echo 'selected="selected"'; ?>>Vreme DO</option>
                  <option value="00">00</option>
                  <?php
                  for ($i=1; $i < 24; $i++) {
                    echo '<option class="" value="'.$i.'"';
                    if ( isset($_GET['end_time']) && $_GET['end_time'] == $i ) echo 'selected="selected"';
                    // elseif ( $i == 15 ) echo 'selected="selected"';
                    echo '>'.$i.'</option>';
                  }
                  ?>
                </select>

                <?php
                if ( Database::num_rows( Database::returnFetchData('komesa') ) > 0 )  { // if isset $_GET['komesa']
                  echo '<select name="komesa" class="select-komesa col-xs-4 wrap-left">';
                  if ( Database::num_rows( Database::returnFetchData('komesa') ) > 0  ) {
                    $komesa_data = Database::fetchData('komesa');
                    echo '<option class="disabled" selected="selected"> Izaberite komesu </option>';
                    foreach ($komesa_data as $komesa ) {
                      echo '<option name="komesa" value="'.$komesa['id'].'" ';
                      if( isset($_GET['komesa']) ? $komesa['id'] == $_GET['komesa'] : false) {
                        echo 'selected="selected"';
                      }
                      echo '> [ ' . $komesa['id']. ' ][ '. $komesa['komesa_id'] .'][ '. $komesa['komesa_name'] .'] </option>';
                    }
                  } else {
                    echo '<option name="komesa" value="0"> Nema trenutno komesa </option>';
                  }

                  echo '</select>';
                  // echo '<div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-4">Change komesa</div>';
                }
                ?>

              </div>

              <div class="col-xs-12">
                <?php
                if ( Database::num_rows( Database::returnFetchData('operacije') ) > 0 )  { // if isset $_GET['komesa']
                  echo '<select name="operacija" class="select-operacije col-xs-4 wrap-left">';
                  if ( Database::num_rows( Database::returnFetchData('operacije') ) > 0  ) {
                    $op_data = Database::fetchData('operacije');
                    echo '<option class="disabled" selected="selected"> Izaberite operaciju </option>';
                    foreach ($op_data as $op ) {
                      echo '<option name="operacija" value="'.$op['id'].'" ';
                      if( isset($_GET['operacija']) ? $op['id'] == $_GET['operacija'] : false) {
                        echo 'selected="selected"';
                      }
                      echo '> [ ' . $op['id']. ' ][ '. $op['operacija_id'] .'][ '. $op['operacija_name'] .'] </option>';
                    }
                  } else {
                    echo '<option name="operacija" value="0"> Nema trenutno operacija </option>';
                  }

                  echo '</select>';
                  // echo '<div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-4">Change komesa</div>';
                }
                ?>
                
              </div>
              <div class="col-xs-12">
                
                <span class="dropdown-box">
                  <input class="hidden radnik_main_value" type="text" value="0" name="radnik">
                  <!-- Radnici -->
                  <input class="live-search-radnika col-xs-4 wrap-right" type="text" value="" placeholder="Unesite radnika" name="live-serach-radnika">

                  <div class="dropdown-live-search-radnika text-left">
                  </div>
                  <span class="clear"></span>
                </span>
                
                <?php
                $sve_operacije = Database::fetchData('operacije');
                ?>
                <!-- <span class="col-xs-4 wrap-top input_padding">Izaberite operaciju</span>
                <select class="col-xs-4" name="operacija">
                  <option value="0"<?php if ( isset($_GET['operacija']) && $_GET['operacija'] == 0 ) echo 'selected="selected"'; ?>>Izaberi operaciju</option>
                  <?php
                  foreach ($sve_operacije as $operacija) {
                    echo '<option value="'.$operacija['id'].'"';
                    if ( isset($_GET['operacija']) && $_GET['operacija'] == $operacija['id'] ) {
                      echo 'selected="selected"';
                    }
                    echo '">ID:'.$operacija['id'].' | '.$operacija['operacija_name'].'</option>';
                  }
                  ?>
                </select>
                <!-- Linija -->
                <?php $sve_linije = Database::fetchData('linija'); ?>
                <!-- <span class="col-xs-2 wrap-top input_padding">Izaberite liniju</span> -->
                <select class="col-xs-4" name="linija">
                  <option value="0"<?php if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) echo 'selected="selected"'; ?>>Izaberi liniju</option>
                  <?php
                  foreach ($sve_linije as $linija) {
                    echo '<option value="'.$linija['id'].'"';
                    if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
                      echo 'selected="selected"';
                    }
                    echo '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
                  }
                  ?>
                </select>

                <input class="col-xs-3 wrap-left" type="number" name="min_eff" value="" placeholder="Min Eff">

              </div>

              <hr class="delimiter">

              <div class="col-xs-12">
                <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />
                <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->
                <a id="reset_filter_form" class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=analiza">Reset Filter</a>
                <a id="reset_all_filter_form" class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=analiza">Reset All Filter</a>

              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
    </form>
  </div>
  <div class="filter-btn-visible btn btn-success wrap">Show filter options</div>

  <?php
  $o = '';

  // Helper::pre($_GET);

    if ( isset($_GET['date']) && ($_GET['date'] != '' || $_GET['date'] !=0) ) {
      // $o .= 'Datum je  izabran';
      $date = $_GET['date'].' ';
      $old_date = explode('-', $date);
      $new_date = ltrim($old_date[1],'0').'_'.$old_date[0];
    } else {
      // 'Datum nije izabran'
      $date ='';
      $new_date = date('m').'_'.date('Y');
    }


    $new_table_name = 'working_day_session_details_'.$new_date;

    $sql = "SELECT * FROM `$new_table_name` ";

    // filter
    if ( $date != '' ) {
      $sql .= " WHERE working_day='$date' ";
    } else {
      // $sql .= " WHERE working_day>'2016-01-01' ";
      $sql .= " WHERE working_day='".date('Y-m-d')."' ";
    }

    // time filter
    $start_time = '00:00:00';
    if ( isset($_GET['start_time']) && ($_GET['start_time'] != 0 && $_GET['start_time'] != '') ) {
      $start_time = $_GET['start_time'].':00:00';
      $sql .= " AND `vreme_operacije` >= '".date("Y-m-d", strtotime($date)) . ' ' . $start_time."'";
    }

    if ( isset($_GET['end_time']) && ($_GET['end_time'] != 0 && $_GET['end_time'] != '') ) {
      $end_time = (isset($_GET['date']) ? '' : date('Y-m-d') ) .' 23:59:59';
      $end_time = $_GET['end_time'].':00:00';
      $sql .= " AND `vreme_operacije` <= '". $date . $end_time."'";
    }
    $query_end_time = '23:59:59';
    // Helper::pre($query_date);
    // Helper::pre(date('Y-m-d'));
    // Helper::pre($query_end_time);



    // komesa filter
    if ( isset($_GET['komesa']) && ($_GET['komesa'] != 0 && $_GET['komesa'] != '') ) {
      $sql .= " AND `komesa_id`='".$_GET['komesa']."'";
    } else {
    }

    // RADNIK filter
    if ( isset($_GET['radnik']) && ($_GET['radnik'] != 0 && $_GET['radnik'] != '') ) {
      $rid = $_GET['radnik'];
      $radnik = Database::whereQuery('radnici_lista', array('id'=>$rid) );
      $sql .= " AND `radnik_id`='".$rid."'";
    } else {
    }

    // linija filter
    if ( isset($_GET['linija']) && ($_GET['linija'] != 0 && $_GET['linija'] != '') ) {
      $linija = $_GET['linija'] - 1;
      $sql .= " AND `linija_id`='".$linija."'";
    } else {
    }


    // operacija filter
    if ( isset($_GET['operacija']) && ($_GET['operacija'] != 0 && $_GET['operacija'] != '') ) {
      $sql .= " AND `operacija_id`='".$_GET['operacija']."'";
    } else {
    }

    // min eff
    if ( isset($_GET['min_eff']) && ($_GET['min_eff'] != 0 && $_GET['min_eff'] != '') ) {
      $sql .= " AND `procenat_ucinka`>'".$_GET['min_eff']."'";
    } else {
    }


    $sql .= ' AND hint_code = "F001"';

    // change sql if working day is sset from get varibale
    if ( isset($_GET['working_day_session_id']) && $_GET['working_day_session_id'] != '' ) {
      $sql = "SELECT * FROM `working_day_session_details`  WHERE 1=1  AND `working_day_session_id`='".$_GET['working_day_session_id']."'";
    }

    $sql .= ' ORDER BY id DESC';

    // Helper::pre($sql);
    // $_SESSION['test3'] = $sql;

    if ( Database::num_rows($sql) > 0 ) {
      $data = Database::fetch($sql);
      // Helper::pre($data);

      if ( isset($_GET['radnik']) && $_GET['radnik'] != '' && $_GET['radnik'] != null  ) {
        require_once 'analiza_detalja_radnika.php';
      }
      if ( isset($_GET['linija']) && $_GET['linija'] != '' && $_GET['linija'] != null && $_GET['linija']!=0 ) {
        $lid = $_GET['linija']-1;
          $danasnji_komadi = Database::num_rows( Database::returnWhereQuery('working_day_session_details', array('linija_id'=>$lid, 'hint_code'=>'F001')));
          $efikasnost_data = Database::whereQuery('working_day_session_details', array('linija_id'=>$lid, 'hint_code'=>'F001'));
          $efikasnost = 0;
          foreach ( $efikasnost_data as $data_item ) {
            $efikasnost += $data_item['procenat_ucinka'];
          }

          $o .= '<div class="alert alert-info">';
          $o .= '<div class="col-xs-4"> Linija </div> <div class="col-xs-8"> LINIJA ' . $lid . '</div>';
          $o .= '<div class="col-xs-4"> Broj danasnjih komada </div> <div class="col-xs-8"> ' . $danasnji_komadi . '</div>';
          $o .= '<div class="col-xs-4"> Efikasnos linije </div> <div class="col-xs-8"> ' . floor($efikasnost/$danasnji_komadi) . ' % </div>';
          $o .= '<div class="clear"></div>';
          $o .= '</div>';

      }
      if ( isset($_GET['komesa']) && $_GET['komesa'] != '' && $_GET['komesa'] != null && $_GET['komesa']!=0 ) {
          $kid = $_GET['komesa'];
          if ( Database::num_rows( Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$kid)) ) > 0  ) {
            $komesa_data = Database::whereQuery('komesa', array('id'=>$kid));

            $danasnji_komadi = Database::num_rows( Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$kid,'working_day'=>date('Y-m-d'), 'hint_code'=>'F001')));
            $ukupno = Database::num_rows( Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$kid, 'hint_code'=>'F001')));
            $efikasnost_data = Database::whereQuery('working_day_session_details', array('komesa_id'=>$kid,'working_day'=>date('Y-m-d'), 'hint_code'=>'F001'));
            $efikasnost = 0;
            if ( $danasnji_komadi == 0 ) {
              $count_eficasnost = 0;
            } else {
              foreach ( $efikasnost_data as $data_item ) {
                $efikasnost += $data_item['procenat_ucinka'];
              }
              $count_eficasnost = floor($efikasnost/$danasnji_komadi);
            }
  $o .= '<div class="row">';

            $o .= '<div class="alert alert-info">';
            $o .= '<div class="col-xs-4"> KOMESA REDNI BROJ </div> <div class="col-xs-8"> [ ' . $komesa_data[0]['id']. ' ] </div>';
            $o .= '<div class="col-xs-4"> KOMESA ID  </div> <div class="col-xs-8"> [ ' . $komesa_data[0]['komesa_id']. ' ] </div>';
            $o .= '<div class="col-xs-4"> KOMESA NAME </div> <div class="col-xs-8"> [ ' . $komesa_data[0]['komesa_name']. ' ] </div>';
            $o .= '<div class="col-xs-4"> Broj danasnjih komada </div> <div class="col-xs-8"> ' . $danasnji_komadi . '</div>';
            $o .= '<div class="col-xs-4"> Ukupno uradjenih </div> <div class="col-xs-8"> ' . $ukupno . '</div>';
            $o .= '<div class="col-xs-4"> Danasnja efikasnost linije </div> <div class="col-xs-8"> ' . $count_eficasnost . ' % </div>';
            $o .= '<div class="clear"></div>';
            $o .= '</div>';


          } else {
            $o .= '<div class="alert alert-danger">';
            $o .= '<h2 class="text-center"> Nema podataka za ovu komesu </h2>';
            $o .= '</div>';

          }
  $o .= '</div>';

      }

      if ( isset($_GET['date']) && $_GET['date'] != '' && $_GET['date'] != null ) {
        $where_array = array(
          'work_day'=>date("Y-m-d", strtotime($data[0]['vreme_operacije']))
        );
        if ( isset($_GET['vreme_start']) ) {

        }
        $subresult = Database::whereQuery('working_day_session', $where_array );
        // Helper::pre( Database::returnWhereQuery('working_day_session', $where_array ));
        // Helper::pre($subresult);
        // Helper::pre($data);
        //
        $sum = 0;
        $svi_procenti = 0;
        $prosek_procenata = 0;

        $counted_data = count($data);
        foreach ( $data as $xitem ) {
          $svi_procenti += $xitem['procenat_ucinka'];
        }
        $prosek_procenata = $svi_procenti / $counted_data;
        foreach ( $subresult as $item ) {
          $sum += $item['operacija_hint'];
        }
        $o .= '<div class="row">';

          $o .= '<div class="alert alert-info">';
          $o .= '<p>Ukupno odradjenih operacija za dan '.date("d.m.Y", strtotime($data[0]['vreme_operacije'])).' je : '. count($data).'</p>';
          $o .= '<p >Prosek u procentima je : <span class="alert-lightblue">'. floor(number_format($prosek_procenata, 2)).' % </span> </p>';
          $o .= '</div>';
        $o .= '</div>';
      }

      /*
       id
       working_day_session_id
       working_day
       komesa_id
       vreme_operacije
       razlika_u_vremenu
       procenat_ucinka
       hint_code
       */

       //pagination data
       $item_per_page=10;
       if(!isset($_GET['ppage']) || $_GET['ppage']==1){
         $ppage=1;
         $start=0;
       }else{
         $ppage=$_GET['ppage'];
         $start=($ppage-1)*$item_per_page;
       }
       $next=$ppage+1;
       $prev=$ppage-1;
       $limit=$start.','.$item_per_page;
       $current_page = isset($_GET['ppage']) ? $_GET['ppage'] : $current_page=1;
       $data['pagination']=array(
         'item_per_page'=>$item_per_page,
         'ppage'=>$ppage,
         'start'=>$start,
         'next'=>$next,
         'prev'=>$prev,
         'limit'=>$limit,
         'current_page'=>$current_page,
         'url' => ADMINURL.'?page=analiza'
       );

      $count = count(Database::fetch($sql));
      // $count = 1;

       $sql .= ' LIMIT '.$start.','.$item_per_page;

      //  Helper::pre($sql);

       if ( Database::num_rows($sql) > 0 ) {
         $komesa_linija_list = Database::fetch($sql);
         // Helper::pre($komesa_linija_list);

         $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

        //  $komesa_linija_list = Database::query($sql);

         // show data into table
         $o .= '<div class="table-responsive">';
         $o .= '<table class="table table-striped">';
         $o .= '  <thead>';
         $o .= '    <tr>';
         //$o .= '      <th>ID</th>';
         //$o .= '      <th>working_day_session_id</th>';
         $o .= '      <th>Working Day</th>';
         $o .= '      <th>komesa_id</th>';
         $o .= '      <th>Radnik</th>';
         $o .= '      <th>Operacija</th>';
         //$o .= '      <th>vreme_operacije</th>';
         $o .= '      <th>Ime Operacije</th>';
         $o .= '      <th>Vreme operacije</th>';
         $o .= '      <th>razlika_u_vremenu</th>';
         $o .= '      <th>procenat_ucinka</th>';
         //$o .= '      <th>hint_code</th>';
         $o .= '    </tr>';
         $o .= '  </thead>';
         $o .= '  <tbody>';
         foreach ($komesa_linija_list as $item ) {
           $o .= '<tr class="input_padding">';
           //   id
           //$o .= '<td>'.$item['id'].'</td>';
           //  working day session id
           //$o .= '<td>'.$item['working_day_session_id'].'</td>';
           //  working day
           $o .= '<td>'.$item['working_day'].'<br/> <small>'.$item['vreme_operacije'].'</small></td>';
           //  komesa_id
           $komesa_data = Database::whereQuery('komesa',array('id'=>$item['komesa_id']));
          //  Helper::pre(Database::returnWhereQuery('komesa',array('id'=>$item['komesa_id'])));
           $o .= '<td title="#'.$komesa_data[0]['komesa_id'].'-'.$komesa_data[0]['komesa_name'].'"><a href="#'.$komesa_data[0]['komesa_id'].'-'.$komesa_data[0]['komesa_name'].'">'.$komesa_data[0]['komesa_id'].'</a></td>';
           // radnik
           $radnik_data = Database::whereQuery('radnici_lista',array('radnik_id'=>$item['radnik_id']));
           $o .= '<td title="'.$radnik_data[0]['radnik_name'].'"><a href="'.ADMINURL.'?page=analiza&radnik='.$radnik_data[0]['radnik_id'].'">'.$radnik_data[0]['radnik_name'].'</a></td>';
           // operacija
           $operacija_data = Database::whereQuery('operacije', array('id'=>$item['operacija_id']));

           $o .= '<td><a href="'.ADMINURL.'?page=analiza&operacija='.$operacija_data[0]['id'].'">'.$operacija_data[0]['opis_operacije'].'</a></td>';
           $o .= '<td><a href="'.ADMINURL.'?page=analiza&operacija='.$operacija_data[0]['id'].'">'.$operacija_data[0]['operacija_name'].'</a></td>';
           
          $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));
          // artikal_data_105GD6538_73
          $artikal_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];
          $this_artikal_data = Database::whereQuery($artikal_table, array('operacija_id'=>$operacija_data[0]['operacija_id']));

          $vreme_operacije = floor(3600 / $this_artikal_data[0]['tempo_operacije']);
          $vreme_disable_btn = $radnik_data[0]['btn_disable_time'];
          $vreme_disable_btn_sec = floor(($vreme_operacije / 100) * $vreme_disable_btn);

           
           $o .= '<td> '. $vreme_operacije .'s ( '.$vreme_disable_btn.'% = '. $vreme_disable_btn_sec.'s ) </td>';

           $razlika_u_sekundama = explode(':', $item['razlika_u_vremenu']);

            $o .= '<td>'.$item['razlika_u_vremenu'].' ( '.($razlika_u_sekundama[0] * 3600 + $razlika_u_sekundama[1] * 60 + $razlika_u_sekundama[2]).' )</td>';
           // procenat ucinka
           $o .= '<td ';
           if ( $item['procenat_ucinka'] < '80' ) {
             $o .= 'class="alert alert-danger"';
           } elseif ($item['procenat_ucinka'] >= '80')  {
             $o .= 'class="alert alert-success"';
           }
           $o .= '>'.$item['procenat_ucinka'].'</td>';
           // hintt code
           //$o .= '<td>'.$item['hint_code'].'</td>';
           $o .= '</tr>';

         }
         $o .= '  </tbody>';
         $o .= '</table>';
         $o .= '</div>';

         $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

       } else {
         $o .= '<h1> Nema podataka </h1>';
       }
    } else {
      $o .= '<p class="alert-lightred text-center wrap-top wrap-bottom input_padding">There are not records for ';
      if ( isset($date) ) {
        $o .= $date;
      } else {
        $o .= date('d.m.Y');
      };
      $o .= ' at the mommnet?</p>';
    }

  echo $o;


}
