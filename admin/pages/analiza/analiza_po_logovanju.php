<?php

// Helper::pre($_POST);
// Helper::pre($_SESSION);
// Helper::pre($_GET);

// Function for displaying table fileds
// mysql_fetch_field($result)

$q = Database::query('DESCRIBE `working_day_session`');
// Helper::pre($q);

  foreach ($q as $item ) {
    // Helper::pre($item);
    echo '<p>'.$item['Field'].'-'.$item['Type'].'</p>';
  }

?>
<h2 class="text-center alert alert-grey"> Analiza ucinka po logovanju </h2>

<div class="filter-container alert-info">

  <form action="<?php echo ADMINURL; ?>" method="get">
    <input name="page" value="analiza" hidden="hidden"/>
    <div class="container">
      <div class="row">
        <!-- <span class=""> Izaberi datum </span></br> -->
        <div class="col-xs-12">
          <div class="filter-box">
            <input class="col-xs-12 col-sm-3 col-md-3 col-lg-2" type="text" name="date" id="datepicker-1" value="<?php echo (isset($_GET['date']) ? $_GET['date'] : '' ); ?>" placeholder="Izaberite datum">
            <!-- Vreme OD -->
            <!-- <span  class="">Vreme od</span> -->
            <select class="col-xs-6 col-sm-3 col-md-3 col-lg-2"  id="start_time" name="start_time">
              <option value="0"<?php if ( isset($_GET['start_time']) && $_GET['start_time'] == 7 ) echo 'selected="selected"'; ?>>Vreme OD</option>
              <option value="00">00</option>
              <?php
              for ($i=1; $i < 24; $i++) {
                echo '<option class="" value="'.$i.'"';
                if ( isset($_GET['start_time']) && $_GET['start_time'] == $i ) echo 'selected="selected"';
                // elseif ( $i == 7 ) echo 'selected="selected"';
                echo '>'.$i.'</option>';
              }
              ?>
            </select>
            <!-- Vreme DO -->
            <!-- <span  class="">Vreme do</span> -->
            <select class="col-xs-6 col-sm-3 col-md col-lg-2" name="end_time">
              <option value="0"<?php if ( isset($_GET['end_time']) && $_GET['end_time'] == 15 ) echo 'selected="selected"'; ?>>Vreme DO</option>
              <option value="00">00</option>
              <?php
              for ($i=1; $i < 24; $i++) {
                echo '<option class="" value="'.$i.'"';
                if ( isset($_GET['end_time']) && $_GET['end_time'] == $i ) echo 'selected="selected"';
                // elseif ( $i == 15 ) echo 'selected="selected"';
                echo '>'.$i.'</option>';
              }
              ?>
            </select>
          </div>
          <?php
          if ( true )  { // if isset $_GET['komesa']
            echo '<select name="komesa" class="select-komesa col-xs-9 col-sm-6 col-md-3 col-lg-2">';
            if( isset($_GET['komesa']) && $_GET['komesa'] != '0' ) {
              echo '<option ';
              echo 'selected="selected"';
              echo ' value="'.$_GET['komesa'].'">'.$_GET['komesa'].'</option>';
            } else {
              echo '<option name="komesa"> Izaberite datum </option>';
            }
            echo '</select>';
          }
          ?>
          <div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-3 col-sm-3 col-md-3 col-lg-2">Change komesa</div>

          <!-- Radnici -->
          <?php $radnici = Database::fetchData('radnici_lista'); ?>
          <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="radnik">
            <option value="0"<?php if ( isset($_GET['radnik']) && $_GET['radnik'] == 0 ) echo 'selected="selected"'; ?>>Izaberi radnika</option>
            <?php
            foreach ($radnici as $radnik) {
              echo '<option value="'.$radnik['id'].'"';
              if ( isset($_GET['radnik']) && $_GET['radnik'] == $radnik['id'] ) {
                echo 'selected="selected"';
              }
              echo '>ID:'.$radnik['id'].' | '.$radnik['radnik_name'].'</option>';
            }
            ?>
          </select>

          <!-- Linija -->
          <?php $sve_linije = Database::fetchData('linija'); ?>
          <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="linija">
            <option value="0"<?php if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) echo 'selected="selected"'; ?>>Izaberi liniju</option>
            <?php
            foreach ($sve_linije as $linija) {
              echo '<option value="'.$linija['id'].'"';
              if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
                echo 'selected="selected"';
              }
              echo '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
            }
            ?>
          </select>
          <div class="clear"></div>

          <hr class="delimiter">

          <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />
          <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->
          <a class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=analiza">Reset Filter</a>
        </div>
      </div>
    </div>
  </form>

</div> <!-- filter-box -->
<div class="filter-btn-visible btn btn-success wrap">Show filter options</div>
<?php
$o = '';

// Helper::pre($_GET);

  if ( isset($_GET['date']) && $_GET['date'] != '' ) {
    // $o .= 'Datum je  izabran';
    $date = $_GET['date'];
  } else {
    // 'Datum nije izabran'
    $date ='';
  }

  $sql = "SELECT * FROM working_day_session ";

  // filter
  if ( $date != '' ) {
    $sql .= " WHERE work_day='$date' ";
  } else {
    // $sql .= " WHERE working_day>'2016-01-01' ";
    $sql .= " WHERE 1=1 ";
  }

  // Helper::pre($query_date);
  // Helper::pre(date('Y-m-d'));
  // Helper::pre($query_end_time);



  // komesa filter
  if ( isset($_GET['komesa']) && ($_GET['komesa'] != 0 && $_GET['komesa'] != '') ) {
    $sql .= " AND komesa_id='".$_GET['komesa']."'";
  } else {
  }

  // RADNIK filter
  if ( isset($_GET['radnik']) && ($_GET['radnik'] != 0 && $_GET['radnik'] != '') ) {
    $sql .= " AND radnik_id='".$_GET['radnik']."'";
  } else {
  }

  // linija filter
  if ( isset($_GET['linija']) && ($_GET['linija'] != 0 && $_GET['linija'] != '') ) {
    $sql .= " AND linija_id='".$_GET['linija']."'";
  } else {
  }

  // if ( isset($_GET['working_day_session_id']) && $_GET['working_day_session_id'] != '' ) {
  //   $sql = "SELECT * FROM `working_day_session_details`  WHERE 1=1  AND `working_day_session_id`='".$_GET['working_day_session_id']."'";
  //
  //
  // }


  Helper::pre($sql);
  $_SESSION['test3'] = $sql;

  if ( Database::num_rows($sql) > 0 ) {
    $data = Database::fetch($sql);
    // Helper::pre($data);

    if ( isset($_GET['date']) && $_GET['date'] != '' && $_GET['date'] != null ) {
      $where_array = array(
        'work_day'=>date("Y-m-d", strtotime($data[0]['vreme_operacije']))
      );
      if ( isset($_GET['vreme_start']) ) {

      }
      $subresult = Database::whereQuery('working_day_session', $where_array );
      // Helper::pre( Database::returnWhereQuery('working_day_session', $where_array ));
      // Helper::pre($subresult);
      //
      $sum = 0;
      $svi_procenti = 0;
      $prosek_procenata = 0;

      $counted_data = count($data);
      foreach ( $data as $xitem ) {
        $svi_procenti += $xitem['procenat_ucinka'];
      }
      $prosek_procenata = $svi_procenti / $counted_data;
      foreach ( $subresult as $item ) {
        $sum += $item['operacija_hint'];
      }
      $o .= '<div class="alert alert-info">';
      $o .= '<p>Ukupno odradjenih operacija za dan '.date("d.m.Y", strtotime($data[0]['vreme_operacije'])).' je : '. $sum.'</p>';
      $o .= '<p>Prosek u procentima je : '. number_format($prosek_procenata, 2).'</p>';
      $o .= '</div>';

    }

    /*
     id
     working_day_session_id
     working_day
     komesa_id
     vreme_operacije
     razlika_u_vremenu
     procenat_ucinka
     hint_code
     */

     //pagination data
     $item_per_page=10;
     if(!isset($_GET['ppage']) || $_GET['ppage']==1){
       $ppage=1;
       $start=0;
     }else{
       $ppage=$_GET['ppage'];
       $start=($ppage-1)*$item_per_page;
     }
     $next=$ppage+1;
     $prev=$ppage-1;
     $limit=$start.','.$item_per_page;
     $current_page = isset($_GET['ppage']) ? $_GET['ppage'] : $current_page=1;
     $data['pagination']=array(
       'item_per_page'=>$item_per_page,
       'ppage'=>$ppage,
       'start'=>$start,
       'next'=>$next,
       'prev'=>$prev,
       'limit'=>$limit,
       'current_page'=>$current_page,
       'url' => ADMINURL.'?page=analiza'
     );

    $count = count(Database::fetch($sql));
    // $count = 1;

     $sql .= ' LIMIT '.$start.','.$item_per_page;

     if ( Database::num_rows($sql) > 0 ) {
       $komesa_linija_list = Database::fetch($sql);

       $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

      //  $komesa_linija_list = Database::query($sql);

       // show data into table
       $o .= '<div class="table-responsive">';
       $o .= '<table class="table table-striped">';
       $o .= '  <thead>';
       $o .= '    <tr>';
       $o .= '      <th>working_day_session_id</th>';
       $o .= '      <th>work_day</th>';
       $o .= '      <th>Radnik ID</th>';
       $o .= '      <th>Linija</th>';
       $o .= '      <th>Komesa</th>';
       $o .= '      <th>Artikal</th>';
       $o .= '      <th>Operacija</th>';
       $o .= '      <th>Operacija Hint</th>';
       $o .= '      <th>#</th>';
       $o .= '    </tr>';
       $o .= '  </thead>';
       $o .= '  <tbody>';
       foreach ($komesa_linija_list as $item ) {
         $o .= '<tr class="input_padding">';
         //  working day session id
         $o .= '<td>'.$item['working_day_session_id'].'</td>';
         //  working day
         $o .= '<td>'.$item['work_day'].'</td>';
         //  komesa_id
         $o .= '<td>'.$item['radnik_id'].'</td>';
         // veme operacije
         $o .= '<td>'.$item['linija'].'</td>';
         //  razlika_u_vremenu
          $o .= '<td>'.$item['komesa'].'</td>';
          // hintt code
          $o .= '<td>'.$item['artikal_id'].'</td>';
          // hintt code
          $o .= '<td>'.$item['operacija_id'].'</td>';
          // hintt code
          $o .= '<td>'.$item['operacija_hint'].'</td>';
          // details link
          $o .= '<td><a href="'.ADMINURL.'?page=analiza&&working_day_session_id='.$item['working_day_session_id'].'"> Details </a></td>';
         $o .= '</tr>';
       }
       $o .= '  </tbody>';
       $o .= '</table>';
       $o .= '</div>';

       $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

     } else {
       $o .= '<h1> Nema podataka </h1>';
     }
  } else {
    $o .= '<p class="alert alert-danger">There are not records for '.$date.' at the mommnet?</p>';
  }

echo $o;
