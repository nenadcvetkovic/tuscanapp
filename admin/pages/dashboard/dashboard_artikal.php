<?php

// Helper::pre($_GET);


$o .= '<a class="btn btn-info" href="'.ADMINURL.'"> Back to dashboard </a>';

$o .= '<h2 class="sub-header text-left input_padding">Artikal</h2>';

if ( Database::num_rows("SELECT * FROM `artikal`") > 0 ) {

  if ( isset($_GET['artikal_id']) ) {
    // Vrati detalje artikal sa ide
    $sql = "SELECT * FROM `artikal` WHERE id='".$_GET['artikal_id']."'";
    $count = Database::num_rows($sql);
    $sql .= ' LIMIT '.$start.','.$item_per_page;

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $artikal_data = Database::query($sql);

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>Artikal ID</th>';
    $o .= '      <th>Operacije</th>';
    $o .= '      <th class="text-center">Komada po komesi</th>';
    $o .= '      <th class="text-center">Ukupno uradjenih</th>';
    $o .= '      <th class="text-center">Danasnji komadi</th>';
    // $o .= '      <th>Datum unosa</th>';
    $o .= '      <th>Status</th>';
    $o .= '      <th></th>';
    $o .= '      <th></th>';
    $o .= '      <th></th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';


    foreach ($artikal_data as $item) {
      if ( $artikal_item = Database::fetchData('artikal_data_'.$item['artikal_id'].'_'.$item['id']) ) {
      
      $o .= '<tr>';
      $o .= '<td><a href="'.ADMINURL.'?page=artikal&options=edit&id='.$item['id'].'">'.$item['artikal_name'].'</a></td>';
      $o .= '<td>';
      foreach ($artikal_item as $artikal_operacija) {
        $o .= '<div><a href="#">'.$artikal_operacija['operacija_name'].'</a></div>';
      }
      $o .= '</td>';

      $komesa_data = Database::whereQuery('komesa', array('artikal_id'=>$item['id']));
      $komada_po_komesi = 0;
      if ( $komesa_data != '' || $komesa_data != NULL ) {
        foreach ($komesa_data as $kom_item ) {
          $komada_po_komesi += $kom_item['artikal_komada_per_komesa'];
        }
      }
      $o .= '<td class="text-center"> '.$komada_po_komesi.' </td>';

      $o .= '<td class="text-center">';
      foreach ($artikal_item as $artikal_operacija) {
        // Helper::pre($artikal_operacija);
        $op = Database::whereQuery('operacije', array('operacija_id'=>$artikal_operacija['operacija_id']));
        $op_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('artikal_id'=>$item['id'] ,'operacija_id'=>$op[0]['id'])));
        $o .= '<div style="border-bottom:thin solid #000"';
        if ($komada_po_komesi * $artikal_operacija['komada_po_artiklu'] <= $op_data_count ) {
          $o .= ' class="text-center alert-danger" ';
        } else {
          $o .= ' class="text-center alert-success" ';
        }
        $o .= '>'.$komada_po_komesi * $artikal_operacija['komada_po_artiklu'] . ' / ' . $op_data_count.'</div>';

      }
      $o .= '</td>';

      $o .= '<td class="text-center">';
      foreach ($artikal_item as $artikal_operacija) {
        $op = Database::whereQuery('operacije', array('operacija_id'=>$artikal_operacija['operacija_id']));
        $op_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('artikal_id'=>$item['id'], 'working_day'=>date('Y-m-d') ,'operacija_id'=>$op[0]['id'])));
        $o .= '<div>'.$op_data_count.'</div>';
      }
      $o .= '</td>';

      $o .= '<td>';
      $o .= '<div class="btn';
      if ($item['artikal_status'] == 0 ) {
        $o .= ' btn-danger';
      } elseif ( $item['artikal_status'] == 1 ) {
        $o .= ' btn-success';
      }
      $o .= '">'.$item['artikal_status'].'</div>';
      $o .= '</td>';

      $o .= '</tr>';      
  } else {

    }


  }

  $o .= '  </tbody>';
  $o .= '</table>';
  $o .= '</div>';


  }

} else {
  // Nema artikla
  $o .= '<div class="alert alert-lightred text-center input_padding">';
  $o .= '<h3> Nema artikla trenutno </h3>';
  $o .= '<h5><a href="'.ADMINURL.'?page=artikal&options=add_artikal"> Dodajte novi artikal </a></h5>';
  $o .= '</div>';
}
