<?php

// Helper::pre($_GET);

$o .= '<a class="btn btn-info" href="'.ADMINURL.'"> Back to dashboard </a>';
$o .= '<a class="btn btn-info wrap wrap-left" href="'.ADMINURL.'?page=dashboard&options=komesa"> Back to sve komese </a>';

if ( isset($_GET['id']) && $_GET['id']!='' ) {

  $id = $_GET['id'];
  $komesa_data = Database::whereQuery('komesa', array('id'=>$id));
  $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));
  $artikal_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];

  // Helper::pre($komesa_data);
  // Helper::pre($artikal_data);


  $o .= '<form action="'.ADMINURL.'?page=dashboard&options=komesa&id='.$id.'" method="post" class="wrap-bottom">';
  $o .= '<input name="date" id="datepicker" class="pull-left">';
  $o .= '<input id="komesa_datum" class="pull-left btn btn-success input_padding wrap-left wrap-bottom" type="submit" name="komesa_datum" value="Izaberi datum">';

  if ( isset($_POST['date']) ) {
    $date = explode('-', $_POST['date']);
    $year = $date[0];
    $month = $date[1];
    $day = $date[2];
    $full_date = $day.'.'.$month.'.'.$year;
  
    $o .= '<a href="'.ADMINURL.'?page=dashboard&options=komesa&id='.$id.'" class="pull-left btn btn-danger input_padding wrap-left wrap-top">Danasnji datum</a>';
  
  } else {
    $year = date('Y');
    $month = date('m');
    $day = date('d');
    $full_date = date('d.m.Y');

  }

  $o .= '<div class="clearfix"></div>';
  $o .= '</form>';



  $o .= '<hr class="delimiter">';

  $o .= '<blockquote class="wrap-top">';
  $o .= '  <h3 class="sub-header"> Komesa <i>'.$komesa_data[0]['komesa_id'].'</i> </h3>';
  $o .= '  <footer>Artikal <strong><i>'.$artikal_data[0]['artikal_id'].'</i></strong> </footer>';
  $o .= '  <footer>Datum <strong><i> '.$full_date.' </i></strong> </footer>';
  $o .= '</blockquote>';

  $o .= '';
  $o .= '';

  if ( Database::num_rows(Database::returnFetchData($artikal_table)) > 0 ) {
    $artikal = Database::fetchData($artikal_table);
    // Helper::pre($artikal);


    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>#</th>';
    $o .= '      <th>Operacija</th>';
    $o .= '      <th>Naziv operacije</th>';
    $o .= '      <th class="text-center">Komada po komesi</th>';
    $o .= '      <th class="text-center">Ukupno komada</th>';
    $o .= '      <th class="text-center">Danasnji komadi</th>';
    $o .= '      <th></th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';

    $index = 1;

    foreach ($artikal as $item ) {

      $komada_po_artiklu = $item['komada_po_artiklu'];
      $komada_po_operaciji = $item['komada_po_operaciji'];

      if ( Database::num_rows(Database::returnWhereQuery('komesa_data_'.$id, array('operacija_id'=>$item['id'], 'year'=>$year, 'month'=>$month, 'day'=>$day))) > 0 ) {
        $operacija_per_komesa_info_today = Database::whereQuery('komesa_data_'.$id, array('operacija_id'=>$item['id'], 'year'=>$year, 'month'=>$month, 'day'=>$day));
        $today = $operacija_per_komesa_info_today[0]['komada_po_operaciji'];
      } else {
        $today = 0;
      }


      $op_data = Database::whereQuery('operacije', array('operacija_id'=>$item['operacija_id']));

      if ( isset($_POST['date']) ) { $date=$_POST['date']; } else { $date=date('Y-m-d'); }
      
      if ( Database::num_rows(Database::returnWhereQuery('komesa_data_details_'.$komesa_data[0]['id'] , array('operacija_id'=>$op_data[0]['id']))) > 0 ) {
        $ukupno_po_operaciji = Database::whereQuery('komesa_data_details_'.$komesa_data[0]['id'] , array('operacija_id'=>$op_data[0]['id'])); 
        $ukupno_op = $ukupno_po_operaciji[0]['ukupno_komada'];
      } else {
        $ukupno_op = 0;
      }

      $o .= "<tr>";
      $o .= '<td>'.$index.'</td>';
      $o .= '<td>'.$item['operacija_id'].'</td>';
      $o .= '<td>'.$item['operacija_name'].' <br/> <h6>'.$item['opis_operacije'].'</h6></td>';
      $o .= '<td class="text-center">'.$komesa_data[0]['artikal_komada_per_komesa']*$komada_po_artiklu.'</td>';
      $o .= '<td class="alert-success text-center">'.$ukupno_op.'</td>';
      $o .= '<td class="alert-danger text-center">'.$today.'</td>';
      $o .= '</tr>';

      $index++;
    }

    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';


  } else {
    $o .= '<h2 class="alert alert-danger">Nema podesavanja za ovaj artikal </h2>';
  }




} else {
  $o .= '<h2 class="sub-header text-left input_padding">Komese</h2>';
  // SHOW ALLKOMESA
  if ( Database::num_rows("SELECT * FROM `komesa`") > 0 ) {
    $komesa_data = Database::fetchData('komesa');

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>#</th>';
    $o .= '      <th>Komesa ID</th>';
    $o .= '      <th>Komesa Name</th>';
    $o .= '      <th></th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';

    $index = 1;
    foreach ($komesa_data as $item ) {
      $komesa_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('komesa_id'=>$item['id'])));

      $o .= "<tr onclick=\"window.document.location='".ADMINURL."?page=dashboard&options=komesa&id=".$item['id']."';\">";
      $o .= '<td>'.$index.'</td>';
      $o .= '<td>'.$item['komesa_id'].'</td>';
      $o .= '<td>'.$item['komesa_name'].'</td>';
      $o .= '</tr>';

      $index++;
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';


  } else {
    // Nema artikla
    $o .= '<div class="alert alert-lightred text-center input_padding">';
    $o .= '<h3> Nema artikla trenutno </h3>';
    $o .= '<h5><a href="'.ADMINURL.'?page=artikal&options=add_artikal"> Dodajte novi artikal </a></h5>';
    $o .= '</div>';
  }

}

