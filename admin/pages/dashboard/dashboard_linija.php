<?php

// Helper::pre($_GET);


$o .= '<a class="btn btn-info" href="'.ADMINURL.'"> Back to dashboard </a>';

$o .= '<h2 class="sub-header text-left input_padding">Linije</h2>';

if ( Database::num_rows("SELECT * FROM `linija`") > 0 ) {
  $linije_data = Database::fetchData('linija');

  $o .= '<div class="table-responsive">';
  $o .= '<table class="table table-striped">';
  $o .= '  <thead>';
  $o .= '    <tr>';
  $o .= '      <th>Linija ID</th>';
  $o .= '      <th>Linija Name</th>';
  $o .= '      <th class="text-center">Ukupno uradjenih</th>';
  $o .= '      <th class="text-center">Danasnji komadi</th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '    </tr>';
  $o .= '  </thead>';
  $o .= '  <tbody>';

// Helper::pre($linije_data);
  foreach ($linije_data as $item ) {
    if ( $item['linija_id'] == '0' ){
      continue;
    };

    $all_linija_data = Database::whereQuery('working_day_session_details', array('linija_id'=>$item['id']));
    $all_linija_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('linija_id'=>$item['id'])));
    $today_linija_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('work_day'=>date('Y-m-d'), 'linija_id'=>$item['id'])));
    $komesa_data = Database::whereQuery('komesa', array('artikal_id'=>$item['id']));


    $o .= '<tr>';
    $o .= '<td>'.$item['linija_id'].'</td>';
    $o .= '<td>'.$item['linija_name'].'</td>';
    $o .= '<td class="text-center">'.$all_linija_data_count.'</td>';
    $o .= '<td class="text-center">'.$today_linija_data_count.'</td>';
    $o .= '<td></td>';
    $o .= '</tr>';
    $o .= '</a>';
  }
  $o .= '  </tbody>';
  $o .= '</table>';
  $o .= '</div>';
} else {
  // Nema artikla
  $o .= '<div class="alert alert-lightred text-center input_padding">';
  $o .= '<h3> Nema artikla trenutno </h3>';
  $o .= '<h5><a href="'.ADMINURL.'?page=artikal&options=add_artikal"> Dodajte novi artikal </a></h5>';
  $o .= '</div>';
}
