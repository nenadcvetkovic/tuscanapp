<?php

// Helper::pre($_POST);


$o .= '<a class="btn btn-info" href="'.ADMINURL.'"> Back to dashboard </a>';

$o .= '<h2 class="sub-header text-left input_padding">Operacije</h2>';

$o .= '<form method="post" action="'.ADMINURL.'?page=dashboard&options=operacija">';
if ( Database::num_rows( Database::returnFetchData('komesa') ) > 0 )  { // if isset $_GET['komesa']
  $o .=  '<select name="komesa" class="col-xs-6">';
  if ( Database::num_rows( Database::returnFetchData('komesa') ) > 0  ) {
    $komesa_data = Database::fetchData('komesa');
    $o .=  '<option class="disabled" value="0" selected="selected"> Izaberite komesu </option>';
    foreach ($komesa_data as $komesa ) {
      $o .=  '<option name="komesa" value="'.$komesa['id'].'" ';
      if( isset($_POST['komesa']) ? $komesa['id'] == $_POST['komesa'] : false) {
        $o .=  'selected="selected"';
      }
      $o .=  '> '. $komesa['komesa_name'] .' </option>';
    }
  } else {
    $o .=  '<option name="komesa" value="0"> Nema trenutno komesa </option>';
  }

  $o .=  '</select>';
}
$o .= '<input class="col-xs-1 wrap-left btn-info" type="submit" name="submit" value="Go">';
$o .= '<input class="col-xs-1 wrap-left btn-danger" type="reset" name="reset" value="Reset">';
$o .= '</form>';
$o .= '<div class="clear"></div>';


if ( Database::num_rows("SELECT * FROM `operacije`") > 0 ) {

  $sql = "SELECT * FROM `operacije`";
  $count = Database::num_rows($sql);
  // $sql .= ' LIMIT '.$start.','.$item_per_page;
  //
  // $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

  $operacije_data = Database::query($sql);

  $o .= '<div class="table-responsive">';
  $o .= '<table class="table table-striped">';
  $o .= '  <thead>';
  $o .= '    <tr>';
  $o .= '      <th>Operacija ID</th>';
  $o .= '      <th>Operacija Name</th>';
  $o .= '      <th class="text-center">Ukupno uradjenih</th>';
  $o .= '      <th class="text-center">Danasnji komadi</th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '      <th></th>';
  $o .= '    </tr>';
  $o .= '  </thead>';
  $o .= '  <tbody>';

  foreach ($operacije_data as $item ) {

    if ( isset($_POST['komesa']) && $_POST['komesa']!='0' && (Database::num_rows(Database::returnWhereQuery('komesa', array('id'=>$_POST['komesa'])))>0) ) {
      $komesa = $_POST['komesa'];
      $operacija_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('operacija_id'=>$item['id'], 'komesa_id'=>$komesa)));
      $today_operacija_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('working_day'=>date('Y-m-d'), 'operacija_id'=>$item['id'], 'komesa_id'=>$komesa)));
    } else {
      $operacija_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('operacija_id'=>$item['id'])));
      $today_operacija_data_count = Database::num_rows(Database::returnWhereQuery('working_day_session_details', array('working_day'=>date('Y-m-d'), 'operacija_id'=>$item['id'])));
    }

    // Helper::pre(Database::returnWhereQuery('working_day_session_details', array('operacija_id'=>$item['id'])));
    // Helper::pre(Database::returnWhereQuery('working_day_session_details', array('working_day'=>date('Y-m-d'),'operacija_id'=>$item['id'])));

    $o .= '<tr>';
    $o .= '<td>'.$item['operacija_id'].'</td>';
    $o .= '<td>'.$item['operacija_name'].'</td>';
    $o .= '<td class="text-center">'.$operacija_count.'</td>';
    $o .= '<td class="text-center">'.$today_operacija_data_count.'</td>';
    // $o .= '<td>'.$item['artikal_datum_unosa'].'</td>';
    $o .= '<td>';
    $o .= '<td></td>';
    $o .= '</tr>';
    $o .= '</a>';
  }
  $o .= '  </tbody>';
  $o .= '</table>';
  $o .= '</div>';
} else {
  // Nema artikla
  $o .= '<div class="alert alert-lightred text-center input_padding">';
  $o .= '<h3> Nema operacije trenutno </h3>';
  $o .= '<h5><a href="'.ADMINURL.'?page=operacije&options=add"> Dodajte novi artikal </a></h5>';
  $o .= '</div>';
}
