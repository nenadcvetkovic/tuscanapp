<?php

$table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

  $today = date('Y-m-d');
  
	if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>$today))) > 0 ) {
		$all_day_zadaci = Database::whereQuery('komesa_line_settings', array('working_day'=>$today));
		// Helper::pre($all_day_zadaci);

		$o .= '<div class="danasnje_komese">';
		$o .= '<table class="table"> <caption>Danasnje komese</caption>';
		$o .= '<thead>';
		$o .= '<tr>';
		$o .= '<th> # </th>';
		// $o .= '<th>Working Day</th>';
		$o .= '<th>Linija</th>';
		$o .= '<th>Komesa ID</th>';
		$o .= '<th>Artikal ID</th>';
		$o .= '<th>Komada po komesi</th>';
		$o .= '</tr>';
		$o .= '</thead>';

		$o .= '	<tbody>';
		$index = 1;
		foreach ($all_day_zadaci as $komesa) {
			$komesa_info = Database::whereQuery('komesa', array('id'=>$komesa['komesa_id']));
			// Helper::pre($komesa_info);

      		$o .= "<tr onclick=\"window.document.location='".ADMINURL."?page=dashboard&options=komesa&id=".$komesa_info[0]['id']."';\">";
 			$o .= '			<th scope="row">'.$index.'</th>';
			// $o .= '			<td>'.$komesa['working_day'].'</td>';
			$o .= '			<td>'.$komesa['linija_id'].'</td>';
			$o .= '			<td>'.$komesa_info[0]['komesa_id'].'</td>';
			$o .= '			<td>'.$komesa_info[0]['artikal_id'].'</td>';
			$o .= '			<td>'.$komesa_info[0]['artikal_komada_per_komesa'].'</td>';
			$o .= '		</tr>';
			$index++;
		}
		$o .= '	</tbody>';
		$o .= '</table>';
		$o .= '</div>';

	} else {
		$o .= '<div class="alert alert-danger"><h3 class="text-center">Nema komesa za danasnji dan</h3></div>';
	}
