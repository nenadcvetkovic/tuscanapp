<?php
// Helper::pre($_GET);
$o = '';


if ( isset($_GET['options']) && $_GET['options']=='add_radnik' ) {
   if ( file_exists('pages/'.$array['page'].'/'.$_GET['options'].'.php') ) {
     // echo '<h1>',$array['page'],'</h1>';
     require_once 'pages/'.$array['page'].'/'.$_GET['options'].'.php';
   } else {
     require_once 'pages/404.php';
   }
} elseif ( isset($_GET['options']) && $_GET['options']=='edit_radnika' ) {
   if ( file_exists('pages/'.$array['page'].'/'.$_GET['options'].'.php') ) {
     // echo '<h1>',$array['page'],'</h1>';
     require_once 'pages/'.$array['page'].'/'.$_GET['options'].'.php';
   } else {
     require_once 'pages/404.php';
   }
} else {

  $page_links = array(

  );
  $o = '';

  $o .= '<div class="clear"></div>';
  $o .= '<h2 class="sub-header text-left">Radnici</h2>';

  if ( isset($_GET['option']) && $_GET['option']=='loged_user' ) {

    $o .= '<a class="btn btn-info wrap-right wrap-bottom" href="'.ADMINURL.'?page=radnici"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Svi radnici</a>';
    $o .= '<div class="clear"></div>';

    if ( Database::num_rows("SELECT * FROM `working_day_session` WHERE logout_time IS NULL ") > 0 ) {
      $loged_users_data = Database::fetch("SELECT * FROM `working_day_session` WHERE logout_time IS NULL ");
      // Helper::pre($loged_users_data);
      $o .= '<h3>Loged User</h3>';
      $o .= '<input name="radnik_filter" class="filter col-xs-6" type="text" placeholder="Filtriraj radnike">';
      $o .= '<div class="btn btn-danger wrap-left wrap-top reset_input_filter"> RESTE FILTER</div>';

      $o .= '<div class="clearfix"></div>';
      // $o .= '<hr class="delimiter">';
      foreach ( $loged_users_data as $luser ) {
        $user = Database::whereQuery('radnici_lista', array('radnik_id'=>$luser['radnik_id']));
        $o .= '<div class="wrap radnik_filter">';

        $o .= '<a href="'.ADMINURL.'?page=analiza&radnik='.$luser['radnik_id'].'" class="alert btn-info col-md-4 wrap-right">Radnik: ' . $user[0]['radnik_name'] . '</a>';

        $operacija_data = Database::whereQuery('operacije', array('id'=>$luser['operacija_id']));

        $o .= '<button class="alert btn-primary col-md-3 wrap-right text-left">Operacija: ' . $operacija_data[0]['operacija_name'] . '</button>';
        $o .= '<a href="'.ADMINURL.'?page=radnici&option=logout-user&radnik_id='.$luser['radnik_id'].'" class="btn btn-danger" content="Logout" title="Logout user"><i class="fa fa-trash"></i></a>';
        $o .= '<div class="clear"></div>';

        $o .= '</div>';


        // $table_head = array('ID','working Day Session ID', 'Workday', 'Radnik ID', 'Linija', 'Login time', 'Logout time', 'Pausa 1', 'End pausa 1', 'Pausa 2', 'End pausa 2', 'Komesa', 'Artikal', 'Operacija', 'Operacija Hint');
        // $url_array = array(
        //   'Logout User' => ADMINURL.'?page=radnici&option=logout-user&radnik_id='.$luser['radnik_id']
        // );
        // $o .= Table::returnListDataTable( $table_head ,$loged_users_data,$url_array );
      }
    } else {
      // $o .= '<hr class="delimiter">';
      $o .='<div class="clear"></div>';
      $o .= '<div class="text-center alert alert-warning text-center">';
      $o .= '<p>No loged user at themomment</p>';
      $o .= '</div>';
    }

  } elseif ( isset($_GET['option']) && $_GET['option'] == 'logout-user' && isset($_GET['radnik_id']) ) {
    $_SESSION['info_msg'] = 'User '.$_GET['radnik_id'].' had been logout';
    $query = "UPDATE `working_day_session` SET logout_time='".date("Y-m-d H:i:s")."' WHERE radnik_id=".$_GET['radnik_id'];
    if ( Database::query( $query ) ){
      Url::header_status(ADMINURL.'?page=radnici&option=loged_user&status=1');
    } else {
      // echo $query;
      Url::header_status(ADMINURL.'?page=radnici&option=loged_user&status=0');
    }

  } elseif ( isset($_GET['option']) && $_GET['option'] == 'radnik' && isset($_GET['radnik_id']) ) { // Change options for one worker
    // code here
    if ( isset($_GET['back_url']) && $_GET['back_url']!='' ) {
      $o .= '<a class="btn alert-lightgreen wrap-right wrap-bottom" href="'.ADMINURL.urldecode($_GET['back_url']).'"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Nazad na predhodnu stranicu</a>';
    }

    $o .= '<a class="btn btn-success wrap-right wrap-bottom" href="'.ADMINURL.'?page=radnici"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Nazad na sve radnike</a>';
    // $o .= '<hr class="delimiter">';
    // $o .= '<p class="alert alert-warning">Action isset to radnici</p>';

    $o .= '<div class="clear"></div>';

    $radnik_id = $_GET['radnik_id'];
    $radnik_data = Database::whereQuery('radnici_lista', array('id'=>$radnik_id));
    // Helper::pre($radnik_data);
    $o .= '<div class="alert alert-info">';
    $o .= 'Radnik ID: '.$radnik_data[0]['radnik_id'].'<br/>';
    $o .= 'Radnik Name: '.$radnik_data[0]['radnik_name'].'<br/>';
    $o .= 'Radnik od: '.$radnik_data[0]['radnik_lifetime_from'].'<br/>';
    $o .= 'Linija: '.$radnik_data[0]['linija'];
    $o .= '</div>';


      if ( Database::num_rows("SELECT * FROM `working_day_session` WHERE radnik_id = '".$radnik_data[0]['radnik_id']."' LIMIT ". $start.",".$item_per_page) > 0) {
      $time_working = Database::whereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id']), '', $start.','.$item_per_page);
      // echo  Database::returnWhereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id']), '', $start.','.$item_per_page);

      $count = count(Database::whereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id'])));

      $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

      $o .= '<div class="table-responsive">';
      $o .= '<table class="table table-striped">';
      $o .= '  <thead>';
      $o .= '    <tr>';
      $o .= '      <th>Radnik ID</th>';
      $o .= '      <th>Radnik Name</th>';
      $o .= '      <th>Operacija</th>';
      $o .= '      <th>Komesa</th>';
      $o .= '      <th>Artikal</th>';
      $o .= '      <th>Login Time</th>';
      $o .= '      <th>Hintova Po Logovanju</th>';
      $o .= '    </tr>';
      $o .= '  </thead>';
      $o .= '  <tbody>';


      foreach ($time_working as $item ) {
        $o .= '<tr>';
        $o .= '<td>'.$item['radnik_id'].'</td>';
        $o .= '<td>'.$item['radnik_name'].'</td>';
        $o .= '<td>'.$item['operacija_id'].'</td>';
        $o .= '<td>'.$item['komesa'].'</td>';
        $o .= '<td>'.$item['artikal_id'].'</td>';
        $o .= '<td>'.$item['login_time'].'</td>';
        $o .= '<td>'.$item['operacija_hint'].'</td>';
        $o .= '</tr>';
        $o .= '</a>';
      }
      $o .= '  </tbody>';
      $o .= '</table>';
      $o .= '</div>';

      $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    } else {
      $o .= '<div class="alert alert-warning">';
      $o .= 'Nema sacuvanih podataka za radnika sa id : '. $radnik_id;
      $o .= '</div>';
    }
  } else {
    $o .= '<a class="btn btn-success wrap-right wrap-bottom" href="'.ADMINURL.'?page=radnici&option=loged_user">Ulogovani Radnici</a>';

    $sql = 'SELECT * FROM `radnici_lista` WHERE ';
    if ( isset($_GET['pocetak_ugovora']) && $_GET['pocetak_ugovora']) {
      $pocetak_ugovora = $_GET['pocetak_ugovora'];
      $sql .= " radnik_lifetime_from>='".$pocetak_ugovora."'" ;
    } else {
      $pocetak_ugovora = '0';
      $sql .= " radnik_lifetime_from>='".$pocetak_ugovora."'" ;
    }

    if ( isset($_GET['linija']) && $_GET['linija'] != '' && $_GET['linija']!= 0) {
      $linija = $_GET['linija'];
      $sql .= " AND linija='".$linija."'" ;
    }

    $sql .= '';

    $o .= Messages::infoErrorMsg($_SESSION);
    $o .= '<a class="btn btn-info wrap-right wrap-bottom" href="'.ADMINURL.'?page=radnici&options=add_radnik"><span class="fa fa-plus"></span> Dodaj novog radnika</a>';


    // filter box

    $o .= '<div class="filter-container alert-info">';
    $o .= '<form action="'. ADMINURL . '?page=radnici" method="get">';
    $o .= '<input name="page" value="radnici" hidden="hidden"/>';
    $o .= '    <div class="container">';
    $o .= '      <div class="row">';
    $o .= '        <!-- <span class=""> Izaberi datum pocetka rada</span></br> -->';
    $o .= '        <div class="col-xs-12">';
    $o .= '          <div class="filter-box">';
    $o .= '            <input class="col-xs-12 col-sm-3 col-md-3 col-lg-2" type="text" name="pocetak_ugovora" id="datepicker-1" value="';
    $o .= (isset($_GET['pocetak_ugovora']) ? $_GET['pocetak_ugovora'] : '' );
    $o .= '" placeholder="Izaberite datum pocetka ugovora">';


    $o .= '          </div>';

    $o .= '          <!-- Linija -->';
    $sve_linije = Database::fetchData('linija');
    $o .= '          <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="linija">';
    $o .= '            <option value="0"';
    if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) {
      $o .= 'selected="selected"';
    }
    $o .= '          >Izaberi liniju</option>';
    foreach ($sve_linije as $linija) {
      $o .= '<option value="'.$linija['id'].'"';
      if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
        $o .= 'selected="selected"';
      }
      $o .= '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
    }
    $o .= '          </select>';
    $o .= '          <div class="clear"></div>';

    $o .= '          <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />';
    $o .= '          <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->';
    $o .= '          <a class="btn btn-warning wrap" href="'. ADMINURL .'?page=radnici">Reset Filter</a>';
    $o .= '        </div>';
    $o .= '      </div>';
    $o .= '    </div>';
    $o .= '  </form>';

    $o .= '</div> <!-- filter-box -->';
    $o .= '<div class="filter-btn-visible btn btn-info wrap-right wrap-bottom pull-right">Show filter options</div>';

    $o .= '<div class="clear"></div>';
    // end filter box
    $radnici_list = 0;

    // Helper::pre($sql);

    if ( isset($_GET['pocetak_ugovora']) && $_GET['pocetak_ugovora']!='' ||
          isset($_GET['linija']) && $_GET['linija']!= '' &&  ( Database::fetch( $sql ) != 0 ) ) {
            if ( Database::fetch($sql) == 0 ) {
              $o .= '<div class="alert alert-danger wrap wrap">';
              $o .= 'Nema rezultata za zeljenu pretragu';
              $o .= '</div>';
            } elseif ( Database::fetch( $sql ) != 0 ) {
              $radnici_list = Database::fetch( $sql );
            }
    } else {
      // $radnici_list = Database::fetchData('radnici_lista');
      $linija_result = Database::fetchData('linija');
      // Helper::pre($linija_result);

      $o .= '<ul class="list-group">';
      foreach ($linija_result as $litem) {
        $o .= '<li class="list-group-item alert-grey"> Linija '.$litem['linija_id'].'</li>';

        if ( Database::num_rows(Database::returnWhereQuery('radnici_lista', array('linija'=>$litem['linija_id']))) > 0 ) {
          $radnici_result = Database::whereQuery('radnici_lista', array('linija'=>$litem['linija_id']));
          // Helper::pre($radnici_result);

          // $table_head = array('ID','Radnik ID', 'Radnik Name', 'Radnik od', 'Linija', 'Sifra');
          // $url_array = array(
          //   'Details' => ADMINURL.'?page=komesa&options=edit',
          //   'Delete'  => ADMINURL.'?page=komesa&options=delete'
          // );
          // $o .= Table::returnListDataTable( $table_head ,$radnici_result,$url_array );


          $o .= '<div class="table-responsive">';
          $o .= '<table class="table table-striped">';
          $o .= '  <thead>';
          $o .= '    <tr>';
          $o .= '      <th>Radnik ID</th>';
          $o .= '      <th>Radnik Name</th>';
          $o .= '      <th>Zaposljen</th>';
          $o .= '      <th>Disable %</th>';
          $o .= '      <th>Link</th>';
          $o .= '      <th>Operacija</th>';
          $o .= '    </tr>';
          $o .= '  </thead>';
          $o .= '  <tbody>';
          foreach ($radnici_result as $item ) {
            $o .= '<tr>';
            $o .= '<a href="#">';
            $o .= '<td>'.$item['radnik_id'].'</td>';
            $o .= '<td>'.$item['radnik_name'].'</td>';
            $o .= '<td>'.$item['radnik_lifetime_from'].'</td>';
            $o .= '<td>'.$item['btn_disable_time'].' %</td>';
            // $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&option=radnik&radnik_id='.$item['id'].'">Detailss</a></td>';
            $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&options=edit_radnika&id='.$item['id'].'"> <i class="fa fa-pencil-square-o"></i> </a></td>';
            $o .= '<td><a class="btn btn-danger" href="'.ADMINURL.'?page=radnici&option=delete&radnik_id='.$item['id'].'"> <i class="fa fa-trash"></i> </a></td>';
            $o .= '</a>';
            $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=stat&options=count-stat&radnik_id='.$item['id'].'">Statistika</a></td>';

            $o .= '</tr>';
          }
          $o .= '  </tbody>';
          $o .= '</table>';
          $o .= '</div>';

        } else {
          $o .= '<div class="alert alert-lightred text-center"> Nema dodatih radnika za ovu liniju </div>';
        }

        $o .= '<a href="'.ADMINURL.'?page=linije&options=edit&id='.$litem['linija_id'].'&back_url='.urlencode('?page=radnici').'" class="btn btn-success wrap-top wrap-bottom"> <i class="fa fa-info-circle"></i> Linija '.$litem['linija_id'].' </a>';
      }
      $o .= '</ul>';
    }
  // die();



  if ( $radnici_list != 0 ) {

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>Radnik ID</th>';
    $o .= '      <th>Radnik Name</th>';
    $o .= '      <th>Zaposljen</th>';
    $o .= '      <th>Procenat efikasnosti</th>';
    $o .= '      <th>Link</th>';
    $o .= '      <th>Operacija</th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';
    foreach ($radnici_list as $item ) {
      $o .= '<tr>';
      $o .= '<a href="#">';
      $o .= '<td>'.$item['radnik_id'].'</td>';
      $o .= '<td>'.$item['radnik_name'].'</td>';
      $o .= '<td>'.$item['radnik_lifetime_from'].'</td>';
      $o .= '<td>'.$item['procenat_efficasnosti'].'</td>';
      $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&option=radnik&radnik_id='.$item['id'].'">Detailss</a></td>';
      $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&option=delete&radnik_id='.$item['id'].'">Delete</a></td>';
      $o .= '</a>';
      $o .= '</tr>';
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';


    $o .= '<hr/>';

  }

  }


  echo $o;


}



?>
