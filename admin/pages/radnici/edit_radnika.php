<?php

// [id] => 1
// [radnik_id] => 101
// [radnik_name] => Isidora Bjelica
// [radnik_lifetime_from] => 2015-05-13 00:00:00
// [linija] => 1
// [sifra] => 0

if ( !empty($_GET) && isset($_GET['id']) ) {
  $id = (int)$_GET['id'];


  if ( !empty($_POST) ) {
    if ( isset($_POST['radnik_id']) &&
        isset($_POST['radnik_name']) &&
        isset($_POST['radnik_lifetime_from']) &&
        isset($_POST['linija']) &&
        isset($_POST['sifra']) &&
        isset($_POST['btn_disable_time'])
        ){
      // Helper::pre($_POST);

      $radnik_id = $_POST['radnik_id'];
      $radnik_name = $_POST['radnik_name'];
      $radnik_lifetime_from = $_POST['radnik_lifetime_from'];
      $linija = $_POST['linija'];
      $btn_disable_time = $_POST['btn_disable_time'];
      $sifra = $_POST['sifra'];

      $data_array = array(
        'radnik_id'             => $radnik_id,
        'radnik_name'           => $radnik_name,
        'radnik_lifetime_from'  => $radnik_lifetime_from,
        'linija'                => $linija,
        'btn_disable_time'      => $btn_disable_time,
        'sifra'                 => $sifra
      );
      // UPDATE table_name SET column1=value, column2=value2,... WHERE some_column=some_value
      if ( Database::updateData('radnici_lista', $data_array, array('id'=>$id))){
        $msg = 'Data is updated!';
      } else {
        $msg = 'Error inserting data!'.' '.Database::returnUpdateData('radnici_lista', $data_array, array('id'=>$id));
      }
    } else {
      Url::header_status(ADMINURL.'?page=404');
    }
  }

  $o = '';

  $radnik = Database::whereQuery('radnici_lista', array('id'=>$_GET['id']));
  // Helper::pre($radnik);
  //
  // $radnici_list_shema = Database::query("SHOW COLUMNS FROM `radnici_lista`");
  // foreach ($radnici_list_shema as $item ) {
  //   echo $item['Field'].',';
  // }
  $o .= '<a href="'.ADMINURL.'?page=radnici"><i class="fa fa-angle-double-left"></i> Radnici</a>';

  $o .= '<h2 class="sub-header">Detalji radnika</h2>';
  if ( !empty($msg) ) {
    $o .= '<div class="alert alert-danger text-center input_padding">'.$msg.'</div>';
    $msg='';
  }

  $o .= '<form class="radnik_data_update" action="'.ADMINURL.'?page=radnici&options=edit_radnika&id='.$id.'" method="post">';

  $o .= '<label class="col-xs-4 text-center input_padding"> Radnik ID </label><input class="col-xs-6 text-right" name="radnik_id" type="text" placeholder="Radnik ID" value="'.$radnik[0]['radnik_id'].'">';

  $o .= '<label class="col-xs-4 text-center input_padding"> Radnik Name  </label><input class="col-xs-6 text-right" name="radnik_name" type="text" placeholder="Radnik Name" value="'.$radnik[0]['radnik_name'].'">';

  $o .= '<label class="col-xs-4 text-center input_padding"> Zaposljen </label><input class="col-xs-6 text-right" id="datepicker-1" name="radnik_lifetime_from" type="datetime" placeholder="Zaposljen od" value="'.$radnik[0]['radnik_lifetime_from'].'">';

  $o .= '<label class="col-xs-4 text-center input_padding"> Linija </label>';
  $o .= '<select id="select_liniju" class="col-xs-6 text-right" class="select_liniju" name="linija">';
  $o .= '  <optgroup label="linija_group">';
  $o .= '    <option value="default" disabled selected="selected">Select Liniju</option>';
      $linija_data = Database::fetchData('linija');
      foreach ($linija_data as $item) {
        # code...
        $o .= '<option value="'.$item['linija_id'].'"';
        if ( $item['linija_id'] == $radnik[0]['linija']) {
          $o .= ' selected="selected"';
        }
        $o .= '>'.$item['linija_name'].'</option>';
      }
  $o .= '  </optgroup>';
  $o .= '</select>';
  $o .= '<label class="col-xs-4 text-center input_padding"> Sifra </label><input class="col-xs-6 text-right" name="sifra" type="text" placeholder="Sifra" value="'.$radnik[0]['sifra'].'">';

  $o .= '<label class="col-xs-4 text-center input_padding"> Procenat </label><input class="col-xs-6 text-right" id="btn_disable_time" name="btn_disable_time" type="number" placeholder="btn_disable_time" value="'.$radnik[0]['btn_disable_time'].'">';

  $o .= '<div class="clear"></div>';
  $o .= '<div class="text-center"><input type="submit" class="btn btn-info col-xs-offset-7" name="submit" value="Update Data"></div>';
  $o .= '</form>';


  echo $o;

}
