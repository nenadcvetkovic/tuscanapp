<?php
$o = '';

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'edit':
      require_once 'edit_masina.php';
      break;
    case 'add':
      require_once 'add_masina.php';
      break;
    case 'details':
      require_once 'details_masina.php';
      break;
    case 'insert_masinu':
      require_once 'insert_masinu.php';
      break;
    case 'delete':
      // $id = isset($_GET['id']) ? $_GET['id'] : null;
      // if ( $id != null ) {
      //   //  return data for artikal id
      //   if ( $komesa_data = Database::whereQuery('komesa', array('id'=>$id) ) ) {
      //     // delete from database table artikal
      //     $query = "DELETE FROM `komesa` WHERE id = '{$id}'";
      //     if ( Database::query($query) ) {
      //       $o .= 'Uspelo je';
      //       // delete database table for artikal details table
      //       Url::header_status(ADMINURL.'?page=komesa');
      //     }
      //
      //   } else {
      //     // return error
      //   }
      // }
      break;
    default:
      # code...
      break;
  }

} else {

  $o .= '<h1 class="sub-header">Sve masine</h1>';
  $o .= '<div class="breadcrumb">';
  $o .=   '<a href="'.ADMINURL.'?page=masine&options=add" class="btn btn-small btn-info wrap-right"> Add masinu </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  $o .= '</div>';


  if ( Database::num_rows(Database::returnFetchData('masine')) > 0) {
    $masine_data = Database::fetchData('masine');
    // echo  Database::returnWhereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id']), '', $start.','.$item_per_page);

    $count = count($masine_data);


    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>id</th>';
    $o .= '      <th>Masnina name</th>';
    $o .= '      <th></th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';

    $index = 1;
    foreach ($masine_data as $item ) {
    //Helper::pre($operacija_data);
      $o .= '<tr>';
      $o .= '<td>'.$item['id'].'</td>';
      $o .= '<td>'.$item['masina_name'].'</td>';
      // $o .= '<td>'.$item['converted_tablet_ip'].'</td>';
      // $o .= '<td><a href="'.ADMINURL.'?page=masine&options=edit&id='.$item['id'].'" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a></td>';
      // $o .= '<td><a href="'.ADMINURL.'?page=masine&options=edit&masina_id='.$item['id'].'" class="btn btn-info"><i class="fa fa-list"></i></a></td>';
      // $o .= '<td><a href="'.ADMINURL.'?page=masine&options=delete&id='.$item['id'].'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
      $o .= '<td>';
      $o .= '<a href="'.ADMINURL.'?page=masine&options=edit&id='.$item['id'].'" class="btn btn-info wrap-right"><i class="fa fa-pencil-square-o"></i></a>';
      $o .= '<a href="'.ADMINURL.'?page=masine&options=edit&masina_id='.$item['id'].'" class="btn btn-info wrap-right"><i class="fa fa-list"></i></a>';
      $o .= '<a href="'.ADMINURL.'?page=masine&options=delete&id='.$item['id'].'" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
      $o .= '</td>';
      $o .= '</tr>';
      $o .= '</a>';
      $index++;
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

  } else {
    $o .= '<div class="alert alert-warning">';
    // $o .= 'Nema sacuvanih podataka za radnika sa id : '. $radnik_id;
    $o .= '</div>';
  }

}

echo $o;
