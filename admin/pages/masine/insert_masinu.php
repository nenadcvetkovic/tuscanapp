<?php
//
// Helper::pre($_POST);
// die();

if ( isset($_POST) ) {

  $array_data = $_POST;

  $insert_data_array = array(
    'masina_name' => $_POST['masina_name'],
    'datum_dodavanja' => date('Y-m-d H-i-m')
  );

  // Helper::pre($insert_data_array);
  // Helper::pre(Database::num_rows(Database::returnInsertData('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip']))));
  // Helper::pre(Database::num_rows(Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip']))));

  //if ( Database::num_rows( Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$array_data['converted_tablet_ip'])) ) == 0 ) {

  if ( Database::num_rows(Database::returnWhereQuery('masine', array('masina_name'=>$_POST['masina_name']))) == 0 ) {
    // die('nema takvih ovde');
    if ( $last_id = Database::insert_data('masine', $insert_data_array) ) {

      // create new tablet db table
      $sql = "CREATE TABLE IF NOT EXISTS masina_data_".$last_id."( ".
      "id INT NOT NULL AUTO_INCREMENT, ".
      "operacija_id VARCHAR(11) NULL, ".
      "operacija_datum_dodavanja DATETIME NULL , ".
      "PRIMARY KEY ( id )); ";
      if ( Database::query($sql) ) {
        Url::header_status(ADMINURL.'?page=masine&options=edit&masina_id='.$last_id);
      }

    } else {
      $_SESSION['error_page_msg'] = '';
      unset($_SESSION['error_page_msg']);
      $_SESSION['error_page_msg'] = 'Error inserting new ip into database!';
    }
    Url::header_status(ADMINURL.'?page=masine');

  } else {
    // die('Ima takvih');
    $_SESSION['error_page_msg'] = '';
    unset($_SESSION['error_page_msg']);
    $_SESSION['error_page_msg'] = 'Postoji masina sa ovim imenom';
    // Helper::pre(Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$insert_data_array['tablet_ip'], 'converted_tablet_ip'=>$insert_data_array['converted_tablet_ip'])));
    // die('Ima ove ip adrese');
    Url::header_status(ADMINURL.'?page=masine');

  }


}
