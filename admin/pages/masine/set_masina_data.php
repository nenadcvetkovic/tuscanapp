<?php

  if ( isset($_POST) && !empty($_POST) ) {
    // Helper::pre($_POST);

    $masina_id = $_GET['masina_id'];
    $tablet_table = 'masina_data_'.$masina_id;

    if ( !empty($_POST['operacija']) ) {
      $operacija_array = $_POST['operacija'];

      foreach ($operacija_array as $operacija) {
        # code...
        if ( Database::num_rows(Database::returnWhereQuery($tablet_table, array('operacija_id'=>$operacija))) > 0 ) {
          // ima takvih operacija
        } else {
          $key_value_data = array(
            'operacija_id'          => $operacija,
            'operacija_datum_dodavanja' => date("Y-m-d H:i:s"),
          );
          Database::insert_data($tablet_table, $key_value_data);

        }
      }
    }

    // die();
  }

  if ( isset($_GET['masina_id']) && Database::num_rows("SHOW TABLES LIKE 'masina_data_".$_GET['masina_id']."'") == 1 ) {

    $o .= '<form method="post">';
    $o .= '<div class="clear"></div>';

    $masina_id = $_GET['masina_id'];
    $masina_data = Database::whereQuery('masine', array('id'=>$masina_id));

    $o .= '<h2 class="sub-header input_padding text-left"> Edit masinu '.$masina_data[0]['masina_name'].'</h2>';
    $o .= Messages::infoErrorMsg($_SESSION);

    $o .= '<a class="btn btn-primary" href="'.ADMINURL.'?page=masine" class=""><i class="fa fa-angle-double-left"></i> Povratak na sve masine </a>';

      if ( isset($_GET['back_url']) ) {
        $o .= '<a class="btn btn-warning wrap-left" href="'.ADMINURL.'?'.$_GET['back_url'].'" class=""><i class="fa fa-angle-double-left"></i> Povratak na predhodnu stranu</a>';
      }
    $o .= '<hr class="delimiter">';

    $o .= '<div class="panel panel-default">';
    $o .= '  <div class="panel-heading">';
    $o .= '     <h3> Operacije </h3>';
    $o .= '  </div>';
    // $o .= '  <div class="panel-body">';
    // $o .= '  </div>';
    $o .= '     <ul class="list-group">';

    $new_table = 'masina_data_'.$masina_id;

    if ( Database::num_rows("SELECT * FROM `masina_data_".$masina_id."`") > 0 ) {
      $sve_operacije = Database::fetch("SELECT * FROM `masina_data_".$masina_id."`");
      // Helper::pre($sve_operacije);
      $index = 1;
      foreach ($sve_operacije as $operacija) {
        if ( Database::num_rows(Database::returnWhereQuery('operacije', array('operacija_id'=>$operacija['operacija_id']))) > 0 ) {
          $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija['operacija_id']));
          // Helper::pre($operacija_data);
          // $o .= '<div class="alert alert-info">'.$operacija['operacija_id'].' Ime Operacije: '.$operacija['opis_operacije'].'</div>';
          $o .= '  <li class="list-group-item list-group-item-info">';
          $o .= '<span class="col-xs-1 text-center"> '.$index . ' </span>';
          // $o .= $operacija['id'] . ' | ';
          $o .= '<a href="'.ADMINURL.'?page=operacije&options=edit&id='.$operacija_data[0]['id'].'&back_url='. urlencode('?page=ip_settings&options=edit&tablet_id=28').'">';
          $o .= '<span class="col-xs-1 text-center"> '.$operacija_data[0]['operacija_id'].'</span> ';
          $o .= '<span class="col-xs-9 text-left">Operacija: '.$operacija_data[0]['opis_operacije'].'</span>';
          $o .= '</a>';
          // $o .= '<span class="col-xs-3 text-center"> '.$operacija_data[0]['tempo_operacije'].'</span>';
          $o .= '<a href="'.ADMINURL.'?page=ip_settings&options=delete_masini_operaciju&masina_id='.$masina_id.'&operacija_id='.$operacija_data[0]['id'].'" data-tablename="'.$new_table.'" value ="'.$operacija_data[0]['id'].'" class="remove_operaciju pull-right glyphicon glyphicon-remove" aria-hidden="true"></a>';
          $o .= '<div class="clear"></div>';
          $o .= '</li>';
          $index++;
        } else {
          // $o .= 'Nema';
        }
      }
      if ( $index == 1 ) {
        $o .= '<div class="alert alert-warning">Nisu dodate opercije za ovau masinu</div>';
      }
    } else {
      $o .= '<div class="alert alert-warning">Nisu dodate opercije za ovau masinu</div>';
    }
    $o .= '     </ul>';
    $o .= '</div>';
    $o .= '<div class="select-box-container"></div>';
    $o .= '<div class="clear"></div>';
    $o .= '<a id="add_operations_to_masinu" class="btn btn-primary">Dodaj operacije masini</a>';

    $o .= '<div class="clear"></div>';
    $o .= '<input class="add_data_for_artikal pull-right btn btn-primary input_padding" type="submit" name="add_data_for_artikal" value="Sacuvaj Podesavanja" />';
    $o .= '</form>';


  } else {
    $o .= '<a class="btn btn-primary" href="'.ADMINURL.'?page=masine" class=""><i class="fa fa-double-angle"></i> Povratak na sve masine </a>';
    $o .= '<p class="alert alert-danger text-center">Nema tabele za ovau masinu >> << </p>';
  }
  ?>
