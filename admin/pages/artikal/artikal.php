<?php
// Helper::pre($_SESSION);
$o = '';

// Back link

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'add_artikal':
      require_once 'add_artikal.php';
      break;
    case 'edit':
      require_once 'edit_artikal.php';
      break;
    case 'delete_artikal':
      $id = isset($_GET['id']) ? $_GET['id'] : null;
      if ( $id != null ) {
        //  return data for artikal id
        if ( $artikal_data = Database::whereQuery('artikal', array('id'=>$id) ) ) {
          // delete from database table artikal
          $query = "DELETE FROM `artikal` WHERE id = '{$id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            // delete database table for artikal details table
            $artikal_query = "DROP TABLE `artikal_data_".$artikal_data[0]['artikal_id']."_".$id."`";
            if ( Database::query($artikal_query) ) {
              $_SESSION['info_page_msg'] = 'Artikal '.$id.' je uspesno izbrisan';
              Database::query("DROP TABLE `artikal_data_".$artikal_data[0]['artikal_id']."_".$id."_color`");
              Database::query("DROP TABLE `artikal_data_".$artikal_data[0]['artikal_id']."_".$id."_size`");

              Url::header_status(ADMINURL.'?page=artikal');
            } else {
              $_SESSION['error_page_msg'] = 'Artikal '.$id.' nije izbrisan';
              Url::header_status(ADMINURL.'?page=artikal');
            }
          }

        } else {
          // return error
        }
      }
      break;
    default:
      # code...
      break;
  }

} else {
  // $artikal_list = Database::fetchData('artikal');
  // $o .= '<h2 class="sub-header text-left input_padding">Artikal</h2>';
  // $o .= '<a class="btn btn-info add_btn text-center add_artikal_btn" href="'.ADMINURL.'?page=artikal&options=add_artikal"><i class="fa fa-plus"></i> <span class="show-text"> Dodaj Novi Artikal </span></a>';
  //
  // $table_head = array('ID','Artikal ID', 'Artikal Name', 'Artikal Desk', 'Datum Unosa', 'Status');
  // $url_array = array(
  //   'Edit' => ADMINURL.'?page=artikal&options=edit',
  //   'Details' => ADMINURL.'?page=artikal&options=details',
  //   'Delete' => ADMINURL.'?page=artikal&options=delete_artikal'
  // );
  // $o .= Table::returnListDataTable( $table_head ,$artikal_list, $url_array );

  $o .= '<h2 class="sub-header text-left input_padding">Artikal</h2>';
  $o .= '<a class="btn btn-info add_btn text-center add_artikal_btn" href="'.ADMINURL.'?page=artikal&options=add_artikal"><i class="fa fa-plus"></i> <span class="show-text"> Dodaj Novi Artikal </span></a>';

    $o .= '<div class="clearfix"></div>';

    $o .= '<input type="text" id="filter_artikal_val" class="col-xs-6" placeholder="Pretrazi ARTIKLE">';
    $o .= '<div class="wrap-top btn btn-danger wrap-left reset_artikal">Reset</div>';

    $o .= '<div class="clear"></div>';

  if ( Database::num_rows("SELECT * FROM `artikal`") > 0 ) {
    $artikal_data = Database::fetchData('artikal');

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>ID</th>';
    $o .= '      <th>Artikal ID</th>';
    $o .= '      <th>Artikal Name</th>';
    $o .= '      <th>Opis artikla</th>';
    $o .= '      <th>Linija rada</th>';
    // $o .= '      <th>Datum unosa</th>';
    $o .= '      <th>Status</th>';
    $o .= '      <th></th>';
    $o .= '      <th></th>';
    $o .= '      <th></th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';


    foreach ($artikal_data as $item ) {
      $o .= '<tr class="table_row">';
      $o .= '<td>'.$item['id'].'</td>';
      $o .= '<td>'.$item['artikal_id'].'</td>';
      $o .= '<td>'.$item['artikal_name'].'</td>';
      $o .= '<td>'.$item['artikal_desc'].'</td>';
      $o .= '<td><span class="btn btn-info">';
      if ( $item['linija_rada'] == NULL) {
        $o .= '0';
      } else {
        $o .= $item['linija_rada'];
      }
      $o .= '</span></td>';
      // $o .= '<td>'.$item['artikal_datum_unosa'].'</td>';
      $o .= '<td>';
      $o .= '<div class="btn';
      if ($item['artikal_status'] == 0 ) {
        $o .= ' btn-danger';
      } elseif ( $item['artikal_status'] == 1 ) {
        $o .= ' btn-success';
      }
      $o .= '">'.$item['artikal_status'].'</div>';
      $o .= '</td>';
      $o .= '<td></td>';
      $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=artikal&options=edit&id='.$item['id'].'"> <i class="fa fa-pencil-square-o"></i> </a></td>';
      // $o .= '<td><a class="btn btn-success" href="'.ADMINURL.'?page=artikal&options=details&id='.$item['id'].'"> <i class="fa fa-list"></i> </a></td>';
      $o .= '<td><a class="btn btn-danger" href="'.ADMINURL.'?page=artikal&options=delete_artikal&id='.$item['id'].'"> <i class="fa fa-trash"></i> </a></td>';
      $o .= '</tr>';
      $o .= '</a>';
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';
  } else {
    // Nema artikla
    $o .= '<div class="alert alert-lightred text-center input_padding">';
    $o .= '<h3> Nema artikla trenutno </h3>';
    $o .= '<h5><a href="'.ADMINURL.'?page=artikal&options=add_artikal"> Dodajte novi artikal </a></h5>';
    $o .= '</div>';
  }


}

echo $o;

?>
