<?php
// if isset artikal id then go and change th world :-D

if ( isset($_GET['id']) ) {
  // Helper::pre($_GET);
  $colors = Database::fetchData('colors');
  $sql = "INSERT INTO table_name (color_id, color_datum_dodavanja, color_status) VALUES";
  foreach ($colors as $item) {
    // Helper::pre($item);
    $sql .= " ( ".$item['id'].",".date('Y-m-d').", 0),";
  }
  $sql = rtrim($sql, ',');
  Database::query($sql);
  // Helper::pre($sql);
  $o = '';

  $o .= '<a href="'.ADMINURL.'?page=artikal"><i class="fa fa-angle-double-left"></i> Svi artikli</a>';

  $o .= '<form method="post">';
  $o .= '<div class="clear"></div>';

  $o .= '<h2 class="sub-header input_padding text-left"> EDIT artikal '. $_GET['id'].' </h2>';


  $artikal_id = $_GET['id'];

  if ( Database::num_rows("SELECT * FROM `artikal` WHERE id='$artikal_id'") > 0 ) {
    // $artikal_array_data = Database::fetch("SELECT * FROM `artikal` WHERE id='$artikal_id'")
    $artikal_array_data = Database::whereQuery('artikal', array('id'=>$artikal_id));
    $new_table = "artikal_data_".$artikal_array_data[0]['artikal_id']."_".$artikal_array_data[0]['id'];
    // Helper::pre($artikal_array_data);
    // Helper::pre($new_table);
    // POST DATA

    $o .= '';
    $o .= '<div class="pull-right">';
    $o .= '<div class="onoffswitch">';
    $o .= '    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"';
    if ( $artikal_array_data[0]['artikal_status'] == '1' ) {
       $o .= ' checked';
    }
    $o .= '>';
    $o .= '    <label class="onoffswitch-label" for="myonoffswitch">';
    $o .= '        <span class="onoffswitch-inner"></span>';
    $o .= '        <span class="onoffswitch-switch"></span>';
    $o .= '    </label>';
    $o .= '</div>';
    $o .= '</div>';


    if ( !empty($_POST) && isset($_POST) && $_POST != '' ) {
      // Helper::pre($_SESSION);
      // Helper::pre($artikal_array_data);
      // Helper::pre($new_table);
      // id,operacija_id,opis_operacije, tempo_operacije, kolicina_po_operaciji
      // Helper::pre($_POST);
      // die();

      if ( isset($_POST['edit_tempo_operacije']) && $_POST['edit_tempo_operacije']!='' ) {
        $edit_operacije = $_POST['edit_tempo_operacije'];
        $table_name = $new_table;
        $error = 0;
        // Helper::pre($edit_operacije);
        foreach ($edit_operacije as $key => $value) {
          $key_value_data =array('tempo_operacije'=>$value);

          if (Database::updateData($table_name, $key_value_data, array('operacija_id'=>$key))){
            //
          } else {
            $error++;
          }
        }
        if ( $error == 0 ) {
          echo '<h4 class="input_padding alert-success text-center wrap-top">Uspesno sacuvani podaci</h4>';
        } else {
          echo '<h4 class="input_padding alert-danger text-center wrap-top">podaci nisu sacuvani</h4>';
        }
      }
      if ( isset($_POST['edit_komada_po_operaciji']) && $_POST['edit_komada_po_operaciji']!='' ) {
        $edit_komada_po_operaciji = $_POST['edit_komada_po_operaciji'];
        $table_name = $new_table;
        $error = 0;
        // Helper::pre($edit_operacije);
        foreach ($edit_komada_po_operaciji as $key => $value) {
          $key_value_data =array('komada_po_operaciji'=>$value);
          // Helper::pre(Database::returnUpdateQuery($table_name, $key_value_data, array('id'=>$key)));

          if (Database::updateData($table_name, $key_value_data, array('operacija_id'=>$key))){
            //
          } else {
            $error++;
          }
        }
        if ( $error == 0 ) {
          echo '<h4 class="input_padding alert-success text-center wrap-top">Uspesno sacuvani podaci</h4>';
        } else {
          echo '<h4 class="input_padding alert-danger text-center wrap-top">podaci nisu sacuvani</h4>';
        }
      }

      if ( isset($_POST['edit_komada_po_artiklu']) && $_POST['edit_komada_po_artiklu']!='' ) {
        $edit_komada_po_artiklu = $_POST['edit_komada_po_artiklu'];
        $table_name = $new_table;
        $error = 0;
        // Helper::pre($edit_operacije);
        foreach ($edit_komada_po_artiklu as $key => $value) {
          $key_value_data =array('komada_po_artiklu'=>$value);
          // Helper::pre(Database::returnUpdateQuery($table_name, $key_value_data, array('id'=>$key)));

          if (Database::updateData($table_name, $key_value_data, array('operacija_id'=>$key))){
            //
          } else {
            $error++;
          }
        }
        if ( $error == 0 ) {
          echo '<h4 class="input_padding alert-success text-center wrap-top">Uspesno sacuvani podaci</h4>';
        } else {
          echo '<h4 class="input_padding alert-danger text-center wrap-top">podaci nisu sacuvani</h4>';
        }
      }


      if ( isset($_POST['operacija']) ){
        $post_operacija_id = $_POST['operacija'];
        $key_value_data = array();
        foreach ($post_operacija_id as $item) {
          $res = Database::whereQuery('operacije', array('operacija_id'=>$item) );
          // Helper::pre(Database::returnWhereQuery('operacije', array('operacija_id'=>$item)));
          $key_value_data = array_merge($key_value_data, $res);
        }

        if ( isset($key_value_data[0]) && is_array($key_value_data[0]) ) {
          $index = 0;
          foreach ($key_value_data as $item) {
            $array = array();
            foreach ($item as $key => $value) {
              # code...
              reset($key_value_data[0]);
              $first_key = key($key_value_data[0]);
              next($key_value_data[0]);

              if ( $first_key == $key ) {
                //
                $array = array_merge($array,array('operacija_id'=>$value));
              } else {
                $new_array = array($key=>$value);
                $array = array_merge($array,$new_array);
              }
              if ( $_POST['tempo_operacije'][$index] != '' || $_POST['tempo_operacije'][$index]!= '' ) {
                $array['tempo_operacije'] = $_POST['tempo_operacije'][$index];
              }
              if ( $_POST['komada_po_operaciji'][$index] != '' || $_POST['komada_po_operaciji'][$index]!= '' ) {
                $array['komada_po_operaciji'] = $_POST['komada_po_operaciji'][$index];
              }
              if ( $_POST['komada_po_artiklu'][$index] != '' || $_POST['komada_po_artiklu'][$index]!= '' ) {
                $array['komada_po_artiklu'] = $_POST['komada_po_artiklu'][$index];
              }
            }

            if (  Database::num_rows(Database::returnWhereQuery($new_table, array('operacija_id'=>$array['operacija_id']))) == 0 ) {
              // nema operacije ubacene sa ovim podacima
              // Helper::pre(Database::returnWhereQuery($new_table, array('operacija_id'=>$array['operacija_id'])));
              $res = Database::insert_data($new_table, $array);
              // Helper::pre(Database::returnInsertData($new_table, $array));
              // echo "<script>alert('Inserted');</script>";
            } else {
              // ima operacije ubacene sa ovim podacima
              // preskaci ubacivanje u bazu
              // ovde se zezamo malo i plezimo :-P
              echo "<script>alert('Postoji operacija sa ovim imenom. Dodajte drugu operaciju');</script>";
            }
            $index++;
          }

        } else {

        }
        // die('Database cheking.....');

        if ( true ) { // check if isset operacija in table data
          // Helper::pre($key_value_data);
          // Helper::pre(Database::insert_data($new_table, $key_value_data));
        } else {
          Helper::pre('Vec ima unesena operacija kao sto je ova');
        }

      }
      if ( isset($_POST['color_options']) ) {
          $color_options = $_POST['color_options'];
          if ( Database::query('UPDATE `'.$new_table.'_color` SET color_status="0" ') ) {
            // echo '<script>alert("Color status set to 0");</script>';
            // $sql = "UPDATE `".$new_table."_color` SET color_status = '1'";
            foreach ($color_options as $color) {
                // Helper::pre($color);
                Database::query("UPDATE `".$new_table."_color` SET color_status = '1' WHERE color_id='".$color."'");
                // Helper::pre("UPDATE `".$new_table."_color` SET color_status = '1' WHERE color_id='".$color."'");
            }
          }
      }
      if ( isset($_POST['size_options']) ) {
          $size_options = $_POST['size_options'];
          if ( Database::query('UPDATE `'.$new_table.'_size` SET size_status="0" ') ) {
            // echo '<script>alert("Size status set to 0");</script>';
            // $sql = "UPDATE `".$new_table."_color` SET color_status = '1'";
            foreach ($size_options as $size) {
                // Helper::pre($color);
                Database::query("UPDATE `".$new_table."_size` SET size_status = '1' WHERE size_id='".$size."'");
                // Helper::pre("UPDATE `".$new_table."_color` SET color_status = '1' WHERE color_id='".$color."'");
            }
          }
      }
      if ( isset($_POST['onoffswitch']) ) {
        // Helper::pre($_POST['onoffswitch']);
        // die('Ima');
        Database::query("UPDATE `artikal` SET artikal_status = '1' WHERE id='".$artikal_id."'");
      } else {
        // die('Nema');
        Database::query("UPDATE `artikal` SET artikal_status = '0' WHERE id='".$artikal_id."'");
      }

    } else {
      // Helper::pre('Nema podataka trenutno!!!');
    }

    // $o .= 'New table for artikal is <i>'.$new_table.'</i>';

    $table_tructure = array(
      'id',
      'operacija_id',
      'opis_operacije',
      'tempo_operacije'
    );
    // Helper::pre($artikal_array_data[0]);

    //
    $o .= '<div class="alert alert-info">';
    $o .= 'ID: '.$artikal_array_data[0]['id'].'<br/>';
    $o .= 'Artikal ID: '.$artikal_array_data[0]['artikal_id'].'<br/>';
    $o .= 'Artikal name: '.$artikal_array_data[0]['artikal_name'].'<br/>';
    $o .= 'Linija rada: Linija '.$artikal_array_data[0]['linija_rada'].'<br/>';
    $o .= 'Datum unosa: '.date('Y-m-d', strtotime($artikal_array_data[0]['artikal_datum_unosa']));
    $o .= '</div>';

    $o .= '<div class="clear"></div>';

    //

    $o .= '<div class="panel panel-default">';
    $o .= '  <div class="panel-heading">';
    $o .= '     <h3> Operacije </h3>';
    $o .= '  </div>';
    // $o .= '  <div class="panel-body">';
    // $o .= '  </div>';
    $o .= '     <ul class="list-group">';
    if ( Database::num_rows("SELECT * FROM `".$new_table."`") > 0 ) {
      $sve_operacije = Database::fetch("SELECT * FROM `".$new_table."`");
      // Helper::pre($sve_operacije);
      $index = 1;
      if ( $sve_operacije != 0 ) {

        $o .= '<div class="table-responsive">';
        $o .= '<table class="table table-strriped">';
        $o .= '  <thead>';
        $o .= '    <tr>';
        $o .= '      <th class="col-xs-2 text-left">Operacija ID</th>';
        $o .= '      <th class="col-xs-4 text-left">Opis operacije</th>';
        $o .= '      <th class="col-xs-2 text-left">Tempo</th>';
        $o .= '      <th class="col-xs-2 text-left">Komada po operaciji</th>';
        $o .= '      <th class="col-xs-2 text-left">Komada po artiklu</th>';
        $o .= '      <th class="col-xs-1 text-left"></th>';
        $o .= '    </tr>';
        $o .= '  </thead>';
        $o .= '  <tbody class="alert-info">';
        foreach ($sve_operacije as $operacija ) {
          if ( Database::num_rows(Database::returnWhereQuery('operacije', array('operacija_id'=>$operacija['operacija_id']))) > 0 ) {
            $o .= '<tr>';
            // $o .= '<td class="input_padding">'.$operacija['operacija_id'].'</td>';
            // $o .= '<td class="input_padding">'.$operacija['opis_operacije'].'</td>';
            // $o .= '<td><input class="col-xs-6  tempo_operacije" type="text" name="edit_tempo_operacije['.$operacija['operacija_id'].']" value="'.$operacija['tempo_operacije'].'" placeholder="Tempo" ></td>';
            // $o .= '<td><input class="col-xs-6  komada_po_operaciji" type="text" name="edit_komada_po_operaciji['.$operacija['komada_po_operaciji'].']" value="'.$operacija['komada_po_operaciji'].'" placeholder="Kom/operacija" ></td>';
            // $o .= '<span data-tablename="'.$new_table.'" value ="'.$operacija['id'].'" class="remove_operaciju pull-right glyphicon glyphicon-remove" aria-hidden="true"></span>';

            $o .= '<td>';
            $o .= '<span class="col-xs-2 text-center"> '.$operacija['operacija_id'].'</span> ';
            $o .= '</td>';

            $o .= '<td>';
            $o .= '<span class="col-xs-10 text-left">Operacija: <br/> '.$operacija['opis_operacije'].'</span>';
            $o .= '</td>';

            $o .= '<td>';
            $o .= '<input class="col-xs-12  tempo_operacije" type="text" name="edit_tempo_operacije['.$operacija['operacija_id'].']" value="'.$operacija['tempo_operacije'].'" placeholder="Tempo" >';
            $o .= '</td>';

            $o .= '<td>';
            $o .= '<input class="col-xs-12  komada_po_operaciji" type="text" name="edit_komada_po_operaciji['.$operacija['operacija_id'].']" value="'.$operacija['komada_po_operaciji'].'" placeholder="Kom/operacija" >';
            $o .= '</td>';

            $o .= '<td>';
            $o .= '<input class="col-xs-12  komada_po_artiklu" type="text" name="edit_komada_po_artiklu['.$operacija['operacija_id'].']" value="'.$operacija['komada_po_artiklu'].'" placeholder="Kom/artikal" >';
            $o .= '</td>';

            $o .= '<td>';
            $o .= '<span data-tablename="'.$new_table.'" value ="'.$operacija['id'].'" class="remove_operaciju pull-right glyphicon glyphicon-remove" aria-hidden="true"></span>';

            $o .= '</td>';
            $o .= '<div class="clear"></div>';

            $o .= '</tr>';
          }

                $index++;

        }
        $o .= '  </tbody>';
        $o .= '</table>';
        $o .= '</div>';


        $o .= '<hr/>';

      }

      if ( $index == 1 ) {
        $o .= '<div class="alert alert-warning">Nisu dodate opercije za ovaj artikal</div>';
      }
    } else {
      $o .= '<div class="alert alert-warning">Nisu dodate opercije za ovaj artikal</div>';
    }
    $o .= '     </ul>';
    $o .= '</div>';

    // $o .= '<h3>Dodaj operacije za ovaj artikal</h3>';

    $url = ADMINURL.'?page=artikal&options=edit&id='.$artikal_array_data[0]['id'];
    // $o .= Helper::pre($url);




    $o .= '<div class="select-box-container">';
    $o .= '</div>';
    $o .= '<div class="clear"></div>';

    $o .= '<a id="set_artikal_data_options" class="btn btn-success btn-small input_padding"><i class="fa fa-plus" aria-hidden="true"></i> Dodaj operacije za ovaj artikal</a>';

    $o .= '<div class="clear"></div>';

    $o .= '<div class="color_options col-xs-6 wrap-top btn-groupp" data-toggle="buttons">';
    $o .= '<h4 class="sub-header">Izaberite boju za artikal</h4>';
    // Helper::pre();
    if ( $all_colors = Database::fetchData('colors') ) {

      // Helper::pre($all_colors);
      foreach ($all_colors as $color) {
        $o .= '<label class="col-xs-1 btn';
        if ( Database::num_rows(Database::returnWhereQuery($new_table.'_color', array('color_id'=>$color['id'],'color_status'=>'1'))) == 1 ) {
          $o .= ' active';
        }
        $o .= '" style="background-color:#'.$color['color_value'].'">';
          $o .= '<input type="checkbox" name="color_options[]" id="'.$color['color_name'].'" autocomplete="on"';
          if ( Database::num_rows(Database::returnWhereQuery($new_table.'_color', array('color_id'=>$color['id'],'color_status'=>'1'))) == 1 ) {
            $o .= ' checked';
          }
          $o .= ' value="'.$color['id'].'">';
          $o .= '<span class="glyphicon glyphicon-ok"></span>';
        $o .= '</label>';
      }
    } else {
      // if ( $all_colors = Database::fetchData('colors') ) {
      //   foreach ($all_colors as $color) {
      //     $o .= '<label class="col-md-2 btn" style="background-color:#'.$color['color_value'].'">';
      //       $o .= '<input type="checkbox" name="color_options[]" id="green" autocomplete="off" value="'.$color['id'].'">';
      //       $o .= '<span class="glyphicon glyphicon-ok"></span>';
      //     $o .= '</label>';
      //   }
      // }
    }
    $o .= '</div>';

    $o .= '<div class="color_options col-xs-12 wrap-top btn-groupp" data-toggle="buttons">';
    $o .= '<h4 class="sub-header">Izaberite velicinu za artikal</h4>';

    if ( $all_sizes = Database::fetchData('sizes') ) {
      // Helper::pre($all_colors);
      foreach ($all_sizes as $size) {
        $o .= '<label class="col-xs-1 btn';
        if ( Database::num_rows(Database::returnWhereQuery($new_table.'_size', array('size_id'=>$size['id'],'size_status'=>'1'))) == 1 ) {
          $o .= ' active';
        }
        $o .= '" style="background-color:#eee">';
          $o .= '<input type="checkbox" name="size_options[]" id="'.$size['size_name'].'" autocomplete="on"';
          if ( Database::num_rows(Database::returnWhereQuery($new_table.'_size', array('size_id'=>$size['id'],'size_status'=>'1'))) == 1 ) {
            $o .= ' checked';
          }
          $o .= ' value="'.$size['id'].'">';
          $o .= '<span class="glyphicon glyphicon-ok"></span>';
          $o .= '<h5>'.$size['size_name'].'</h5>';
        $o .= '</label>';
      }
    } else {
    }
    $o .= '</div>';


    $o .= '<div class="clear"></div>';
    $o .= '<hr class="delimiter">';
    $o .= '<input class="add_data_for_artikal pull-right btn btn-primary input_padding" type="submit" name="add_data_for_artikal" value="Sacuvaj Artikal" />';
    // $o .= '<input class="col-xs-12 input_padding" id="colorpicker" />';

    $o .= '</form>';

  } else {
    $o .= '<div class="alert alert-danger text-center">';
    $o .= '<h2>Trenutno nema artikla sa ovim podacima</h2>';
    $o .= '<br/>';
    $o .= '<a class="btn btn-info wrap" href="'.ADMINURL.'?page=artikal">Vratite se na sve artikle</a>';
    $o .= '</div>';
  }

} else {
  $o .= '<p>No id isset for now</p>';
}
?>
