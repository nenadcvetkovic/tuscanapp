<?php

echo '<a href="'.ADMINURL.'?page=options" class="btn btn-success"> Options </a>';
echo '<hr class="delimiter">';
// first check if table detail_eff_table exists
// 

if ( Database::num_rows("SHOW TABLES LIKE 'detail_eff_table'") == 0 ) {
    // CRETE TABLE IF NOT EXIST
    // 
    $sql = "CREATE TABLE IF NOT EXISTS `detail_eff_table` (

      `id` int(11) NOT NULL auto_increment,   
      `table_name` varchar(14)  NULL,     
      `table_status` int(11) NOT NULL default '0',
       PRIMARY KEY  (`id`)

    )";
    // if (Database::query($sql)) {
    if (true) {
        echo '<script>alert("Uspesno kreirana tabela detail_eff_table");</script>';
    }

}

$start_month = 1;
$start_year = 2017;

for ( $i=$start_year; $i<=date('Y'); $i++) {
    if ( $i != date('Y') ) {
        $max_month = 12;
    } else {
        $max_month = date('m');
    }

    for ( $m=$start_month; $m<=$max_month; $m++) {
        Helper::pre($i . ' = ' . $m);

        if ( Database::num_rows("SHOW TABLES LIKE `detail-".$m."-".$i."`") == 0  ) {
            Helper::pre('Nema tabele');

            $sql = "CREATE TABLE IF NOT EXISTS `detail-".$m."-".$i."` (

              `id` int(11) NOT NULL auto_increment,   
              `datum` varchar(2)  NULL,     
              `radnik_id` int(11) NULL ,       
              `ukupna_eff`  int(3) NULL,     
              `eff_status` int(11) NOT NULL default '0',
               PRIMARY KEY  (`id`)

            )";

            // if (Database::query($sql)) {
            if ( true ) {
                echo Helper::custom_script("Uspesno kreirana tabela detail-'.$m.'-'.$i.' ");
            } else {
                echo Helper::custom_script("Nije uspesno kreirana tabela detail-'.$m.'-'.$i.' ");
            }

        } else {
            echo Helper::custom_script("Ima tabele");
        }

    }
}
