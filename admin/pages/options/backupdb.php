<?php

/*

// mysqldump code for exporting //
mysqldump -u username -p database_to_backup > backup_name.sql

// mysqldump code for restoring  //
mysql -u username -p database_name < backup_name.sql

*/
$o = '';
$o .= '<a class="btn" href="'.ADMINURL.'?page=options"> BAck to options </a>';

// Detalji url tj get varibale
// Helper::pre($_GET);
// Return current page url / full url
// $o .= '<code>'.Url::returnCurrentPageUrl().'</code>';

backup_tables('localhost','root','root','blog');

/* backup the db OR just a table */
function backup_tables($host,$user,$pass,$name,$tables = '*')
{

	$link = mysqli_connect($host,$user,$pass);
	mysqli_select_db($name,$link);

	//get all of the tables
	if($tables == '*')
	{
		$tables = array();
		$result = mysqli_query('SHOW TABLES');
		while($row = mysqli_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}

	//cycle through
	foreach($tables as $table)
	{
		$result = mysqli_query('SELECT * FROM '.$table);
		$num_fields = mysqli_num_fields($result);

		$return.= 'DROP TABLE '.$table.';';
		$row2 = mysqli_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";

		for ($i = 0; $i < $num_fields; $i++)
		{
			while($row = mysqli_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j < $num_fields; $j++)
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j < ($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
	}

	//save file
	$handle = fopen('db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
	fwrite($handle,$return);
	fclose($handle);
}

$filename='database_backup_'.date('G_a_m_d_y').'.sql';

$result=exec('mysqldump blog --password=root --user=root --single-transaction >/var/backups/'.$filename,$output);

if($output==''){
  /* no output is good */
  echo 'jesss';
} else {
  /* we have something to log the output here*/
  echo 'Noooo';
}


// Main part of action
if ( isset($_GET) && isset($_GET['action']) ) {
    if ( $_GET['action'] == 'komesa' ) {
      if ( isset($_GET['id']) ) {
        $komesa_id = $_GET['id'];


        // Detalji komese
        $komesa_data = Database::whereQuery('komesa', array('id'=>$komesa_id));
        // Helper::pre($komesa_data);

        $o .= '<div class="alert alert-info">';
        $o .= '<h4>Detalji komese '.$komesa_data[0]['id'].'</h4>';
        $o .= '<hr class="delimiter">';
        $o .= 'Id : '.$komesa_data[0]['id'].'<br/>';
        $o .= 'Komesa ID : '.$komesa_data[0]['komesa_id'].'<br/>';
        $o .= 'Komesa Name : '.$komesa_data[0]['komesa_name'].'<br/>';
        $o .= 'Komesa datum unosa : '.$komesa_data[0]['komesa_datum_unosa'].'<br/>';
        $o .= '</div>';

        $o .= '<div class="alert alert-info">';
        $o .= '<h4>Detalji artikla za komesu '.$komesa_data[0]['komesa_name'].'</h4>';
        $o .= '<hr class="delimiter">';
        $o .= 'Artikal ID : '.$komesa_data[0]['artikal_id'].'<br/>';
        $o .= 'Broj komada po artiklu : '.$komesa_data[0]['artikal_komada_per_komesa'].'<br/>';
        $o .= '</div>';

        // Detalji artikla koji se nalazi u komesi
        $artikal_data = Database::whereQuery('artikal', array('id'=>$komesa_data[0]['artikal_id']));
        // Helper::pre($artikal_data);


        $artikal_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];
        // $o .=  $artikal_table;
        //  Detalji operacija koje su vezane za artikal iz komese
        $artikal_table_data = Database::fetchData($artikal_table);
        // Helper::pre($artikal_table_data);



        $o .= '<div class="alert alert-info">';
        $o .= '<h4>Operacije koje se nalaze u artiklu</h4>';
        $o .= '<hr class="delimiter">';
        foreach ($artikal_table_data as $operacija) {
          $o .= 'ID: '.$operacija['id'].' | ';
          $o .= 'Operacija ID: '.$operacija['operacija_id'].' | ';
          $o .= 'Opis operacije: '.$operacija['opis_operacije'].' | ';
          $o .= 'Tempo Operacije: '.$operacija['tempo_operacije'].' | ';
          $o .= 'Kolicina po operaciji: '.$operacija['kolicina_po_operaciji'].'<br/>';
          $o .= '<hr class="delimiter">';

          // $o .= Database::returnWhereQuery('working_day_session', array('komesa'=>$komesa_id, 'artikal_id'=>$komesa_data[0]['artikal_id'], 'operacija_id'=>$operacija['id']) );

          $sum_operacije_list = Database::whereQuery('working_day_session', array('komesa'=>$komesa_id, 'artikal_id'=>$komesa_data[0]['artikal_id'], 'operacija_id'=>$operacija['id']) );

          $array_hints = array();
          $hints_value = 0;
          foreach ($sum_operacije_list as $item) {
            $hints_value += $item['operacija_hint'];
            $array_hints = array( "operacija_".$item['operacija_id']."_hints" => $hints_value);
          }
          $percent = ( $hints_value * 100 ) / $komesa_data[0]['artikal_komada_per_komesa'];
          $o .= Helper::progressBar($percent);
        }
        // Helper::pre($array_hints);
        $o .= '</div>';

        $o .= '<hr/>';

        $o .= $komesa_data[0]['artikal_komada_per_komesa'];

        $o .= '<hr/>';

        $o .= '<h1>Test</h1>';

        $list = Database::whereQuery('working_day_session', array('komesa'=>$komesa_id,'artikal_id'=>$komesa_data[0]['artikal_id']) );
        // Helper::pre($list);
        foreach ($list as $item) {
          $o .= '<div class="alert alert-info">WSD ID: '.$item['id'].' | OPERACIJA ID: '.$item['operacija_id'].' | HINTS: '.$item['operacija_hint'].'</div>';
        }

      } else {
        $o .= '<p>No id isset</p>';
      }
    } elseif ( $_GET['action'] == 'artikal' ) {
      $artikal_id = $_GET['id'];
      $o .= '<p>Action isset to artikal</p>';
      $artikal_data = Database::whereQuery('artikal', array('id'=>$artikal_id));
      // Helper::pre($artikal_data);
      $o .= 'Detalji artikla sa ID: '.$artikal_data[0]['artikal_id'].' Ime: '.$artikal_data[0]['artikal_name'];

      $o .= '<div class="alert alert-info">';
      $o .= '<h4>Opis artikla</h4>';
      $o .= '<p class="alert alert-info">';
      $o .= $artikal_data[0]['artikal_desc'];
      $o .= '<hr class="delimiter">';
      $o .= '<h4> Datum unosa</h4>';
      $o .= $artikal_data[0]['artikal_datum_unosa'];
      $o .= '</p>';

      $o .= '</div>';

      $new_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];
      // $o .= $new_table;
      $artikal_table_data = Database::fetchData($new_table);
      // Helper::pre($artikal_table_data);
      foreach ($artikal_table_data as $table) {
        $o .= '<div class="alert alert-info">'.$table['opis_operacije'].' || '.$table['operacija_id'].'</div>';
      }

      $o .= '<hr class="delimiter">';

      $o .= '<a class="btn btn-danger" href="'.ADMINURL.'?page=set_artikal&id='.$artikal_id.'">Promeni podatke za artikal</a>';

    } elseif ( $_GET['action'] == 'radnici' ) {
      $o .= '<p>Action isset to radnici</p>';

      $radnik_id = $_GET['id'];
      $radnik_data = Database::whereQuery('radnici_lista', array('id'=>$radnik_id));
      Helper::pre($radnik_data);

      $time_working = Database::whereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id']));

      // Helper::pre($time_working);

      foreach ($time_working as $item) {
        $o .= $item['id'].'='.$item['radnik_id'].' | '.$item['operacija_id'].' | '.$item['radni_broj'].' | '.$item['artikal_id'].$item['operacija_hint'].' | '.$item['login_time'].' | '.$item['logout_time'];
        $o .= '<hr/>';
      }

    } elseif ( $_GET['action'] == 'linija' ) {
      $sve_linije_url = ADMINURL.'?page=linije';

      if ( isset($_GET['id']) ) {
        $linija_id = $_GET['id'];
        $linija_data = Database::whereQuery('linija', array('id'=>$linija_id));
        if (  $linija_data >= 1 ) {
          $o .= '<a class="btn btn-danger" href="'.$sve_linije_url.'"> Back to all lines </a>';
          $o .= '<hr class="delimiter">';
          $o .= 'Action is isset to Linija';

          $o .= $linija_data[0]['id'] . ' - ' . $linija_data[0]['linija_id'] . ' - ' . $linija_data[0]['linija_name']. ' </br/>';

        } else {
          $o .= '<div class="alert alert-danger">';
          $o .= 'Linija ID is not set.';
          $o .= '<a class="btn btn-danger" href="'.$sve_linije_url.'"> Back to all lines </a> to choose Line ID';
          $o .= '</div>';
        }
      } else {
        $o .= '<div class="alert alert-danger">';
        $o .= 'Linija ID is not set.';
        $o .= '<a class="btn btn-danger" href="'.$sve_linije_url.'"> Back to all lines </a> to choose Line ID';
        $o .= '</div>';
      }
    }


} else {
  $o .= '<p>No action isset</p>';
}


echo $o;
