<?php
$o = '';

if ( isset($_GET['options']) && $_GET['options']!='') {
  if ( $_GET['options'] == 'edit' ) {
    require_once 'edit_linija.php';
  }

} elseif ( isset($_GET['options']) && $_GET['options']=='' ) {
  Url::header_status(ADMINURL.'?page=404');
} else {

  $o .= '<h2 class="sub-header text-left input_padding">Linije</h2>';

  $linije_list = Database::fetchData('linija');


  $linije_count = count($linije_list);
  if ( $linije_count > 0 ) {

    $table_head = array( 'ID','Linija ID', 'Linija Name', '#' );
    $url_array = array(
      'Details' => ADMINURL.'?page=linije&options=edit'
    );
    $o .= Table::returnListDataTable( $table_head ,$linije_list,$url_array );

  } else {
    $o .= '<p class="alert alert-danger">There are not records for Linije at the mommnet?</p>';
  }

}



echo $o;
