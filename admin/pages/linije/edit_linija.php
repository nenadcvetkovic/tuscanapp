<?php

if ( isset($_GET['id']) && $_GET['id']!='' ) {
  $id = $_GET['id'];

  $o .= '<h2 class="sub-header text-left input_padding"> Linija '.$id.' </h2>';

  if ( isset($_GET['back_url']) && $_GET['back_url']!='' ) {
    $o .= '  <a href="'.ADMINURL.urldecode($_GET['back_url']).'" class="btn btn-info wrap-bottom"><i class="fa fa-angle-double-left"></i> Povratak na predhodnu stranicu </a>';
  }

  $o .= '  <a href="'.ADMINURL.'?page=linije" class="btn btn-info add_btn wrap-bottom"><i class="fa fa-angle-double-left"></i> <span class="show-text"> Povratak na sve linije </span></a>';

  $linija_info = Database::whereQuery('linija', array('id'=>$id) );

  // Helper::pre($linija_info);

  $o .= '<div class="alert alert-info">';
  $o .= 'Artikal ID: '.$linija_info[0]['linija_id'].'<br/>';
  $o .= 'Artikal name: '.$linija_info[0]['linija_name'].'<br/>';
  $o .= '</div>';

  $o .= '<div class="clear"></div>';
  $all_radnici = Database::whereQuery('radnici_lista', array('linija'=>$id) );
  // Helper::pre($all_radnici);

  // $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

  $o .= '<div class="table-responsive">';
  $o .= '<table class="table table-striped">';
  $o .= '  <thead>';
  $o .= '    <tr>';
  $o .= '      <th>Radnik ID</th>';
  $o .= '      <th>Radnik Name</th>';
  $o .= '      <th>Zaposljen</th>';
  $o .= '      <th>Linija</th>';
  $o .= '      <th>Link</th>';
  $o .= '      <th>#</th>';
  $o .= '    </tr>';
  $o .= '  </thead>';
  $o .= '  <tbody>';
  foreach ($all_radnici as $item ) {
    $o .= '<tr>';
    $o .= '<a href="#">';
    $o .= '<td>'.$item['radnik_id'].'</td>';
    $o .= '<td>'.$item['radnik_name'].'</td>';
    $o .= '<td>'.$item['radnik_lifetime_from'].'</td>';
    $o .= '<td>'.$item['linija'].'</td>';
    $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&option=radnik&radnik_id='.$item['id'].'&back_url='. urlencode('?page=linije&options=edit&id='.$id) .'">Detailss</a></td>';
    $o .= '<td><a class="btn btn-info" href="'.ADMINURL.'?page=radnici&option=delete&radnik_id='.$item['id'].'&back_url=?page=linije&options=edit&id='.$id.'">Delete</a></td>';
    $o .= '</a>';
    $o .= '</tr>';
  }
  $o .= '  </tbody>';
  $o .= '</table>';
  $o .= '</div>';

  // $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);


} else {
  Url::header_status(ADMINURL.'?page=404');
}
