<?php
$o = '';

$o .= '<h2 class="sub-header text-left input_padding"> Edit <i> Operacija '. $_GET['id'].' </i> </h2>';

$o .= '<div class="breadcrumb">';
if ( isset($_GET['back_url']) && $_GET['back_url']!='' ) {
  $o .=  '<a href="'.ADMINURL.urldecode($_GET['back_url']).'" class="btn btn-danger wrap-right"><i class="fa fa-angle-double-left"></i> Povratak na predhodnu stranicu</a>';
}
$o .=  '<a href="'.ADMINURL.'?page=operacije" class="btn btn-info add_btn"><i class="fa fa-angle-double-left"></i> <span class="show-text"> Povratak na sve opercije</span></a>';
$o .= '</div>';

$o .= Messages::infoErrorMsg($_SESSION);

if ( isset($_GET['id']) ) {
  $id = $_GET['id'];
  // Helper::pre($_GET);

  if ( isset($_POST) ) {
    // Helper::pre($_POST);

    if (
      (isset($_POST['operacija_id']) && $_POST['operacija_id']!='' ) &&
      (isset($_POST['operacija_name']) && $_POST['operacija_name']!='' ) &&
      (isset($_POST['opis_operacije']) && $_POST['opis_operacije']!='' ) &&
      (isset($_POST['tempo_operacije']) && $_POST['tempo_operacije']!='' && $_POST['tempo_operacije'] > 0 )
      ) {

        $operacija_id = $_POST['operacija_id'];
        $operacija_name = $_POST['operacija_name'];
        $opis_operacije = $_POST['opis_operacije'];
        $tempo_operacije = $_POST['tempo_operacije'];

        $table_name = 'operacije';
        $key_value_data = array(
          'operacija_id'    => $operacija_id,
          'operacija_name'  => $operacija_name,
          'opis_operacije'  => $opis_operacije,
          'tempo_operacije' => $tempo_operacije
        );
        $where_array = array(
          'id' => $id
        );

        // Helper::pre(Database::returnUpdateQuery($table_name, $key_value_data, $where_array));

        if ( $success = Database::updateData($table_name, $key_value_data, $where_array) ) {
          $_SESSION['info_page_msg'] = 'Operacija uspesno sacuvana';
          $o .= '<h4 class="alert alert-lightblue text-center input_paddings"> Operacije uspesno sacuvan.</h4>';
        } else {
          $o .= '<h2 class="alert alert-lightred text-center input_paddings"> Operacije nije sacuvan. Proverite greske u formi</h2>';
        }
        // Url::header_status(ADMINURL.'?page=operacije&options=edit&id='.$id);
      }

  }


  if ( Database::num_rows(Database::returnWhereQuery('operacije',array('id'=>$id))) > 0 ) {
    $operacija_array_data = Database::fetch(Database::returnWhereQuery('operacije',array('id'=>$id)));
    // POST DATA
    // Helper::pre($operacija_array_data);

    $o .= '<form method="post">';

    $o .= '<div class="col-xs-12 alert alert-primary">';
    $o .= '  <form class="edit_operacija" action="'.ADMINURL.'?page=operacije&options=edit&id='.$id.'" method="post">';
    $o .= '    <div class="col-xs-3 text-left input_padding wrap-10">Operacija ID</div>';
    $o .= '    <input id="operacija_id" class="col-xs-8" type="text" name="operacija_id" value="'. $operacija_array_data[0]['operacija_id'] .'" placeholder="Operacija ID">';

    $o .= '    <div class="col-xs-3 text-left input_padding wrap-10">Ime operacije</div>';
    $o .= '    <input id="opis_operacije" class="col-xs-8" type="text" name="operacija_name" value="'. $operacija_array_data[0]['operacija_name'] .'" placeholder="Opis operacije">';

    $o .= '    <div class="col-xs-3 text-left input_padding wrap-10">Opis operacije</div>';
    $o .= '    <input id="opis_operacije" class="col-xs-8" type="text" name="opis_operacije" value="'. $operacija_array_data[0]['opis_operacije'] .'" placeholder="Opis operacije">';

    $o .= '    <div class="col-xs-3 text-left input_padding wrap-10">Tempo operacije</div>';
    $o .= '    <input id="tempo_operacije" class="col-xs-8" type="number" name="tempo_operacije" value="'. $operacija_array_data[0]['tempo_operacije'] .'" placeholder="Tempo operacije">';

    $o .= '    <div class="clear"></div>';
    $o .= '<hr class="delimiter">';
    $o .= '    <button id="edit_operacija" class="text-center btn btn-warning input-style-btn wrap-top pull-left col-xs-offset-4" type="submit" name="save_operacija" value="1">Save opearaciju</button>';
    $o .= '  </form>';
    $o .= '</div>';


    $o .= '</form>';

  } else {
    $o .= '<div class="alert alert-danger text-center">';
    $o .= '<h2>Trenutno nema operacije sa ovim podacima</h2>';
    $o .= '<br/>';
    $o .= '<a class="btn btn-info wrap" href="'.ADMINURL.'?page=operacija">Vratite se na sve operacije</a>';
    $o .= '</div>';
  }

} else {
  $o .= '<p>No id isset for now</p>';
}
echo $o;
?>
