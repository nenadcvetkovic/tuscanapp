<?php

/*
/ id
/ operacija_id
/ opis_opearacije
/ tempo_operacije ?
*/

$o = '';

$o .= '<h2 class="sub-header text-left input_padding">Dodaj novu operaciju</h2>';
$o .= '<div class="breadcrumb">';
$o .= '  <a href="'.ADMINURL.'?page=operacije" class="btn btn-info add_btn"><i class="fa fa-angle-double-left"></i> <span class="show-text">Back</span></a>';
$o .= '</div>';

$o .= Messages::infoErrorMsg($_SESSION);
// Helper::pre($_POST);

if (  (isset($_POST['operacija_id']) && $_POST['operacija_id']!='' ) &&
      (isset($_POST['operacija_name']) && $_POST['operacija_name']!='' ) &&
      (isset($_POST['opis_operacije']) && $_POST['opis_operacije']!='' )
) {
  if ( isset($_POST)) {
    $operacija_id = $_POST['operacija_id'];
    $operacija_name = $_POST['operacija_name'];
    $opis_operacije = $_POST['opis_operacije'];
    $tempo_operacije = '0';

    $table_name = 'operacije';

    $key_value_data = array(
      'operacija_id' => strtoupper($operacija_id),
      'operacija_name' => $operacija_name,
      'opis_operacije' => $opis_operacije,
      'operacija_datum_dodavanja' => date("Y-m-d H:i:s"),
      'tempo_operacije' => $tempo_operacije
    );

    if ( Database::num_rows(Database::returnWhereQuery('operacije', array('operacija_id'=>$operacija_id) )) == 0 ) {
      // nema takve komese

      if ( Database::insert_data($table_name, $key_value_data) ) {
        // $_SESSION['info_page_msg'] = 'Uspesno dodATA operacija';
        $o .= '<h4 class="alert alert-lightblue input_paddings text-center"> Operacija uspesno sacuvana. </h4>';
      } else {
        // $_SESSION['error_page_msg'] = 'Operacija nije dodata';
        Helper::pre(Database::returnInsertData($table_name, $key_value_data));
        $o .= '<h4 class="alert alert-lightred input_paddings text-center"> Greska prilikom dodavanja operacije </h4>';
      }

    } else {
      // ima takve komese
      // $_SESSION['error_page_msg'] = 'Ima takve operacije';
      $o .= '<h4 class="alert alert-lightred input_paddings text-center"> Ima takve operacije </h4>';
      // Url::header_status(ADMINURL.'?page=add_operacija&i=111');
    }
  }

} else {
}


  $o .= '<form method="post">';

  $o .= '<div class="col-xs-12 alert alert-primary">';
  $o .= '  <form class="edit_operacija" action="'.ADMINURL.'?page=add_operacija" method="post">';
  $o .= '    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left input_padding wrap-10">Operacija ID</div>';
  $o .= '    <input id="operacija_id" class="col-xs-12 col-sm-12 col-md-6 col-lg-8" type="text" name="operacija_id" value="" placeholder="Operacija ID">';
  $o .= '    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left input_padding wrap-10">Ime operacije</div>';
  $o .= '    <input id="opis_operacije" class="col-xs-12 col-sm-12 col-md-6 col-lg-8" type="text" name="operacija_name" value="" placeholder="Ime operacije">';
  $o .= '    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left input_padding wrap-10">Opis operacije</div>';
  $o .= '    <input id="opis_operacije" class="col-xs-12 col-sm-12 col-md-6 col-lg-8" type="text" name="opis_operacije" value="" placeholder="Opis operacije">';
  $o .= '    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left input_padding wrap-10">Tempo operacije</div>';
  $o .= '    <input id="tempo_operacije" class="col-xs-6 col-sm-6 col-md-6 col-lg-8" type="number" name="tempo_operacije" value="" placeholder="Tempo operacije">';
  $o .= '    <div class="clear"></div>';
  $o .= '    <button id="edit_operacija" class="text-center btn btn-warning col-xs-12 col-sm-6 col-md-3 col-lg-3 input-style-btn wrap-top pull-right" type="submit" name="save_operacija" value="1">Save opearaciju</button>';
  $o .= '  </form>';
  $o .= '</div>';


  $o .= '</form>';

  echo $o;
