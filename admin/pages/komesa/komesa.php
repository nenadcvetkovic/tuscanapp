<?php
// Helper::pre($_SESSION);
$o = '';

if ( isset($_GET['options']) && $_GET['options'] != '') {

  switch ( $_GET['options'] ) {
    case 'edit':
      require_once 'edit_komesa.php';
      break;
    case 'add':
      require_once 'add_komesa.php';
      break;
    case 'details':
      require_once 'details_komesa.php';
      break;
    case 'delete':
      $id = isset($_GET['id']) ? $_GET['id'] : null;
      if ( $id != null ) {
        //  return data for artikal id
        if ( $komesa_data = Database::whereQuery('komesa', array('id'=>$id) ) ) {
          // delete from database table artikal
          $query = "DELETE FROM `komesa` WHERE id = '{$id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            // delete database table for artikal details table
            Url::header_status(ADMINURL.'?page=komesa');
          }

        } else {
          // return error
        }
      }
      break;
    default:
      # code...
      break;
  }

} else {
  if ( $komesa_list = @Database::fetchData('komesa') ) {

    // Helper::pre($komesa_list);


    $o .= '<h2 class="text-left sub-header">Komesa</h2>';
    $o .= '<a href="'.ADMINURL.'?page=komesa&options=add" class="btn btn-info add_btn"><i class="fa fa-plus"></i> <span class="show-text">Add komesu</span></a>';
    $o .= '<div class="clearfix"></div>';

    $o .= '<input type="text" id="filter_komesa_val" class="col-xs-6">';
    $o .= '<div class="wrap-top btn btn-danger wrap-left reset_komesa">Reset</div>';

    $o .= '<div class="clear"></div>';

    $table_head = array('ID','Komesa ID', 'Komesa Name', 'Datum unosa', 'Artikal ID', 'Komada po komesi');
    $url_array = array(
      'Edit' => ADMINURL.'?page=komesa&options=edit',
      // 'Details' => ADMINURL.'?page=komesa&options=details',
      'Delete'  => ADMINURL.'?page=komesa&options=delete'
    );
    $o .= Table::returnListDataTable( $table_head ,$komesa_list,$url_array );

  } else {
    $o .= '<div class="wrap wrap-top alert alert-warning">';
    $o .= '<p>Trenutno nema komese. Dodajte novu komesu</p>';
    $o .= '<a class="btn btn-success" href="'.ADMINURL.'?page=komesa&options=add_komesa"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add komesu </a>';
    $o .= '</div>';
  }
}

echo $o;
