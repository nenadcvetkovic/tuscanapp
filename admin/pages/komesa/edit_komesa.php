<?php
$o = '';

if ( (isset($_GET['id']) && $_GET['id']!='' ) ) {

  $komesa_id = $_GET['id'];

}

if (  (isset($_POST['komesa_name']) && $_POST['komesa_name']!='' ) &&
      (isset($_POST['select_artikal']) && $_POST['select_artikal']!='' ) &&
      (isset($_POST['artikal_komada_po_komesi']) && $_POST['artikal_komada_po_komesi']!='' )
) {
  if ( isset($_POST)) {
    $komesa_name = $_POST['komesa_name'];
    $artikal_id = $_POST['select_artikal'];
    $artikal_komada_per_komesa = $_POST['artikal_komada_po_komesi'];

    $table_name = 'komesa';

    $key_value_data = array(
      'komesa_id' => $komesa_id,
      'komesa_name' => strtoupper($komesa_name),
      'komesa_datum_unosa' => date("Y-m-d H:i:s"),
      'artikal_id' => $artikal_id,
      'artikal_komada_per_komesa' => $artikal_komada_per_komesa
    );

    // Helper::pre(Database::returnUpdateQuery($table_name, $key_value_data));
    // Helper::pre($_POST);
    // die();
    if ( Database::whereQuery('komesa', array('komesa_id'=>$komesa_id) ) != 0 ) {
      // nema takve komese

      if ( Database::updateData($table_name, $key_value_data) ) {
        $_SESSION['info_page_msg'] = 'Uspesno dodATA komesa';
        // die('dodata');
        $_SESSION['info_page_msg'] = 'Izmena je sacuvana';
        Url::header_status(ADMINURL.'?page=komesa&options=edit&id='.$komesa_id.'&x=1');
      } else {
        $_SESSION['error_page_msg'] = 'Komesa nije dodata';
        // Helper::pre(Database::returnInsertData($table_name, $key_value_data));
        // die('nije');
        // Url::header_status(ADMINURL.'?page=add_komesa&i=000');
      }

    } else {
      // ima takve komese
      $_SESSION['error_page_msg'] = 'Nema takve komese';
      echo 'Mrk';
      // Url::header_status(ADMINURL.'?page=komesa&options=edit&id='.$komesa_id);
    }
  }

} else {
  // $o .= '<div class="alert alert-danger">';
  // $o .= 'No data inserted into form';
  // $o .= '</div>';
}

$return_komesa_data = Database::whereQuery('komesa', array('id'=>$komesa_id) );
// Helper::pre($return_komesa_data);
?>
<div class="clear"></div>
  <h2 class="text-left sub-header">Edit Komesu <?php echo $komesa_id; ?></h2>
  <div class="breadcrumb">
    <a href="<?php echo ADMINURL; ?>?page=komesa" class="btn btn-info"><i class="fa fa-angle-double-left"></i> Povratak na sve komese</a>
    <div class="clear"></div>
  </div>

<div id="message_box" class="text-center"></div>
  <form class="add_komesa wrap-bottom" action="<?php echo ADMINURL.'?page=komesa&options=edit&id='.$komesa_id; ?>" method="post">
    <div class="col-xs-3 text-right wrap-10 input_padding"> Komesa ID </div>
    <input id="komesa_id" class="col-xs-8" type="text" name="komesa_id" value="<?php echo $return_komesa_data[0]['komesa_id']; ?>" placeholder="Komesa ID">

    <div class="col-xs-3 text-right wrap-10 input_padding"> Komesa Name </div>
    <input id="komesa_name" class="col-xs-8" type="text" name="komesa_name" value="<?php echo $return_komesa_data[0]['komesa_name']; ?>" placeholder="Komesa Name">

    <div class="col-xs-12">
    <div class="col-xs-3 text-right wrap-10 input_padding"> Artikal </div>
    <select id="select_artikal" class="col-xs-8" class="select_artikal" name="select_artikal">
      <optgroup label="artikal_group">
        <option value="default" disabled>Select Artikal</option>
        <?php
        $artikal_list = Database::fetchData('artikal');
        foreach ($artikal_list as $item) {
          # code...
          echo '<option value="'.$item['id'].'"';
          if ( $return_komesa_data[0]['artikal_id'] == $item['id'] ) {
            echo ' selected="selected"';
          }
          echo '>'.$item['id'].'-'.$item['artikal_id'].'-'.$item['artikal_name'].'</option>';
        }
        ?>
      </optgroup>
    </select>
  </div>

    <div class="col-xs-3 text-right wrap-10 input_padding"> Broj komada po komesi </div>
    <input id="artikal_komada_po_komesi" class="col-xs-8" type="number" name="artikal_komada_po_komesi" value="<?php echo $return_komesa_data[0]['artikal_komada_per_komesa']; ?>" placeholder="Komada Po KomesiS">
    <div class="clear"></div>
    <button id="add_komesa" class="pull-right text-center btn btn-warning col-xs-12 col-sm-6 col-md-6 col-lg-3 input-style-btn wrap-top" type="submit" name="submit" value="1">Azuriraj Komesu</button>
  </form>
<div class="clear"></div>
