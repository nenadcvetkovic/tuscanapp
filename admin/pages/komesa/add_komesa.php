<?php
// Helper::pre($_SESSION);

$o = '';

if (  (isset($_POST['komesa_id']) && $_POST['komesa_id']!='' ) &&
      (isset($_POST['select_artikal']) && $_POST['select_artikal']!='' ) &&
      (isset($_POST['artikal_komada_po_komesi']) && $_POST['artikal_komada_po_komesi']!='' )
) {
  if ( isset($_POST)) {
    // Helper::pre($_POST);
    $komesa_id = $_POST['komesa_id'];
    $komesa_name = isset($_POST['komesa_name']) ? $_POST['komesa_name'] : $komesa_id;
    $artikal_id = $_POST['select_artikal'];
    $artikal_komada_per_komesa = $_POST['artikal_komada_po_komesi'];

    $table_name = 'komesa';

    $key_value_data = array(
      'komesa_id' => $komesa_id,
      'komesa_name' => strtoupper($komesa_name),
      'komesa_datum_unosa' => date("Y-m-d H:i:s"),
      'artikal_id' => $artikal_id,
      'artikal_komada_per_komesa' => $artikal_komada_per_komesa
    );

    if ( Database::whereQuery('komesa', array('komesa_id'=>$komesa_id) ) == 0 ) {
      // nema takve komese

      if ( $last_id = Database::insert_data($table_name, $key_value_data) ) {
        $_SESSION['info_page_msg'] = 'Uspesno dodATA komesa';
        // die('dodata');

        $new_komesa_table = 'komesa_data_'.$last_id;
        // CRETAE NEW TABLE FOR OPERATION COUNT FOR WORKING DAY
        if ( Database::num_rows("SHOW TABLES LIKE '".$new_komesa_table."'") == 0 ) {
          // row doesnt exist
          // create table 
          $table_sql = "CREATE TABLE IF NOT EXISTS `".$new_komesa_table."` (
            `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `operacija_id` int(11) DEFAULT NULL,
            `month` varchar(2) DEFAULT NULL,
            `year` varchar(4) DEFAULT NULL,
            `day` varchar(4) DEFAULT NULL,
            `komada_po_operaciji` int(3) NOT NULL DEFAULT '0'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

          if ( Database::query($table_sql) ) {
            echo '<script> Uspesno kreirana tabela za komesu '.$komesa_id.'</script>';

            $new_komesa_details_table_name = 'komesa_data_details_'.$last_id;


            $new_komesa_details_table = "CREATE TABLE IF NOT EXISTS `".$new_komesa_details_table_name."` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `operacija_id` int(11) DEFAULT NULL,
              `ukupno_komada` int(11) NOT NULL DEFAULT '0',
              `date` DATE DEFAULT NULL,
              `status` BOOLEAN NOT NULL DEFAULT FALSE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            if ( Database::query($new_komesa_details_table) ) {
              Url::header_status(ADMINURL.'?page=komesa');
            } else {
              echo '<scritp>alert("Problem sa kreiranjem tabele za detalje komese!");</script>';
            }
          } else {
            echo '<script> Nije kreirana tabela za novu komesu</script>';
          }
        }

      } else {
        $_SESSION['error_page_msg'] = 'Komesa nije dodata';
        Helper::pre(Database::returnInsertData($table_name, $key_value_data));
        // die('nije');
        // Url::header_status(ADMINURL.'?page=add_komesa&i=000');
      }

    } else {
      // ima takve komese
      $_SESSION['errors']['error_page_msg'] = 'Ima takve komese';
      Url::header_status(ADMINURL.'?page=komesa&options=add&i=111');
    }
  }

} else {
  // $o .= '<div class="alert alert-danger">No data inserted into form</div>';
}


?>
<div class="clear"></div>
  <h2 class="sub-header text-left input_padding">Dodaj novu komesu</h2>
  <a href="<?php echo ADMINURL; ?>?page=komesa" class="btn btn-info"><i class="fa fa-angle-double-left"></i> Povratak na sve komese</a>

  <?php echo Messages::infoErrorMsg($_SESSION); ?>

  <div id="message_box" class="text-center"></div>
  <div class="col-xs-12 col-md-12 alert alert-primary">
    <form class="add_komesa" action="<?php echo ADMINURL . '?page=komesa&options=add'; ?>" method="post">
      <div class="col-xs-3 text-right input_padding">Komesa ID</div><input id="komesa_id" class="col-xs-9 " type="text" name="komesa_id" value="" placeholder="Komesa ID">
      <div class="col-xs-3 text-right input_padding">Komesa Name</div><input id="komesa_name" class="col-xs-9" type="text" name="komesa_name" value="" placeholder="Komesa Name">
      <!-- <input type="text" name="artikal_id" value="" placeholder="Artikal ID"><br/> -->
          <?php
          $o = '';
          // Helper::pre(Database::returnWhereQuery('artikal',array('status'=>1)));
          if ( Database::num_rows(Database::returnWhereQuery('artikal',array('artikal_status'=>1))) == 0 ) {
            $o .= '<div class="col-xs-3 text-right input_padding">izaberite artikal</div>';
            $o .= '<a class="btn btn-warning col-xs-9 input_padding" href="'.ADMINURL.'?page=artikal&options=add_artikal"> Nema trenutno artikla. Dodajte novi artikal </a>';
            $o .= '<div class="clear"></div>';
          } else {
            $o .= '<div class="col-xs-3 text-right input_padding">izaberite artikal</div>';
            $o .=   '<select id="select_artikal" class="col-xs-9" class="select_artikal" name="select_artikal">';
            $o .=     '<optgroup label="artikal_group">';
            $o .=       '<option value="default" disabled selected="selected">Select Artikal</option>';
            $artikal_list = Database::whereQuery('artikal', array('artikal_status'=>'1'));
            foreach ($artikal_list as $item) {
              # code...
              $o .= '<option value="'.$item['id'].'">'.$item['id']."-".$item['artikal_id']."-".$item['artikal_name'].'</option>';
            }
            $o .=     '</optgroup>';
            $o .=   '</select>';
          }
          echo $o;
          $o = '';
          ?>
      <div class="col-xs-3 text-right input_padding">Broj komada po komesi</div>
      <input id="artikal_komada_po_komesi" class="col-xs-9" type="number" name="artikal_komada_po_komesi" value="" placeholder="Komada Po KomesiS">
      <div class="clear"></div>
      <button id="add_komesa" class="text-center btn btn-warning col-xs-12 col-sm-6 col-md-3 col-lg-3 input-style-btn wrap-top pull-right" type="submit" name="submit" value="1">Add KomesA</button>
    </form>
  </div>
