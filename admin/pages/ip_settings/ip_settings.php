<?php

// Helper::pre($_SESSION);
$o = '';

if ( isset($_GET['options']) && $_GET['options']!='' ) {

  // if isset options

  switch ( $_GET['options'] ) {
    case 'add_tablet':
      require_once 'add_tablet.php';
      break;
    case 'insert_tablet':
      require_once 'insert_tablet.php';
      break;
    case 'edit':
      require_once 'edit_tablet.php';
      break;
    case 'details_tablet':
      require_once 'details_tablet.php';
      break;
    case 'delete_tablet_operaciju':
      // echo 'Deleted';
      $tablet_id = $_GET['tablet_id'];

      $id = isset($_GET['operacija_id']) ? $_GET['operacija_id'] : null;
      if ( $id != null ) {
        $tablet_table = 'tablet_data_'.$tablet_id;
        $operacija = Database::whereQuery('operacije', array('id'=>$id));
        //  return data for artikal id
        if ( Database::num_rows(Database::returnWhereQuery($tablet_table, array('operacija_id'=>$operacija[0]['operacija_id']) )) > 0 ) {
          // delete from database table artikal
          $operacija_id = $operacija[0]['operacija_id'];
          $query = "DELETE FROM `tablet_data_".$tablet_id."` WHERE operacija_id = '{$operacija_id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            $sql = "DROP TABLE `tablet_data_".$id."`";
            if (Database::query($sql)) {
              Url::header_status(ADMINURL.'?page=ip_settings&options=edit&tablet_id='.$tablet_id);
            }
            // delete database table for artikal details table
            Url::header_status(ADMINURL.'?page=ip_settings&options=edit&tablet_id='.$tablet_id.'&msg=nije');

          }

        } else {
          // return error
          echo '??';
        }
      }
      break;
    case 'delete_masini_operaciju':
      // echo 'Deleted';
      $masina_id = $_GET['masina_id'];

      $id = isset($_GET['operacija_id']) ? $_GET['operacija_id'] : null;
      if ( $id != null ) {
        $masina_table = 'masina_data_'.$masina_id;
        $operacija = Database::whereQuery('operacije', array('id'=>$id));
        //  return data for artikal id
        if ( Database::num_rows(Database::returnWhereQuery($masina_table, array('operacija_id'=>$operacija[0]['operacija_id']) )) > 0 ) {
          // delete from database table artikal
          $operacija_id = $operacija[0]['operacija_id'];
          $query = "DELETE FROM `masina_data_".$masina_id."` WHERE operacija_id = '{$operacija_id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            Url::header_status(ADMINURL.'?page=masine&options=edit&masina_id='.$masina_id);
          } else {
            // delete database table for artikal details table
            Url::header_status(ADMINURL.'?page=masine&options=edit&masina_id='.$masina_id.'&msg=nije');

          }

        } else {
          // return error
          echo '??';
        }
      }
      break;
    case 'delete':
      $id = isset($_GET['id']) ? $_GET['id'] : null;
      if ( $id != null ) {
        //  return data for artikal id
        if ( $ip_data = Database::whereQuery('tablet_ip_settings', array('id'=>$id) ) ) {
          // delete from database table artikal
          $query = "DELETE FROM `tablet_ip_settings` WHERE id = '{$id}'";
          if ( Database::query($query) ) {
            $o .= 'Uspelo je';
            $sql = "DROP TABLE `tablet_data_".$id."`";
            if (Database::query($sql)) {
              Url::header_status(ADMINURL.'?page=ip_settings&msg=uspeloje');
            }
            // delete database table for artikal details table
            Url::header_status(ADMINURL.'?page=ip_settings&msg=nije');

          }

        } else {
          // return error
        }
      }
      break;
    default:
      # code...
      break;
  }


} else {

  $user_real_adress = Ip::getUserRealIpAdress();
  $converted_tablet_ip = Ip::ReturnConvertedIp(Ip::getUserRealIpAdress());

  $o .= '<h2 class="sub-header text-left input_padding">Ip tablet settings</h2>';

  $o .= Messages::infoErrorMsg($_SESSION);

  // $o .= '<div class="alert alert-lightblue">';
  // $o .= '<h3> Info ovog uredjaja </h3>';
  // $o .= '<hr class="delimiter">';
  // $o .= '<p> Real ip adress: ' . $user_real_adress . ' </p>';
  // $o .= '<p> Converted ip adress: ' . $converted_tablet_ip . ' </p>';
  // $o .= '</div>';


  // if ( Database::num_rows(Database::returnWhereQuery('tablet_ip_settings', array('tablet_ip'=>$user_real_adress, 'converted_tablet_ip' => $converted_tablet_ip ) )) == 0 ) {
  //   $o .= '<p class="text-center alert alert-lightred"> Nema podesavanja za ovu ip adressu</p>';
  // } else {
  //   $o .= '<p class="text-center alert alert-lightred"> Ima podesavanja za ovu ip adressu</p>';
  // }


  $o .= '<div class="breadcrumb">';
  $o .=   '<a href="'.ADMINURL.'?page=ip_settings&options=add_tablet" class="btn btn-small btn-info wrap-right"><i class="fa fa-plus"></i> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  // $o .=   '<a href="#" class="btn btn-small btn-info wrap-right disabled"> Add tablet </a>';
  $o .= '</div>';


  if ( $user_real_adress == '127.0.0.1' || $user_real_adress == '192.168.1.200') {
    // $o .= '<h1 class="alert alert-lightgreen input_padding wrap-top wrap-bottom text-center"> Root access ' . $user_real_adress . '</h1>';

  /*
  / id
  / tablet_id
  / tablet_name
  / linija_id
  / operacija_id
  / operacija_name
  / operacija_desc
  / tablet_ip
  / converted_tablet_ip
  */


  //  svei tableti

  if ( Database::num_rows(Database::returnFetchData('tablet_ip_settings')) > 0) {
    $ip_data = Database::fetchData('tablet_ip_settings');
    // echo  Database::returnWhereQuery('working_day_session', array('radnik_id'=>$radnik_data[0]['radnik_id']), '', $start.','.$item_per_page);

    $count = count($ip_data);

    $o .= '<input type="text" name="filter_ip_class" class="filter_ip_val" value="">';

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

    $o .= '<div class="table-responsive">';
    $o .= '<table class="table table-striped">';
    $o .= '  <thead>';
    $o .= '    <tr>';
    $o .= '      <th>id</th>';
    $o .= '      <th>tablet_id</th>';
    $o .= '      <th>Masina</th>';
    $o .= '      <th>tablet_ip</th>';
    // $o .= '      <th>converted_tablet_ip</th>';
    $o .= '      <th>#</th>';
    $o .= '      <th>#</th>';
    $o .= '    </tr>';
    $o .= '  </thead>';
    $o .= '  <tbody>';

    $index = 1;
    foreach ($ip_data as $item ) {
    //Helper::pre($operacija_data);
      $masina_data = Database::whereQuery('masine',array('id'=>$item['masina']));
      $o .= '<tr class="filter_ip">';
      $o .= '<td>'.$index.'</td>';
      $o .= '<td class="filter_ip_id">'.$item['tablet_id'].'</td>';
      $o .= '<td><a href="'.ADMINURL.'?page=masine&options=edit&masina_id='.$masina_data[0]['id'].'&back_url='.urlencode('page=ip_settings').'">'.$masina_data[0]['masina_name'].'</a></td>';
      $o .= '<td>'.$item['tablet_ip'].'</td>';
      // $o .= '<td>'.$item['converted_tablet_ip'].'</td>';
      $o .= '<td><a href="'.ADMINURL.'?page=ip_settings&options=edit&id='.$item['id'].'" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a></td>';
      $o .= '<td><a href="'.ADMINURL.'?page=ip_settings&options=delete&id='.$item['id'].'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
      $o .= '</tr>';
      $o .= '</a>';
      $index++;
    }
    $o .= '  </tbody>';
    $o .= '</table>';
    $o .= '</div>';

    $o .= Pagination::displayPagination($count, $data['pagination']['item_per_page'], $data['pagination']['prev'], $data['pagination']['next'], $data['pagination']['current_page'], $data['pagination']['url']);

  } else {
    $o .= '<div class="alert alert-warning">';
    // $o .= 'Nema sacuvanih podataka za radnika sa id : '. $radnik_id;
    $o .= '</div>';
  }


  }


}

echo $o;
