<?php

/*
id
tablet_id
tablet_name
linija_id
operacija_id
operacija_name
operacija_desc
tablet_ip
converted_tablet_ip
*/

/* post Database
tablet_id
linija_id
operacija_id
tablet_ip
*/

$o = '';

// Helper::pre(Database::returnWhereQuery('tablet_ip_settings',array('id'=> $_GET['id'])));
// die();

if ( isset($_GET['id']) && ($_GET['id'] != 0) && ($_GET['id'] != '') && (Database::num_rows(Database::returnWhereQuery('tablet_ip_settings',array('id'=>(int)$_GET['id']))) > 0) ) {

// check if isset id from get

$id = (int)$_GET['id'];

if ( !empty($_POST) && isset($_POST) && $_POST!='' ) {
  // Helper::pre($_POST);

  $post = $_POST;
  $array_data = array(
    'tablet_id'   => $_POST['tablet_id'],
    'masina'   => $_POST['masina'],
    'tablet_ip'   => $_POST['tablet_ip']
  );


  if ( Database::updateData('tablet_ip_settings', $array_data, array('id'=>$id) ) ) {
    $o .= "<script>alert('Tablet IP successfuly updated');</script>";
  }
}

// continue with edit page

  $tablet_details = Database::whereQuery('tablet_ip_settings',array('id'=>$id));
  // Helper::pre($tablet_details);

  $o .= '<h1 class="sub-header">Edit tablet </h1>';

  $o .= Messages::infoErrorMsg($_SESSION);

  $o .= '<a class="btn btn-primary" href="'.ADMINURL.'?page=ip_settings" class=""><i class="fa fa-double-angle"></i> Povratak na sve tablete </a>';
  $o .= '<a class="btn btn-warning" href="'.ADMINURL.'?page=ip_settings" class=""><i class="fa fa-double-angle"></i> Povratak na sve tablete </a>';

  $o .= '<form class="add_komesa" action="'.ADMINURL.'?page=ip_settings&options=edit&id='.$id.'" method="post">';

  // tablet id
  $o .= '  <div class="col-xs-12 col-sm-3 text-right input_padding wrap-10">Tablet ID</div>';
  $o .= '   <input id="tablet_id" class="col-xs-12 col-sm-8" type="text" name="tablet_id" value="'.$tablet_details[0]['tablet_id'].'" placeholder="Tablet ID">';


  // linija_id
  $o .= '<div class="col-xs-12 col-sm-3 text-right input_padding wrap-10">Masina</div>';
  $o .=   '<select id="masina" class="col-xs-12 col-sm-8" class="masina" name="masina">';
  $o .=     '<optgroup label="artikal_group">';
  $o .=       '<option value="default" disabled>Izaberite masinu</option>';
  $masine_list = Database::fetchData('masine');
  foreach ($masine_list as $item) {
    # code...
    $o .= '<option value="'.$item['id'].'"';
    if ( $item['id'] == $tablet_details[0]['masina']) {
      $o .= ' selected="selected"';
    }
    $o .= '> [ Masina ID: '.$item['id'].' ] [ Masina Name : ' .$item['masina_name']. ' ] </option>';
  }
  $o .=     '</optgroup>';
  $o .=   '</select>';

  // tablet id
  $o .= '  <div class="col-xs-12 col-sm-3 text-right input_padding wrap-10">IP Adressa tableta</div>';
  $o .= '   <input id="tablet_id" class="col-xs-12 col-sm-8" type="text" name="tablet_ip" value="'.$tablet_details[0]['tablet_ip'].'" placeholder="tablet_ip">';


  $o .= '  <div class="clear"></div>';
  // $o .= '  <button id="add_tablet" class="text-center btn btn-warning col-xs-12 col-sm-6 col-md-3 col-lg-3 input-style-btn wrap-top pull-right" type="submit" name="submit" value="1">Update tablet</button>';
  $o .= '<input name="update_tablet" type="submit" value="Update data">';
  $o .= '</form>';


  $o .= '<div class="clear"></div>';


} else {
  $_SESSION['error_page_msg'] = 'Nema tableta sa ovim id-om';
  Url::header_status(ADMINURL.'?page=ip_settings');
}
