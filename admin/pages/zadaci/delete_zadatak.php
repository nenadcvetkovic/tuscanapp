<?php

Helper::pre($_GET);

if ( isset($_GET['page']) && $_GET['page']=='zadaci' ) {

  if ( isset($_GET['options']) && $_GET['options']=='delete' && isset($_GET['id']) && $_GET['id'] != '' && $_GET['id'] != 0 ) {

    $id = $_GET['id'];

    if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings', array('id'=>$id) ) ) > 0 ) {
      $_SESSION['msg']['info_page_msg'] = 'Usmesno brisanje zadatka sa ID-em. <i>'.$id.'</i>';
      $_SESSION['msg']['info_page_msg2'] = 'Usmesno brisanje zadatka sa ID-em. <i>'.$id.'</i>';
      $res = Database::deleteData('komesa_line_settings', array('id'=>$id) );
      Url::header_status(ADMINURL.'?page=zadaci');
    } else {
      $_SESSION['msg']['error_page_msg'] = 'Nema zadatka sa ovim ID-em. <i>ID : '.$id.'</i>';
      $_SESSION['msg']['error_page_msg2'] = 'Nema zadatka sa ovim ID-em. <i>ID : '.$id.'</i>';
      Url::header_status(ADMINURL.'?page=zadaci');
    }
  }

}
