<?php
// Helper::pre($_POST);
  // update data
  if ( (isset($_POST['komesa_datum_rada']) && $_POST['komesa_datum_rada']!= '' ) &&
        (isset($_POST['select_linija']) && $_POST['select_linija']!= '' ) &&
        (isset($_POST['select_komesa']) && $_POST['select_komesa']!= '' ) &&
        (isset($_POST['add_zadatak']) && $_POST['add_zadatak']==1 )
  ) {
    $working_day = $_POST['komesa_datum_rada'];
    $linija_id = $_POST['select_linija'];
    $komesa_id = $_POST['select_komesa'];

    $table_name = 'komesa_line_settings';
    $key_value_data = array(
      'working_day' => $working_day,
      'linija_id' => $linija_id,
      'komesa_id' => $komesa_id
    );

    echo 'Korak';

    if ( $last_id = Database::insert_data($table_name, $key_value_data)){
      echo 'jeeeeee';
      $_SESSION['info_page_msg'] = 'Uspesno dodat zadatka';
      Url::header_status(ADMINURL.'?page=zadaci&options=edit&id='.$last_id);
    } else {
      echo 'Neeeee';
      $_SESSION['error_page_msg'] = 'Neuspesno dodat zadatka';
      Url::header_status(ADMINURL.'?page=zadaci');
    }
  } else {

  }

  if ( true ) {

    ?>
      <h2 class="sub-header text-left input_padding">Dodaj Zadatak</h2>
      <a href="<?php echo ADMINURL.'?page=zadaci'; ?>" class="btn btn-info"><i class="fa fa-angle-double-left"></i> Povratak na sve zadatke</a>
      <div class="row alert-white">
        <div id="message_box" class="text-center"></div>
        <div class="col-xs-12 alert alert-primary">
          <form class="edit_zadatak" action="?page=zadaci&options=add_zadatak" method="post">
            <div class="alert-lightblue col-xs-12 col-sm-3 col-lg-3 text-left input_padding wrap">Datum rada</div>
            <input id="datepicker-1" class="datum_rada col-xs-12 col-sm-8 col-md-6 col-lg-8 wrap-left" type="text" name="komesa_datum_rada" value="" placeholder="Datum rada | primer: 2016-07-18">
            <!-- <input type="text" name="artikal_id" value="" placeholder="Artikal ID"><br/> -->
            <div class="alert-lightblue col-xs-12 col-sm-3 col-lg-3 text-left input_padding wrap">Izaberite liniju</div>
            <select id="select_linija" class="col-xs-12 col-sm-8 col-md-6 col-lg-8 wrap-left" class="select_linija" name="select_linija">
              <optgroup label="Sve Linije">
                <option value="default" disabled selected="selected">Select linija</option>
                <?php
                $o = '';
                $linija_list = Database::fetchData('linija');
                foreach ($linija_list as $item) {
                  # code...
                  $o .= '<option value="'.$item['id'].'"';
                  if ( false ) {
                    $o .= ' selected="selected"';
                  }
                  $o .= '>'.$item['id']."-".$item['linija_id']."-".$item['linija_name'].'</option>';
                }
                echo $o;
                ?>
              </optgroup>
            </select>

            <div class="clear"></div>

            <div class="alert-lightblue col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left input_padding wrap">izaberite komesu</div>
            <select id="select_komesa" class="col-xs-12 col-sm-8 col-md-6 col-lg-8 wrap-left" class="select_komesa" name="select_komesa">
              <optgroup label="Sve Komese">
                <option value="default" disable selected="selected">Izaberite komesu</option>
                <?php
                $o = '';
                $komesa_list = Database::fetchData('komesa');
                foreach ($komesa_list as $item) {
                  $o .= '<option value="'.$item['id'].'"';
                  if ( false ) {
                    $o .= ' selected="selected"';
                  } else {
                  }
                  $o .= '>'.$item['id']."-".$item['komesa_id']."-".$item['komesa_name'].'</option>';
                }
                echo $o;
                $o ='';
                ?>
              </optgroup>
            </select>
            <div class="clear"></div>

            <div class="col-xs-12">
              <button id="add_zadatak" class="pull-right text-center btn btn-warning col-xs-12 col-sm-6 col-md-2 col-lg-2 input-style-btn wrap-top" type="submit" name="add_zadatak" value="1">Dodaj Zadatak</button>
            </div>
          </form>
        </div>
        <div class="clear"></div>
      </div>
    </div>

  </div>

    <?php
  }

?>
