$(document).ready(function(){
	var logovani_radnici = $('.radnik_filter').find('a').html();

	var filter_text = $('.filter').val();

	$('.filter').on('input', function() { 
	    var input_val = $(this).val(); // get the current value of the input field.

	    var divs = $('.radnik_filter');

	    $.each(divs, function( index, value ) {
		  $(this).hide();
		});

		$.each(divs, function( index, value ) {
			// console.log( $(this).find('a').html());
			var upper_val = input_val.toUpperCase();
			if ( $(this).find('a').html().indexOf(upper_val) >= 1 ) {
				$(this).show();
			}
		});

	});

	$('.reset_input_filter').on('click', function(e){
		e.preventDefault();

		$('.filter').val('');

	    var divs = $('.radnik_filter');

	    $.each(divs, function( index, value ) {
		  $(this).show();
		});

	});

});