$(document).ready(function(){

    $('#filter_operacija_val').on('input', function(){
        var input_text = $(this).val();
        var divs = $('tr.table_row');

        $.each(divs, function() {
          $(this).hide();
        });

        $.each(divs, function() {
            var operacija_id = $(this).find('td').eq('1').html();
            if ( operacija_id.indexOf(input_text) > -1 ) {
                $(this).show();
                console.log(operacija_id);
            }   
        });

        $.each(divs, function() {

            var operacija_name = $(this).find('td').eq('2').html().toUpperCase();
            var upper_text = input_text.toUpperCase();

            if ( operacija_name.indexOf(upper_text) > -1 ) {
                $(this).show();
                console.log(operacija_name);
                console.log(upper_text);
            }   
        });

    });

    $('.reset_operacija').on('click', function(e){
        e.preventDefault();

        $('#filter_operacija_val').val('');

        var divs = $('tr.table_row');

        $.each(divs, function( index, value ) {
          $(this).show();
        });

    });


});