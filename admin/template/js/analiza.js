$(document).ready(function(){
    // alert();
    
    var base_url = 'http://192.168.1.200/tuscan/';
    // var base_url = 'http://localhost/tuscan/';


    $(function() {
       $( "#datepicker-1" ).datepicker({
         dateFormat: "yy-mm-dd",
         minDate: '',
         maxDate: '0',
         showButtonPanel: false,
         showAnim: '',
        //  showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true
       });
    });

    // all hints
    // add color to hints


    // $('.komesa-filter-btn').addClass('hidden');

    $('input#datepicker-1').change(function(e){
      // if ( $('.komesa-filter-btn').hasClass('hidden') ){
      //   $('.komesa-filter-btn').removeClass('hidden');
      // }
      // e.preventDefault();



      $('.alert-msg').remove();
      $('.select-komesa').removeClass('alert alert-danger');
      var komesa_date_filter = $('#datepicker-1').val();
      // alert(komesa_date_filter);
      //
      // return false;
      if ( komesa_date_filter == '' ) {

        $.ajax({
          url: base_url + "data.php?action=return-data",
          method: "POST",
          data: {
            options: "komesa",
            // where: {komesa_datum_rada:komesa_date_filter}
            where: {
              table_name: 'komesa_line_settings',
              working_day:komesa_date_filter
            }
          },
          dataType: 'json'
        })
        .done(function( data ) {

          if ( data == 0 ) {

            if ( $('.alert-msg').length == 0 ) {
              $('.komesa-filter-btn').after('<div class="alert-msg alert alert-danger">Nema komese za ovaj dan</div>');
            }

          } else {

            if ( $('.select-komesa').length == 0 ) {
              var string = '<select name="komesa" class="select-komesa col-xs-8 col-sm-8 col-md-8 col-lg-8">';
              string = string + '<option selected="selected" desibled>Izaberi komesu</option>';

              data.forEach(function(entry) {
                string = string + '<option value="'+ entry.komesa_id +'">' + entry.komesa_id + '</option>';
              });

              string = string + '</select>';
              $('.komesa-filter-btn').after(string);
            } else {
              alert();
            }

          }
        })


      } else {


        $.ajax({
          url: base_url + "data.php?action=return-data",
          method: "POST",
          data: {
            options: "komesa",
            // where: {komesa_datum_rada:komesa_date_filter}
            where: {working_day:komesa_date_filter}
          },
          dataType: 'json',
        })
        .done(function( data ) {
          if ( data == 0 ) {
            if ( $('.select-komesa').length == 0 ) {
              var string = '<select name="komesa" class="select-komesa alert alert-danger col-xs-12">';
              string = string + '<option value="6666"> Nema komese za ovaj dan </option>';
              string = string + '</select>';
              $('.komesa-filter-btn').after(string);

            } else if ( $('.select-komesa').length > 0 ) {
              $('.select-komesa').html('<option value="6666"> Nema komese za ovaj dan </option>');
              if ( !$('.select-komesa').hasClass('alert alert-warning') ){
                $('.select-komesa').addClass('alert alert-danger');
              }
            } else if ( $('.alert-msg').length == 0 ) {
              $('.komesa-filter-btn').after('<div class="wrap clear alert-msg alert alert-danger">Nema komese za ovaj dan</div>');
            }
          } else {

            var body_string = '<option selected="selected" desibled>Izaberi komesu</option>';

            data.forEach(function(entry) {
              body_string = body_string + '<option value="'+ entry.komesa_id +'">' + entry.komesa_id + '</option>';
            });

            if ( $('.select-komesa').length == 0 ) {
              var string = '<select name="komesa" class="select-komesa col-xs-12">';
              string = string + body_string;
              string = string + '</select>';
              $('.komesa-filter-btn').after(string);

            } else {
              $('.select-komesa').html(body_string);
            }
          }
        })

      }


    e.preventDefault();


    }); // END $('input#datepicker-1').change

    $('.komesa-filter-btn').on('click', function(e){
        $('.alert-msg').remove();
        $('.select-komesa').removeClass('alert alert-danger');
        var komesa_date_filter = $('#datepicker-1').val();
        // alert(komesa_date_filter);
        //
        // return false;
        if ( komesa_date_filter == '' ) {

          $.ajax({
            url: base_url + "data.php?action=return-data",
            method: "POST",
            data: {
              options: "komesa",
              // where: {komesa_datum_rada:komesa_date_filter}
              where: {
                table_name: 'komesa_line_settings',
                working_day:komesa_date_filter
              }
            },
            dataType: 'json'
          })
          .done(function( data ) {

            if ( data == 0 ) {

              if ( $('.alert-msg').length == 0 ) {
                $('.komesa-filter-btn').after('<div class="alert-msg alert alert-danger">Nema komese za ovaj dan</div>');
              }

            } else {

              if ( $('.select-komesa').length == 0 ) {
                var string = '<select name="komesa" class="select-komesa col-xs-12">';
                string = string + '<option selected="selected" desibled>Izaberi komesu</option>';

                data.forEach(function(entry) {
                  string = string + '<option value="'+ entry.komesa_id +'">' + entry.komesa_id + '</option>';
                });

                string = string + '</select>';
                $('.komesa-filter-btn').after(string);
              } else {
                alert();
              }

            }
          })


        } else {


          $.ajax({
            url: base_url + "data.php?action=return-data",
            method: "POST",
            data: {
              options: "komesa",
              // where: {komesa_datum_rada:komesa_date_filter}
              where: {working_day:komesa_date_filter}
            },
            dataType: 'json',
          })
          .done(function( data ) {
            if ( data == 0 ) {
              if ( $('.select-komesa').length == 0 ) {
                var string = '<select name="komesa" class="select-komesa alert alert-danger col-xs-12">';
                string = string + '<option value="6666"> Nema komese za ovaj dan </option>';
                string = string + '</select>';
                $('.komesa-filter-btn').after(string);

              } else if ( $('.select-komesa').length > 0 ) {
                $('.select-komesa').html('<option value="6666"> Nema komese za ovaj dan </option>');
                if ( !$('.select-komesa').hasClass('alert alert-warning') ){
                  $('.select-komesa').addClass('alert alert-danger');
                }
              } else if ( $('.alert-msg').length == 0 ) {
                $('.komesa-filter-btn').after('<div class="wrap clear alert-msg alert alert-danger">Nema komese za ovaj dan</div>');
              }
            } else {

              var body_string = '<option selected="selected" desibled>Izaberi komesu</option>';

              data.forEach(function(entry) {
                body_string = body_string + '<option value="'+ entry.komesa_id +'">' + entry.komesa_id + '</option>';
              });

              if ( $('.select-komesa').length == 0 ) {
                var string = '<select name="komesa" class="select-komesa col-xs-12">';
                string = string + body_string;
                string = string + '</select>';
                $('.komesa-filter-btn').after(string);

              } else {
                $('.select-komesa').html(body_string);
              }
            }
          })

        }


      e.preventDefault();
    }); //$('.komesa-filter-btn').on('click'


    // ========================================= //
    //              Live Serach                  //
    // ========================================= //

    
    // $('.live-search-radnika').change(function(e){
    //   var live_searche_text = $('.live-search-radnika').val();
    //       $.ajax({
    //         url: base_url + "api/data.php?action=return_data_query",
    //         method: "POST",
    //         data: {
    //           query: "SELECT * FROM radnici_lista WHERE radnik_name LIKE '%"+live_searche_text+"%'"
    //         },
    //         dataType: 'json',
    //       })
    //       .done(function( data ) {
    //         console.log(data);
    //       });

    //   e.preventDefault();
    // });


    $('.live-search-radnika').on('focus', function() { 
        // alert($(this).val()); // get the current value of the input field.

      $('.dropdown-live-search-radnika').show();
      $(this).val("");

      $(this).attr('value', '');
      console.log($('.live-search-radnika').val());

      var live_searche_text = $(this).val();
          $.ajax({
            url: base_url + "api/data.php?action=return_data_query",
            method: "POST",
            data: {
              query: "SELECT * FROM radnici_lista"
            },
            dataType: 'json',
          })
          .done(function( data ) {

            if ( data.length !== null ) {
              console.log(data);
              var res = '';
              res = res + '<span class="close_dropdown" style="position:absolute;z-index: 1111;background-color: red; padding: 5px;cursor: pointer;top:0;"> x </span>';
              res = res + '<ul class="list-group">';
              for (var i = 0; i < data.length; i++) {
                res = res + '<li class="list-group-item radnici_result_lista" value="'+ data[i]['radnik_id'] + '">' + data[i]['radnik_name'] + '</li>';
                // res = res + '<option value="' + data[i]['id'] + '">' +  data[i]['radnik_name'] + '</option>';
              };

              res = res + '</ul>';

              $('.dropdown-live-search-radnika').html(res);

            }
            bindButtonClick();
            hideDropdownBox();


          });

    }).stop();


    $('.live-search-radnika').on('input', function() { 
        // alert($(this).val()); // get the current value of the input field.

      var live_searche_text = $(this).val();

          if ( $.isNumeric(live_searche_text) ) {
            var q = "SELECT * FROM radnici_lista WHERE radnik_id = '"+live_searche_text+"'";
          } else {
            console.log('nije');
            var q = "SELECT * FROM radnici_lista WHERE radnik_name LIKE '%"+live_searche_text+"%'";
          }


          $.ajax({
            url: base_url + "api/data.php?action=return_data_query",
            method: "POST",
            data: {
              query: q
            },
            dataType: 'json',
          })
          .done(function( data ) {

          if ( data !== null ) {
              console.log(data);
              var res = '';
              res = res + '<span class="close_dropdown" style="position:absolute;z-index: 1111;background-color: red; padding: 5px;cursor: pointer"> x </span>';
              for (var i = 0; i < data.length; i++) {
                res = res + '<li class="col-xs-12 btn radnici_result_lista" value="'+ data[i]['radnik_id'] + '">' + data[i]['radnik_name'] + '</li>';
                // res = res + '<option value="' + data[i]['id'] + '">' +  data[i]['radnik_name'] + '</option>';
              };

              $('.dropdown-live-search-radnika').html(res);

            }
            bindButtonClick();

            hideDropdownBox();

          });

    });

function bindButtonClick(){
    $('li.radnici_result_lista').click(function(){
        $('.live-search-radnika').val($(this).html());
        $('.dropdown-live-search-radnika').hide();
        $('.radnik_main_value').attr('value', $(this).val());
        console.log($(this).val());
    });

}

function hideDropdownBox(){
  $('.close_dropdown').click(function() {
    $(this).parent().hide();
  });

} 

// ==================================================== //
// ================= GOOGLE CHART ===================== //
// ==================================================== //

      // google.charts.load('current', {'packages':['bar']});
      // google.charts.setOnLoadCallback(drawChart);
      // function drawChart() {
      //   var data = google.visualization.arrayToDataTable([
      //     ['Meseci', 'Efikasnost'],
      //     ['Januar', 50],
      //     ['Februar', 70],
      //     ['Mart', 66],
      //     ['April', 22],
      //     ['Maj', 55],
      //     ['Jun', 12],
      //     ['Jul', 88],
      //     ['Avgust', 90],
      //     ['Septembar', 24],
      //     ['Oktobar', 56],
      //     ['Novembar', 66],
      //     ['Decembar', 99]
      //   ]);

      //   var options = {
      //     chart: {
      //       title: 'Uspesnost radnika po mesecima',
      //       subtitle: 'Uspesnost radnika po mesecima za 2017',
      //     }
      //   };

      //   var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

      //   chart.draw(data, options);

}); // Doucument ready end of
