<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tuskan Family</title>

    <!-- Normalize CSS -->
    <link href="<?php echo ADMINURL; ?>template/css/normalize.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo CSSURL; ?>bootstrap.min.css" rel="stylesheet">

    <!-- Main CSS -->
    <link href="<?php echo CSSURL; ?>style.css" rel="stylesheet">
    <link href="<?php echo ADMINURL; ?>template/css/adminstyle.css" rel="stylesheet">

    <!-- Font Awesome  -->
    <link rel="stylesheet" href="<?php echo BASEURL; ?>template/font-awesome-4.6.2/css/font-awesome.min.css">

<script src="<?php echo ADMINURL.'template/js/jscolor.min.js'; ?>"></script>

    <!-- Custom CSS -->
    <style>
    /**/
    #datepicker-1{
      -webkit-display: none;
    }
    </style>

    <!-- <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet"> -->
    <link href="<?php echo ADMINURL; ?>template/css/jquery-ui.css" rel="stylesheet">

    <!--  Custom JS -->
    <script type="text/javascript">
      // $(document).ready(function(){
      //
      // }); // Doucument ready end of

    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
  <!-- <div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div> -->
