<div id="wrapper">
<?php

require_once 'pages/navigation-side.php';

?>

  <div id="page-wrapper">
    <div class="container-fluid">

<?php

require_once 'pages/messages.php';

if ( isset($_GET['page'])) {
  $array['page'] = $_GET['page'];

  if ( file_exists('pages/'.$array['page'].'/'.$array['page'].'.php') ) {
    // echo '<h1>',$array['page'],'</h1>';
    require_once 'pages/'.$array['page'].'/'.$array['page'].'.php';
  } else {
    require_once 'pages/404.php';
  }

} else {

  require_once 'pages/dashboard/dashboard.php';

}
?>

    </div><!-- div container fluid -->
  </div><!-- div page wrapper -->

</div>
