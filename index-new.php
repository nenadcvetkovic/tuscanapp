<?php
session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once 'system/function.php';
require_once 'system/config/config.php';

// Include classes here
$class = array('Database','Url','Helper','Ip','Table');
__autoload($class);


// Place fol Helper
if ( !empty($_POST) ){
  Helper::pre($_POST);
}
?>

<?php

include('template/header.php');

if ( Ip::checkIpAddressValidation() ) {

  Url::header_status(BASEURL.'404.php');
  die('Nemate dozvolu da pristupite serveru. Molimo vas da se konsultujete sa administratormom. Hvala');

} elseif ( Ip::getUserRealIpAdress() == BASEIP ) {
  Url::header_status(ADMINURL);
} else {

  $data = Database::whereQuery('tablet_ip_settings', array('tablet_ip'=>Ip::getUserRealIpAdress(), 'converted_tablet_ip'=>Ip::ReturnConvertedIp()) );
  // Helper::pre($data);
  $o = '';

  $o .= '<div class="container">';

  $o .= '<div class="col-xs-12">';

  $o .= '<form name="login" method="post">';
  $o .= '<input name="sifra" name="number" placeholder="Insert ">';

  $o .= '<select name="artikal" class="artikal">';
  $all_artikal = Database::whereQuery('artikal');
  foreach ($all_artikal as $item ) {
    $o .= '<option>'.$item['artikal_name'].'</option>';
  }
  $o .= '</select>';

  $o .= '<input name="submit" type="submit" value="login">';
  $o .= '</form>';

  $o .= '</div>';

  echo $o;

}



?>


<?php require_once('template/footer.php'); ?>
