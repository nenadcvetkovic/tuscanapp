<?php

ALTER TABLE `radnici_lista` ADD `procenat_effikasnosti` INT(3) NULL DEFAULT NULL AFTER `linija`;
ALTER TABLE `radnici_lista` CHANGE `procenat_efficasnosti` `procenat_effikasnosti` INT(3) NULL DEFAULT NULL;
ALTER TABLE `radnici_lista` CHANGE `procenat_effikasnosti` `btn_disable_time` INT(3) NULL DEFAULT NULL;

ALTER TABLE `radnici_lista` ADD `btn_disable_time` INT(3) NULL DEFAULT NULL AFTER `linija`;
