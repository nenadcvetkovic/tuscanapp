<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

session_start();

// config files
require_once 'system/config/config.php';
// requires files
require_once 'system/libs/Database.php';
require_once 'system/libs/Url.php';

if ( isset($_SESSION['working-session']['working-session-id']) && isset($_SESSION['working-session']['radnik']) ) {
  $working_session_id = $_SESSION['working-session']['working-session-id'];
  $radnik_id = $_SESSION['working-session']['radnik'];
  $logout_time = date("Y-m-d H:i:s");

  $mysqli = Database::connect();
  $query = "UPDATE working_day_session SET logout_time = '".$logout_time."' WHERE id = '".$working_session_id."' AND radnik_id ='".$radnik_id."' ";
  $result = $mysqli->query( $query );

}
if ( isset($_SESSION['working-session']['working-session-id-2']) && isset($_SESSION['working-session']['radnik2']) ) {
  $working_session_id_2 = $_SESSION['working-session']['working-session-id-2'];
  $radnik_id_2 = $_SESSION['working-session']['radnik2'];
  $logout_time = date("Y-m-d H:i:s");

  $mysqli = Database::connect();
  $query = "UPDATE working_day_session SET logout_time = '".$logout_time."' WHERE id = '".$working_session_id_2."' AND radnik_id ='".$radnik_id_2."' ";
  $result = $mysqli->query( $query );

}
// echo $mysqli->error;
// echo $query;

unset($_SESSION);
session_destroy();
Url::header_status(BASEURL);



?>
