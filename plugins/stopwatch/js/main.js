var timer = document.getElementById('timer');
var toggleBtn = document.getElementById('toggle');
var resetBtn = document.getElementById('reset');
var elem = document.getElementById('elem');

var watch = new Stopwatch(elem);

toggleBtn.addEventListener('click', function(){
  if ( watch.isOn ) {
    watch.stop();
    toggleBtn.textContent = 'Stop';
  } else {
    watch.start();
  }
});

resetBtn.addEventListener('click', function(){
  watch.reset();
  toggleBtn.textContent = 'Start';
});
