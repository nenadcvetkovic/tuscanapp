<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="<?php echo BASEURL; ?>" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-startup-image" href="/startup.png">

    <meta name="mobile-web-app-capable" content="yes">
    <link rel="manifest" href="<?php echo BASEURL; ?>template/manifest.json">
    <meta name="application-name" content="TuskanFamily">
    <link rel="icon" sizes="192x192" href="<?php echo BASEURL; ?>template/images/icon.png">

    <title>Tuskan Family</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo BASEURL; ?>template/css/bootstrap.min.css" rel="stylesheet">

    <!-- Main CSS -->
    <link href="<?php echo BASEURL; ?>template/css/style.css" rel="stylesheet">

    <!-- Font Awesome  -->
    <link rel="<?php echo BASEURL; ?>stylesheet" href="<?php echo BASEURL; ?>template/font-awesome-4.6.2/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <style>
    /*  */
    .color_options label{
        border: thin solid #999;
    }
    </style>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
<script type='text/javascript' charset='utf-8'>
    // Hides mobile browser's address bar when page is done loading.
      window.addEventListener('load', function(e) {
        setTimeout(function() { window.scrollTo(0, 1); }, 1);
      }, false);

</script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<bodyid="app" onload="window.scrollTo(0, 1);">
  <div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
