$(document).ready(function(){


// ============================================ //
// ============== SCRIPT FILE ================= //
// ============================================ //


// ============================================= //
// ================ BASEURL  =================== //
// ============================================= //
  // var base_url = "http://local?host/tuscan/";
  var base_url = "http://192.168.1.200/tuscan/";



  setTimeout(function(){
    $('#loader-wrapper').addClass('hidden').addClass('loaded');
	}, 2000);

$('#radnik').hide();

// ================================================= //
// =============  DROUBLE USER LOGIN =============== //
// ================================================= //

$('#double-login').on('click', function(e) {
  e.preventDefault();

  if ( $('#input_sifra_2').length > 0 ) {
    // ne radi nista, vec ima polje
    console.log('Ne moze da se ubaci polje, polje vec postoji');
  } else {
    // nema polje, ubaci polje

      $('#input_sifra').after(
        '<input id="input_sifra_2" type="number" name="sifra-radnik_2" class="col-md-12 col-xs-12 col-ms-12" placeholder="Sifra radnika" />'
      );
      $('#double-login').addClass('hidden');
      $('#double-login-remove').removeClass('hidden');
  }

  $('#double-login-remove').on('click', function(e) {
    if ( $('#input_sifra_2').length > 0 ) {
      // ne radi nista, vec ima polje
      $('#input_sifra_2').remove();
      $('#double-login').removeClass('hidden');
      $('#double-login-remove').addClass('hidden');
    } else {
      // nema polje, ubaci polje
      console.log('Ne moze da se ubaci polje, polje vec postoji');
    }
  });


  e.preventDefault();
});

// ============================================= //
// ================ LOGIN PART ================= //
// ============================================= //

$('#login-new-btn').click(function(e) {
    // return false;

    var input_sifra = $('#input_sifra').val();
    var input_sifra_2 = $('#input_sifra_2').val();
    var input_linija = $('#input_linija').val();

    if ( input_sifra != '' ) {

      if ( input_sifra == 'admin' ) {
        window.location = base_url + 'admin';
      } else {
        if ( input_sifra=='' ) {
          $('#error_msg').removeClass('hidden').addClass('alert alert-danger').html(data['error_msg']);
          $('#error_msg').text('Nepoznata sifra radnika ili je polje prazno');
        } else if ( $('#input_sifra_2').length >0 && input_sifra_2=='') {
          $('#error_msg').removeClass('hidden').addClass('alert alert-danger').html(data['error_msg']);
          $('#error_msg').text('Nema sifre u polju SIFRA RADNIKA za drugog radnika. Molimo vas da unesete sifru');
        }else if (input_sifra !== '' && input_sifra_2 !== '' || input_sifra_2 !== null) {
          var m = 'Sifra 1: '+ input_sifra;
          if ( input_sifra_2 =='' || input_sifra_2 !== null ) {
            m = m + '\n' + ' Sifra 2: ' + input_sifra_2;
            console.log(input_sifra_2);
            // alert(input_sifra_2);
          }


          $('#data').text(m);
          $('#data').addClass('alert alert-info');

          if ( input_sifra === input_sifra_2 ) {
            // check if pass is same value
            $('#error_msg').removeClass('hidden').addClass('alert alert-danger').html(data['error_msg']);
            $('#error_msg').text('Sifre su iste. Molimo ispravite sifre.');
          } else {

            $.ajax({
              url: base_url + "api/data.php?action=login-data-new",
              method: "POST",
              data: {
                sifra: input_sifra,
                sifra2: input_sifra_2,
                // linija_id: input_linija
              },
              dataType: "json",
              // contentType: "aplication/json",
              success: function(data, status, xhr) {
                //
              },
              error: function(data, xhr, status, error) {
                alert( 'Ne vraca podatka sa data page ' + data );
                console.log(data);
                console.log(xhr);
                console.log(status);
                console.log(error);
              }
            })
            .done(function( data ) {
              if ( data['status'] == 'error' ) {
                $('#error_msg').removeClass('hidden').addClass('alert alert-danger').html(
                  data['error_msg'] + data['status_msg'] + data['db_query'] + data['user_status']
                );
              } else {
                $('#error_msg').removeClass('alert alert-danger').addClass('hidden');
                // alert(JSON.stringify(data));
                $('#data').html('<code>'+JSON.stringify(data, null, 4)+'</code>');
                $('#data').addClass('alert alert-danger');
                window.location = base_url;
              }
            });
          }
        }
      }


    } else {
      $('#error_msg').addClass('alert-danger text-center').html('Nema sifre u polju <i>SIFRA RADNIKA</i>. Molimo vas da unesete sifru');
    }



  e.preventDefault();
});

  // job select page
  $.ajax({
    url: base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "operacija"
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
    .done(function( data ) {
      if ( console && console.log ) {
        console.log( "Sample of data:", data.slice( 0, 100 ) );
      }
    });

  $('#reset-job-btn').click(function(){
    // alert();
    $('#komesa').hide();
    $('#operacija').hide();
    $('.color_options').hide();
    $('.size_options').hide();
  });


$('#komesa').hide();

$('#artikal').change(function(){
  $('#komesa').hide();
  $('#operacija').hide();
  $('.color_options').hide();
  $('.size_options').hide();

  var artikal_id = $('#artikal').val();

  $.ajax({
    url: base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "komesa",
      'artikal_id': artikal_id
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
    .done(function( data ) {
      // alert(data);
      console.log(data);
      // $('#loader-wrapper').removeClass('hidden').removeClass('loaded');

      if ( console && console.log ) {
        console.log( "Sample of data:", data.slice( 0, 100 ) );
      }
      if ( data !== 0) {
        $('#komesa').fadeIn("slow");
        $('#komesa').parents().find('h4.komesa.hidden').removeClass('hidden');
        $('#komesa').children().html(data);
        console.log(data);

        // form-job-selected-2
        // alert($('select#artikal option:selected').val());
        var artikal_id = $('select#artikal option:selected').val();
        $.ajax({
          url: base_url + "api/data.php?action=return-data",
          method: "POST",
          data: {
            options: "full_artikal",
            'artikal_id': artikal_id
          },
          beforeSend: function( xhr ) {
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
          }
        })
        .done(function( data ) {
          $('.artikal_color').append(data);
        });
      }

    });

});
$('#komesa').change(function(){
  // alert();
  $('#operacija').removeClass('hidden');
  $('#operacija').css('display','block');

  var artikal_id = $('select#artikal option:selected').val();

  $.ajax({
    url: base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "operacija",
      artikal_id: artikal_id
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
  .done(function( data ) {
    // alert(data);
    $('#operacija').html(data);
  });


});

// ==================================================== //
// ================ JOB SELECTED PART ================= //
// ==================================================== //

$('#job-selected-2-btn').click(function(e) {

  $('#loader-wrapper').removeClass('hidden').removeClass('loaded');

  var input_komesa = $('#komesa option:selected').val();
  var name_komesa = $( "#komesa option:selected" ).attr('name');
  var input_artikal = $('#artikal option:selected').val();
  var name_artikal = $( "#artikal option:selected" ).attr('name');

  var error_count = 0;
  if ( input_komesa=='' ||  input_artikal=='') {
    $('#error_msg').addClass('alert alert-danger').html(data['error-msg']);
  } else {
    var new_text = input_komesa+' '+input_artikal;
    $('#data').text(new_text);

    // if field are empty
    if ($('#komesa option:selected').val() == 'default' || $('#artikal option:selected').val() == '' ){
      alert('Komesa i artikal nisu izabrani');
      error_count++;
      // alert(error_count);
    }

    if ( $( "select#operacija option:selected" ).val() == '' || $( "select#operacija option:selected" ).val()== 'default' ) {
        alert('Operacija nije izabrana');
        // alert(operacija);
        // alert($( "select#operacija option:selected" ).val());
        error_count++;
    } else if ( $( "input.operacija_value" ).length > 0 ) {
      var operacija = $( "input.operacija_value" ).val();
    } else {
      var operacija = $( "#operacija option:selected" ).val();
    }

    if ( $('.color_options label.active input').length > 0) {
      color_options = $('.color_options label.active input').attr('id');
      // alert(color_options);
    } else {
      alert('Boja nije izabrana');
      error_count++;
    }
    // alert(color_options);
    // return false;

    if ( $('.size_options label.active input').length > 0) {
      size_options = $('.size_options label.active input').attr('id');
    } else {
      alert('Velicina nije izabrana');
      error_count++;
    }

    // alert(error_count);

    if ( error_count == 0) {
      $.ajax({
        url: base_url + "api/data.php?action=job-data",
        method: "POST",
        type: 'POST',
        data: {
          komesa: input_komesa,
          name_komesa: name_komesa,
          artikal: input_artikal,
          name_artikal: name_artikal,
          operacija: operacija,
          color: color_options,
          size: size_options
        },
        dataType: "json",
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
       },
        success: function(data, status, xhr) {
            // console.log(input_komesa);
            // console.log(name_komesa);
            // console.log(input_artikal);
            // console.log(name_artikal);
            // console.log(operacija);
            // console.log(color_options);
            // console.log(size_options);
        },
        error: function(xhr, status, error) {
            //  alert(status);
        }
      })
        .done(function( data ) {
          // if ( console && console.log ) {
          //   console.log( "Sample of data:", data.slice( 0, 100 ) );
          // }
          // $('#data').text(data[0]);
          //
          // if ( data == 'Nenad Cvetkovic' ) {
          //   $('#login-data').hide();
          // } else {
          //   $('#error_msg').text('Bad login data');
          // }
          //alert(data.status);
          if ( data.status == 'error' ) {
            console.log(data);
          } else if ( data.status == 'success'  ) {
            //alert(data.status);
            console.log('Status: ' + data.status);
            console.log(data);
            window.location = base_url + 'index.php';
          }
          // $('#login-page').fadeOut("slow");
          // var logout_link = '<a href="http://localhost:8888/projekti/svn-projekti/tuskan-family/libs/logout.php" class="btn btn-lg btn-primary">Logout</a>';
          // setTimeout(function() {
          //   $('#working-page').html('<h1>Working Page</h1>'+logout_link);
          // }, 1000);
        });

    } else {
      e.preventDefault();
      console.log('die');
      window.location = base_url + 'index.php';
      return false;
    }


  }


  e.preventDefault();
});


function logout_user() {
 $.ajax({
    url: base_url + "api/data.php?action=login-data-new",
    method: "POST",
    data: {
      sifra: input_sifra,
      sifra2: input_sifra_2,
      // linija_id: input_linija
    },
    dataType: "json",
    // contentType: "aplication/json",
    success: function(data, status, xhr) {
      //
    },
    error: function(data, xhr, status, error) {
      alert( 'Ne vraca podatka sa data page ' + data );
      console.log(data);
      console.log(xhr);
      console.log(status);
      console.log(error);
    }
  })
  .done(function( data ) {
    console.log('Logerror');
  });

}


}); // Doucument ready end of
