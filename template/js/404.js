$(document).ready(function(){

  var base_url = 'http://localhost/tuscan/';

  setInterval(ajaxCall, 30000);

  function ajaxCall() {
  $.ajax({
    url: base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "zadatak",
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
  .done(function( data ) {
    console.log( data );

    var data = JSON.parse(data);
     if ( data.status == 'true') {
      window.location = base_url;
    }

  });
}

});
