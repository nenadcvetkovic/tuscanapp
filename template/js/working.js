$(document).ready(function(){



  // var base_url = 'http://localhost/tuscan/';
  var base_url = 'http://192.168.1.200/tuscan/';


  var vreme = $('#vreme_po_operaciji').find('.value').html();



// ==================================================== //
// ==================== IF NO WIFI ==================== //
// ==================================================== //

$('#myModal').modal('hide');

$('#myModal').modal({
  backdrop: 'static',
  keyboard: false,
  show: false
}); 
// $('#myModal').modal({ show: false });

var check_wifi_status = function(){
  
  $.ajax({
    url: base_url + "api/get/get.php",
    method: "POST",
    data: {
      options: "check_wifi_status",
      radnik_id: radnik_id
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
  .fail(function() {
    alert( "error" );
    $('#myModal').modal('show');
  })
  // .error(function() {
  //   alert( "error" );
  //   $('#myModal').modal('show');
  // })
  .done(function( data ) {
    // alert( "complete" );
        $('#myModal').modal('hide');

    // console.log( data );
  });

  // alert();

};

setInterval(check_wifi_status, 1000*1*10);
// Save error data in hint

  // =========== END GRESKA ============= //

  $('#no_wifi_btn').on('click', function(e){
    $.ajax({
      url: base_url + "api/put/put.php?action=insert-wifi-data",
      method: "POST",
      data: {
        options:          "no_wifi_connection",
        hint_code:        'NWFC'
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      error: function(xhr, status, error) {
          console.log( "Sample of data:", xhr );
          console.log( "Sample of data:", status );
          console.log( "Sample of data:", error );
      }
    })
    .fail(function(){
      alert('Error inserting no wifi data');
    })
    .done(function(data){
      console.log('Connection establish');
      window.location = base_url;
    });
    e.preventDefault();
    return false;
  });


    if ( $('#myModal').css('display') == 'none' ) {

      var status;
      var hint_btn = document.getElementById('operation-hint-btn');
      var watch = new Stopwatch(hint_btn);


      // on refresh
      $.get(base_url + "time_data.php?time=1", function(data, status){
        var t = JSON.parse(data);
        var times = t.time; // vreme operacije
        watch.start(times);
        status = true;

        console.log('Time: ' + t.time);
        console.log('Full Time: ' + t.full_time);
        console.log('Diff Time: ' + t.diff_time);

        var time = new Date(t.full_time);
        var minutes = time.getMinutes().toString();

        // var hours = time.getHours().toString();
        var deo = time.getSeconds();
        var seconds = deo.toString();
        var miliseconds = time.getMilliseconds().toString();

        if ( minutes.length < 2 ) {
          minutes = '0' + minutes;
        }
        if ( seconds.length < 2 ) {
          seconds = '0' + seconds;
        }
        while ( miliseconds.length < 3 ) {
          miliseconds = '0' + miliseconds;
        }

        console.log('MInutes ' + minutes + ' Seconds ' + seconds + ' miliseconds ' + miliseconds + ' = ' + time);



        $('#operation-hint-btn').addClass('disabled-hint');
        $('#operation-hint-btn').addClass('disabled');
      });
    };

/* ************************************* */

  // ================================= //
  // =========== PAUSA !============== //  
  // ================================= //  

  $('.end_pausa').on('click', function(e){
    $.ajax({
      url: base_url + "api/data.php?action=insert-pausa-data",
      method: "POST",
      data: {
        options:          "end_pausa",
        hint_code:        'P002'
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      error: function(xhr, status, error) {
          console.log( "Sample of data:", xhr );
          console.log( "Sample of data:", status );
          console.log( "Sample of data:", error );
      }
    }).done(function(data){
      console.log('Pausa se zavrsila');
      // console.log(data);
      
      window.location = base_url;
    });

    e.preventDefault();
  });

  if ( $('.end_pausa').length == 0 ) {

    var status;
    var hint_btn = document.getElementById('operation-hint-btn');

    var watch = new Stopwatch(hint_btn);


    // on refresh
    $.get(base_url + "time_data.php?time=1", function(data, status){
      var t = JSON.parse(data);
      var times = t.time; // vreme operacije
      watch.start(times);
      status = true;

      console.log('Time: ' + t.time);
      console.log('Full Time: ' + t.full_time);
      console.log('Diff Time: ' + t.diff_time);

      var time = new Date(t.full_time);
      var minutes = time.getMinutes().toString();

      // var hours = time.getHours().toString();
      var deo = time.getSeconds();
      var seconds = deo.toString();
      var miliseconds = time.getMilliseconds().toString();

      if ( minutes.length < 2 ) {
        minutes = '0' + minutes;
      }
      if ( seconds.length < 2 ) {
        seconds = '0' + seconds;
      }
      while ( miliseconds.length < 3 ) {
        miliseconds = '0' + miliseconds;
      }

      console.log('MInutes ' + minutes + ' Seconds ' + seconds + ' miliseconds ' + miliseconds + ' = ' + time);



      $('#operation-hint-btn').addClass('disabled-hint');
      $('#operation-hint-btn').addClass('disabled');
    });

    // ============================================ //
    // ================= PAUSA 2 ================== //
    // ============================================ //

    $('.end_pausa_2').on('click', function(e){
      $.ajax({
        url: base_url + "api/data.php?action=insert-pausa-data",
        method: "POST",
        data: {
          options:          "end_pausa",
          hint_code:        'P004'
        },
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        },
        error: function(xhr, status, error) {
            console.log( "Sample of data:", xhr );
            console.log( "Sample of data:", status );
            console.log( "Sample of data:", error );
        }
      }).done(function(data){
        console.log('Pausa se zavrsila');
        // console.log(data);
        
        window.location = base_url;
      });

      e.preventDefault();
    });

    if ( $('.end_pausa_2').length == 0 ) {

      var status;
      var hint_btn = document.getElementById('operation-hint-btn');
      var watch = new Stopwatch(hint_btn);


      // on refresh
      $.get(base_url + "time_data.php?time=1", function(data, status){
        var t = JSON.parse(data);
        var times = t.time; // vreme operacije
        watch.start(times);
        status = true;

        console.log('Time: ' + t.time);
        console.log('Full Time: ' + t.full_time);
        console.log('Diff Time: ' + t.diff_time);

        var time = new Date(t.full_time);
        var minutes = time.getMinutes().toString();

        // var hours = time.getHours().toString();
        var deo = time.getSeconds();
        var seconds = deo.toString();
        var miliseconds = time.getMilliseconds().toString();

        if ( minutes.length < 2 ) {
          minutes = '0' + minutes;
        }
        if ( seconds.length < 2 ) {
          seconds = '0' + seconds;
        }
        while ( miliseconds.length < 3 ) {
          miliseconds = '0' + miliseconds;
        }

        console.log('MInutes ' + minutes + ' Seconds ' + seconds + ' miliseconds ' + miliseconds + ' = ' + time);



        $('#operation-hint-btn').addClass('disabled-hint');
        $('#operation-hint-btn').addClass('disabled');
      });
    };

// ======================================================= //
//                         GRESKA 
// ======================================================= //

  // ============== COUNT GRESKA =============== //

  $('#count_greska').on('click', function(e){
    var count_greska = $('#count_greska').html();
    // alert(count_greska);

    $.ajax({
      url: base_url + "api/data.php?action=insert-greska-data",
      method: "POST",
      data: {
        options:          "count_greska",
        hint_code:        'CG01'
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      error: function(xhr, status, error) {
          console.log( "Sample of data:" + xhr );
          console.log( "Sample of data:" + status );
          console.log( "Sample of data:" + error );
      }
    }).done(function(data){
      console.log('Count greska +1');
      console.log(data);
      var data = JSON.parse(data);
      if ( data.status == 'success') {
        $('#count_greska').html(parseInt(parseInt(count_greska)+1));
      }
    });
    e.preventDefault();
    return false;
  });

  // =========== END GRESKA ============= //

  $('.end_greska').on('click', function(e){
    $.ajax({
      url: base_url + "api/data.php?action=insert-greska-data",
      method: "POST",
      data: {
        options:          "end_greska",
        hint_code:        'G002'
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      error: function(xhr, status, error) {
          console.log( "Sample of data:", xhr );
          console.log( "Sample of data:", status );
          console.log( "Sample of data:", error );
      }
    }).done(function(data){
      console.log('Greska je regulisana');
      // console.log(data);
      var data = JSON.parse(data);
      if ( data.status == 'success') {
        window.location = base_url;
      }
    });
    e.preventDefault();
    return false;
  });

    if ( $('.end_greska').length == 0 ) {

      var status;
      var hint_btn = document.getElementById('operation-hint-btn');
      var watch = new Stopwatch(hint_btn);


      // on refresh
      $.get(base_url + "time_data.php?time=1", function(data, status){
        var t = JSON.parse(data);
        var times = t.time; // vreme operacije
        watch.start(times);
        status = true;

        console.log('Time: ' + t.time);
        console.log('Full Time: ' + t.full_time);
        console.log('Diff Time: ' + t.diff_time);

        var time = new Date(t.full_time);
        var minutes = time.getMinutes().toString();

        // var hours = time.getHours().toString();
        var deo = time.getSeconds();
        var seconds = deo.toString();
        var miliseconds = time.getMilliseconds().toString();

        if ( minutes.length < 2 ) {
          minutes = '0' + minutes;
        }
        if ( seconds.length < 2 ) {
          seconds = '0' + seconds;
        }
        while ( miliseconds.length < 3 ) {
          miliseconds = '0' + miliseconds;
        }

        console.log('MInutes ' + minutes + ' Seconds ' + seconds + ' miliseconds ' + miliseconds + ' = ' + time);



        $('#operation-hint-btn').addClass('disabled-hint');
        $('#operation-hint-btn').addClass('disabled');
      });
    };


  /* ============================= */
  /*        CHANGE JOB             */
  /* ============================= */

  //  URADI OVDE AJAX REQUEST ZA LOGOUT TJ CHANGE JOB LOGOUT USER ZA PREDHODNI RAD
  $('#change_job-selected-2-btn').on('click', function(e){

    console.log('eeeeeeee');

    var radnik_id = $('#radnik_id').html();
    var working_session_id = $('#working_session_id').html();

    $.ajax({
      url: base_url + "api/data.php?action=logout_session",
      method: "POST",
      data: {
        radnik_id: radnik_id,
        working_session_id: working_session_id
      },
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
      },
      error: function(xhr, status, error) {
          console.log( "Sample of data:", xhr + status + error );
      }
    }).done(function( data ) {
      var data = JSON.parent(data);
      if ( data['logout_session_status'] === 'failed' ) {
        e.preventDefault();
      }
    });

  });




  // ================================================ //
  // ================ WORKING PAGE ================== //
  // ================================================ //

  // on refres dnevni ucinak


      // on refresh
    $.get(base_url + "api/get/get.php?time=last-time", function(data, status){
    // $.get(base_url + "time_data.php?=1", function(data, status){
      var t = JSON.parse(data);
      var times = t.time; // vreme operacije
      watch.start(times);
      status = true;

      console.log('Time: ' + t.time);
      console.log('Full Time: ' + t.full_time);
      console.log('Diff Time: ' + t.diff_time);

      var time = new Date(t.full_time);
      var minutes = time.getMinutes().toString();

      // var hours = time.getHours().toString();
      var deo = time.getSeconds();
      var seconds = deo.toString();
      var miliseconds = time.getMilliseconds().toString();

      if ( minutes.length < 2 ) {
        minutes = '0' + minutes;
      }
      if ( seconds.length < 2 ) {
        seconds = '0' + seconds;
      }
      while ( miliseconds.length < 3 ) {
        miliseconds = '0' + miliseconds;
      }

      console.log('MInutes ' + minutes + ' Seconds ' + seconds + ' miliseconds ' + miliseconds + ' = ' + time);



      $('#operation-hint-btn').addClass('disabled-hint');
      $('#operation-hint-btn').addClass('disabled');
    });

  // 
  // 
  var all_day_uspesnost_val = $('#all_day_data').find('.all_day_percent').html();
  switchClassForAllDayUspesnost(all_day_uspesnost_val);

  if ( document.getElementById('operation-hint-btn') ) {

  // remove disable class from hint btn
  var radnik_id = $('#radnik_id').html();

  ///////////////////////////////////////////////
  // ***************************************** //
  ///////////////////////////////////////////////

  $.ajax({
    url: base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "get_options",
      option_name: "btn_disable_time",
      radnik_id: radnik_id
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    },
    error: function(xhr, status, error) {
        console.log( "Sample of data:", xhr + status + error );
    }
  })
    .done(function( data ) {
      var data =JSON.parse(data);
      posto_disable = data['btn_disable_time'];
      console.log(data);
      console.log(posto_disable);
      // alert(data);
      setInterval(function(){

        var vreme_disable = parseInt(Math.ceil( (parseInt(vreme) / 100) * parseInt(posto_disable)) );
        var current_time = $('#operation-hint-btn').html();
        var res = current_time.split(' . ');
        var new_res = res[0].split(' : ');
        var sum = parseInt(new_res[0])*60+parseInt(new_res[1]);

        if ( vreme_disable <= sum ) {
          // alert(vreme + ' = ' + vreme_disable + ' =  ' + sum + ' = ' + current_time);
            $('#operation-hint-btn').removeClass('disabled-hint');
            $('#operation-hint-btn').removeClass('disabled');
            // console.log('Disable class removed');
            status = true;
            // console.log(parseInt(vreme) + ' = ' + vreme_disable + ' =  ' + sum + ' = ' + current_time);
        } else {
          // console.log(parseInt(vreme) + ' = ' + vreme_disable + ' =  ' + sum + ' = ' + current_time);
        }

      },2000);

    });

  var time_hinted_seconds = 0;

  hint_btn.addEventListener('click',function(){
    time_hinted_seconds =  $('#operation-hint-btn').html();

    if ( hint_btn.classList.contains('disabled-hint') ) {
      // hint is disabled :-D
      console.log('Hint brn status: ' + hint_btn.classList.contains('disabled-hint'));
    } else {
      if ( status == false ) {
        console.log('Hint brn status: ' + hint_btn.classList.contains('disabled-hint'));
        console.log('Status : false ');
        // hint_btn.textContent = 'Hinted';
        watch.reset();
        watch.start();
        status = true;
      } else {
        console.log('Hint btn status: ' + hint_btn.classList.contains('disabled-hint'));
        console.log('status : true ');
        watch.reset();
        watch.start();
        status = false;
      }
    }

  });

  // $('#MSTimer').html(today);

  function startTime() {
    // var today = new Date();
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var sto = today.getMilliseconds();
    m = checkTime(m);
    s = checkTime(s);
    // document.getElementById('clock').innerHTML = h + ":" + m + ":" + s+'.'+sto;
    document.getElementById('clock').innerHTML = h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 1);
  }
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
  }
  //
  $('#clock').html(startTime());
  // working page main part

  var t = new Date();
  if( t.getHours() > 15 ||  t.getHours() < 7) {
    $('#clock').removeClass('btn-primary').addClass('btn-danger');
  };

  /****************************************************************/
  // Working page scripting place

  var last, diff;
  var vreme_po_operaciji = $('#vreme_po_operaciji').find('.value').html();
  var a = parseInt(vreme_po_operaciji);

  var hourly_percent = $('.work-percent').html();
  switchClassForUspesnost(hourly_percent);


  // ================================================== //
  // ================= HINT BTN PART ================== //
  // ================================================== //

  $('#operation-hint-btn').on('click', function(e){

    // ===================================== //
    //        CHECK USER LOGIN STATUS        //
    // ===================================== //

    function checkUserLoginStatus() {
      var radnik_id = $('#radnik_id').html();
      $.ajax({
        url: base_url + "api/data.php?action=return-data",
        method: "POST",
        data: {
          options: "check_login_status",
          radnik_id: radnik_id
        },
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        },
        error: function(xhr, status, error) {
          console.log( "xhr:" + xhr + " status:" + status + " error:" + error );
        }
      })
      .done(function( data ) {
        var data = JSON.parse(data);
        if ( data.login_status === 'false' ) {
          window.location = base_url + 'logout.php';
          // console.log('user has  been logouted');
          // console.log( data );
          return false;
        } else {
          // console.log('User is loged');
        }
      });
    }
    checkUserLoginStatus();

    // ================================= //
    // ======== CONTINUSE HINT ========= //
    // ================================= //

    if ( $('#operation-hint-btn').hasClass('disabled-hint') || $('#operation-hint-btn').hasClass('disabled') ){
      
      return false;
    
    } else {

      // disable click event
      $('#operation-hint-btn').addClass('disabled-hint');
      $('#operation-hint-btn').addClass('disabled');

      // colect details about click
      hints = $('.operation-hints').html();
      hints_per_operation = $('.hints-pre-operations').html();
      new_hints = parseInt(hints) + parseInt(hints_per_operation);

      var time_hint = $('#clock').html();

      if ( time_hint === '00:00:00' && $('#operation-hint-btn').hasClass('disabled')) {
        return false;
      }

      $('#time_hint').html(' '+time_hint);

      if ( last ) {
        diff = e.timeStamp - last;
        $('#razlika').html( "WORKING...." );
        // $('#razlika').html( "Razlika: " + diff + " - " + new Date() +"<br>" );
        console.log("Razlika: " + diff + "<br>" );
      } else {
        diff = 0;
      }
      last = e.timeStamp;
      $('.operation-hints').html(new_hints);
      $('.uradjenih_operacija').html(new_hints);

      var all_day_count = $('#all_day_data').find('#all_day_count').html();
      $('#all_day_data').find('#all_day_count').html(parseInt(all_day_count)+1);


      if (  $('.operacija_po_kliku').length > 0 ) {
        var operacija_po_kliku = $('.operacija_po_kliku').html();
      } else {
        var operacija_po_kliku = 1;
      }

      // console.log('Sending ajax data');
      var working_session_id = $('#working_session_id').html();
      var working_day_session_id = $('#working_day_session_id').html();
      var operacija_id = $('#operacija_id').html();
      var name_artikal = $('#name_artikal').html();
      var artikal = $('#artikal').html();
      var radnik = $('#radnik').html();
      var linija = $('#linija').html();
      var komesa = $('#komesa').html();

      var res = time_hinted_seconds.split(":");
      var ukupno_vreme = parseInt(res[0])*60 + parseInt(res[1]);

      var vreme_operacije = new Date($.now());

      $.ajax({
        url: base_url + "api/put/put.php?action=insert-data",
        method: "POST",
        data: {
          options:                "operacija-hints",
          operacija_hints:        new_hints,
          hint_code:              'F001',
          operacija_po_kliku:     operacija_po_kliku,
          time_hint:              time_hinted_seconds,
          working_session_id:     working_session_id,
          working_day_session_id: working_day_session_id,
          operacija_id:           operacija_id,
          name_artikal:           name_artikal,
          artikal:                artikal,
          radnik:                 radnik,
          linija:                 linija,
          komesa:                 komesa,
          vreme_operacije:        vreme_operacije
        },
        beforeSend: function( xhr ) {
          xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        },
        error: function(xhr, status, error) {
            console.log( "xhr:" + xhr + " status:" + status + " error:" + error );
        }
      }).done(function( data ) {

        console.log('Data ' + data);
        var data_obj = JSON.parse(data);

        $('.work-percent').html(data_obj['procenat_ucinka']);
        hourly_percent = $('.work-percent').html();
        switchClassForUspesnost(hourly_percent);
        $('#razlika').html('Razlika: '+data_obj['razlika_izmedju_zadnje_operacije']);
        // $('#razlika').html('WORKING...');

        var s = data_obj['razlika_izmedju_zadnje_operacije'];
        var d = s.split(":");
        var suma = (parseInt(d[0])*3600) + (parseInt(d[1])*60) + parseInt(d[2]);
        $('.razlika_u_sekundama').html(' Razklika u sekundama: '+ suma+'s');

        var working_hour_percent_success = Math.ceil((100 * data_obj['hinta_per_hour_this_hour'])/$('.hourly-rate').html());

        $('.working_hour_percent_success').html(working_hour_percent_success);

        if ( data_obj.insert_status) {
          // alert('Insert status ' + data_obj.insert_status);
        } else {
          // alert('Nooooo ' + data_obj.insert_status);
        }

      });



        var disable_btn = function(disable_time) {
          setTimeout(function(){
            $('#operation-hint-btn').removeClass('disabled-hint');
            $('#operation-hint-btn').removeClass('disabled');
            console.log('Disable class removed');
          }, disable_time);
        };


        // ======================================================//
        // =============== RETURN ALL DAY DATA ==================//        
        // ======================================================//

        var radnik_id = $('#radnik_id').html();
        console.log('Radnik '+radnik_id);
        var working_day_session_id = $('#working_day_session_id').html();
        var operacija_id = $('#operacija_id').html();
        
        // alert('radnik ' + radnik_id + ' wokr ' + working_day_session_id + ' operacija ' + operacija_id);
        
        $.ajax({
          url: base_url + "api/data.php?action=return-data",
          method: "POST",
          data: {
            options:                "all_day_data",
            options_all_summ:       true,
            working_day_session_id: working_day_session_id,
            operacija_id:           operacija_id,
            radnik_id:              radnik_id
          },
          beforeSend: function( xhr ) {
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
          }
        }).done(function( data ) {
          console.log('Return data : ' + data);
            var data =JSON.parse(data);

            $('#all_day_data').find('.all_day_percent').html(data.dnevna_efikasnost);
            switchClassForAllDayUspesnost(data.dnevna_efikasnost);

        });


    }

      e.preventDefault();
  });
   
  }

}


// ====================================================== //
// ================ CHECK LOGIN STATUS ================== //
// ====================================================== //


var check_login_status = function() {
  $.ajax({
    url: window.base_url + "api/data.php?action=return-data",
    method: "POST",
    data: {
      options: "check_login_status",
      radnik_id: radnik_id
    },
    beforeSend: function( xhr ) {
      xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    }
  })
  .done(function( data ) {
    console.log( data );
  });

}


}); 

        // ====================================================== //
        // =============== CHANGE COLOR ALL DATA ================ //
        // ====================================================== //        
        var switchClassForAllDayUspesnost = function(suma){
          switch(true) {
            case ( suma > 90 ):
              // alert('100-');
              $('#all_day_data')
              .removeClass()
              .addClass('text-center alert alert-green');
              break;
            case ( suma < 90 ) && ( suma > 80 ):
              $('#all_day_data')
              .removeClass()
              .addClass('text-center alert alert-yellow');
              // alert('100-50');
              break;
            case ( suma < 80 ) && ( suma > 70 ):
              $('#all_day_data')
              .removeClass()
              .addClass('text-center alert alert-orange');
              // alert('50-25');
              break;
            case ( suma < 70 ) && ( suma >= 0 ):
              $('#all_day_data')
              .removeClass()
              .addClass('text-center alert alert-red');
              // alert('25-0');
              break;
            default:
              // // alert();
            break;
          }
        }


// ================================================== //
// ================ CHANGE EFF COLOR ================ //
// ================================================== //
var switchClassForUspesnost = function(hourly_percent){
  switch(true) {
    case ( hourly_percent > 90 ):
      // alert('100-');
      $('#uspesnost')
      .removeClass()
      .addClass('text-center alert alert-green');
      break;
    case ( hourly_percent < 90 ) && ( hourly_percent > 80 ):
      $('#uspesnost')
      .removeClass()
      .addClass('text-center alert alert-yellow');
      // alert('100-50');
      break;
    case ( hourly_percent < 80 ) && ( hourly_percent > 70 ):
      $('#uspesnost')
      .removeClass()
      .addClass('text-center alert alert-orange');
      // alert('50-25');
      break;
    case ( hourly_percent < 70 ) && ( hourly_percent >= 0 ):
      $('#uspesnost')
      .removeClass()
      .addClass('text-center alert alert-red');
      // alert('25-0');
      break;
    default:
      // // alert();
    break;
  }
}

// =================================================== //
// ================   ERROR BTN  ===================== //
// =================================================== //

$('#error1').on('click', function(e){
  e.preventDefault();

  watch.reset;
  watch.start;
  status = true;
});