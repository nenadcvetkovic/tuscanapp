-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2016 at 08:11 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tuskan_family_2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikal`
--

CREATE TABLE `artikal` (
  `id` int(11) NOT NULL,
  `artikal_id` varchar(50) DEFAULT NULL,
  `artikal_name` varchar(50) DEFAULT NULL,
  `artikal_desc` text,
  `artikal_datum_unosa` datetime DEFAULT NULL,
  `artikal_status` varchar(10) DEFAULT NULL,
  `linija_rada` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal`
--

INSERT INTO `artikal` (`id`, `artikal_id`, `artikal_name`, `artikal_desc`, `artikal_datum_unosa`, `artikal_status`, `linija_rada`) VALUES
(59, '1122888', 'TEST PROBA ARTIKAL,SIZE,COLOR 2', 'Test Proba artikal,size,color 2', '2016-11-03 23:04:02', '1', NULL),
(60, 'dfgh', 'BRVBDFCV', 'fdbcvncgb', '2016-11-08 15:45:23', '1', NULL),
(61, '112233erfr', 'TEST PROBA ARTIKAL,SIZE,COLOR 2', 'json probas', '2016-11-24 16:10:27', '1', 2),
(62, '1236Q1446', '1236Q1446', '1236Q1446', '2016-12-05 16:35:20', '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1236Q1446_62`
--

CREATE TABLE `artikal_data_1236Q1446_62` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1236Q1446_62`
--

INSERT INTO `artikal_data_1236Q1446_62` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(1, '01.003', 'SURFILATURA MANICHE T/C', 'T/C SURFILATURA MANICHE', 340, NULL, '2016-12-05 16:15:57'),
(2, '01.004', 'T/C SURFILATURA SPALLE', 'T/C SURFILATURA SPALLE', 200, NULL, '2016-12-05 16:16:55'),
(3, '01.007', 'RIMAGLIO SPALLE', 'RIMAGLIO SPALLE', 100, NULL, '2016-12-05 16:19:53'),
(4, '01.012', 'RIMAGLIO MANICHE', 'RIMAGLIO MANICHE', 70, NULL, '2016-12-05 16:23:36'),
(5, '01.013', 'RIMAGLIO FIANCHI', 'RIMAGLIO FIANCHI', 45, NULL, '2016-12-05 16:24:22'),
(6, '01.011', 'RIMAGLIO FIANCO COLLO', 'RIMAGLIO FIANCO COLLO', 140, NULL, '2016-12-05 16:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1236Q1446_62_color`
--

CREATE TABLE `artikal_data_1236Q1446_62_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1236Q1446_62_color`
--

INSERT INTO `artikal_data_1236Q1446_62_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '2016-12-05 00:00:00', 1),
(2, 2, '2016-12-05 00:00:00', 0),
(3, 3, '2016-12-05 00:00:00', 0),
(4, 4, '2016-12-05 00:00:00', 0),
(5, 5, '2016-12-05 00:00:00', 0),
(6, 6, '2016-12-05 00:00:00', 0),
(7, 7, '2016-12-05 00:00:00', 0),
(8, 8, '2016-12-05 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1236Q1446_62_size`
--

CREATE TABLE `artikal_data_1236Q1446_62_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1236Q1446_62_size`
--

INSERT INTO `artikal_data_1236Q1446_62_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '2016-12-05 00:00:00', 1),
(2, 2, '2016-12-05 00:00:00', 0),
(3, 3, '2016-12-05 00:00:00', 0),
(4, 4, '2016-12-05 00:00:00', 0),
(5, 5, '2016-12-05 00:00:00', 0),
(6, 6, '2016-12-05 00:00:00', 0),
(7, 7, '2016-12-05 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61`
--

CREATE TABLE `artikal_data_112233erfr_61` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61`
--

INSERT INTO `artikal_data_112233erfr_61` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(1, '01.014', 'MANICHE', 'TAGLIACUCI UNIONE', 211, NULL, '2016-10-25 22:20:26'),
(2, '01.008', 'SEP. TELO SCHIENA + MANICA', 'TAGLIO A MANO', 114, NULL, '2016-10-25 22:18:32'),
(3, '01.012', 'F. COLLO', 'RIMAGLIO', 45, NULL, '2016-10-25 22:19:46'),
(4, '01.019', 'SAG. 1 F. B. E M. + GIROM. B. E M. (NO SPALLE) + S', 'TAGLIAC. SURFILATURA', 458, NULL, '2016-10-25 22:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61_color`
--

CREATE TABLE `artikal_data_112233erfr_61_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61_color`
--

INSERT INTO `artikal_data_112233erfr_61_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '2016-11-24 00:00:00', 1),
(2, 2, '2016-11-24 00:00:00', 1),
(3, 3, '2016-11-24 00:00:00', 0),
(4, 4, '2016-11-24 00:00:00', 1),
(5, 5, '2016-11-24 00:00:00', 1),
(6, 6, '2016-11-24 00:00:00', 0),
(7, 7, '2016-11-24 00:00:00', 0),
(8, 8, '2016-11-24 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_112233erfr_61_size`
--

CREATE TABLE `artikal_data_112233erfr_61_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_112233erfr_61_size`
--

INSERT INTO `artikal_data_112233erfr_61_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '2016-11-24 00:00:00', 1),
(2, 2, '2016-11-24 00:00:00', 1),
(3, 3, '2016-11-24 00:00:00', 1),
(4, 4, '2016-11-24 00:00:00', 1),
(5, 5, '2016-11-24 00:00:00', 0),
(6, 6, '2016-11-24 00:00:00', 0),
(7, 7, '2016-11-24 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59`
--

CREATE TABLE `artikal_data_1122888_59` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59`
--

INSERT INTO `artikal_data_1122888_59` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(5, '01.008', 'SEP. TELO SCHIENA + MANICA', 'TAGLIO A MANO', 100, NULL, '2016-10-25 22:18:32'),
(6, '01.011', 'COLLO SING. C 205 1X1---> C 221 1X1', 'RIMAGLIO', 150, NULL, '2016-10-25 22:19:31'),
(7, '123456', 'test brisajnja', 'Test brisanja', 11122, NULL, '2016-11-29 00:45:25');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59_color`
--

CREATE TABLE `artikal_data_1122888_59_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59_color`
--

INSERT INTO `artikal_data_1122888_59_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 1),
(3, 3, '0000-00-00 00:00:00', 1),
(4, 4, '0000-00-00 00:00:00', 1),
(5, 5, '0000-00-00 00:00:00', 1),
(6, 6, '0000-00-00 00:00:00', 1),
(7, 7, '0000-00-00 00:00:00', 0),
(8, 8, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_1122888_59_size`
--

CREATE TABLE `artikal_data_1122888_59_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_1122888_59_size`
--

INSERT INTO `artikal_data_1122888_59_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 1),
(3, 3, '0000-00-00 00:00:00', 1),
(4, 4, '0000-00-00 00:00:00', 1),
(5, 5, '0000-00-00 00:00:00', 1),
(6, 6, '0000-00-00 00:00:00', 1),
(7, 7, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60`
--

CREATE TABLE `artikal_data_dfgh_60` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `opis_operacije` varchar(100) DEFAULT NULL,
  `operacija_name` varchar(100) DEFAULT NULL,
  `tempo_operacije` int(3) DEFAULT NULL,
  `kolicina_po_operaciji` int(5) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60`
--

INSERT INTO `artikal_data_dfgh_60` (`id`, `operacija_id`, `opis_operacije`, `operacija_name`, `tempo_operacije`, `kolicina_po_operaciji`, `operacija_datum_dodavanja`) VALUES
(1, '01.011', 'COLLO SING. C 205 1X1---> C 221 1X1', 'RIMAGLIO', 31, NULL, '2016-10-25 22:19:31'),
(2, '01.017', '1', 'TRAVETTA', 125, NULL, '2016-10-25 22:21:05'),
(3, '01.013', '2A SPALLA (STRETTA)', 'TAGLIACUCI UNIONE', 50, NULL, '2016-10-25 22:20:08'),
(4, '01.014', 'MANICHE', 'TAGLIACUCI UNIONE', 105, NULL, '2016-10-25 22:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60_color`
--

CREATE TABLE `artikal_data_dfgh_60_color` (
  `id` int(11) NOT NULL,
  `color_id` int(3) DEFAULT NULL,
  `color_datum_dodavanja` datetime DEFAULT NULL,
  `color_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60_color`
--

INSERT INTO `artikal_data_dfgh_60_color` (`id`, `color_id`, `color_datum_dodavanja`, `color_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 0),
(3, 3, '0000-00-00 00:00:00', 0),
(4, 4, '0000-00-00 00:00:00', 0),
(5, 5, '0000-00-00 00:00:00', 1),
(6, 6, '0000-00-00 00:00:00', 0),
(7, 7, '0000-00-00 00:00:00', 0),
(8, 8, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artikal_data_dfgh_60_size`
--

CREATE TABLE `artikal_data_dfgh_60_size` (
  `id` int(11) NOT NULL,
  `size_id` int(3) DEFAULT NULL,
  `size_datum_dodavanja` datetime DEFAULT NULL,
  `size_status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikal_data_dfgh_60_size`
--

INSERT INTO `artikal_data_dfgh_60_size` (`id`, `size_id`, `size_datum_dodavanja`, `size_status`) VALUES
(1, 1, '0000-00-00 00:00:00', 1),
(2, 2, '0000-00-00 00:00:00', 0),
(3, 3, '0000-00-00 00:00:00', 0),
(4, 4, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(2) NOT NULL,
  `color_name` varchar(40) DEFAULT NULL,
  `color_value` varchar(6) DEFAULT NULL,
  `color_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `color_name`, `color_value`, `color_date`) VALUES
(1, 'Sucmurasta', 'AB2567', '2016-10-22 00:00:00'),
(2, 'Bezi', '0C07AB', '2016-10-22 00:00:00'),
(3, 'Neka tamo', '0A15AB', '2016-10-22 00:00:00'),
(4, 'Nedefenisina', '43grdf', '2016-10-22 00:00:00'),
(5, 'Blue', '598AAB', '2016-10-22 00:00:00'),
(6, 'Nedefenisina', '3EAB1D', '2016-10-23 00:00:00'),
(7, 'Nedefenisina', 'BD2972', '2016-10-23 00:00:00'),
(8, 'Nedefenisina', 'ABAB60', '2016-10-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `komesa`
--

CREATE TABLE `komesa` (
  `id` int(11) NOT NULL,
  `komesa_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `komesa_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `komesa_datum_unosa` date NOT NULL,
  `artikal_id` int(11) NOT NULL,
  `artikal_komada_per_komesa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `komesa`
--

INSERT INTO `komesa` (`id`, `komesa_id`, `komesa_name`, `komesa_datum_unosa`, `artikal_id`, `artikal_komada_per_komesa`) VALUES
(6, '7', ';DSKJFLKEAJ', '2016-11-18', 60, 21),
(7, '7', ';DSKJFLKEAJ', '2016-11-18', 60, 21),
(8, '7', ';DSKJFLKEAJ', '2016-11-18', 60, 21),
(9, 'esdfedasf', 'KMFCKMSCKM', '2016-11-18', 60, 25415),
(10, '562656', 'TES 2', '2016-11-18', 59, 525),
(11, 'edkflnklasnm', 'KMFCKMSCKM', '2016-11-27', 59, 25425),
(12, '45875', '87545', '2016-12-04', 61, 1254),
(13, '464q458', 'JHGYUGJHGV', '2016-12-04', 60, 23432),
(14, 'lillherdfghbrdfjg', 'NULL', '2016-12-04', 59, 858484),
(15, '171_ 2365_0', '171_ 2365_0', '2016-12-05', 62, 3633);

-- --------------------------------------------------------

--
-- Table structure for table `komesa_line_settings`
--

CREATE TABLE `komesa_line_settings` (
  `id` int(11) NOT NULL,
  `working_day` date NOT NULL,
  `linija_id` int(11) NOT NULL,
  `komesa_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komesa_line_settings`
--

INSERT INTO `komesa_line_settings` (`id`, `working_day`, `linija_id`, `komesa_id`) VALUES
(5, '2016-08-17', 1, 5),
(6, '2016-10-09', 1, 5),
(7, '2016-10-10', 1, 5),
(8, '2016-10-10', 1, 5),
(9, '2016-10-11', 1, 5),
(10, '2016-10-12', 1, 5),
(11, '2016-10-13', 1, 5),
(12, '2016-10-16', 1, 5),
(13, '2016-10-18', 1, 5),
(14, '2016-10-18', 1, 5),
(15, '2016-10-22', 1, 5),
(16, '2016-10-24', 1, 5),
(17, '2016-10-25', 1, 5),
(18, '2016-10-26', 1, 5),
(20, '2016-10-27', 1, 5),
(21, '2016-10-30', 1, 5),
(22, '2016-11-01', 1, 5),
(23, '2016-11-03', 1, 5),
(24, '2016-11-04', 1, 5),
(25, '2016-11-05', 1, 5),
(26, '2016-11-06', 1, 6),
(27, '2016-11-07', 1, 5),
(28, '2016-11-08', 1, 5),
(29, '2016-11-08', 2, 6),
(30, '2016-11-17', 1, 5),
(33, '2016-11-18', 1, 5),
(34, '2016-11-24', 1, 6),
(35, '2016-11-27', 1, 7),
(37, '2016-11-28', 1, 6),
(38, '2016-11-28', 1, 11),
(39, '2016-11-28', 1, 8),
(40, '2016-11-29', 2, 11),
(41, '2016-11-29', 2, 11),
(42, '2016-11-29', 3, 8),
(43, '2016-12-01', 1, 6),
(44, '2016-12-01', 2, 9),
(45, '2016-12-01', 3, 11),
(46, '2016-12-04', 1, 8),
(47, '2016-12-05', 2, 14),
(48, '2016-12-05', 2, 15),
(49, '2016-12-06', 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `linija`
--

CREATE TABLE `linija` (
  `id` int(3) NOT NULL,
  `linija_id` int(3) NOT NULL,
  `linija_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linija`
--

INSERT INTO `linija` (`id`, `linija_id`, `linija_name`) VALUES
(1, 0, 'Linija 000'),
(2, 1, 'Linija 001'),
(3, 2, 'Linija 002'),
(4, 3, 'Linija 003');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_3`
--

CREATE TABLE `masina_data_3` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_3`
--

INSERT INTO `masina_data_3` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.007', '2016-11-28 18:56:00'),
(2, '01.010', '2016-11-28 19:20:35'),
(3, '01.013', '2016-11-28 19:20:39'),
(4, '01.015', '2016-11-28 19:20:43'),
(5, '01.017', '2016-11-28 19:20:48'),
(6, '123456', '2016-11-29 00:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_4`
--

CREATE TABLE `masina_data_4` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_4`
--

INSERT INTO `masina_data_4` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '534513', '2016-11-28 19:11:47'),
(2, '01.001', '2016-12-05 16:25:58'),
(3, '01.002', '2016-12-05 16:25:58'),
(4, '01.003', '2016-12-05 16:25:58'),
(5, '01.004', '2016-12-05 16:25:58'),
(6, '01.005', '2016-12-05 16:25:58'),
(7, '01.006', '2016-12-05 16:25:58');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_5`
--

CREATE TABLE `masina_data_5` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_5`
--

INSERT INTO `masina_data_5` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.007', '2016-12-05 16:27:18'),
(2, '01.008', '2016-12-05 16:27:18'),
(3, '01.009', '2016-12-05 16:27:18'),
(4, '01.011', '2016-12-05 16:27:18'),
(5, '01.010', '2016-12-05 16:27:18'),
(6, '01.012', '2016-12-05 16:27:18'),
(7, '01.013', '2016-12-05 16:27:18');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_6`
--

CREATE TABLE `masina_data_6` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_6`
--

INSERT INTO `masina_data_6` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.019', '2016-11-28 21:22:37'),
(2, '01.014', '2016-11-28 23:30:06'),
(3, '01.011', '2016-11-29 00:31:26'),
(4, '01.017', '2016-11-29 00:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_7`
--

CREATE TABLE `masina_data_7` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_8`
--

CREATE TABLE `masina_data_8` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_8`
--

INSERT INTO `masina_data_8` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '01.007', '2016-11-28 21:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_9`
--

CREATE TABLE `masina_data_9` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masina_data_10`
--

CREATE TABLE `masina_data_10` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina_data_10`
--

INSERT INTO `masina_data_10` (`id`, `operacija_id`, `operacija_datum_dodavanja`) VALUES
(1, '123456', '2016-11-29 00:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `masine`
--

CREATE TABLE `masine` (
  `id` int(11) NOT NULL,
  `masina_name` varchar(50) DEFAULT NULL,
  `datum_dodavanja` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masine`
--

INSERT INTO `masine` (`id`, `masina_name`, `datum_dodavanja`) VALUES
(3, 'OV', '2016-11-28 18:52:11'),
(4, 'T/C', '2016-11-28 19:11:11'),
(5, 'Rimaglio', '2016-11-28 19:12:11'),
(6, 'Lineare', '2016-11-28 19:12:11'),
(7, 'Travetta', '2016-11-28 19:12:11'),
(8, 'Manichino', '2016-11-28 19:12:11'),
(9, 'Taglio Fin.', '2016-11-28 19:13:11'),
(10, 'Bordatrice', '2016-11-28 19:13:11');

-- --------------------------------------------------------

--
-- Table structure for table `operacije`
--

CREATE TABLE `operacije` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(10) DEFAULT NULL,
  `operacija_name` varchar(50) NOT NULL,
  `opis_operacije` varchar(50) DEFAULT NULL,
  `tempo_operacije` int(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operacije`
--

INSERT INTO `operacije` (`id`, `operacija_id`, `operacija_name`, `opis_operacije`, `tempo_operacije`, `operacija_datum_dodavanja`) VALUES
(19, '01.001', 'T/C COLLO DAVANTI', 'T/C COLLO DAVANTI', 0, '2016-12-05 16:13:15'),
(20, '01.002', 'T/C SURFILATURA SCHIENE', 'T/C SURFILATURA SCHIENE', 0, '2016-12-05 16:15:23'),
(21, '01.003', 'T/C SURFILATURA MANICHE', 'SURFILATURA MANICHE T/C', 0, '2016-12-05 16:15:57'),
(22, '01.004', 'T/C SURFILATURA SPALLE', 'T/C SURFILATURA SPALLE', 0, '2016-12-05 16:16:55'),
(23, '01.005', 'T/C SPALLE TAGLIATO', 'T/C SPALLE TAGLIATO', 0, '2016-12-05 16:17:52'),
(24, '01.006', 'T/C FIANCHI TAGLIATO', 'T/C FIANCHI TAGLIATO', 0, '2016-12-05 16:18:31'),
(25, '01.007', 'RIMAGLIO SPALLE', 'RIMAGLIO SPALLE', 0, '2016-12-05 16:19:53'),
(26, '01.008', 'RIMAGLIO 1Â° SPALLA', 'RIAMGLIO 1Â° SPALLA', 0, '2016-12-05 16:20:43'),
(27, '01.009', 'RIMAGLIO 2Â° SPALLA', 'RIMAGLIO 2Â° SPALLA', 0, '2016-12-05 16:21:19'),
(28, '01.011', 'RIMAGLIO FIANCO COLLO', 'RIMAGLIO FIANCO COLLO', 0, '2016-12-05 16:22:16'),
(29, '01.010', 'RIMAGLIO COLLO', 'RIAMGLIO COLLO', 0, '2016-12-05 16:23:03'),
(30, '01.012', 'RIMAGLIO MANICHE', 'RIMAGLIO MANICHE', 0, '2016-12-05 16:23:36'),
(31, '01.013', 'RIMAGLIO FIANCHI', 'RIMAGLIO FIANCHI', 0, '2016-12-05 16:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `radnici_lista`
--

CREATE TABLE `radnici_lista` (
  `id` int(11) NOT NULL,
  `radnik_id` int(11) NOT NULL,
  `radnik_name` varchar(50) NOT NULL,
  `radnik_lifetime_from` datetime NOT NULL,
  `linija` int(3) NOT NULL,
  `sifra` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `radnici_lista`
--

INSERT INTO `radnici_lista` (`id`, `radnik_id`, `radnik_name`, `radnik_lifetime_from`, `linija`, `sifra`) VALUES
(1, 101, 'Tanija Stankovic', '2014-11-03 00:00:00', 1, 0),
(2, 102, 'Tanjia stankovic', '2015-05-13 00:00:00', 0, 102),
(3, 3, 'Nevena', '2015-05-30 00:00:00', 0, 3),
(4, 4, 'Jelena', '2015-05-13 00:00:00', 0, 0),
(5, 5, 'Dragica', '2014-09-09 00:00:00', 0, 5),
(6, 6, 'Tanja Kostadinovic', '2015-05-13 00:00:00', 0, 6),
(7, 7, 'Tara ', '2016-11-24 00:00:00', 0, 7),
(8, 8, 'Ana Pajc', '2014-01-15 00:00:00', 0, 8),
(9, 9, 'TINA', '2016-12-06 00:00:00', 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(2) NOT NULL,
  `size_name` varchar(40) DEFAULT NULL,
  `size_value` varchar(6) DEFAULT NULL,
  `size_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size_name`, `size_value`, `size_date`) VALUES
(1, 'xs', 'xs', '2016-11-03 00:00:00'),
(2, 's', 's', '2016-11-03 00:00:00'),
(3, 'm', 'm', '2016-11-03 00:00:00'),
(4, 'l', 'l', '2016-11-03 00:00:00'),
(5, 'xl', 'xl', '2016-11-03 00:00:00'),
(6, '2xl', '2xl', '2016-11-03 00:00:00'),
(7, '3xl', '3xl', '2016-11-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_31`
--

CREATE TABLE `tablet_data_31` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_32`
--

CREATE TABLE `tablet_data_32` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_33`
--

CREATE TABLE `tablet_data_33` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_34`
--

CREATE TABLE `tablet_data_34` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_35`
--

CREATE TABLE `tablet_data_35` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_36`
--

CREATE TABLE `tablet_data_36` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_37`
--

CREATE TABLE `tablet_data_37` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_38`
--

CREATE TABLE `tablet_data_38` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_39`
--

CREATE TABLE `tablet_data_39` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_40`
--

CREATE TABLE `tablet_data_40` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_41`
--

CREATE TABLE `tablet_data_41` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_42`
--

CREATE TABLE `tablet_data_42` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_43`
--

CREATE TABLE `tablet_data_43` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_44`
--

CREATE TABLE `tablet_data_44` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_data_45`
--

CREATE TABLE `tablet_data_45` (
  `id` int(11) NOT NULL,
  `operacija_id` varchar(11) DEFAULT NULL,
  `operacija_datum_dodavanja` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_ip_settings`
--

CREATE TABLE `tablet_ip_settings` (
  `id` int(3) NOT NULL,
  `tablet_id` varchar(50) NOT NULL,
  `masina` varchar(50) DEFAULT NULL,
  `tablet_ip` varchar(50) NOT NULL,
  `converted_tablet_ip` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tablet_ip_settings`
--

INSERT INTO `tablet_ip_settings` (`id`, `tablet_id`, `masina`, `tablet_ip`, `converted_tablet_ip`) VALUES
(28, '127.0.0.1', '6', '127.0.0.1', '2130706433'),
(33, '25', '5', '192.168.1.25', '3232235801'),
(32, '30', '5', '192.168.1.30', '3232235806'),
(31, '111', '3', '192.168.1.8', '3232235784'),
(34, '34', '5', '192.168.1.34', '3232235810'),
(35, '21', '5', '192.168.1.21', '3232235797'),
(36, '28', '5', '192.168.1.28', '3232235804'),
(37, '27', '5', '192.168.1.27', '3232235803'),
(38, '22', '5', '192.168.1.22', '3232235798'),
(39, '29', '4', '192.168.1.29', '3232235805'),
(40, '32', '4', '192.168.1.32', '3232235808'),
(41, '33', '4', '192.168.1.33', '3232235809'),
(42, '26', '4', '192.168.1.26', '3232235802'),
(43, '23', '5', '192.168.1.23', '3232235799'),
(44, '24', '4', '192.168.1.24', '3232235800'),
(45, '192.168.1.200', '4', '192.168.1.200', '3232235976');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `json_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `json_data`) VALUES
(1, '{\r\n"id":"0001",\r\n"value":"1"\r\n}'),
(2, '{\r\n"id":"0002",\r\n"value":"2"\r\n}');

-- --------------------------------------------------------

--
-- Table structure for table `tuscan_family_options`
--

CREATE TABLE `tuscan_family_options` (
  `option_id` int(5) NOT NULL,
  `option_name` varchar(50) NOT NULL,
  `option_value` longtext NOT NULL,
  `option_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuscan_family_options`
--

INSERT INTO `tuscan_family_options` (`option_id`, `option_name`, `option_value`, `option_status`) VALUES
(1, 'siteurl', 'http://192.168.1.200/tuscan/', '1'),
(2, 'btn_disable_time', '20', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials_tbl`
--

CREATE TABLE `tutorials_tbl` (
  `tutorial_id` int(11) NOT NULL,
  `tutorial_title` varchar(100) NOT NULL,
  `tutorial_author` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `working_day_session`
--

CREATE TABLE `working_day_session` (
  `id` int(11) NOT NULL,
  `working_day_session_id` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `work_day` date DEFAULT NULL,
  `radnik_id` int(11) DEFAULT NULL,
  `linija` int(3) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `pausa_1_start_time` datetime DEFAULT NULL,
  `pausa_1_end_time` datetime DEFAULT NULL,
  `pausa_2_start_time` datetime DEFAULT NULL,
  `pausa_2_end_time` datetime DEFAULT NULL,
  `komesa` int(11) DEFAULT NULL,
  `artikal_id` int(11) DEFAULT NULL,
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operacija_id` int(11) DEFAULT NULL,
  `operacija_hint` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `working_day_session`
--

INSERT INTO `working_day_session` (`id`, `working_day_session_id`, `work_day`, `radnik_id`, `linija`, `login_time`, `logout_time`, `pausa_1_start_time`, `pausa_1_end_time`, `pausa_2_start_time`, `pausa_2_end_time`, `komesa`, `artikal_id`, `color`, `size`, `operacija_id`, `operacija_hint`) VALUES
(1, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 101, 2, '2016-11-08 04:08:53', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 3),
(2, '2-101-8-59-6-Sucmurasta-m', '2016-11-08', 101, 2, '2016-11-08 04:18:51', '2016-12-04 17:21:29', '0000-00-00 00:00:00', '2016-11-08 00:00:00', NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 1),
(4, NULL, '2016-11-08', 101, NULL, '2016-11-08 04:34:48', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(6, '6-4-8-59-6-Sucmurasta-s', '2016-11-08', 4, 2, '2016-11-08 04:43:38', '2016-12-06 06:21:06', '2016-11-08 04:46:38', NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 0),
(7, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 4, 2, '2016-11-08 04:48:51', '2016-12-06 06:21:06', '2016-11-08 04:49:09', '2016-11-08 04:54:32', NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 7),
(8, NULL, '2016-11-08', 3, NULL, '2016-11-08 04:59:32', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(9, '9-5-11-59-6-Sucmurasta-s', '2016-11-08', 5, 2, '2016-11-08 05:02:10', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 11, 1),
(10, '10-101-8-59-5-Sucmurasta-s', '2016-11-08', 101, 1, '2016-11-08 15:32:26', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 1),
(11, '11-5-11-59-6-Sucmurasta-s', '2016-11-08', 5, 2, '2016-11-08 15:41:35', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 11, 0),
(12, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 5, 2, '2016-11-08 15:43:31', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 6, 59, 'NULL', 'NULL', 8, 5),
(13, NULL, '2016-11-08', 5, NULL, '2016-11-08 16:21:44', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(14, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 16:43:57', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 11, 3),
(15, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 2, 1, '2016-11-17 16:49:13', '2016-11-18 23:03:36', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 11, 21),
(16, '16-101-8-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 23:55:34', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(17, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 101, 1, '2016-11-17 23:56:18', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 10),
(18, '18-101-8-59-5-Sucmurasta-s', '2016-11-18', 101, 1, '2016-11-18 00:58:57', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(19, '19-101-8-59-5-Sucmurasta-s', '2016-11-18', 101, 1, '2016-11-18 01:15:55', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(20, '20-6-8-59-5-Sucmurasta-s', '2016-11-18', 6, 1, '2016-11-18 01:16:15', '2016-12-06 15:09:27', NULL, NULL, NULL, NULL, 5, 59, 'NULL', 'NULL', 8, 0),
(21, NULL, '2016-11-18', 2, NULL, '2016-11-18 01:36:17', '2016-11-18 23:03:36', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(22, NULL, '2016-11-18', 101, NULL, '2016-11-18 16:15:10', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(23, NULL, '2016-11-18', 101, NULL, '2016-11-18 16:17:58', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(24, '24-101-11-60-6-Sucmurasta-xs', '2016-11-24', 101, 1, '2016-11-24 16:57:15', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 6, 60, 'NULL', 'NULL', 11, 0),
(25, NULL, '2016-11-24', 101, NULL, '2016-11-24 17:14:09', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(26, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 101, 1, '2016-11-27 14:48:17', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 7, 60, 'NULL', 'NULL', 1, 5),
(27, '27-101-01.011-59-11-Nedefenisina-xs', '2016-11-28', 101, 1, '2016-11-28 15:45:58', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 0),
(28, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 3, 1, '2016-11-28 20:46:12', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 12),
(29, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 3, 3, '2016-11-29 00:19:32', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 17),
(30, '30-3-01.014-60-8-Blue-l', '2016-11-29', 3, 3, '2016-11-29 00:32:06', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 19),
(31, NULL, '2016-11-29', 3, NULL, '2016-11-29 00:55:52', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(32, NULL, '2016-11-29', 4, NULL, '2016-11-29 20:52:48', '2016-12-06 06:21:06', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(33, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 4, 2, '2016-11-29 20:55:18', '2016-12-06 06:21:06', NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 4),
(34, NULL, '2016-12-01', 101, NULL, '2016-12-01 15:34:56', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', 0, 0),
(35, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 101, 1, '2016-12-01 15:35:50', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 6, 60, 'NULL', 'NULL', 1, 2),
(36, '36-101-01.011-59-11-Sucmurasta-xs', '2016-12-01', 101, 3, '2016-12-01 15:42:19', '2016-12-04 17:21:29', NULL, NULL, NULL, NULL, 11, 59, 'NULL', 'NULL', 1, 3),
(37, '37-10-01.017-60-8-0-Univerzalna', '2016-12-04', 10, 1, '2016-12-04 11:59:30', '2016-12-04 17:21:30', NULL, NULL, NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 3),
(38, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 101, 1, '2016-12-04 17:26:25', '2016-12-04 17:36:34', '2016-12-04 17:29:24', '2016-12-04 17:34:46', NULL, NULL, 8, 60, 'NULL', 'NULL', 1, 7),
(39, NULL, '2016-12-05', 101, NULL, '2016-12-05 16:51:38', '2016-12-05 16:52:15', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(40, NULL, '2016-12-05', 101, NULL, '2016-12-05 16:52:33', '2016-12-05 16:53:09', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(41, NULL, '2016-12-05', 101, NULL, '2016-12-05 16:53:20', '2016-12-05 16:55:42', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(42, '42-101-01.004-62-15-0-Univerzalna', '2016-12-05', 101, 2, '2016-12-05 16:56:03', '2016-12-05 17:01:51', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(43, '43-3-01.013-62-15-0-Univerzalna', '2016-12-05', 3, 2, '2016-12-05 16:58:12', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 1),
(44, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 101, 2, '2016-12-05 17:02:01', '2016-12-05 17:25:04', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 15),
(45, '45-3-01.013-62-15-0-Univerzalna', '2016-12-05', 3, 2, '2016-12-05 17:17:01', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(49, '49-5-01.013-62-15-0-Univerzalna', '2016-12-05', 5, 2, '2016-12-05 17:22:36', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(50, '50-3-01.013-62-15-0-Univerzalna', '2016-12-05', 3, 2, '2016-12-05 17:22:56', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(51, NULL, '2016-12-05', 4, NULL, '2016-12-05 17:40:42', '2016-12-06 06:21:06', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(52, '52-5-01.011-62-15-0-Univerzalna', '2016-12-05', 5, 2, '2016-12-05 17:40:55', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 1),
(53, NULL, '2016-12-05', 5, NULL, '2016-12-05 17:42:34', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(54, NULL, '2016-12-05', 5, NULL, '2016-12-05 17:43:06', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(55, '55-4-01.004-62-15-0-Univerzalna', '2016-12-06', 4, 2, '2016-12-06 06:16:27', '2016-12-06 06:31:48', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(56, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 4, 2, '2016-12-06 06:21:09', '2016-12-06 12:55:00', '2016-12-06 07:53:12', NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 1420),
(57, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 5, 2, '2016-12-06 06:32:00', '2016-12-06 15:09:31', '2016-12-06 09:58:16', NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 612),
(58, '58-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 06:35:03', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(59, NULL, '2016-12-06', 8, NULL, '2016-12-06 06:51:48', '2016-12-06 07:04:48', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(60, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 06:54:39', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 30),
(61, NULL, '2016-12-06', 102, NULL, '2016-12-06 07:01:28', '2016-12-06 07:04:36', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(62, NULL, '2016-12-06', 102, NULL, '2016-12-06 07:03:35', '2016-12-06 07:05:35', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(63, '63-102-01.007-62-15-0-Univerzalna', '2016-12-06', 102, 2, '2016-12-06 07:05:51', '2016-12-06 07:24:35', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(64, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 6, 2, '2016-12-06 07:07:27', '2016-12-06 15:09:27', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 112),
(65, '65-9-01.012-62-15-Sucmurasta-xs', '2016-12-06', 9, 2, '2016-12-06 07:10:30', '2016-12-06 15:09:29', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(66, '66-7-01.012-62-15-0-Univerzalna', '2016-12-06', 7, 2, '2016-12-06 07:12:36', '2016-12-06 15:09:30', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 0),
(67, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 102, 2, '2016-12-06 07:24:57', '2016-12-06 08:46:07', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 118),
(68, NULL, '2016-12-06', 3, NULL, '2016-12-06 07:28:12', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(69, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 07:29:25', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 147),
(70, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 102, 2, '2016-12-06 08:46:19', '2016-12-06 15:00:57', '2016-12-06 09:55:58', NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 614),
(71, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 09:23:00', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 130),
(72, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 10:32:37', '2016-12-06 15:09:23', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 233),
(73, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 4, 2, '2016-12-06 13:05:39', '2016-12-06 14:57:15', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 1026),
(74, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 5, 2, '2016-12-06 13:28:55', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 713),
(75, NULL, '2016-12-06', 5, NULL, '2016-12-06 14:11:44', '2016-12-06 15:09:31', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(76, NULL, '2016-12-06', 102, NULL, '2016-12-06 15:05:41', '2016-12-06 15:07:58', NULL, NULL, NULL, NULL, NULL, NULL, 'NULL', 'NULL', NULL, 0),
(77, '77-3-01.007-62-15-0-Univerzalna', '2016-12-06', 3, 2, '2016-12-06 15:10:13', '2016-12-06 15:13:12', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 171),
(78, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 102, 2, '2016-12-06 15:13:47', NULL, NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 618),
(79, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 101, 2, '2016-12-06 18:59:43', '2016-12-06 19:01:39', NULL, NULL, NULL, NULL, 15, 62, 'NULL', 'NULL', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `working_day_session_details`
--

CREATE TABLE `working_day_session_details` (
  `id` int(11) NOT NULL,
  `working_day_session_id` varchar(50) NOT NULL,
  `working_day` date DEFAULT NULL,
  `komesa_id` int(11) DEFAULT NULL,
  `radnik_id` int(11) DEFAULT NULL,
  `linija_id` int(11) DEFAULT NULL,
  `artikal_id` int(11) DEFAULT NULL,
  `operacija_id` int(11) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `vreme_operacije` datetime DEFAULT NULL,
  `razlika_u_vremenu` time DEFAULT NULL,
  `procenat_ucinka` int(3) DEFAULT NULL,
  `hint_code` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `working_day_session_details`
--

INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(1, '1-101-8-59-6-Sucmurasta-s', NULL, 6, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(3, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 04:16:54', '00:04:54', 17, 'F001'),
(4, '1-101-8-59-6-Sucmurasta-s', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 04:18:36', '00:01:42', 49, 'F001'),
(5, '2-101-8-59-6-Sucmurasta-m', NULL, 6, 101, NULL, 59, 8, 'Sucmurasta', 'm', NULL, NULL, NULL, NULL),
(6, '2-101-8-59-6-Sucmurasta-m', '2016-11-08', 6, 101, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 04:20:07', '00:00:00', 116, 'F001'),
(8, '3-5-8-59-6-Nedefenisina-s', NULL, 6, 5, NULL, 59, 8, 'Nedefenisi', 's', NULL, NULL, NULL, NULL),
(9, '3-5-8-59-6-Nedefenisina-s', '2016-11-08', 6, 5, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:29:05', '00:00:00', 39, 'F001'),
(10, '3-5-8-59-6-Nedefenisina-s', '2016-11-08', NULL, 5, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:29:30', NULL, NULL, 'P001'),
(11, '5-4-11-59-6-Nedefenisina-xl', NULL, 6, 4, NULL, 59, 11, 'Nedefenisi', 'xl', NULL, NULL, NULL, NULL),
(12, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:36:26', '00:00:00', 131, 'F001'),
(13, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:37:48', '00:01:22', 123, 'F001'),
(15, '5-4-11-59-6-Nedefenisina-xl', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 'xl', '2016-11-08 04:41:33', '00:01:35', 106, 'F001'),
(16, '6-4-8-59-6-Sucmurasta-s', NULL, 6, 4, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(19, '7-4-8-59-6-Nedefenisina-s', NULL, 6, 4, NULL, 59, 8, 'Nedefenisi', 's', NULL, NULL, NULL, NULL),
(20, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', NULL, 4, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:49:09', NULL, NULL, 'P001'),
(21, '7-4-8-59-6-Nedefenisina-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:53:55', NULL, NULL, ''),
(22, '7-4-8-59-6-Nedefenisina-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-08 04:54:32', NULL, NULL, ''),
(23, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:55:27', '00:00:55', 90, 'F001'),
(24, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:56:22', '00:00:55', 90, 'F001'),
(25, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:57:01', '00:00:39', 128, 'F001'),
(26, '7-4-8-59-6-Nedefenisina-s', '2016-11-08', 6, 4, 2, 59, 6, 'Nedefenisi', 's', '2016-11-08 04:59:23', '00:02:22', 35, 'F001'),
(27, '9-5-11-59-6-Sucmurasta-s', NULL, 6, 5, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(28, '9-5-11-59-6-Sucmurasta-s', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 's', '2016-11-08 05:05:06', '00:00:00', 109, 'F001'),
(29, '10-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(30, '10-101-8-59-5-Sucmurasta-s', '2016-11-08', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-08 15:41:16', '00:00:00', 9, 'F001'),
(31, '11-5-11-59-6-Sucmurasta-s', NULL, 6, 5, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(32, '12-5-8-59-6-Sucmurasta-m', NULL, 6, 5, NULL, 59, 8, 'Sucmurasta', 'm', NULL, NULL, NULL, NULL),
(33, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:46:42', '00:00:00', 33, 'F001'),
(34, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:48:07', '00:01:25', 58, 'F001'),
(35, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:50:05', '00:01:58', 42, 'F001'),
(36, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:51:17', '00:01:12', 111, 'F001'),
(37, '12-5-8-59-6-Sucmurasta-m', '2016-11-08', 6, 5, 2, 59, 6, 'Sucmurasta', 'm', '2016-11-08 15:51:58', '00:00:41', 195, 'F001'),
(38, '14-101-11-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(39, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:45:39', '00:00:00', 104, 'F001'),
(40, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:47:57', '00:02:18', 29, 'F001'),
(41, '14-101-11-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:48:35', '00:00:38', 107, 'F001'),
(42, '15-2-11-59-5-Sucmurasta-s', NULL, 5, 2, NULL, 59, 11, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(43, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:50:11', '00:00:00', 95, 'F001'),
(44, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:50:44', '00:00:33', 123, 'F001'),
(45, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:51:23', '00:00:39', 104, 'F001'),
(46, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:52:11', '00:00:48', 85, 'F001'),
(47, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:52:43', '00:00:32', 127, 'F001'),
(48, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:55:30', '00:02:47', 24, 'F001'),
(49, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:56:01', '00:00:31', 131, 'F001'),
(50, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:56:32', '00:00:31', 131, 'F001'),
(51, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:57:06', '00:00:34', 120, 'F001'),
(52, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:57:42', '00:00:36', 113, 'F001'),
(53, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:58:28', '00:00:46', 88, 'F001'),
(54, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 16:59:41', '00:01:13', 56, 'F001'),
(55, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:04:24', '00:04:43', 14, 'F001'),
(56, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:05:48', '00:01:24', 48, 'F001'),
(57, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:06:42', '00:00:54', 75, 'F001'),
(58, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:17:25', '00:10:43', 6, 'F001'),
(59, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:20:38', '00:03:13', 21, 'F001'),
(60, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:23:59', '00:03:21', 20, 'F001'),
(61, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:30:13', '00:06:14', 10, 'F001'),
(62, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:31:11', '00:00:58', 70, 'F001'),
(63, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:32:29', '00:01:18', 52, 'F001'),
(64, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:33:37', '00:01:08', 60, 'F001'),
(65, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:34:12', '00:00:35', 116, 'F001'),
(66, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:34:56', '00:00:44', 92, 'F001'),
(67, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:35:36', '00:00:40', 102, 'F001'),
(68, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:36:17', '00:00:41', 99, 'F001'),
(69, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:36:50', '00:00:33', 123, 'F001'),
(70, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:37:31', '00:00:41', 99, 'F001'),
(71, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:38:27', '00:00:56', 73, 'F001'),
(72, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:39:04', '00:00:37', 110, 'F001'),
(73, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:39:47', '00:00:43', 95, 'F001'),
(74, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:41:18', '00:01:31', 44, 'F001'),
(75, '15-2-11-59-5-Sucmurasta-s', '2016-11-17', 5, 2, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 17:42:06', '00:00:48', 85, 'F001'),
(76, '16-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(77, '17-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(78, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 23:57:03', '00:00:00', 125, 'F001'),
(79, '17-101-8-59-5-Sucmurasta-s', '2016-11-17', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-17 23:57:38', '00:00:35', 128, 'F001'),
(80, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:33:48', '00:36:10', 2, 'F001'),
(81, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:44:24', '00:10:36', 7, 'F001'),
(82, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:45:22', '00:00:58', 77, 'F001'),
(83, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:47:37', '00:02:15', 33, 'F001'),
(84, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:49:27', '00:01:50', 40, 'F001'),
(85, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:50:13', '00:00:46', 97, 'F001'),
(86, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:50:49', '00:00:36', 125, 'F001'),
(87, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', NULL, 101, NULL, NULL, NULL, NULL, NULL, '2016-11-18 00:51:06', NULL, NULL, 'P001'),
(88, '17-101-8-59-5-Sucmurasta-s', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-11-18 00:51:11', NULL, NULL, 'P002'),
(89, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:52:34', '00:01:23', 54, 'F001'),
(90, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:53:20', '00:00:46', 97, 'F001'),
(91, '17-101-8-59-5-Sucmurasta-s', '2016-11-18', 5, 101, 1, 59, 5, 'Sucmurasta', 's', '2016-11-18 00:54:05', '00:00:45', 100, 'F001'),
(92, '18-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(93, '19-101-8-59-5-Sucmurasta-s', NULL, 5, 101, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(94, '20-6-8-59-5-Sucmurasta-s', NULL, 5, 6, NULL, 59, 8, 'Sucmurasta', 's', NULL, NULL, NULL, NULL),
(95, '24-101-11-60-6-Sucmurasta-xs', NULL, 6, 101, NULL, 60, 11, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(96, '26-101-01.017-60-7-Blue-xs', NULL, 7, 101, NULL, 60, 1, 'Blue', 'xs', NULL, NULL, NULL, NULL),
(97, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:09:52', '00:00:00', 0, 'F001'),
(98, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:10:42', '00:00:50', 0, 'F001'),
(99, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:11:00', '00:00:18', 0, 'F001'),
(100, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:11:51', '00:00:51', 0, 'F001'),
(101, '26-101-01.017-60-7-Blue-xs', '2016-11-27', 7, 101, 1, 60, 7, 'Blue', 'xs', '2016-11-27 15:12:38', '00:00:47', 0, 'F001'),
(102, '27-101-01.011-59-11-Nedefenisina-xs', NULL, 11, 101, NULL, 59, 1, 'Nedefenisi', 'xs', NULL, NULL, NULL, NULL),
(103, '28-3-01.014-60-8-Sucmurasta-xs', NULL, 8, 3, NULL, 60, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(104, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-28 23:57:39', '00:00:00', 0, 'F001'),
(105, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-28', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-28 23:58:26', '00:00:47', 0, 'F001'),
(106, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:01:19', '00:02:53', 0, 'F001'),
(107, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:03:57', '00:02:38', 0, 'F001'),
(108, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:04:36', '00:00:39', 0, 'F001'),
(109, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:06:31', '00:01:55', 0, 'F001'),
(110, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:07:37', '00:01:06', 0, 'F001'),
(111, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:08:03', '00:00:26', 0, 'F001'),
(112, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:09:27', '00:01:24', 0, 'F001'),
(113, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:12:18', '00:02:51', 20, 'F001'),
(114, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:12:46', '00:00:28', 122, 'F001'),
(115, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:13:11', '00:00:25', 137, 'F001'),
(116, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:14:53', '00:01:42', 33, 'F001'),
(117, '28-3-01.014-60-8-Sucmurasta-xs', '2016-11-29', 8, 3, 1, 60, 8, 'Sucmurasta', 'xs', '2016-11-29 00:19:22', '00:04:29', 12, 'F001'),
(118, '29-3-01.014-60-8-Blue-xs', NULL, 8, 3, NULL, 60, 1, 'Blue', 'xs', NULL, NULL, NULL, NULL),
(119, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:26:52', '00:00:00', 126, 'F001'),
(120, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:29:35', '00:02:43', 21, 'F001'),
(121, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:30:10', '00:00:35', 97, 'F001'),
(122, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:31:33', '00:01:23', 41, 'F001'),
(123, '29-3-01.014-60-8-Blue-xs', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'xs', '2016-11-29 00:31:59', '00:00:26', 131, 'F001'),
(124, '30-3-01.014-60-8-Blue-l', NULL, 8, 3, NULL, 60, 1, 'Blue', 'l', NULL, NULL, NULL, NULL),
(125, '30-3-01.014-60-8-Blue-l', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'l', '2016-11-29 00:50:09', '00:00:00', 20, 'F001'),
(126, '30-3-01.014-60-8-Blue-l', '2016-11-29', 8, 3, 3, 60, 8, 'Blue', 'l', '2016-11-29 00:55:26', '00:05:17', 10, 'F001'),
(127, '33-4-01.011-59-11-Sucmurasta-xs', NULL, 11, 4, NULL, 59, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(128, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 20:57:58', '00:00:00', 136, 'F001'),
(129, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 20:59:41', '00:01:43', 112, 'F001'),
(130, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 21:04:17', '00:04:36', 42, 'F001'),
(131, '33-4-01.011-59-11-Sucmurasta-xs', '2016-11-29', 11, 4, 2, 59, 11, 'Sucmurasta', 'xs', '2016-11-29 21:06:27', '00:02:10', 89, 'F001'),
(132, '35-101-01.011-60-6-Sucmurasta-xs', NULL, 6, 101, NULL, 60, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(133, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 6, 101, 1, 60, 6, 'Sucmurasta', 'xs', '2016-12-01 15:39:04', '00:00:00', 138, 'F001'),
(134, '35-101-01.011-60-6-Sucmurasta-xs', '2016-12-01', 6, 101, 1, 60, 6, 'Sucmurasta', 'xs', '2016-12-01 15:42:12', '00:03:08', 61, 'F001'),
(135, '36-101-01.011-59-11-Sucmurasta-xs', NULL, 11, 101, NULL, 59, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(136, '36-101-01.011-59-11-Sucmurasta-xs', '2016-12-01', 11, 101, 3, 59, 11, 'Sucmurasta', 'xs', '2016-12-01 16:15:04', '00:00:00', 117, 'F001'),
(137, '37-10-01.017-60-8-0-Univerzalna', NULL, 8, 10, NULL, 60, 1, '0', 'Univ', NULL, NULL, NULL, NULL),
(138, '37-10-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 10, 1, 60, 8, '0', 'Univ', '2016-12-04 12:24:47', '00:00:00', 16, 'F001'),
(139, '37-10-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 10, 1, 60, 8, '0', 'Univ', '2016-12-04 12:24:54', '00:00:07', 41, 'F001'),
(140, '37-10-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 10, 1, 60, 8, '0', 'Univ', '2016-12-04 12:24:57', '00:00:03', 95, 'F001'),
(141, '38-101-01.017-60-8-0-Univerzalna', NULL, 8, 101, NULL, 60, 1, '0', 'Univ', NULL, NULL, NULL, NULL),
(142, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:27:20', '00:00:00', 8, 'F001'),
(143, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:27:52', '00:00:32', 90, 'F001'),
(144, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:28:25', '00:00:33', 87, 'F001'),
(145, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:29:19', '00:00:54', 53, 'F001'),
(146, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', NULL, 101, NULL, NULL, NULL, NULL, NULL, '2016-12-04 17:29:24', NULL, NULL, 'P001'),
(147, '38-101-01.017-60-8-0-Univerzalna', '0000-00-00', NULL, 2016, NULL, NULL, NULL, NULL, NULL, '2016-12-04 17:34:46', NULL, NULL, 'P002'),
(148, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:35:18', '00:00:32', 90, 'F001'),
(149, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:35:53', '00:00:35', 82, 'F001'),
(150, '38-101-01.017-60-8-0-Univerzalna', '2016-12-04', 8, 101, 1, 60, 8, '0', 'Univ', '2016-12-04 17:36:29', '00:00:36', 80, 'F001'),
(151, '42-101-01.004-62-15-0-Univerzalna', NULL, 15, 101, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(152, '44-101-01.003-62-15-0-Univerzalna', NULL, 15, 101, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(153, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:02:55', '00:00:00', 75, 'F001'),
(154, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:03:19', '00:00:24', 88, 'F001'),
(155, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:03:44', '00:00:25', 84, 'F001'),
(156, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:03:57', '00:00:13', 162, 'F001'),
(157, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:05:38', '00:01:41', 20, 'F001'),
(158, '43-3-01.013-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(159, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:09:15', '00:03:37', 9, 'F001'),
(160, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:09:27', '00:00:12', 176, 'F001'),
(161, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:09:48', '00:00:21', 100, 'F001'),
(162, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:10:00', '00:00:12', 176, 'F001'),
(163, '43-3-01.013-62-15-0-Univerzalna', '2016-12-05', NULL, 3, NULL, NULL, NULL, NULL, NULL, '2016-12-05 17:13:35', NULL, NULL, 'P001'),
(164, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:16:13', '00:06:13', 5, 'F001'),
(165, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:16:49', '00:00:36', 58, 'F001'),
(166, '43-3-01.013-62-15-0-Univerzalna', '2016-12-05', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:16:52', '00:03:17', 40, 'F001'),
(167, '45-3-01.013-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(168, '46-4-01.013-62-15-0-Univerzalna', NULL, 15, 4, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(169, '46-4-01.013-62-15-0-Univerzalna', '2016-12-05', NULL, 4, NULL, NULL, NULL, NULL, NULL, '2016-12-05 17:17:39', NULL, NULL, 'P001'),
(170, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:18:23', '00:01:34', 22, 'F001'),
(171, '46-4-01.013-62-15-0-Univerzalna', '2016-12-05', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:18:44', '00:01:05', 123, 'F001'),
(172, '47-5-01.013-62-15-0-Univerzalna', NULL, 15, 5, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(173, '47-5-01.013-62-15-0-Univerzalna', '2016-12-05', NULL, 5, NULL, NULL, NULL, NULL, NULL, '2016-12-05 17:19:14', NULL, NULL, 'P001'),
(174, '48-6-01.013-62-15-0-Univerzalna', NULL, 15, 6, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(175, '48-6-01.013-62-15-0-Univerzalna', '2016-12-05', NULL, 6, NULL, NULL, NULL, NULL, NULL, '2016-12-05 17:20:20', NULL, NULL, 'P001'),
(176, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:21:42', '00:03:19', 10, 'F001'),
(177, '49-5-01.013-62-15-0-Univerzalna', NULL, 15, 5, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(178, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:22:47', '00:01:05', 32, 'F001'),
(179, '44-101-01.003-62-15-0-Univerzalna', '2016-12-05', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:22:59', '00:00:12', 176, 'F001'),
(180, '50-3-01.013-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(181, '52-5-01.011-62-15-0-Univerzalna', NULL, 15, 5, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(182, '52-5-01.011-62-15-0-Univerzalna', '2016-12-05', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-05 17:41:55', '00:00:00', 135, 'F001'),
(183, '56-4-01.003-62-15-0-Univerzalna', NULL, 15, 4, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(184, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:22:07', '00:00:00', 81, 'F001'),
(185, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:22:23', '00:00:16', 66, 'F001'),
(186, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:22:41', '00:00:18', 58, 'F001'),
(187, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:22:50', '00:00:09', 117, 'F001'),
(188, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:01', '00:00:11', 96, 'F001'),
(189, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:13', '00:00:12', 88, 'F001'),
(190, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:21', '00:00:08', 132, 'F001'),
(191, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:31', '00:00:10', 105, 'F001'),
(192, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:38', '00:00:07', 151, 'F001'),
(193, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:46', '00:00:08', 132, 'F001'),
(194, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:23:56', '00:00:10', 105, 'F001'),
(195, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:24:05', '00:00:09', 117, 'F001'),
(196, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:24:13', '00:00:08', 132, 'F001'),
(197, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:24:25', '00:00:12', 88, 'F001'),
(198, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:24:45', '00:00:20', 52, 'F001'),
(199, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:24:54', '00:00:09', 117, 'F001'),
(200, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:06', '00:00:12', 88, 'F001'),
(201, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:18', '00:00:12', 88, 'F001'),
(202, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:27', '00:00:09', 117, 'F001'),
(203, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:34', '00:00:07', 151, 'F001'),
(204, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:41', '00:00:07', 151, 'F001'),
(205, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:49', '00:00:08', 132, 'F001'),
(206, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:25:59', '00:00:10', 105, 'F001'),
(207, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:26:11', '00:00:12', 88, 'F001'),
(208, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:26:20', '00:00:09', 117, 'F001'),
(209, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:26:29', '00:00:09', 117, 'F001'),
(210, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:27:39', '00:01:10', 15, 'F001'),
(211, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:27:49', '00:00:10', 105, 'F001'),
(212, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:01', '00:00:12', 88, 'F001'),
(213, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:09', '00:00:08', 132, 'F001'),
(214, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:19', '00:00:10', 105, 'F001'),
(215, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:37', '00:00:18', 58, 'F001'),
(216, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:45', '00:00:08', 132, 'F001'),
(217, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:28:53', '00:00:08', 132, 'F001'),
(218, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:02', '00:00:09', 117, 'F001'),
(219, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:11', '00:00:09', 117, 'F001'),
(220, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:26', '00:00:15', 70, 'F001'),
(221, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:34', '00:00:08', 132, 'F001'),
(222, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:42', '00:00:08', 132, 'F001'),
(223, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:29:52', '00:00:10', 105, 'F001'),
(224, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:30:14', '00:00:22', 48, 'F001'),
(225, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:30:23', '00:00:09', 117, 'F001'),
(226, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:30:30', '00:00:07', 151, 'F001'),
(227, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:30:39', '00:00:09', 117, 'F001'),
(228, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:30:56', '00:00:17', 62, 'F001'),
(229, '55-4-01.004-62-15-0-Univerzalna', NULL, 15, 4, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(230, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:31:26', '00:00:30', 35, 'F001'),
(231, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:32:00', '00:00:34', 31, 'F001'),
(232, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:32:11', '00:00:11', 96, 'F001'),
(233, '57-5-01.004-62-15-0-Univerzalna', NULL, 15, 5, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(234, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:32:34', '00:00:00', 138, 'F001'),
(235, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:03', '00:00:52', 20, 'F001'),
(236, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:15', '00:00:12', 88, 'F001'),
(237, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:20', '00:00:46', 39, 'F001'),
(238, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:23', '00:00:08', 132, 'F001'),
(239, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:33', '00:00:10', 105, 'F001'),
(240, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:49', '00:00:16', 66, 'F001'),
(241, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:50', '00:00:30', 60, 'F001'),
(242, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:33:58', '00:00:09', 117, 'F001'),
(243, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:06', '00:00:08', 132, 'F001'),
(244, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:14', '00:00:08', 132, 'F001'),
(245, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:27', '00:00:37', 48, 'F001'),
(246, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:32', '00:00:18', 58, 'F001'),
(247, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:49', '00:00:17', 62, 'F001'),
(248, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:34:59', '00:00:32', 56, 'F001'),
(249, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:18', '00:00:29', 36, 'F001'),
(250, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:22', '00:00:23', 78, 'F001'),
(251, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:29', '00:00:11', 96, 'F001'),
(252, '58-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(253, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:39', '00:00:10', 105, 'F001'),
(254, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:48', '00:00:09', 117, 'F001'),
(255, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:35:50', '00:00:28', 64, 'F001'),
(256, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:00', '00:00:12', 88, 'F001'),
(257, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:10', '00:00:10', 105, 'F001'),
(258, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:18', '00:00:28', 64, 'F001'),
(259, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:22', '00:00:12', 88, 'F001'),
(260, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:32', '00:00:10', 105, 'F001'),
(261, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:43', '00:00:11', 96, 'F001'),
(262, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:36:54', '00:00:11', 96, 'F001'),
(263, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:06', '00:00:12', 88, 'F001'),
(264, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:16', '00:00:10', 105, 'F001'),
(265, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:24', '00:01:06', 27, 'F001'),
(266, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:27', '00:00:11', 96, 'F001'),
(267, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:38', '00:00:11', 96, 'F001'),
(268, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:37:50', '00:00:12', 88, 'F001'),
(269, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:38:06', '00:00:16', 66, 'F001'),
(270, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:38:16', '00:00:10', 105, 'F001'),
(271, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:39:28', '00:02:04', 14, 'F001'),
(272, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:39:29', '00:01:13', 14, 'F001'),
(273, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:39:37', '00:00:08', 132, 'F001'),
(274, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:39:47', '00:00:10', 105, 'F001'),
(275, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:39:57', '00:00:10', 105, 'F001'),
(276, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:40:06', '00:00:09', 117, 'F001'),
(277, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:40:15', '00:00:47', 38, 'F001'),
(278, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:40:18', '00:00:12', 88, 'F001'),
(279, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:40:41', '00:00:23', 46, 'F001'),
(280, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:40:45', '00:00:30', 60, 'F001'),
(281, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:08', '00:00:23', 78, 'F001'),
(282, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:17', '00:00:36', 29, 'F001'),
(283, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:27', '00:00:10', 105, 'F001'),
(284, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:36', '00:00:28', 64, 'F001'),
(285, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:46', '00:00:19', 55, 'F001'),
(286, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:56', '00:00:10', 105, 'F001'),
(287, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:41:59', '00:00:23', 78, 'F001'),
(288, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:42:05', '00:00:09', 117, 'F001'),
(289, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:42:14', '00:00:09', 117, 'F001'),
(290, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:44:03', '00:01:49', 9, 'F001'),
(291, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:44:13', '00:02:14', 13, 'F001'),
(292, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:44:41', '00:00:28', 64, 'F001'),
(293, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:45:02', '00:00:21', 85, 'F001'),
(294, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:45:28', '00:00:26', 69, 'F001'),
(295, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:45:57', '00:00:29', 62, 'F001'),
(296, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:10', '00:02:07', 8, 'F001'),
(297, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:18', '00:00:21', 85, 'F001'),
(298, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:23', '00:00:13', 81, 'F001'),
(299, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:32', '00:00:09', 117, 'F001'),
(300, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:35', '00:00:17', 105, 'F001'),
(301, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:39', '00:00:07', 151, 'F001'),
(302, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:46:49', '00:00:10', 105, 'F001'),
(303, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:03', '00:00:28', 64, 'F001'),
(304, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:17', '00:00:28', 37, 'F001'),
(305, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:25', '00:00:08', 132, 'F001'),
(306, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:33', '00:00:08', 132, 'F001'),
(307, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:40', '00:00:37', 48, 'F001'),
(308, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:42', '00:00:09', 117, 'F001'),
(309, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:47:52', '00:00:10', 105, 'F001'),
(310, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:05', '00:00:25', 72, 'F001'),
(311, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:05', '00:00:13', 81, 'F001'),
(312, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:11', '00:00:06', 176, 'F001'),
(313, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:19', '00:00:08', 132, 'F001'),
(314, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:27', '00:00:22', 81, 'F001'),
(315, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:38', '00:00:19', 55, 'F001'),
(316, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:49', '00:00:11', 96, 'F001'),
(317, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:48:51', '00:00:24', 75, 'F001'),
(318, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:49:10', '00:00:21', 50, 'F001'),
(319, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:49:18', '00:00:08', 132, 'F001'),
(320, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:49:18', '00:00:27', 66, 'F001'),
(321, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:49:43', '00:00:25', 72, 'F001'),
(322, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:50:09', '00:00:26', 69, 'F001'),
(323, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:50:46', '00:00:37', 48, 'F001'),
(324, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:51:24', '00:00:38', 47, 'F001'),
(325, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:51:35', '00:00:11', 163, 'F001'),
(326, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:52:10', '00:00:35', 51, 'F001'),
(327, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:52:41', '00:00:31', 58, 'F001'),
(328, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:53:12', '00:00:31', 58, 'F001'),
(329, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:53:58', '00:00:46', 39, 'F001'),
(330, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:54:26', '00:00:28', 64, 'F001'),
(331, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:54:51', '00:00:25', 72, 'F001'),
(332, '60-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(333, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:55:48', '00:00:57', 31, 'F001'),
(334, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:56:12', '00:00:24', 75, 'F001'),
(335, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:56:14', '00:00:02', 900, 'F001'),
(336, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:56:48', '00:00:34', 52, 'F001'),
(337, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:57:08', '00:00:20', 90, 'F001'),
(338, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:58:51', '00:01:43', 17, 'F001'),
(339, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 06:59:51', '00:01:00', 30, 'F001'),
(340, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:00:27', '00:00:36', 50, 'F001'),
(341, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:00:54', '00:00:00', 21, 'F001'),
(342, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:01:06', '00:00:39', 46, 'F001'),
(343, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:01:35', '00:00:29', 62, 'F001'),
(344, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:01:42', '00:00:48', 150, 'F001'),
(345, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:02:13', '00:00:38', 47, 'F001'),
(346, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:02:14', '00:00:32', 225, 'F001'),
(347, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:02:49', '00:00:35', 205, 'F001'),
(348, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:03:21', '00:01:08', 26, 'F001'),
(349, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:03:21', '00:00:32', 225, 'F001'),
(350, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:04:04', '00:00:43', 167, 'F001'),
(351, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:04:05', '00:00:44', 40, 'F001'),
(352, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:04:21', '00:00:17', 423, 'F001'),
(353, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:05:18', '00:01:13', 24, 'F001'),
(354, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:05:27', '00:01:06', 109, 'F001'),
(355, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:05:28', '00:00:01', 7200, 'F001'),
(356, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:05:47', '00:00:29', 62, 'F001'),
(357, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:03', '00:16:45', 1, 'F001'),
(358, '63-102-01.007-62-15-0-Univerzalna', NULL, 15, 102, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(359, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:20', '00:00:17', 62, 'F001'),
(360, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:22', '00:00:35', 51, 'F001'),
(361, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:28', '00:00:08', 132, 'F001'),
(362, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:38', '00:01:10', 102, 'F001'),
(363, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:40', '00:00:12', 88, 'F001'),
(364, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:46', '00:00:06', 176, 'F001'),
(365, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:46', '00:00:24', 75, 'F001'),
(366, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:06:55', '00:00:09', 117, 'F001'),
(367, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:07:04', '00:00:26', 276, 'F001'),
(368, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:07:34', '00:00:39', 27, 'F001'),
(369, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:07:44', '00:00:10', 105, 'F001'),
(370, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:07:45', '00:00:59', 30, 'F001'),
(371, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:07:53', '00:00:49', 146, 'F001'),
(372, '64-6-01.012-62-15-0-Univerzalna', NULL, 15, 6, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(373, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:22', '00:00:00', 257, 'F001'),
(374, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:24', '00:00:39', 46, 'F001'),
(375, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:24', '00:00:31', 232, 'F001'),
(376, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:35', '00:00:13', 395, 'F001'),
(377, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:56', '00:00:32', 225, 'F001'),
(378, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:08:58', '00:00:34', 52, 'F001'),
(379, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:09:35', '00:00:39', 184, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(380, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:09:36', '00:00:38', 47, 'F001'),
(381, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:10:07', '00:00:31', 58, 'F001'),
(382, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:10:13', '00:00:38', 189, 'F001'),
(383, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:10:28', '00:01:53', 45, 'F001'),
(384, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:10:43', '00:00:30', 240, 'F001'),
(385, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:11:15', '00:00:32', 225, 'F001'),
(386, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:11:43', '00:01:36', 18, 'F001'),
(387, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:12:04', '00:00:49', 146, 'F001'),
(388, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:12:07', '00:00:24', 75, 'F001'),
(389, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:12:38', '00:00:34', 211, 'F001'),
(390, '65-9-01.012-62-15-Sucmurasta-xs', NULL, 15, 9, NULL, 62, 1, 'Sucmurasta', 'xs', NULL, NULL, NULL, NULL),
(391, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:05', '00:02:37', 32, 'F001'),
(392, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:05', '00:05:21', 3, 'F001'),
(393, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:09', '00:00:31', 232, 'F001'),
(394, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:33', '00:00:28', 183, 'F001'),
(395, '66-7-01.012-62-15-0-Univerzalna', NULL, 15, 7, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(396, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:47', '00:01:40', 18, 'F001'),
(397, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:48', '00:00:43', 24, 'F001'),
(398, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:13:51', '00:00:42', 171, 'F001'),
(399, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:14:31', '00:00:40', 180, 'F001'),
(400, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:14:38', '00:01:05', 79, 'F001'),
(401, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:14:38', '00:00:51', 35, 'F001'),
(402, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:15:10', '00:00:39', 184, 'F001'),
(403, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:15:40', '00:00:30', 240, 'F001'),
(404, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:16:01', '00:01:23', 21, 'F001'),
(405, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:16:20', '00:00:40', 180, 'F001'),
(406, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:16:23', '00:00:22', 81, 'F001'),
(407, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:16:53', '00:00:33', 218, 'F001'),
(408, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:17:34', '00:00:41', 175, 'F001'),
(409, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:17:55', '00:01:32', 19, 'F001'),
(410, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:18:07', '00:04:19', 4, 'F001'),
(411, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:18:18', '00:00:44', 163, 'F001'),
(412, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:18:29', '00:00:34', 52, 'F001'),
(413, '60-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:18:38', '00:00:20', 360, 'F001'),
(414, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:19:33', '00:01:04', 28, 'F001'),
(415, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:19:52', '00:00:19', 94, 'F001'),
(416, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:19:56', '00:05:18', 16, 'F001'),
(417, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:20:26', '00:00:34', 52, 'F001'),
(418, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:21:02', '00:00:36', 50, 'F001'),
(419, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:21:33', '00:01:37', 53, 'F001'),
(420, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:21:46', '00:00:44', 40, 'F001'),
(421, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:22:06', '00:00:20', 90, 'F001'),
(422, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:22:36', '00:00:30', 60, 'F001'),
(423, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:23:02', '00:00:26', 69, 'F001'),
(424, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:23:57', '00:00:55', 32, 'F001'),
(425, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:24:21', '00:00:24', 75, 'F001'),
(426, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:24:45', '00:00:24', 75, 'F001'),
(427, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:25:26', '00:00:41', 43, 'F001'),
(428, '67-102-01.007-62-15-0-Univerzalna', NULL, 15, 102, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(429, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:25:50', '00:00:24', 75, 'F001'),
(430, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:25:54', '00:07:47', 2, 'F001'),
(431, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:26:17', '00:00:27', 66, 'F001'),
(432, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:26:48', '00:00:31', 58, 'F001'),
(433, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:26:54', '00:01:00', 17, 'F001'),
(434, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:26:58', '00:00:00', 45, 'F001'),
(435, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:27:06', '00:00:12', 88, 'F001'),
(436, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:27:31', '00:00:43', 41, 'F001'),
(437, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:27:35', '00:00:37', 97, 'F001'),
(438, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:27:59', '00:00:28', 64, 'F001'),
(439, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:28:09', '00:00:34', 105, 'F001'),
(440, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:28:36', '00:00:37', 48, 'F001'),
(441, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:28:41', '00:00:32', 112, 'F001'),
(442, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:01', '00:00:25', 72, 'F001'),
(443, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:20', '00:00:39', 92, 'F001'),
(444, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:28', '00:00:27', 66, 'F001'),
(445, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:45', '00:08:12', 10, 'F001'),
(446, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:45', '00:00:00', 100, 'F001'),
(447, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:45', '00:00:00', 100, 'F001'),
(448, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:45', '00:00:00', 100, 'F001'),
(449, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:53', '00:00:25', 72, 'F001'),
(450, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:29:59', '00:00:14', 367, 'F001'),
(451, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:07', '00:00:47', 76, 'F001'),
(452, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:12', '00:00:13', 395, 'F001'),
(453, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:15', '00:00:03', 1714, 'F001'),
(454, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:15', '00:00:00', 100, 'F001'),
(455, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:15', '00:00:00', 100, 'F001'),
(456, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:15', '00:00:00', 100, 'F001'),
(457, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:20', '00:00:05', 1028, 'F001'),
(458, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:20', '00:00:00', 100, 'F001'),
(459, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:25', '00:00:05', 1028, 'F001'),
(460, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:30:45', '00:00:52', 34, 'F001'),
(461, '69-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(462, '69-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(463, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:31:10', '00:00:25', 72, 'F001'),
(464, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:31:24', '00:01:17', 46, 'F001'),
(465, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:31:37', '00:00:27', 66, 'F001'),
(466, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:31:57', '00:00:33', 109, 'F001'),
(467, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:32:55', '00:00:58', 62, 'F001'),
(468, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:33:11', '00:01:34', 19, 'F001'),
(469, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:33:23', '00:00:28', 128, 'F001'),
(470, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:33:42', '00:00:31', 58, 'F001'),
(471, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:34:02', '00:00:20', 90, 'F001'),
(472, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:34:23', '00:00:21', 85, 'F001'),
(473, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:34:31', '00:01:08', 52, 'F001'),
(474, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:34:47', '00:00:24', 75, 'F001'),
(475, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:35:02', '00:00:31', 116, 'F001'),
(476, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:35:08', '00:00:21', 85, 'F001'),
(477, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:35:35', '00:00:27', 66, 'F001'),
(478, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:36:01', '00:00:59', 61, 'F001'),
(479, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:36:04', '00:00:29', 62, 'F001'),
(480, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:36:35', '00:00:34', 105, 'F001'),
(481, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:36:38', '00:00:34', 52, 'F001'),
(482, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:37:14', '00:00:36', 50, 'F001'),
(483, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:37:17', '00:00:42', 85, 'F001'),
(484, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:37:33', '00:00:19', 94, 'F001'),
(485, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:37:36', '00:10:30', 1, 'F001'),
(486, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:01', '00:00:28', 64, 'F001'),
(487, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:02', '00:00:45', 80, 'F001'),
(488, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:02', '00:00:26', 40, 'F001'),
(489, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:11', '00:00:09', 400, 'F001'),
(490, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:35', '00:00:24', 150, 'F001'),
(491, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:42', '00:00:41', 43, 'F001'),
(492, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:52', '00:00:10', 180, 'F001'),
(493, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:38:58', '00:00:23', 156, 'F001'),
(494, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:39:20', '00:00:28', 64, 'F001'),
(495, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:39:26', '00:00:28', 128, 'F001'),
(496, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:39:49', '00:00:29', 62, 'F001'),
(497, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:39:51', '00:00:25', 144, 'F001'),
(498, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:39:59', '00:00:00', 6, 'F001'),
(499, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:09', '00:00:18', 200, 'F001'),
(500, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:19', '00:00:30', 60, 'F001'),
(501, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:28', '00:00:29', 124, 'F001'),
(502, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:37', '00:00:28', 128, 'F001'),
(503, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:42', '00:00:23', 78, 'F001'),
(504, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:40:57', '00:10:32', 8, 'F001'),
(505, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:04', '00:00:36', 100, 'F001'),
(506, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:10', '00:00:33', 109, 'F001'),
(507, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:18', '00:00:36', 50, 'F001'),
(508, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:33', '00:00:29', 124, 'F001'),
(509, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:38', '00:00:20', 90, 'F001'),
(510, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:41:38', '00:00:28', 128, 'F001'),
(511, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:08', '00:00:30', 60, 'F001'),
(512, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:11', '00:00:33', 109, 'F001'),
(513, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:12', '00:00:39', 92, 'F001'),
(514, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:31', '00:00:23', 78, 'F001'),
(515, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:46', '00:00:34', 105, 'F001'),
(516, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:42:59', '00:02:02', 42, 'F001'),
(517, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:43:03', '00:00:52', 69, 'F001'),
(518, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:43:06', '00:00:35', 51, 'F001'),
(519, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:43:26', '00:00:40', 90, 'F001'),
(520, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:43:31', '00:00:25', 72, 'F001'),
(521, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:43:37', '00:00:34', 105, 'F001'),
(522, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:03', '00:00:37', 97, 'F001'),
(523, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:03', '00:00:32', 56, 'F001'),
(524, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:27', '00:00:24', 75, 'F001'),
(525, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:38', '00:00:35', 102, 'F001'),
(526, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:46', '00:01:09', 52, 'F001'),
(527, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:44:51', '00:00:24', 75, 'F001'),
(528, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:16', '00:00:25', 72, 'F001'),
(529, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:20', '00:00:34', 105, 'F001'),
(530, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:35', '00:00:57', 63, 'F001'),
(531, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:40', '00:00:24', 75, 'F001'),
(532, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:52', '00:00:32', 112, 'F001'),
(533, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:45:52', '00:00:17', 211, 'F001'),
(534, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:46:07', '00:00:27', 66, 'F001'),
(535, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:46:23', '00:00:31', 116, 'F001'),
(536, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:46:26', '00:00:34', 105, 'F001'),
(537, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:47:07', '00:00:44', 81, 'F001'),
(538, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:47:14', '00:04:15', 20, 'F001'),
(539, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:47:30', '00:01:04', 56, 'F001'),
(540, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:47:46', '00:00:39', 92, 'F001'),
(541, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:03', '00:00:49', 104, 'F001'),
(542, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:04', '00:00:34', 105, 'F001'),
(543, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:19', '00:00:33', 109, 'F001'),
(544, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:40', '00:02:33', 11, 'F001'),
(545, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:47', '00:00:44', 116, 'F001'),
(546, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:51', '00:00:47', 76, 'F001'),
(547, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:48:56', '00:00:37', 97, 'F001'),
(548, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:49:15', '00:00:35', 51, 'F001'),
(549, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:49:32', '00:00:41', 87, 'F001'),
(550, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:49:40', '00:00:44', 81, 'F001'),
(551, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:49:53', '00:00:38', 47, 'F001'),
(552, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:05', '00:00:33', 109, 'F001'),
(553, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:18', '00:00:38', 94, 'F001'),
(554, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:22', '00:00:29', 62, 'F001'),
(555, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:35', '00:00:30', 120, 'F001'),
(556, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:37', '00:00:15', 120, 'F001'),
(557, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:50:58', '00:00:40', 90, 'F001'),
(558, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:51:01', '00:00:24', 75, 'F001'),
(559, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:51:14', '00:00:39', 92, 'F001'),
(560, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:51:36', '00:00:38', 94, 'F001'),
(561, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:51:37', '00:00:36', 50, 'F001'),
(562, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:51:50', '00:00:36', 100, 'F001'),
(563, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:52:13', '00:00:37', 97, 'F001'),
(564, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:52:13', '00:00:36', 50, 'F001'),
(565, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:52:37', '00:00:47', 76, 'F001'),
(566, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:52:40', '00:00:27', 66, 'F001'),
(567, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:52:48', '00:00:35', 102, 'F001'),
(568, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:06', '00:00:29', 124, 'F001'),
(569, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:09', '00:15:07', 1, 'F001'),
(570, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:12', '00:00:32', 56, 'F001'),
(571, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', NULL, 4, NULL, NULL, NULL, NULL, NULL, '2016-12-06 07:53:12', NULL, NULL, 'P001'),
(572, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:35', '00:00:47', 76, 'F001'),
(573, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:44', '00:00:38', 94, 'F001'),
(574, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:46', '00:00:34', 52, 'F001'),
(575, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:53:53', '00:00:41', 25, 'F001'),
(576, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:02', '00:00:09', 117, 'F001'),
(577, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:10', '00:00:35', 102, 'F001'),
(578, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:14', '00:00:12', 88, 'F001'),
(579, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:19', '00:00:05', 211, 'F001'),
(580, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:24', '00:00:38', 47, 'F001'),
(581, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:27', '00:00:08', 132, 'F001'),
(582, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:37', '00:00:53', 67, 'F001'),
(583, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:40', '00:00:13', 81, 'F001'),
(584, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:46', '00:00:06', 176, 'F001'),
(585, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:54', '00:00:44', 81, 'F001'),
(586, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:56', '00:00:10', 105, 'F001'),
(587, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:54:59', '00:00:35', 51, 'F001'),
(588, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:05', '00:00:09', 117, 'F001'),
(589, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:23', '00:00:46', 78, 'F001'),
(590, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:29', '00:00:35', 102, 'F001'),
(591, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:31', '00:00:32', 56, 'F001'),
(592, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:34', '00:00:29', 36, 'F001'),
(593, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:40', '00:00:06', 176, 'F001'),
(594, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:50', '00:07:03', 12, 'F001'),
(595, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:55:55', '00:00:05', 1028, 'F001'),
(596, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:00', '00:00:05', 1028, 'F001'),
(597, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:04', '00:00:41', 87, 'F001'),
(598, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:07', '00:00:36', 50, 'F001'),
(599, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:11', '00:00:42', 85, 'F001'),
(600, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:22', '00:00:42', 25, 'F001'),
(601, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:56:42', '00:00:35', 51, 'F001'),
(602, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:11', '00:00:29', 62, 'F001'),
(603, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:17', '00:01:13', 49, 'F001'),
(604, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:17', '00:00:55', 19, 'F001'),
(605, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:37', '00:00:26', 69, 'F001'),
(606, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:41', '00:00:24', 44, 'F001'),
(607, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:44', '00:00:03', 352, 'F001'),
(608, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:57:48', '00:00:04', 264, 'F001'),
(609, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:01', '00:00:24', 75, 'F001'),
(610, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:01', '00:00:44', 81, 'F001'),
(611, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:05', '00:00:17', 62, 'F001'),
(612, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:28', '00:00:23', 46, 'F001'),
(613, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:29', '00:00:01', 1058, 'F001'),
(614, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:35', '00:00:34', 52, 'F001'),
(615, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:39', '00:00:10', 105, 'F001'),
(616, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:44', '00:00:43', 83, 'F001'),
(617, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:47', '00:00:08', 132, 'F001'),
(618, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:58:55', '00:00:08', 132, 'F001'),
(619, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:04', '00:00:29', 62, 'F001'),
(620, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:05', '00:00:10', 105, 'F001'),
(621, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:14', '00:00:09', 117, 'F001'),
(622, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:16', '00:00:32', 112, 'F001'),
(623, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:24', '00:00:10', 105, 'F001'),
(624, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:30', '00:00:26', 69, 'F001'),
(625, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:33', '00:00:09', 117, 'F001'),
(626, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 07:59:56', '00:00:23', 46, 'F001'),
(627, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:01', '00:00:05', 211, 'F001'),
(628, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:03', '00:00:33', 54, 'F001'),
(629, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:03', '00:00:47', 76, 'F001'),
(630, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:08', '00:00:07', 151, 'F001'),
(631, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:18', '00:00:10', 105, 'F001'),
(632, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:35', '00:00:32', 112, 'F001'),
(633, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:00:45', '00:00:42', 42, 'F001'),
(634, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:13', '00:00:38', 94, 'F001'),
(635, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:14', '00:00:56', 18, 'F001'),
(636, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:16', '00:00:31', 58, 'F001'),
(637, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:28', '00:00:14', 75, 'F001'),
(638, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:39', '00:00:11', 96, 'F001'),
(639, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:54', '00:00:41', 87, 'F001'),
(640, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:01:54', '00:00:15', 70, 'F001'),
(641, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:04', '00:00:48', 37, 'F001'),
(642, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:06', '00:00:12', 88, 'F001'),
(643, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:18', '00:00:24', 150, 'F001'),
(644, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:44', '00:06:44', 12, 'F001'),
(645, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:45', '00:00:41', 43, 'F001'),
(646, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:02:48', '00:00:30', 120, 'F001'),
(647, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:07', '00:01:01', 17, 'F001'),
(648, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:24', '00:00:17', 62, 'F001'),
(649, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:25', '00:00:37', 97, 'F001'),
(650, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:38', '00:00:53', 33, 'F001'),
(651, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:47', '00:00:23', 46, 'F001'),
(652, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:47', '00:00:00', 100, 'F001'),
(653, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:52', '00:00:05', 211, 'F001'),
(654, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:03:57', '00:00:32', 112, 'F001'),
(655, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:04:15', '00:00:37', 48, 'F001'),
(656, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:04:21', '00:00:29', 36, 'F001'),
(657, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:04:41', '00:00:20', 52, 'F001'),
(658, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:04:59', '00:00:18', 58, 'F001'),
(659, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:04:59', '00:00:00', 100, 'F001'),
(660, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:01', '00:01:04', 56, 'F001'),
(661, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:02', '00:00:47', 38, 'F001'),
(662, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:04', '00:00:05', 211, 'F001'),
(663, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:07', '00:00:03', 352, 'F001'),
(664, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:08', '00:00:01', 1058, 'F001'),
(665, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:24', '00:00:16', 66, 'F001'),
(666, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:34', '00:00:10', 105, 'F001'),
(667, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:36', '00:00:35', 102, 'F001'),
(668, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:43', '00:00:09', 117, 'F001'),
(669, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:05:51', '00:00:08', 132, 'F001'),
(670, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:06:07', '00:00:31', 116, 'F001'),
(671, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:06:09', '00:00:18', 58, 'F001'),
(672, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:06:17', '00:00:08', 132, 'F001'),
(673, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:06:48', '00:00:31', 34, 'F001'),
(674, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:07:08', '00:02:06', 14, 'F001'),
(675, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:07:13', '00:01:06', 54, 'F001'),
(676, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:07:32', '00:00:44', 24, 'F001'),
(677, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:07:49', '00:00:36', 100, 'F001'),
(678, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:08:05', '00:00:57', 31, 'F001'),
(679, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:08:17', '00:00:28', 128, 'F001'),
(680, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:08:29', '00:00:24', 75, 'F001'),
(681, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:08:30', '00:00:58', 18, 'F001'),
(682, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:02', '00:00:45', 80, 'F001'),
(683, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:02', '00:00:32', 33, 'F001'),
(684, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:14', '00:00:45', 40, 'F001'),
(685, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:20', '00:00:18', 58, 'F001'),
(686, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:32', '00:00:30', 120, 'F001'),
(687, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:39', '00:00:19', 55, 'F001'),
(688, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:09:46', '00:00:32', 56, 'F001'),
(689, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:10:08', '00:00:36', 100, 'F001'),
(690, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:10:19', '00:00:40', 26, 'F001'),
(691, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:10:28', '00:00:09', 117, 'F001'),
(692, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:10:33', '00:00:47', 38, 'F001'),
(693, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:10:43', '00:00:35', 102, 'F001'),
(694, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:03', '00:00:35', 30, 'F001'),
(695, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:12', '00:00:09', 117, 'F001'),
(696, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:19', '00:00:46', 39, 'F001'),
(697, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:22', '00:00:10', 105, 'F001'),
(698, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:31', '00:00:09', 117, 'F001'),
(699, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:39', '00:00:08', 132, 'F001'),
(700, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:47', '00:01:04', 56, 'F001'),
(701, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:49', '00:00:10', 105, 'F001'),
(702, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:11:56', '00:00:07', 151, 'F001'),
(703, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:04', '00:00:08', 132, 'F001'),
(704, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:05', '00:00:46', 39, 'F001'),
(705, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:08', '00:00:21', 171, 'F001'),
(706, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:14', '00:00:10', 105, 'F001'),
(707, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:28', '00:00:23', 78, 'F001'),
(708, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:37', '00:00:23', 46, 'F001'),
(709, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:43', '00:00:35', 102, 'F001'),
(710, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:51', '00:00:23', 78, 'F001'),
(711, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:12:51', '00:00:14', 75, 'F001'),
(712, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:05', '00:00:14', 75, 'F001'),
(713, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:27', '00:00:22', 48, 'F001'),
(714, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:28', '00:00:45', 80, 'F001'),
(715, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:41', '00:00:14', 75, 'F001'),
(716, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:49', '00:00:08', 132, 'F001'),
(717, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:50', '00:00:59', 30, 'F001'),
(718, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:13:58', '00:00:09', 117, 'F001'),
(719, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:06', '00:00:08', 132, 'F001'),
(720, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:13', '00:00:45', 80, 'F001'),
(721, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:24', '00:00:34', 52, 'F001'),
(722, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:27', '00:00:21', 50, 'F001'),
(723, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:27', '00:00:00', 100, 'F001'),
(724, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:57', '00:00:30', 35, 'F001'),
(725, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:14:58', '00:00:45', 80, 'F001'),
(726, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:15:19', '00:00:22', 48, 'F001'),
(727, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:15:29', '00:00:31', 116, 'F001'),
(728, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:15:31', '00:01:07', 26, 'F001'),
(729, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:15:58', '00:00:27', 66, 'F001'),
(730, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:13', '00:00:44', 81, 'F001'),
(731, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:22', '00:00:24', 75, 'F001'),
(732, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:34', '00:00:21', 171, 'F001'),
(733, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:37', '00:01:18', 13, 'F001'),
(734, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:56', '00:00:19', 55, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(735, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:16:58', '00:00:36', 50, 'F001'),
(736, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:17:08', '00:00:34', 105, 'F001'),
(737, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:17:18', '00:00:22', 48, 'F001'),
(738, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:17:40', '00:00:22', 48, 'F001'),
(739, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:17:47', '00:00:39', 92, 'F001'),
(740, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:17:53', '00:00:13', 81, 'F001'),
(741, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:02', '00:00:09', 117, 'F001'),
(742, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:05', '00:01:07', 26, 'F001'),
(743, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:11', '00:00:09', 117, 'F001'),
(744, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:20', '00:00:09', 117, 'F001'),
(745, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:23', '00:00:36', 100, 'F001'),
(746, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:27', '00:00:07', 151, 'F001'),
(747, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:42', '00:00:15', 70, 'F001'),
(748, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:50', '00:00:45', 40, 'F001'),
(749, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:53', '00:00:11', 96, 'F001'),
(750, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:18:59', '00:00:36', 100, 'F001'),
(751, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:01', '00:00:08', 132, 'F001'),
(752, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:10', '00:22:59', 2, 'F001'),
(753, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:14', '00:00:13', 81, 'F001'),
(754, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:23', '00:00:09', 117, 'F001'),
(755, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:27', '00:00:28', 128, 'F001'),
(756, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:32', '00:00:09', 117, 'F001'),
(757, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:46', '00:00:14', 75, 'F001'),
(758, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:55', '00:00:45', 80, 'F001'),
(759, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:19:59', '00:01:09', 26, 'F001'),
(760, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:20:15', '00:00:48', 75, 'F001'),
(761, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:20:29', '00:00:30', 60, 'F001'),
(762, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:20:35', '00:00:40', 90, 'F001'),
(763, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:20:58', '00:00:29', 62, 'F001'),
(764, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:01:20', 13, 'F001'),
(765, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:00:00', 100, 'F001'),
(766, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:00:00', 100, 'F001'),
(767, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:00:00', 100, 'F001'),
(768, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:00:00', 100, 'F001'),
(769, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:06', '00:00:00', 100, 'F001'),
(770, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:10', '00:00:04', 264, 'F001'),
(771, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:11', '00:00:36', 100, 'F001'),
(772, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:16', '00:01:01', 59, 'F001'),
(773, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:18', '00:00:08', 132, 'F001'),
(774, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:26', '00:00:08', 132, 'F001'),
(775, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:30', '00:00:04', 264, 'F001'),
(776, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:32', '00:00:34', 52, 'F001'),
(777, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:33', '00:00:03', 352, 'F001'),
(778, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:39', '00:00:06', 176, 'F001'),
(779, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:40', '00:00:24', 150, 'F001'),
(780, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:41', '00:00:02', 529, 'F001'),
(781, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:21:43', '00:00:32', 112, 'F001'),
(782, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:11', '00:00:28', 128, 'F001'),
(783, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:20', '00:00:39', 27, 'F001'),
(784, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:32', '00:00:52', 69, 'F001'),
(785, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:34', '00:01:02', 29, 'F001'),
(786, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:40', '00:00:20', 52, 'F001'),
(787, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:43', '00:00:03', 352, 'F001'),
(788, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:44', '00:00:33', 109, 'F001'),
(789, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:22:59', '00:00:16', 66, 'F001'),
(790, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:05', '00:00:31', 58, 'F001'),
(791, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:07', '00:00:35', 102, 'F001'),
(792, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:16', '00:00:17', 62, 'F001'),
(793, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:21', '00:00:37', 97, 'F001'),
(794, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:25', '00:00:09', 117, 'F001'),
(795, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:38', '00:00:33', 54, 'F001'),
(796, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:38', '00:00:31', 116, 'F001'),
(797, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:41', '00:00:16', 66, 'F001'),
(798, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:50', '00:00:09', 117, 'F001'),
(799, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:23:58', '00:00:08', 132, 'F001'),
(800, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:00', '00:00:39', 92, 'F001'),
(801, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:14', '00:00:16', 66, 'F001'),
(802, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:21', '00:00:07', 151, 'F001'),
(803, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:26', '00:00:05', 211, 'F001'),
(804, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:29', '00:00:29', 124, 'F001'),
(805, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:44', '00:01:06', 27, 'F001'),
(806, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:45', '00:00:19', 55, 'F001'),
(807, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:52', '00:00:07', 151, 'F001'),
(808, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:24:54', '00:00:25', 144, 'F001'),
(809, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:02', '00:00:10', 105, 'F001'),
(810, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:10', '00:00:08', 132, 'F001'),
(811, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:19', '00:00:09', 117, 'F001'),
(812, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:28', '00:00:09', 117, 'F001'),
(813, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:31', '00:00:37', 97, 'F001'),
(814, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:37', '00:00:09', 117, 'F001'),
(815, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:39', '00:02:01', 29, 'F001'),
(816, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:46', '00:00:09', 117, 'F001'),
(817, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:25:54', '00:00:08', 132, 'F001'),
(818, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:05', '00:00:11', 96, 'F001'),
(819, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:08', '00:01:24', 21, 'F001'),
(820, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:16', '00:00:45', 80, 'F001'),
(821, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:17', '00:00:38', 94, 'F001'),
(822, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:20', '00:00:15', 70, 'F001'),
(823, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:29', '00:00:09', 117, 'F001'),
(824, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:30', '00:00:01', 1058, 'F001'),
(825, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:38', '00:00:30', 60, 'F001'),
(826, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:26:38', '00:00:08', 132, 'F001'),
(827, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:04', '00:00:47', 76, 'F001'),
(828, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:05', '00:00:27', 66, 'F001'),
(829, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:10', '00:00:32', 33, 'F001'),
(830, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:26', '00:01:10', 51, 'F001'),
(831, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:27', '00:00:17', 62, 'F001'),
(832, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:33', '00:00:28', 64, 'F001'),
(833, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:34', '00:00:07', 151, 'F001'),
(834, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:39', '00:00:05', 211, 'F001'),
(835, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:47', '00:00:43', 83, 'F001'),
(836, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:51', '00:00:25', 144, 'F001'),
(837, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:57', '00:00:18', 58, 'F001'),
(838, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:27:59', '00:00:26', 69, 'F001'),
(839, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:20', '00:00:23', 46, 'F001'),
(840, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:28', '00:00:41', 87, 'F001'),
(841, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:30', '00:00:10', 105, 'F001'),
(842, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:31', '00:00:01', 1058, 'F001'),
(843, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:40', '00:00:49', 73, 'F001'),
(844, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:43', '00:00:12', 88, 'F001'),
(845, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:50', '00:00:22', 163, 'F001'),
(846, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:54', '00:00:55', 32, 'F001'),
(847, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:28:54', '00:00:11', 96, 'F001'),
(848, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:13', '00:00:19', 55, 'F001'),
(849, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:14', '00:00:34', 105, 'F001'),
(850, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:16', '00:00:22', 81, 'F001'),
(851, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:23', '00:00:10', 105, 'F001'),
(852, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:31', '00:00:08', 132, 'F001'),
(853, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:35', '00:00:04', 264, 'F001'),
(854, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:41', '00:00:06', 176, 'F001'),
(855, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:46', '00:00:32', 112, 'F001'),
(856, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:47', '00:00:31', 58, 'F001'),
(857, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:49', '00:00:08', 132, 'F001'),
(858, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:50', '00:01:00', 60, 'F001'),
(859, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:29:58', '00:00:09', 117, 'F001'),
(860, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:17', '00:00:19', 55, 'F001'),
(861, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:18', '00:00:31', 58, 'F001'),
(862, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:26', '00:00:36', 100, 'F001'),
(863, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:27', '00:00:41', 87, 'F001'),
(864, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:30', '00:00:13', 81, 'F001'),
(865, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:32', '00:00:02', 529, 'F001'),
(866, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:40', '00:00:08', 132, 'F001'),
(867, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:48', '00:00:08', 132, 'F001'),
(868, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:54', '00:00:36', 50, 'F001'),
(869, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:30:56', '00:00:08', 132, 'F001'),
(870, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:03', '00:00:07', 151, 'F001'),
(871, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:05', '00:00:38', 94, 'F001'),
(872, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:23', '00:00:57', 63, 'F001'),
(873, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:32', '00:00:29', 36, 'F001'),
(874, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:40', '00:00:35', 102, 'F001'),
(875, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:42', '00:00:10', 105, 'F001'),
(876, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:43', '00:00:20', 180, 'F001'),
(877, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:47', '00:00:53', 33, 'F001'),
(878, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:49', '00:00:07', 151, 'F001'),
(879, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:31:57', '00:00:08', 132, 'F001'),
(880, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:06', '00:00:09', 117, 'F001'),
(881, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:14', '00:00:08', 132, 'F001'),
(882, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:14', '00:00:31', 116, 'F001'),
(883, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:19', '00:00:32', 56, 'F001'),
(884, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:23', '00:00:09', 117, 'F001'),
(885, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:46', '00:00:32', 112, 'F001'),
(886, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:50', '00:01:10', 51, 'F001'),
(887, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:32:57', '00:00:38', 47, 'F001'),
(888, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:05', '00:00:15', 240, 'F001'),
(889, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:14', '00:00:51', 20, 'F001'),
(890, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:23', '00:00:09', 117, 'F001'),
(891, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:32', '00:00:09', 117, 'F001'),
(892, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:32', '00:00:35', 51, 'F001'),
(893, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:36', '00:00:50', 72, 'F001'),
(894, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:40', '00:00:08', 132, 'F001'),
(895, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:33:47', '00:00:07', 151, 'F001'),
(896, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:01', '00:00:14', 75, 'F001'),
(897, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:09', '00:00:08', 132, 'F001'),
(898, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:16', '00:00:44', 40, 'F001'),
(899, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:19', '00:00:10', 105, 'F001'),
(900, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:23', '00:01:18', 46, 'F001'),
(901, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:30', '00:00:54', 66, 'F001'),
(902, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:38', '00:00:15', 240, 'F001'),
(903, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:39', '00:00:01', 3600, 'F001'),
(904, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:43', '00:00:24', 44, 'F001'),
(905, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:54', '00:00:11', 96, 'F001'),
(906, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:34:57', '00:00:41', 43, 'F001'),
(907, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:02', '00:00:08', 132, 'F001'),
(908, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:04', '00:00:02', 529, 'F001'),
(909, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:06', '00:00:27', 133, 'F001'),
(910, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:07', '00:00:37', 97, 'F001'),
(911, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:19', '00:00:15', 70, 'F001'),
(912, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:21', '00:00:15', 240, 'F001'),
(913, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:23', '00:00:26', 69, 'F001'),
(914, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:32', '00:00:25', 144, 'F001'),
(915, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:37', '00:00:16', 225, 'F001'),
(916, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:54', '00:00:35', 30, 'F001'),
(917, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:56', '00:00:33', 54, 'F001'),
(918, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:35:58', '00:00:26', 138, 'F001'),
(919, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:04', '00:00:10', 105, 'F001'),
(920, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:04', '00:00:00', 100, 'F001'),
(921, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:04', '00:00:00', 100, 'F001'),
(922, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:33', '00:00:35', 102, 'F001'),
(923, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:35', '00:00:58', 62, 'F001'),
(924, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:36:37', '00:33:53', 2, 'F001'),
(925, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:37:06', '00:00:33', 109, 'F001'),
(926, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:37:07', '00:00:32', 112, 'F001'),
(927, '64-6-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 6, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:37:22', '00:00:45', 114, 'F001'),
(928, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:37:37', '00:00:31', 116, 'F001'),
(929, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:37:57', '00:00:50', 72, 'F001'),
(930, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:38:28', '00:00:51', 70, 'F001'),
(931, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:01:03', 57, 'F001'),
(932, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:02:56', 6, 'F001'),
(933, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(934, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(935, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(936, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(937, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(938, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:00', '00:00:00', 100, 'F001'),
(939, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:01', 1058, 'F001'),
(940, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(941, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(942, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(943, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(944, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(945, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(946, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(947, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(948, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(949, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(950, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:01', '00:00:00', 100, 'F001'),
(951, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:05', '00:00:05', 720, 'F001'),
(952, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:08', '00:00:07', 151, 'F001'),
(953, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:09', '00:00:41', 87, 'F001'),
(954, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:18', '00:00:10', 105, 'F001'),
(955, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:28', '00:00:10', 105, 'F001'),
(956, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:36', '00:00:08', 132, 'F001'),
(957, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:45', '00:00:09', 117, 'F001'),
(958, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:46', '00:00:37', 97, 'F001'),
(959, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:39:56', '00:00:11', 96, 'F001'),
(960, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:07', '00:00:11', 96, 'F001'),
(961, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:13', '00:01:08', 52, 'F001'),
(962, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:15', '00:00:08', 132, 'F001'),
(963, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:20', '00:00:34', 105, 'F001'),
(964, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:24', '00:00:09', 117, 'F001'),
(965, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:27', '00:00:03', 352, 'F001'),
(966, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:32', '00:00:05', 211, 'F001'),
(967, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:40', '00:00:08', 132, 'F001'),
(968, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:45', '00:00:32', 112, 'F001'),
(969, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:48', '00:00:08', 132, 'F001'),
(970, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:53', '00:04:57', 6, 'F001'),
(971, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:40:57', '00:00:09', 117, 'F001'),
(972, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:05', '00:00:08', 132, 'F001'),
(973, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:06', '00:00:46', 78, 'F001'),
(974, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:12', '00:00:07', 151, 'F001'),
(975, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:20', '00:00:08', 132, 'F001'),
(976, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:29', '00:00:09', 117, 'F001'),
(977, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:36', '00:00:43', 41, 'F001'),
(978, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:37', '00:00:08', 132, 'F001'),
(979, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:37', '00:00:31', 116, 'F001'),
(980, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:46', '00:00:09', 117, 'F001'),
(981, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:41:56', '00:00:10', 105, 'F001'),
(982, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:07', '00:00:11', 96, 'F001'),
(983, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:11', '00:00:35', 51, 'F001'),
(984, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:19', '00:00:42', 85, 'F001'),
(985, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:20', '00:00:13', 81, 'F001'),
(986, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:33', '00:00:13', 81, 'F001'),
(987, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:42:47', '00:00:36', 50, 'F001'),
(988, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:04', '00:00:45', 80, 'F001'),
(989, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:16', '00:00:29', 62, 'F001'),
(990, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:19', '00:00:46', 23, 'F001'),
(991, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:32', '00:00:13', 81, 'F001'),
(992, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:35', '00:00:31', 116, 'F001'),
(993, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:41', '00:00:09', 117, 'F001'),
(994, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:42', '00:00:26', 69, 'F001'),
(995, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:49', '00:00:08', 132, 'F001'),
(996, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:43:57', '00:00:08', 132, 'F001'),
(997, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:07', '00:00:10', 105, 'F001'),
(998, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:16', '00:00:09', 117, 'F001'),
(999, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:20', '00:00:38', 47, 'F001'),
(1000, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:28', '00:00:12', 88, 'F001'),
(1001, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:39', '00:00:11', 96, 'F001'),
(1002, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:48', '00:00:09', 117, 'F001'),
(1003, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:57', '00:00:09', 117, 'F001'),
(1004, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:44:58', '00:00:38', 47, 'F001'),
(1005, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:06', '00:00:09', 117, 'F001'),
(1006, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:14', '00:00:08', 132, 'F001'),
(1007, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:22', '00:00:08', 132, 'F001'),
(1008, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:23', '00:00:25', 72, 'F001'),
(1009, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:30', '00:00:08', 132, 'F001'),
(1010, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:40', '00:00:10', 105, 'F001'),
(1011, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:45:50', '00:00:10', 105, 'F001'),
(1012, '67-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:02', '00:02:27', 24, 'F001'),
(1013, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:02', '00:00:12', 88, 'F001'),
(1014, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:14', '00:00:12', 88, 'F001'),
(1015, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:23', '00:00:09', 117, 'F001'),
(1016, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:31', '00:00:08', 132, 'F001'),
(1017, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:43', '00:00:12', 88, 'F001'),
(1018, '70-102-01.007-62-15-0-Univerzalna', NULL, 15, 102, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(1019, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:46:53', '00:00:10', 105, 'F001'),
(1020, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:01', '00:00:08', 132, 'F001'),
(1021, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:09', '00:00:08', 132, 'F001'),
(1022, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:18', '00:00:09', 117, 'F001'),
(1023, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:28', '00:00:10', 105, 'F001'),
(1024, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:37', '00:00:09', 117, 'F001'),
(1025, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:41', '00:02:18', 13, 'F001'),
(1026, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:47', '00:00:10', 105, 'F001'),
(1027, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:57', '00:00:10', 105, 'F001'),
(1028, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:47:57', '00:00:00', 52, 'F001'),
(1029, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:48:21', '00:00:40', 45, 'F001'),
(1030, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:48:31', '00:00:34', 105, 'F001'),
(1031, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:48:42', '00:00:45', 23, 'F001'),
(1032, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:48:52', '00:00:21', 171, 'F001'),
(1033, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:48:53', '00:00:11', 96, 'F001'),
(1034, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:01', '00:00:08', 132, 'F001'),
(1035, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:10', '00:00:09', 117, 'F001'),
(1036, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:12', '00:00:20', 180, 'F001'),
(1037, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:22', '00:00:12', 88, 'F001'),
(1038, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:31', '00:00:09', 117, 'F001'),
(1039, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:32', '00:00:20', 180, 'F001'),
(1040, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:33', '00:01:12', 25, 'F001'),
(1041, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:42', '00:00:11', 96, 'F001'),
(1042, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:53', '00:00:11', 96, 'F001'),
(1043, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:55', '00:00:23', 156, 'F001'),
(1044, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:49:56', '00:00:23', 78, 'F001'),
(1045, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:50:04', '00:00:11', 96, 'F001'),
(1046, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:50:30', '00:00:35', 102, 'F001'),
(1047, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:11', '00:00:41', 87, 'F001'),
(1048, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:21', '00:01:25', 21, 'F001'),
(1049, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:33', '00:01:29', 11, 'F001'),
(1050, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:39', '00:00:28', 128, 'F001'),
(1051, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:47', '00:00:14', 75, 'F001'),
(1052, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:50', '00:00:11', 327, 'F001'),
(1053, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:51:56', '00:00:09', 117, 'F001'),
(1054, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:03', '00:00:42', 42, 'F001'),
(1055, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:07', '00:00:11', 96, 'F001'),
(1056, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:15', '00:00:08', 132, 'F001'),
(1057, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:23', '00:00:08', 132, 'F001'),
(1058, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:33', '00:00:10', 105, 'F001'),
(1059, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:36', '00:00:33', 54, 'F001'),
(1060, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:40', '00:00:50', 72, 'F001'),
(1061, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:46', '00:00:13', 81, 'F001'),
(1062, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:52:54', '00:00:08', 132, 'F001'),
(1063, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:08', '00:00:32', 56, 'F001'),
(1064, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:19', '00:00:25', 42, 'F001'),
(1065, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:22', '00:00:42', 85, 'F001'),
(1066, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:37', '00:00:29', 62, 'F001'),
(1067, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:48', '00:00:29', 36, 'F001'),
(1068, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:53:57', '00:00:09', 117, 'F001'),
(1069, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:06', '00:00:09', 117, 'F001'),
(1070, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:13', '00:00:36', 50, 'F001'),
(1071, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:18', '00:00:12', 88, 'F001'),
(1072, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:27', '00:00:09', 117, 'F001'),
(1073, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:36', '00:00:09', 117, 'F001'),
(1074, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:37', '00:00:24', 75, 'F001'),
(1075, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:46', '00:00:10', 105, 'F001'),
(1076, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:55', '00:00:09', 117, 'F001'),
(1077, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:54:56', '00:01:34', 38, 'F001'),
(1078, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:55:07', '00:00:12', 88, 'F001'),
(1079, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:55:07', '00:00:30', 60, 'F001'),
(1080, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:55:15', '00:00:08', 132, 'F001'),
(1081, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:55:30', '00:00:34', 105, 'F001'),
(1082, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:55:35', '00:00:28', 64, 'F001'),
(1083, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:12', '00:00:37', 48, 'F001'),
(1084, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:23', '00:00:53', 67, 'F001'),
(1085, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:29', '00:01:14', 14, 'F001'),
(1086, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:50', '00:00:38', 47, 'F001'),
(1087, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:51', '00:00:22', 48, 'F001'),
(1088, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:56:59', '00:00:36', 100, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(1089, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:08', '00:00:18', 100, 'F001'),
(1090, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:32', '00:00:41', 25, 'F001'),
(1091, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:33', '00:00:25', 72, 'F001'),
(1092, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:41', '00:00:09', 117, 'F001'),
(1093, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:50', '00:00:09', 117, 'F001'),
(1094, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:50', '00:00:51', 70, 'F001'),
(1095, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:57:54', '00:00:21', 85, 'F001'),
(1096, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:00', '00:00:10', 105, 'F001'),
(1097, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:09', '00:00:09', 117, 'F001'),
(1098, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:20', '00:00:11', 96, 'F001'),
(1099, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:28', '00:00:34', 52, 'F001'),
(1100, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:32', '00:00:12', 88, 'F001'),
(1101, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:41', '00:00:09', 117, 'F001'),
(1102, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:51', '00:00:10', 105, 'F001'),
(1103, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:58:59', '00:00:31', 58, 'F001'),
(1104, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:01', '00:01:11', 50, 'F001'),
(1105, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:02', '00:00:11', 96, 'F001'),
(1106, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:19', '00:00:17', 62, 'F001'),
(1107, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:30', '00:00:31', 58, 'F001'),
(1108, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:32', '00:00:13', 81, 'F001'),
(1109, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:33', '00:00:32', 112, 'F001'),
(1110, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:38', '00:00:06', 176, 'F001'),
(1111, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 08:59:50', '00:00:12', 88, 'F001'),
(1112, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:00', '00:00:30', 60, 'F001'),
(1113, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:01', '00:00:11', 96, 'F001'),
(1114, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:06', '00:00:33', 109, 'F001'),
(1115, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:11', '00:00:10', 105, 'F001'),
(1116, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:20', '00:00:09', 117, 'F001'),
(1117, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:28', '00:00:28', 64, 'F001'),
(1118, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:34', '00:00:14', 75, 'F001'),
(1119, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:39', '00:00:33', 109, 'F001'),
(1120, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:44', '00:00:10', 105, 'F001'),
(1121, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:00:56', '00:00:12', 88, 'F001'),
(1122, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:04', '00:00:36', 50, 'F001'),
(1123, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:09', '00:00:13', 81, 'F001'),
(1124, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:19', '00:00:10', 105, 'F001'),
(1125, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:24', '00:00:45', 80, 'F001'),
(1126, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:28', '00:00:09', 117, 'F001'),
(1127, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:37', '00:00:09', 117, 'F001'),
(1128, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:37', '00:00:33', 54, 'F001'),
(1129, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:01:48', '00:00:11', 96, 'F001'),
(1130, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:03', '00:00:15', 70, 'F001'),
(1131, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:11', '00:00:08', 132, 'F001'),
(1132, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:12', '00:00:48', 75, 'F001'),
(1133, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:22', '00:00:45', 40, 'F001'),
(1134, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:37', '00:00:26', 40, 'F001'),
(1135, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:41', '00:00:29', 124, 'F001'),
(1136, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:47', '00:00:10', 105, 'F001'),
(1137, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:02:52', '00:00:30', 60, 'F001'),
(1138, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:14', '00:00:33', 109, 'F001'),
(1139, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:14', '00:00:27', 39, 'F001'),
(1140, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:25', '00:00:11', 96, 'F001'),
(1141, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:34', '00:00:09', 117, 'F001'),
(1142, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:43', '00:00:09', 117, 'F001'),
(1143, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:44', '00:00:30', 120, 'F001'),
(1144, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:03:49', '00:00:57', 31, 'F001'),
(1145, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:04:17', '00:00:33', 109, 'F001'),
(1146, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:04:21', '00:00:32', 56, 'F001'),
(1147, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:04:53', '00:00:32', 56, 'F001'),
(1148, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:05', '00:00:48', 75, 'F001'),
(1149, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:19', '00:24:34', 2, 'F001'),
(1150, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:19', '00:00:00', 100, 'F001'),
(1151, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:19', '00:00:00', 100, 'F001'),
(1152, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:21', '00:00:28', 64, 'F001'),
(1153, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:32', '00:00:13', 276, 'F001'),
(1154, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:35', '00:00:30', 120, 'F001'),
(1155, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:51', '00:00:19', 189, 'F001'),
(1156, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:51', '00:00:00', 100, 'F001'),
(1157, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:51', '00:00:00', 100, 'F001'),
(1158, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:54', '00:00:03', 1200, 'F001'),
(1159, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:05:58', '00:00:37', 48, 'F001'),
(1160, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:25', '00:02:42', 6, 'F001'),
(1161, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:25', '00:00:27', 66, 'F001'),
(1162, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:31', '00:00:56', 64, 'F001'),
(1163, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:39', '00:00:45', 80, 'F001'),
(1164, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:45', '00:00:20', 52, 'F001'),
(1165, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:49', '00:00:24', 75, 'F001'),
(1166, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:06:59', '00:00:14', 75, 'F001'),
(1167, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:07:17', '00:00:28', 64, 'F001'),
(1168, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:07:19', '00:00:48', 75, 'F001'),
(1169, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:07:28', '00:00:29', 36, 'F001'),
(1170, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:07:40', '00:00:21', 171, 'F001'),
(1171, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:07:53', '00:00:36', 50, 'F001'),
(1172, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:06', '00:00:26', 138, 'F001'),
(1173, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:12', '00:00:44', 24, 'F001'),
(1174, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:24', '00:00:31', 58, 'F001'),
(1175, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:36', '00:00:30', 120, 'F001'),
(1176, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:38', '00:01:59', 30, 'F001'),
(1177, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:43', '00:00:05', 720, 'F001'),
(1178, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:08:54', '00:00:30', 60, 'F001'),
(1179, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:04', '00:00:28', 128, 'F001'),
(1180, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:10', '00:00:27', 133, 'F001'),
(1181, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:27', '00:00:33', 54, 'F001'),
(1182, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:37', '00:00:33', 109, 'F001'),
(1183, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:48', '00:01:36', 11, 'F001'),
(1184, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:09:52', '00:00:25', 72, 'F001'),
(1185, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:06', '00:00:29', 124, 'F001'),
(1186, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:13', '00:01:03', 57, 'F001'),
(1187, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:18', '00:00:26', 69, 'F001'),
(1188, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:21', '00:00:33', 32, 'F001'),
(1189, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:31', '00:00:10', 105, 'F001'),
(1190, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:40', '00:00:09', 117, 'F001'),
(1191, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:40', '00:00:34', 105, 'F001'),
(1192, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:46', '00:00:33', 109, 'F001'),
(1193, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:10:57', '00:00:17', 62, 'F001'),
(1194, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:06', '00:00:09', 117, 'F001'),
(1195, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:06', '00:00:48', 37, 'F001'),
(1196, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:12', '00:00:32', 112, 'F001'),
(1197, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:15', '00:00:09', 117, 'F001'),
(1198, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:19', '00:00:13', 138, 'F001'),
(1199, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:26', '00:00:11', 96, 'F001'),
(1200, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:26', '00:00:40', 90, 'F001'),
(1201, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:34', '00:00:08', 132, 'F001'),
(1202, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:46', '00:00:12', 88, 'F001'),
(1203, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:54', '00:00:35', 51, 'F001'),
(1204, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:11:54', '00:00:08', 132, 'F001'),
(1205, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:05', '00:00:11', 96, 'F001'),
(1206, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:11', '00:00:59', 61, 'F001'),
(1207, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:14', '00:00:09', 117, 'F001'),
(1208, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:16', '00:00:22', 81, 'F001'),
(1209, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:24', '00:00:10', 105, 'F001'),
(1210, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:33', '00:00:09', 117, 'F001'),
(1211, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:34', '00:01:08', 52, 'F001'),
(1212, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:34', '00:00:00', 100, 'F001'),
(1213, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:34', '00:00:00', 100, 'F001'),
(1214, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:38', '00:00:22', 81, 'F001'),
(1215, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:43', '00:00:10', 105, 'F001'),
(1216, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:47', '00:00:36', 100, 'F001'),
(1217, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:12:52', '00:00:09', 117, 'F001'),
(1218, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:02', '00:00:10', 105, 'F001'),
(1219, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:06', '00:00:28', 64, 'F001'),
(1220, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:08', '00:00:34', 105, 'F001'),
(1221, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:15', '00:00:13', 81, 'F001'),
(1222, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:30', '00:00:43', 83, 'F001'),
(1223, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:51', '00:00:21', 171, 'F001'),
(1224, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:13:56', '00:00:50', 36, 'F001'),
(1225, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:07', '00:00:59', 61, 'F001'),
(1226, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:27', '00:01:12', 14, 'F001'),
(1227, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:35', '00:00:44', 81, 'F001'),
(1228, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:36', '00:00:40', 45, 'F001'),
(1229, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:36', '00:00:09', 117, 'F001'),
(1230, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:45', '00:00:09', 117, 'F001'),
(1231, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:14:55', '00:00:10', 105, 'F001'),
(1232, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:04', '00:00:09', 117, 'F001'),
(1233, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:08', '00:00:33', 109, 'F001'),
(1234, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:09', '00:00:33', 54, 'F001'),
(1235, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:13', '00:00:09', 117, 'F001'),
(1236, '69-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:17', '00:01:10', 51, 'F001'),
(1237, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:21', '00:00:08', 132, 'F001'),
(1238, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:36', '00:00:27', 66, 'F001'),
(1239, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:38', '00:00:17', 62, 'F001'),
(1240, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:46', '00:00:08', 132, 'F001'),
(1241, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:47', '00:00:39', 92, 'F001'),
(1242, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:15:55', '00:00:09', 117, 'F001'),
(1243, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:05', '00:00:10', 105, 'F001'),
(1244, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:08', '00:00:21', 171, 'F001'),
(1245, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:10', '00:00:34', 52, 'F001'),
(1246, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:13', '00:00:08', 132, 'F001'),
(1247, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:26', '00:00:13', 81, 'F001'),
(1248, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:34', '00:00:08', 132, 'F001'),
(1249, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:42', '00:00:34', 105, 'F001'),
(1250, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:44', '00:00:10', 105, 'F001'),
(1251, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:48', '00:00:38', 47, 'F001'),
(1252, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:16:53', '00:00:09', 117, 'F001'),
(1253, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:02', '00:00:09', 117, 'F001'),
(1254, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:12', '00:00:10', 105, 'F001'),
(1255, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:16', '00:00:34', 105, 'F001'),
(1256, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:24', '00:00:12', 88, 'F001'),
(1257, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:34', '00:00:10', 105, 'F001'),
(1258, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:48', '00:00:14', 75, 'F001'),
(1259, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:49', '00:01:01', 29, 'F001'),
(1260, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:55', '00:00:39', 92, 'F001'),
(1261, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:17:57', '00:00:09', 117, 'F001'),
(1262, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:06', '00:00:09', 117, 'F001'),
(1263, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:15', '00:00:09', 117, 'F001'),
(1264, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:16', '00:00:27', 66, 'F001'),
(1265, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:24', '00:00:09', 117, 'F001'),
(1266, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:34', '00:00:10', 105, 'F001'),
(1267, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:45', '00:00:11', 96, 'F001'),
(1268, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:18:55', '00:00:10', 105, 'F001'),
(1269, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:04', '00:00:09', 117, 'F001'),
(1270, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:13', '00:00:09', 117, 'F001'),
(1271, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:18', '00:01:02', 29, 'F001'),
(1272, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:23', '00:01:28', 40, 'F001'),
(1273, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:35', '00:00:12', 300, 'F001'),
(1274, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:45', '00:00:32', 33, 'F001'),
(1275, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:54', '00:00:09', 117, 'F001'),
(1276, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:19:57', '00:00:22', 163, 'F001'),
(1277, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:20:06', '00:00:12', 88, 'F001'),
(1278, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:20:30', '00:00:24', 44, 'F001'),
(1279, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:20:30', '00:00:33', 109, 'F001'),
(1280, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:20:40', '00:00:10', 105, 'F001'),
(1281, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:20:49', '00:00:09', 117, 'F001'),
(1282, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:03', '00:01:45', 17, 'F001'),
(1283, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:05', '00:00:16', 66, 'F001'),
(1284, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:08', '00:00:38', 94, 'F001'),
(1285, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:15', '00:00:10', 105, 'F001'),
(1286, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:23', '00:00:20', 90, 'F001'),
(1287, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:27', '00:00:12', 88, 'F001'),
(1288, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:37', '00:00:10', 105, 'F001'),
(1289, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:44', '00:00:21', 85, 'F001'),
(1290, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:46', '00:00:09', 117, 'F001'),
(1291, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:53', '00:00:45', 80, 'F001'),
(1292, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:21:58', '00:00:12', 88, 'F001'),
(1293, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:08', '00:00:10', 105, 'F001'),
(1294, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:12', '00:00:28', 64, 'F001'),
(1295, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:20', '00:00:12', 88, 'F001'),
(1296, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:31', '00:00:11', 96, 'F001'),
(1297, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:34', '00:00:41', 87, 'F001'),
(1298, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:34', '00:00:22', 81, 'F001'),
(1299, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:39', '00:00:08', 132, 'F001'),
(1300, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:22:57', '00:00:23', 78, 'F001'),
(1301, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:12', '00:00:33', 32, 'F001'),
(1302, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:12', '00:00:38', 94, 'F001'),
(1303, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:26', '00:00:29', 62, 'F001'),
(1304, '71-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(1305, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:47', '00:00:35', 30, 'F001'),
(1306, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:49', '00:00:23', 78, 'F001'),
(1307, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:23:58', '00:00:11', 96, 'F001'),
(1308, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:08', '00:00:10', 105, 'F001'),
(1309, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:14', '00:00:25', 72, 'F001'),
(1310, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:24', '00:00:00', 100, 'F001'),
(1311, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:29', '00:00:21', 50, 'F001'),
(1312, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:39', '00:00:10', 105, 'F001'),
(1313, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:40', '00:00:26', 69, 'F001'),
(1314, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:48', '00:00:24', 150, 'F001'),
(1315, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:24:51', '00:01:39', 36, 'F001'),
(1316, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:01', '00:00:22', 48, 'F001'),
(1317, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:11', '00:00:10', 105, 'F001'),
(1318, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:18', '00:00:27', 133, 'F001'),
(1319, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:22', '00:00:42', 42, 'F001'),
(1320, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:23', '00:00:12', 88, 'F001'),
(1321, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:34', '00:00:46', 78, 'F001'),
(1322, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:34', '00:00:11', 96, 'F001'),
(1323, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:51', '00:00:17', 62, 'F001'),
(1324, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:55', '00:00:21', 171, 'F001'),
(1325, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:25:58', '00:00:40', 90, 'F001'),
(1326, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:01', '00:00:10', 105, 'F001'),
(1327, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:07', '00:00:45', 40, 'F001'),
(1328, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:34', '00:00:33', 32, 'F001'),
(1329, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:35', '00:00:28', 64, 'F001'),
(1330, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:44', '00:00:10', 105, 'F001'),
(1331, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:45', '00:00:47', 76, 'F001'),
(1332, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:48', '00:00:53', 67, 'F001'),
(1333, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:26:53', '00:00:09', 117, 'F001'),
(1334, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:04', '00:00:29', 62, 'F001'),
(1335, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:06', '00:00:13', 81, 'F001'),
(1336, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:24', '00:00:18', 58, 'F001'),
(1337, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:26', '00:00:38', 94, 'F001'),
(1338, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:32', '00:00:08', 132, 'F001'),
(1339, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:33', '00:00:29', 62, 'F001'),
(1340, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:45', '00:00:13', 81, 'F001'),
(1341, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:48', '00:01:03', 57, 'F001'),
(1342, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:27:59', '00:00:26', 69, 'F001'),
(1343, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:00', '00:00:15', 70, 'F001'),
(1344, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:01', '00:00:35', 102, 'F001'),
(1345, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:08', '00:00:08', 132, 'F001'),
(1346, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:16', '00:00:08', 132, 'F001'),
(1347, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:26', '00:00:10', 105, 'F001'),
(1348, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:39', '00:00:38', 94, 'F001'),
(1349, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:28:58', '00:00:32', 33, 'F001'),
(1350, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:04', '00:01:05', 27, 'F001'),
(1351, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:08', '00:00:29', 124, 'F001'),
(1352, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:09', '00:01:21', 44, 'F001'),
(1353, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:14', '00:00:16', 66, 'F001'),
(1354, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:21', '00:00:07', 151, 'F001'),
(1355, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:31', '00:00:10', 105, 'F001'),
(1356, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:39', '00:00:35', 51, 'F001'),
(1357, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:42', '00:00:11', 96, 'F001'),
(1358, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:50', '00:00:08', 132, 'F001'),
(1359, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:56', '00:00:48', 75, 'F001'),
(1360, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:29:58', '00:00:08', 132, 'F001'),
(1361, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:08', '00:00:10', 105, 'F001'),
(1362, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:19', '00:00:11', 96, 'F001'),
(1363, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:26', '00:00:47', 38, 'F001'),
(1364, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:28', '00:00:09', 117, 'F001'),
(1365, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:29', '00:01:20', 45, 'F001'),
(1366, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:31', '00:00:35', 102, 'F001'),
(1367, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:37', '00:00:09', 117, 'F001'),
(1368, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:43', '00:00:17', 105, 'F001'),
(1369, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:46', '00:00:09', 117, 'F001'),
(1370, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:30:54', '00:00:08', 132, 'F001'),
(1371, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:02', '00:00:08', 132, 'F001'),
(1372, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:06', '00:00:23', 78, 'F001'),
(1373, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:14', '00:00:12', 88, 'F001'),
(1374, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:16', '00:00:47', 76, 'F001'),
(1375, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:22', '00:00:51', 70, 'F001'),
(1376, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:23', '00:00:09', 117, 'F001'),
(1377, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:31', '00:00:09', 400, 'F001'),
(1378, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:32', '00:00:09', 117, 'F001'),
(1379, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:49', '00:00:33', 109, 'F001'),
(1380, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:31:57', '00:00:25', 42, 'F001'),
(1381, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:32:02', '00:00:56', 32, 'F001'),
(1382, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:32:23', '00:00:34', 105, 'F001'),
(1383, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:32:43', '00:00:41', 43, 'F001'),
(1384, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:01', '00:00:38', 94, 'F001'),
(1385, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:09', '00:01:12', 14, 'F001'),
(1386, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:19', '00:00:10', 105, 'F001'),
(1387, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:24', '00:00:41', 43, 'F001'),
(1388, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:29', '00:00:10', 105, 'F001'),
(1389, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:39', '00:00:10', 105, 'F001'),
(1390, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:47', '00:00:46', 78, 'F001'),
(1391, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:52', '00:00:13', 81, 'F001'),
(1392, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:33:59', '00:00:35', 51, 'F001'),
(1393, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:05', '00:00:13', 81, 'F001'),
(1394, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:15', '00:00:10', 105, 'F001'),
(1395, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:21', '00:00:34', 105, 'F001'),
(1396, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:27', '00:00:28', 64, 'F001'),
(1397, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:30', '00:02:59', 20, 'F001'),
(1398, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:36', '00:00:21', 50, 'F001'),
(1399, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:43', '00:00:07', 151, 'F001'),
(1400, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:46', '00:00:16', 225, 'F001'),
(1401, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:34:59', '00:00:32', 56, 'F001'),
(1402, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:19', '00:00:36', 29, 'F001'),
(1403, '71-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:20', '00:00:34', 105, 'F001'),
(1404, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:29', '00:00:10', 105, 'F001'),
(1405, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:31', '00:00:32', 56, 'F001'),
(1406, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:31', '00:01:10', 51, 'F001'),
(1407, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:40', '00:00:11', 96, 'F001'),
(1408, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:54', '00:00:14', 75, 'F001'),
(1409, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:35:55', '00:00:24', 75, 'F001'),
(1410, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:03', '00:00:09', 117, 'F001'),
(1411, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:13', '00:00:10', 105, 'F001'),
(1412, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:22', '00:00:51', 70, 'F001'),
(1413, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:29', '00:00:16', 66, 'F001'),
(1414, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:39', '00:00:10', 105, 'F001'),
(1415, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:54', '00:00:32', 112, 'F001'),
(1416, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:36:57', '00:00:18', 58, 'F001'),
(1417, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:06', '00:00:09', 117, 'F001'),
(1418, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:08', '00:01:13', 24, 'F001'),
(1419, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:15', '00:00:09', 117, 'F001'),
(1420, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:28', '00:00:34', 105, 'F001'),
(1421, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:30', '00:00:15', 70, 'F001'),
(1422, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:38', '00:00:30', 60, 'F001'),
(1423, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:41', '00:00:11', 96, 'F001'),
(1424, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:37:58', '00:00:17', 62, 'F001'),
(1425, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:03', '00:00:25', 72, 'F001'),
(1426, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:09', '00:00:11', 96, 'F001'),
(1427, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:19', '00:00:10', 105, 'F001'),
(1428, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:33', '00:00:14', 75, 'F001'),
(1429, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:34', '00:00:31', 58, 'F001'),
(1430, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:42', '00:01:14', 48, 'F001'),
(1431, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:45', '00:00:12', 88, 'F001'),
(1432, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:38:56', '00:00:11', 96, 'F001'),
(1433, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:01', '00:00:27', 66, 'F001'),
(1434, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:09', '00:00:13', 81, 'F001'),
(1435, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:14', '00:00:32', 112, 'F001'),
(1436, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:39', '00:00:38', 47, 'F001'),
(1437, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:48', '00:00:34', 105, 'F001'),
(1438, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:39:54', '00:00:45', 23, 'F001'),
(1439, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:08', '00:00:14', 75, 'F001'),
(1440, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:12', '00:00:33', 54, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(1441, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:17', '00:00:09', 117, 'F001'),
(1442, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:21', '00:00:33', 109, 'F001'),
(1443, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:25', '00:00:08', 132, 'F001'),
(1444, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:35', '00:00:10', 105, 'F001'),
(1445, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:43', '00:00:31', 58, 'F001'),
(1446, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:47', '00:00:12', 88, 'F001'),
(1447, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:53', '00:00:32', 112, 'F001'),
(1448, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:40:55', '00:00:08', 132, 'F001'),
(1449, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:03', '00:00:08', 132, 'F001'),
(1450, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:10', '00:00:27', 66, 'F001'),
(1451, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:12', '00:00:09', 117, 'F001'),
(1452, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:20', '00:00:08', 132, 'F001'),
(1453, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:30', '00:00:10', 105, 'F001'),
(1454, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:34', '00:00:41', 87, 'F001'),
(1455, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:41', '00:00:11', 96, 'F001'),
(1456, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:41:51', '00:00:10', 105, 'F001'),
(1457, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:01', '00:00:10', 105, 'F001'),
(1458, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:05', '00:00:31', 116, 'F001'),
(1459, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:10', '00:00:09', 117, 'F001'),
(1460, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:19', '00:00:09', 117, 'F001'),
(1461, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:23', '00:01:13', 24, 'F001'),
(1462, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:29', '00:00:10', 105, 'F001'),
(1463, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:39', '00:00:34', 105, 'F001'),
(1464, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:40', '00:00:11', 96, 'F001'),
(1465, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:55', '00:00:15', 70, 'F001'),
(1466, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:42:56', '00:00:33', 54, 'F001'),
(1467, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:08', '00:00:13', 81, 'F001'),
(1468, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:17', '00:00:09', 117, 'F001'),
(1469, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:25', '00:00:08', 132, 'F001'),
(1470, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:26', '00:00:47', 76, 'F001'),
(1471, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:29', '00:00:33', 54, 'F001'),
(1472, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:39', '00:00:14', 75, 'F001'),
(1473, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:48', '00:00:09', 117, 'F001'),
(1474, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:58', '00:00:10', 105, 'F001'),
(1475, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:43:58', '00:00:29', 62, 'F001'),
(1476, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:44:07', '00:00:09', 117, 'F001'),
(1477, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:44:14', '00:00:48', 75, 'F001'),
(1478, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:44:18', '00:00:11', 96, 'F001'),
(1479, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:44:35', '00:00:17', 62, 'F001'),
(1480, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:44:44', '00:00:09', 117, 'F001'),
(1481, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:45:01', '00:01:03', 28, 'F001'),
(1482, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:45:42', '00:00:41', 43, 'F001'),
(1483, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:13', '00:01:29', 11, 'F001'),
(1484, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:19', '00:00:37', 48, 'F001'),
(1485, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:23', '00:02:09', 27, 'F001'),
(1486, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:28', '00:00:15', 70, 'F001'),
(1487, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:38', '00:00:10', 105, 'F001'),
(1488, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:47', '00:00:09', 117, 'F001'),
(1489, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:51', '00:00:32', 56, 'F001'),
(1490, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:46:56', '00:00:09', 117, 'F001'),
(1491, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:05', '00:00:09', 117, 'F001'),
(1492, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:15', '00:00:10', 105, 'F001'),
(1493, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:22', '00:00:31', 58, 'F001'),
(1494, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:26', '00:00:11', 96, 'F001'),
(1495, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:39', '00:00:13', 81, 'F001'),
(1496, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:49', '00:00:10', 105, 'F001'),
(1497, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:54', '00:00:32', 56, 'F001'),
(1498, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:47:58', '00:00:09', 117, 'F001'),
(1499, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:09', '00:00:11', 96, 'F001'),
(1500, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:17', '00:00:08', 132, 'F001'),
(1501, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:21', '00:01:58', 30, 'F001'),
(1502, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:27', '00:00:10', 105, 'F001'),
(1503, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:33', '00:00:39', 46, 'F001'),
(1504, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:40', '00:00:13', 81, 'F001'),
(1505, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:46', '00:00:25', 144, 'F001'),
(1506, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:48:51', '00:00:11', 96, 'F001'),
(1507, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:02', '00:00:11', 96, 'F001'),
(1508, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:13', '00:00:11', 96, 'F001'),
(1509, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:16', '00:00:43', 41, 'F001'),
(1510, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:23', '00:00:37', 97, 'F001'),
(1511, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:28', '00:00:15', 70, 'F001'),
(1512, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:39', '00:00:11', 96, 'F001'),
(1513, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:43', '00:00:27', 66, 'F001'),
(1514, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:49', '00:00:10', 105, 'F001'),
(1515, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:59', '00:00:36', 100, 'F001'),
(1516, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:49:59', '00:00:10', 105, 'F001'),
(1517, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:09', '00:00:10', 105, 'F001'),
(1518, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:12', '00:00:29', 62, 'F001'),
(1519, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:20', '00:00:11', 96, 'F001'),
(1520, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:29', '00:00:09', 117, 'F001'),
(1521, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:39', '00:00:10', 105, 'F001'),
(1522, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:40', '00:00:28', 64, 'F001'),
(1523, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:42', '00:00:43', 83, 'F001'),
(1524, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:50:51', '00:00:12', 88, 'F001'),
(1525, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:06', '00:00:26', 69, 'F001'),
(1526, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:09', '00:00:18', 58, 'F001'),
(1527, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:17', '00:00:35', 102, 'F001'),
(1528, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:25', '00:00:16', 66, 'F001'),
(1529, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:33', '00:00:27', 66, 'F001'),
(1530, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:51:41', '00:00:16', 66, 'F001'),
(1531, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:00', '00:00:27', 66, 'F001'),
(1532, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:04', '00:00:47', 76, 'F001'),
(1533, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:26', '00:00:45', 23, 'F001'),
(1534, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:30', '00:00:30', 60, 'F001'),
(1535, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:38', '00:00:12', 88, 'F001'),
(1536, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:46', '00:00:08', 132, 'F001'),
(1537, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:53', '00:00:49', 73, 'F001'),
(1538, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:52:54', '00:00:08', 132, 'F001'),
(1539, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:10', '00:00:16', 66, 'F001'),
(1540, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:19', '00:00:09', 117, 'F001'),
(1541, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:29', '00:00:36', 100, 'F001'),
(1542, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:33', '00:00:14', 75, 'F001'),
(1543, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:44', '00:00:11', 96, 'F001'),
(1544, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:53:56', '00:00:12', 88, 'F001'),
(1545, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:10', '00:00:14', 75, 'F001'),
(1546, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:18', '00:00:49', 73, 'F001'),
(1547, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:19', '00:00:09', 117, 'F001'),
(1548, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:28', '00:00:09', 117, 'F001'),
(1549, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:37', '00:00:09', 117, 'F001'),
(1550, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:47', '00:00:10', 105, 'F001'),
(1551, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:53', '00:00:35', 102, 'F001'),
(1552, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:54:57', '00:00:10', 105, 'F001'),
(1553, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:55:03', '00:02:33', 11, 'F001'),
(1554, '56-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:55:07', '00:00:10', 105, 'F001'),
(1555, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 09:55:30', '00:00:27', 66, 'F001'),
(1556, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', NULL, 102, NULL, NULL, NULL, NULL, NULL, '2016-12-06 09:55:58', NULL, NULL, 'P001'),
(1557, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', NULL, 5, NULL, NULL, NULL, NULL, NULL, '2016-12-06 09:58:16', NULL, NULL, 'P001'),
(1558, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:14:08', '00:18:10', 3, 'F001'),
(1559, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:14:36', '00:16:20', 1, 'F001'),
(1560, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:15:07', '00:00:59', 61, 'F001'),
(1561, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:15:53', '00:00:46', 78, 'F001'),
(1562, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:16:03', '00:01:27', 20, 'F001'),
(1563, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:16:27', '00:00:24', 75, 'F001'),
(1564, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:16:40', '00:00:47', 76, 'F001'),
(1565, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:17:02', '00:00:35', 51, 'F001'),
(1566, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:17:18', '00:00:38', 94, 'F001'),
(1567, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:17:45', '00:00:43', 41, 'F001'),
(1568, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:18:04', '00:00:46', 78, 'F001'),
(1569, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:18:12', '00:00:27', 66, 'F001'),
(1570, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:18:39', '00:00:27', 66, 'F001'),
(1571, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:18:49', '00:00:45', 80, 'F001'),
(1572, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:19:04', '00:00:25', 72, 'F001'),
(1573, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:19:28', '00:00:39', 92, 'F001'),
(1574, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:19:31', '00:00:27', 66, 'F001'),
(1575, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:20:01', '00:00:30', 60, 'F001'),
(1576, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:20:07', '00:00:39', 92, 'F001'),
(1577, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:20:30', '00:00:29', 62, 'F001'),
(1578, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:20:59', '00:00:29', 62, 'F001'),
(1579, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:21:32', '00:00:33', 54, 'F001'),
(1580, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:21:38', '00:01:31', 39, 'F001'),
(1581, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:22:01', '00:00:23', 156, 'F001'),
(1582, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:22:22', '00:00:50', 36, 'F001'),
(1583, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:22:45', '00:00:23', 78, 'F001'),
(1584, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:22:48', '00:00:47', 76, 'F001'),
(1585, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:23:12', '00:00:27', 66, 'F001'),
(1586, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:23:26', '00:00:38', 94, 'F001'),
(1587, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:23:40', '00:00:28', 64, 'F001'),
(1588, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:24:02', '00:00:36', 100, 'F001'),
(1589, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:24:19', '00:00:39', 46, 'F001'),
(1590, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:24:40', '00:00:38', 94, 'F001'),
(1591, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:24:45', '00:00:26', 69, 'F001'),
(1592, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:25:28', '00:00:43', 41, 'F001'),
(1593, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:26:02', '00:00:34', 52, 'F001'),
(1594, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:26:23', '00:01:43', 34, 'F001'),
(1595, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:26:42', '00:00:40', 45, 'F001'),
(1596, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:27:35', '00:00:53', 33, 'F001'),
(1597, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:27:55', '00:01:32', 39, 'F001'),
(1598, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:27:58', '00:00:23', 78, 'F001'),
(1599, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:28:35', '00:00:40', 90, 'F001'),
(1600, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:28:39', '00:00:41', 43, 'F001'),
(1601, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:29:11', '00:00:36', 100, 'F001'),
(1602, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:29:43', '00:01:04', 28, 'F001'),
(1603, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:29:49', '00:00:38', 94, 'F001'),
(1604, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:30:23', '00:00:34', 105, 'F001'),
(1605, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:30:56', '00:00:33', 109, 'F001'),
(1606, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:30:57', '00:01:14', 24, 'F001'),
(1607, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:31:31', '00:00:34', 52, 'F001'),
(1608, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:31:44', '00:00:48', 75, 'F001'),
(1609, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:32:15', '00:00:44', 40, 'F001'),
(1610, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:32:48', '00:00:33', 54, 'F001'),
(1611, '72-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(1612, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:33:10', '00:00:22', 81, 'F001'),
(1613, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:33:13', '00:01:29', 40, 'F001'),
(1614, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:33:37', '00:00:27', 66, 'F001'),
(1615, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:33:46', '00:00:00', 156, 'F001'),
(1616, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:33:54', '00:00:41', 87, 'F001'),
(1617, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:34:05', '00:00:28', 64, 'F001'),
(1618, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:34:30', '00:00:25', 72, 'F001'),
(1619, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:35:17', '00:00:47', 38, 'F001'),
(1620, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:35:27', '00:01:33', 38, 'F001'),
(1621, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:35:39', '00:00:22', 81, 'F001'),
(1622, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:36:06', '00:00:39', 92, 'F001'),
(1623, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:36:06', '00:00:27', 66, 'F001'),
(1624, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:36:35', '00:00:29', 62, 'F001'),
(1625, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:36:51', '00:00:45', 80, 'F001'),
(1626, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:37:06', '00:00:31', 58, 'F001'),
(1627, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:37:27', '00:00:36', 100, 'F001'),
(1628, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:38:07', '00:00:40', 90, 'F001'),
(1629, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:38:45', '00:01:39', 18, 'F001'),
(1630, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:39:26', '00:00:41', 43, 'F001'),
(1631, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:39:53', '00:01:46', 33, 'F001'),
(1632, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:39:53', '00:00:27', 66, 'F001'),
(1633, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:40:07', '00:06:21', 9, 'F001'),
(1634, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:40:07', '00:00:14', 257, 'F001'),
(1635, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:40:22', '00:00:29', 62, 'F001'),
(1636, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:40:49', '00:00:42', 85, 'F001'),
(1637, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:40:55', '00:00:33', 54, 'F001'),
(1638, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:26', '00:00:37', 97, 'F001'),
(1639, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:31', '00:01:24', 42, 'F001'),
(1640, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:31', '00:00:00', 100, 'F001'),
(1641, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:32', '00:00:01', 3600, 'F001'),
(1642, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:32', '00:00:00', 100, 'F001'),
(1643, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:32', '00:00:00', 100, 'F001'),
(1644, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:32', '00:00:00', 100, 'F001'),
(1645, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:36', '00:00:04', 900, 'F001'),
(1646, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:43', '00:00:07', 514, 'F001'),
(1647, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:41:53', '00:00:10', 360, 'F001'),
(1648, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:42:01', '00:00:35', 102, 'F001'),
(1649, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:42:25', '00:01:30', 20, 'F001'),
(1650, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:42:34', '00:00:33', 109, 'F001'),
(1651, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:42:54', '00:00:29', 62, 'F001'),
(1652, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:07', '00:00:33', 109, 'F001'),
(1653, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:11', '00:01:18', 46, 'F001'),
(1654, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:23', '00:00:29', 62, 'F001'),
(1655, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:24', '00:00:13', 276, 'F001'),
(1656, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:46', '00:00:39', 92, 'F001'),
(1657, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:43:52', '00:00:29', 62, 'F001'),
(1658, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:44:20', '00:00:28', 64, 'F001'),
(1659, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:44:31', '00:01:07', 53, 'F001'),
(1660, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:44:51', '00:00:31', 58, 'F001'),
(1661, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:44:53', '00:00:22', 163, 'F001'),
(1662, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:45:23', '00:00:30', 120, 'F001'),
(1663, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:45:39', '00:00:48', 37, 'F001'),
(1664, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:11', '00:00:48', 75, 'F001'),
(1665, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:21', '00:00:10', 360, 'F001'),
(1666, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:27', '00:00:48', 37, 'F001'),
(1667, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:46', '00:03:00', 20, 'F001'),
(1668, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:55', '00:00:28', 64, 'F001'),
(1669, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:46:55', '00:00:09', 400, 'F001'),
(1670, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:47:18', '00:00:23', 156, 'F001'),
(1671, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:47:27', '00:01:06', 54, 'F001'),
(1672, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:47:35', '00:00:40', 45, 'F001'),
(1673, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:47:58', '00:00:40', 90, 'F001'),
(1674, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:48:00', '00:00:25', 72, 'F001'),
(1675, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:48:29', '00:01:02', 58, 'F001'),
(1676, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:48:29', '00:00:29', 62, 'F001'),
(1677, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:48:37', '00:00:08', 450, 'F001'),
(1678, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:48:59', '00:00:30', 60, 'F001'),
(1679, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:09', '00:01:11', 50, 'F001'),
(1680, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:28', '00:00:29', 62, 'F001'),
(1681, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:32', '00:00:55', 65, 'F001'),
(1682, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:40', '00:00:31', 116, 'F001'),
(1683, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:54', '00:00:22', 163, 'F001'),
(1684, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:49:57', '00:00:29', 62, 'F001'),
(1685, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:50:17', '00:00:37', 97, 'F001'),
(1686, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:50:30', '00:00:33', 54, 'F001'),
(1687, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:50:49', '00:00:55', 65, 'F001'),
(1688, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:50:59', '00:00:42', 85, 'F001'),
(1689, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:51:18', '00:00:48', 37, 'F001'),
(1690, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:51:49', '00:00:50', 72, 'F001'),
(1691, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:52:20', '00:01:02', 29, 'F001'),
(1692, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:52:30', '00:00:41', 87, 'F001'),
(1693, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:52:39', '00:00:19', 94, 'F001'),
(1694, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:53:01', '00:00:31', 116, 'F001'),
(1695, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:53:18', '00:00:39', 46, 'F001'),
(1696, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:53:47', '00:00:29', 62, 'F001'),
(1697, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:53:50', '00:00:49', 73, 'F001'),
(1698, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:54:11', '00:00:24', 75, 'F001'),
(1699, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:54:21', '00:00:31', 116, 'F001'),
(1700, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:54:26', '00:00:15', 120, 'F001'),
(1701, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:55:33', '00:01:07', 26, 'F001'),
(1702, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:55:36', '00:01:15', 48, 'F001'),
(1703, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:56:09', '00:00:33', 109, 'F001'),
(1704, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:57:03', '00:00:54', 66, 'F001'),
(1705, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:57:24', '00:00:21', 171, 'F001'),
(1706, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:58:00', '00:00:36', 100, 'F001'),
(1707, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:58:20', '00:02:47', 10, 'F001'),
(1708, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:58:30', '00:00:30', 120, 'F001'),
(1709, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:58:51', '00:00:31', 58, 'F001'),
(1710, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:59:08', '00:00:38', 94, 'F001'),
(1711, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:59:20', '00:00:29', 62, 'F001'),
(1712, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:59:48', '00:00:28', 64, 'F001'),
(1713, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 10:59:50', '00:00:42', 85, 'F001'),
(1714, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:00:23', '00:00:33', 109, 'F001'),
(1715, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:00:29', '00:00:41', 43, 'F001'),
(1716, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:01:01', '00:00:38', 94, 'F001'),
(1717, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:01:10', '00:00:41', 43, 'F001'),
(1718, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:01:40', '00:00:30', 60, 'F001'),
(1719, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:01:45', '00:00:44', 81, 'F001'),
(1720, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:02:07', '00:00:27', 66, 'F001'),
(1721, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:02:25', '00:00:40', 90, 'F001'),
(1722, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:02:33', '00:00:26', 69, 'F001'),
(1723, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:03:37', '00:01:04', 28, 'F001'),
(1724, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:04:32', '00:02:07', 28, 'F001'),
(1725, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:04:36', '00:00:59', 30, 'F001'),
(1726, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:05:08', '00:00:36', 100, 'F001'),
(1727, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:05:18', '00:00:42', 42, 'F001'),
(1728, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:05:48', '00:00:40', 90, 'F001'),
(1729, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:05:54', '00:00:36', 50, 'F001'),
(1730, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:06:15', '00:00:21', 85, 'F001'),
(1731, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:06:25', '00:00:37', 97, 'F001'),
(1732, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:06:46', '00:00:31', 58, 'F001'),
(1733, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:07:02', '00:00:37', 97, 'F001'),
(1734, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:07:20', '00:00:34', 52, 'F001'),
(1735, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:07:44', '00:00:42', 85, 'F001'),
(1736, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:07:58', '00:00:38', 47, 'F001'),
(1737, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:08:21', '00:00:37', 97, 'F001'),
(1738, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:08:40', '00:00:42', 42, 'F001'),
(1739, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:09:12', '00:00:32', 56, 'F001'),
(1740, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:09:44', '00:00:32', 56, 'F001'),
(1741, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:09:59', '00:01:38', 36, 'F001'),
(1742, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:10:32', '00:00:48', 37, 'F001'),
(1743, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:10:33', '00:00:34', 105, 'F001'),
(1744, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:10:53', '00:00:20', 180, 'F001'),
(1745, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:11:04', '00:00:32', 56, 'F001'),
(1746, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:11:14', '00:00:21', 171, 'F001'),
(1747, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:11:42', '00:00:38', 47, 'F001'),
(1748, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:12:13', '00:00:59', 61, 'F001'),
(1749, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:12:13', '00:00:31', 58, 'F001'),
(1750, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:12:28', '00:00:15', 240, 'F001'),
(1751, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:12:36', '00:21:47', 2, 'F001'),
(1752, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:12:52', '00:00:39', 46, 'F001'),
(1753, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:13:18', '00:00:50', 72, 'F001'),
(1754, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:13:27', '00:00:35', 51, 'F001'),
(1755, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:13:40', '00:00:22', 163, 'F001'),
(1756, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:14:11', '00:00:44', 40, 'F001'),
(1757, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:14:40', '00:00:29', 62, 'F001'),
(1758, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:15:01', '00:01:21', 44, 'F001'),
(1759, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:15:16', '00:00:15', 240, 'F001'),
(1760, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:15:41', '00:00:25', 144, 'F001'),
(1761, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:16:22', '00:00:41', 87, 'F001'),
(1762, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:17:07', '00:02:27', 12, 'F001'),
(1763, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:17:20', '00:00:58', 62, 'F001'),
(1764, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:18:02', '00:00:55', 32, 'F001'),
(1765, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:18:07', '00:00:47', 76, 'F001'),
(1766, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:18:10', '00:05:34', 10, 'F001'),
(1767, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:18:36', '00:00:34', 52, 'F001'),
(1768, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:18:43', '00:00:36', 100, 'F001'),
(1769, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:19:10', '00:01:00', 60, 'F001'),
(1770, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:19:10', '00:00:34', 52, 'F001'),
(1771, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:19:24', '00:00:41', 87, 'F001'),
(1772, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:19:42', '00:00:32', 56, 'F001'),
(1773, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:20:06', '00:00:24', 75, 'F001'),
(1774, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:20:11', '00:00:47', 76, 'F001'),
(1775, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:20:20', '00:00:14', 128, 'F001'),
(1776, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:20:56', '00:00:36', 50, 'F001'),
(1777, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:21:21', '00:01:10', 51, 'F001'),
(1778, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:21:32', '00:00:36', 50, 'F001'),
(1779, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:21:39', '00:00:18', 200, 'F001'),
(1780, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:22:05', '00:00:33', 54, 'F001'),
(1781, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:22:20', '00:00:41', 87, 'F001'),
(1782, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:22:31', '00:00:26', 69, 'F001'),
(1783, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:23:07', '00:00:36', 50, 'F001'),
(1784, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:23:26', '00:01:06', 54, 'F001'),
(1785, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:23:41', '00:00:34', 52, 'F001'),
(1786, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:24:03', '00:00:37', 97, 'F001'),
(1787, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:24:12', '00:00:31', 58, 'F001'),
(1788, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:24:38', '00:00:35', 102, 'F001'),
(1789, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:25:23', '00:01:11', 25, 'F001'),
(1790, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:25:34', '00:00:56', 64, 'F001'),
(1791, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:26:08', '00:00:45', 40, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(1792, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:26:32', '00:00:24', 75, 'F001'),
(1793, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:27:05', '00:00:33', 54, 'F001'),
(1794, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:27:37', '00:00:32', 56, 'F001'),
(1795, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:27:57', '00:02:23', 25, 'F001'),
(1796, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:28:27', '00:00:30', 120, 'F001'),
(1797, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:28:45', '00:01:08', 26, 'F001'),
(1798, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:29:04', '00:00:37', 97, 'F001'),
(1799, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:29:45', '00:10:35', 5, 'F001'),
(1800, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:29:54', '00:00:50', 72, 'F001'),
(1801, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:30:09', '00:01:24', 21, 'F001'),
(1802, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:30:31', '00:00:37', 97, 'F001'),
(1803, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:30:50', '00:00:19', 189, 'F001'),
(1804, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:30:54', '00:00:45', 40, 'F001'),
(1805, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:31:39', '00:00:49', 73, 'F001'),
(1806, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:31:43', '00:01:58', 30, 'F001'),
(1807, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:31:47', '00:00:08', 450, 'F001'),
(1808, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:03', '00:00:16', 225, 'F001'),
(1809, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:13', '00:00:10', 360, 'F001'),
(1810, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:18', '00:01:24', 21, 'F001'),
(1811, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:21', '00:00:08', 450, 'F001'),
(1812, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:31', '00:00:10', 360, 'F001'),
(1813, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:40', '00:00:09', 400, 'F001'),
(1814, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:47', '00:00:29', 62, 'F001'),
(1815, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:49', '00:00:09', 400, 'F001'),
(1816, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:32:59', '00:00:10', 360, 'F001'),
(1817, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:12', '00:00:13', 276, 'F001'),
(1818, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:17', '00:00:05', 720, 'F001'),
(1819, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:26', '00:00:09', 400, 'F001'),
(1820, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:34', '00:00:08', 450, 'F001'),
(1821, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:46', '00:00:12', 300, 'F001'),
(1822, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:33:52', '00:00:06', 600, 'F001'),
(1823, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:34:03', '00:00:11', 327, 'F001'),
(1824, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:34:24', '00:00:21', 171, 'F001'),
(1825, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:34:47', '00:00:23', 156, 'F001'),
(1826, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:03:41', 16, 'F001'),
(1827, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:00:00', 100, 'F001'),
(1828, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:00:00', 100, 'F001'),
(1829, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:00:00', 100, 'F001'),
(1830, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:00:00', 100, 'F001'),
(1831, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:24', '00:00:00', 100, 'F001'),
(1832, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:35:26', '00:00:39', 92, 'F001'),
(1833, '72-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:36:07', '00:00:43', 83, 'F001'),
(1834, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:36:10', '00:00:44', 81, 'F001'),
(1835, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:37:03', '00:00:53', 67, 'F001'),
(1836, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:37:07', '00:04:20', 6, 'F001'),
(1837, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:37:32', '00:00:29', 124, 'F001'),
(1838, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:37:34', '00:00:27', 66, 'F001'),
(1839, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:38:18', '00:00:44', 40, 'F001'),
(1840, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:38:28', '00:00:56', 64, 'F001'),
(1841, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:38:56', '00:00:28', 128, 'F001'),
(1842, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:39:10', '00:00:52', 34, 'F001'),
(1843, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:40:14', '00:01:18', 46, 'F001'),
(1844, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:40:31', '00:00:17', 211, 'F001'),
(1845, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:41:00', '00:00:29', 124, 'F001'),
(1846, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:41:38', '00:00:38', 94, 'F001'),
(1847, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:42:21', '00:00:43', 83, 'F001'),
(1848, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:43:01', '00:03:51', 7, 'F001'),
(1849, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:43:29', '00:01:08', 52, 'F001'),
(1850, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:44:05', '00:01:04', 28, 'F001'),
(1851, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:44:27', '00:00:58', 62, 'F001'),
(1852, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:44:38', '00:00:33', 54, 'F001'),
(1853, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:45:10', '00:00:32', 56, 'F001'),
(1854, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:45:48', '00:00:38', 47, 'F001'),
(1855, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:45:50', '00:01:23', 43, 'F001'),
(1856, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:46:23', '00:00:35', 51, 'F001'),
(1857, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:46:37', '00:00:47', 76, 'F001'),
(1858, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:46:54', '00:00:31', 58, 'F001'),
(1859, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:47:21', '00:00:44', 81, 'F001'),
(1860, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:47:22', '00:00:28', 64, 'F001'),
(1861, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:47:58', '00:00:36', 50, 'F001'),
(1862, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:48:25', '00:01:04', 56, 'F001'),
(1863, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:48:33', '00:00:35', 51, 'F001'),
(1864, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:48:40', '00:00:15', 240, 'F001'),
(1865, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:49:10', '00:00:37', 48, 'F001'),
(1866, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:49:35', '00:00:25', 72, 'F001'),
(1867, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:50:11', '00:00:36', 50, 'F001'),
(1868, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:50:18', '00:01:38', 36, 'F001'),
(1869, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:50:41', '00:00:30', 60, 'F001'),
(1870, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:51:13', '00:00:32', 56, 'F001'),
(1871, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:51:49', '00:00:36', 50, 'F001'),
(1872, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:52:20', '00:00:31', 58, 'F001'),
(1873, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:52:53', '00:00:33', 54, 'F001'),
(1874, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:53:54', '00:01:01', 29, 'F001'),
(1875, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:54:46', '00:00:52', 34, 'F001'),
(1876, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:55:18', '00:00:32', 56, 'F001'),
(1877, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:55:42', '00:00:24', 75, 'F001'),
(1878, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:56:09', '00:00:27', 66, 'F001'),
(1879, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:56:46', '00:00:37', 48, 'F001'),
(1880, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:57:16', '00:00:30', 60, 'F001'),
(1881, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:57:43', '00:00:27', 66, 'F001'),
(1882, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:58:10', '00:00:27', 66, 'F001'),
(1883, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:58:36', '00:00:26', 69, 'F001'),
(1884, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 11:58:57', '00:00:21', 85, 'F001'),
(1885, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:01:04', '00:02:07', 14, 'F001'),
(1886, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:02:49', '00:01:45', 17, 'F001'),
(1887, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:03:39', '00:00:50', 36, 'F001'),
(1888, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:04:23', '00:00:44', 40, 'F001'),
(1889, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:04:26', '00:14:08', 4, 'F001'),
(1890, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:04:41', '00:00:15', 240, 'F001'),
(1891, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:05:54', '00:01:31', 19, 'F001'),
(1892, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:06:12', '00:01:31', 39, 'F001'),
(1893, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:06:46', '00:00:52', 34, 'F001'),
(1894, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:07:15', '00:00:29', 62, 'F001'),
(1895, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:07:44', '00:00:29', 62, 'F001'),
(1896, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:07:54', '00:01:42', 35, 'F001'),
(1897, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:08:04', '00:00:10', 360, 'F001'),
(1898, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:08:21', '00:00:37', 48, 'F001'),
(1899, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:08:49', '00:00:28', 64, 'F001'),
(1900, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:08:51', '00:00:47', 76, 'F001'),
(1901, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:09:25', '00:00:34', 105, 'F001'),
(1902, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:09:35', '00:00:46', 39, 'F001'),
(1903, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:09:46', '00:00:11', 163, 'F001'),
(1904, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:10:09', '00:00:23', 78, 'F001'),
(1905, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:10:32', '00:01:07', 53, 'F001'),
(1906, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:11:05', '00:00:56', 32, 'F001'),
(1907, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:11:19', '00:00:47', 76, 'F001'),
(1908, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:11:29', '00:00:24', 75, 'F001'),
(1909, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:11:51', '00:00:32', 112, 'F001'),
(1910, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:12:16', '00:00:47', 38, 'F001'),
(1911, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:12:26', '00:00:10', 180, 'F001'),
(1912, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:12:32', '00:00:41', 87, 'F001'),
(1913, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:12:52', '00:00:26', 69, 'F001'),
(1914, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:13:04', '00:00:32', 112, 'F001'),
(1915, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:13:22', '00:00:30', 60, 'F001'),
(1916, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:13:46', '00:00:42', 85, 'F001'),
(1917, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:13:55', '00:00:33', 54, 'F001'),
(1918, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:14:19', '00:00:33', 109, 'F001'),
(1919, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:14:26', '00:00:31', 58, 'F001'),
(1920, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:15:00', '00:00:34', 52, 'F001'),
(1921, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:15:24', '00:00:24', 75, 'F001'),
(1922, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:15:43', '00:01:24', 42, 'F001'),
(1923, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:15:48', '00:00:24', 75, 'F001'),
(1924, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:16:15', '00:00:32', 112, 'F001'),
(1925, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:16:41', '00:00:53', 33, 'F001'),
(1926, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:16:59', '00:00:44', 81, 'F001'),
(1927, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:18:11', '00:01:12', 50, 'F001'),
(1928, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:18:31', '00:01:50', 16, 'F001'),
(1929, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:18:51', '00:00:40', 90, 'F001'),
(1930, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:19:32', '00:01:01', 29, 'F001'),
(1931, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:20:02', '00:00:30', 60, 'F001'),
(1932, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:20:16', '00:00:14', 128, 'F001'),
(1933, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:20:27', '00:01:36', 37, 'F001'),
(1934, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:20:37', '00:00:10', 360, 'F001'),
(1935, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:20:47', '00:00:31', 58, 'F001'),
(1936, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:21:14', '00:00:37', 97, 'F001'),
(1937, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:21:17', '00:00:30', 60, 'F001'),
(1938, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:21:37', '00:00:23', 156, 'F001'),
(1939, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:22:09', '00:00:52', 34, 'F001'),
(1940, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:22:43', '00:00:34', 52, 'F001'),
(1941, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:22:54', '00:01:17', 46, 'F001'),
(1942, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:23:17', '00:00:34', 52, 'F001'),
(1943, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:23:30', '00:00:36', 100, 'F001'),
(1944, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:23:37', '00:00:20', 90, 'F001'),
(1945, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:24:09', '00:00:39', 92, 'F001'),
(1946, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:24:11', '00:00:34', 52, 'F001'),
(1947, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:24:52', '00:00:43', 83, 'F001'),
(1948, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:25:32', '00:00:40', 90, 'F001'),
(1949, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:26:16', '00:00:44', 81, 'F001'),
(1950, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:26:55', '00:00:39', 92, 'F001'),
(1951, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:27:30', '00:00:35', 102, 'F001'),
(1952, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:27:52', '00:03:41', 8, 'F001'),
(1953, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:28:08', '00:00:38', 94, 'F001'),
(1954, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:28:20', '00:00:28', 64, 'F001'),
(1955, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:29:07', '00:00:59', 61, 'F001'),
(1956, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:29:30', '00:01:10', 25, 'F001'),
(1957, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:29:38', '00:00:31', 116, 'F001'),
(1958, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:30:00', '00:00:30', 60, 'F001'),
(1959, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:30:16', '00:00:38', 94, 'F001'),
(1960, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:30:27', '00:00:27', 66, 'F001'),
(1961, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:31:04', '00:00:48', 75, 'F001'),
(1962, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:31:08', '00:00:41', 43, 'F001'),
(1963, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:31:37', '00:00:29', 62, 'F001'),
(1964, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:31:43', '00:00:39', 92, 'F001'),
(1965, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:32:06', '00:00:29', 62, 'F001'),
(1966, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:32:19', '00:00:36', 100, 'F001'),
(1967, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:32:32', '00:00:26', 69, 'F001'),
(1968, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:32:55', '00:00:36', 100, 'F001'),
(1969, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:33:17', '00:00:45', 40, 'F001'),
(1970, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:33:44', '00:00:49', 73, 'F001'),
(1971, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:33:47', '00:00:30', 60, 'F001'),
(1972, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:34:16', '00:00:29', 62, 'F001'),
(1973, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:34:23', '00:00:39', 92, 'F001'),
(1974, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:34:42', '00:00:26', 69, 'F001'),
(1975, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:35:12', '00:00:30', 60, 'F001'),
(1976, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:35:13', '00:00:50', 72, 'F001'),
(1977, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:35:46', '00:00:34', 52, 'F001'),
(1978, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:36:15', '00:00:29', 62, 'F001'),
(1979, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:36:21', '00:01:08', 52, 'F001'),
(1980, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:36:42', '00:00:27', 66, 'F001'),
(1981, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:37:00', '00:00:39', 92, 'F001'),
(1982, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:37:08', '00:00:26', 69, 'F001'),
(1983, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:37:31', '00:00:31', 116, 'F001'),
(1984, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:38:13', '00:00:42', 85, 'F001'),
(1985, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:38:48', '00:01:40', 18, 'F001'),
(1986, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:38:50', '00:00:37', 97, 'F001'),
(1987, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:39:22', '00:00:34', 52, 'F001'),
(1988, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:39:29', '00:00:39', 92, 'F001'),
(1989, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:39:41', '00:00:19', 94, 'F001'),
(1990, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:40:05', '00:00:36', 100, 'F001'),
(1991, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:40:07', '00:00:26', 69, 'F001'),
(1992, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:40:35', '00:00:28', 64, 'F001'),
(1993, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:40:57', '00:00:22', 81, 'F001'),
(1994, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:41:10', '00:01:05', 55, 'F001'),
(1995, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:41:23', '00:00:26', 69, 'F001'),
(1996, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:41:51', '00:00:28', 64, 'F001'),
(1997, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:42:11', '00:00:20', 90, 'F001'),
(1998, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:42:15', '00:01:05', 55, 'F001'),
(1999, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:42:44', '00:00:33', 54, 'F001'),
(2000, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:43:05', '00:00:50', 72, 'F001'),
(2001, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:43:13', '00:00:29', 62, 'F001'),
(2002, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:43:37', '00:00:24', 75, 'F001'),
(2003, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:43:40', '00:00:35', 102, 'F001'),
(2004, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:44:01', '00:00:24', 75, 'F001'),
(2005, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:44:11', '00:00:31', 116, 'F001'),
(2006, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:44:30', '00:00:29', 62, 'F001'),
(2007, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:44:52', '00:00:41', 87, 'F001'),
(2008, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:45:03', '00:00:33', 54, 'F001'),
(2009, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:45:25', '00:00:33', 109, 'F001'),
(2010, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:45:35', '00:00:32', 56, 'F001'),
(2011, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:46:07', '00:00:42', 85, 'F001'),
(2012, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:46:34', '00:00:59', 30, 'F001'),
(2013, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:46:53', '00:00:19', 94, 'F001'),
(2014, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:47:23', '00:00:30', 60, 'F001'),
(2015, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:47:25', '00:01:18', 46, 'F001'),
(2016, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:48:01', '00:00:36', 100, 'F001'),
(2017, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:48:15', '00:00:52', 34, 'F001'),
(2018, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:48:34', '00:00:33', 109, 'F001'),
(2019, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:48:36', '00:00:21', 85, 'F001'),
(2020, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:49:07', '00:00:33', 109, 'F001'),
(2021, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:49:12', '00:00:36', 50, 'F001'),
(2022, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:49:38', '00:00:26', 69, 'F001'),
(2023, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:49:42', '00:00:35', 102, 'F001'),
(2024, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:50:03', '00:00:25', 72, 'F001'),
(2025, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:50:30', '00:00:27', 66, 'F001'),
(2026, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:50:57', '00:00:27', 66, 'F001'),
(2027, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:51:24', '00:00:27', 66, 'F001'),
(2028, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:51:39', '00:01:57', 30, 'F001'),
(2029, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:51:50', '00:00:26', 69, 'F001'),
(2030, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:51:58', '00:00:19', 189, 'F001'),
(2031, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:52:14', '00:00:24', 75, 'F001'),
(2032, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:52:34', '00:00:36', 100, 'F001'),
(2033, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:52:44', '00:00:10', 360, 'F001'),
(2034, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:53:02', '00:00:18', 200, 'F001'),
(2035, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:53:31', '00:00:29', 124, 'F001'),
(2036, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:54:07', '00:01:53', 15, 'F001'),
(2037, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:54:16', '00:00:45', 80, 'F001'),
(2038, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 12:54:30', '00:00:23', 78, 'F001'),
(2039, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:04:57', '00:10:27', 2, 'F001'),
(2040, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:05:27', '00:00:30', 60, 'F001'),
(2041, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:05:56', '00:00:29', 62, 'F001'),
(2042, '73-4-01.003-62-15-0-Univerzalna', NULL, 15, 4, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(2043, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:20', '00:00:00', 132, 'F001'),
(2044, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:22', '00:00:26', 69, 'F001'),
(2045, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:30', '00:12:14', 4, 'F001'),
(2046, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:35', '00:00:15', 70, 'F001'),
(2047, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:43', '00:00:08', 132, 'F001'),
(2048, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:49', '00:00:27', 66, 'F001'),
(2049, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:49', '00:00:19', 189, 'F001'),
(2050, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:06:52', '00:00:09', 117, 'F001'),
(2051, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:13', '00:00:21', 50, 'F001'),
(2052, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:21', '00:00:08', 132, 'F001'),
(2053, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:26', '00:00:37', 48, 'F001'),
(2054, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:32', '00:00:11', 96, 'F001'),
(2055, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:40', '00:00:51', 70, 'F001'),
(2056, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:41', '00:00:09', 117, 'F001'),
(2057, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:51', '00:00:10', 105, 'F001'),
(2058, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:55', '00:00:29', 62, 'F001'),
(2059, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:07:59', '00:00:08', 132, 'F001'),
(2060, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:10', '00:00:11', 96, 'F001'),
(2061, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:20', '00:00:25', 72, 'F001'),
(2062, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:21', '00:00:11', 96, 'F001'),
(2063, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:28', '00:00:07', 151, 'F001'),
(2064, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:35', '00:00:07', 151, 'F001'),
(2065, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:43', '00:00:08', 132, 'F001'),
(2066, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:44', '00:00:24', 75, 'F001'),
(2067, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:51', '00:01:11', 50, 'F001'),
(2068, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:08:54', '00:00:11', 96, 'F001'),
(2069, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:01', '00:00:07', 151, 'F001'),
(2070, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:14', '00:00:23', 156, 'F001'),
(2071, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:18', '00:00:17', 62, 'F001'),
(2072, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:27', '00:00:09', 117, 'F001'),
(2073, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:51', '00:00:37', 97, 'F001'),
(2074, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:09:57', '00:00:30', 35, 'F001'),
(2075, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:09', '00:00:12', 88, 'F001'),
(2076, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:17', '00:00:08', 132, 'F001'),
(2077, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:28', '00:00:11', 96, 'F001'),
(2078, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:37', '00:00:09', 117, 'F001'),
(2079, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:39', '00:00:48', 75, 'F001'),
(2080, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:42', '00:01:58', 15, 'F001'),
(2081, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:44', '00:00:07', 151, 'F001'),
(2082, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:10:54', '00:00:10', 105, 'F001'),
(2083, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:04', '00:00:10', 105, 'F001'),
(2084, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:14', '00:00:10', 105, 'F001'),
(2085, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:17', '00:00:35', 51, 'F001'),
(2086, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:23', '00:00:09', 117, 'F001'),
(2087, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:32', '00:00:09', 117, 'F001'),
(2088, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:42', '00:00:10', 105, 'F001'),
(2089, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:47', '00:00:30', 60, 'F001'),
(2090, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:51', '00:01:12', 50, 'F001'),
(2091, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:11:54', '00:00:12', 88, 'F001'),
(2092, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:05', '00:00:11', 96, 'F001'),
(2093, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:14', '00:00:09', 117, 'F001'),
(2094, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:17', '00:00:30', 60, 'F001'),
(2095, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:22', '00:00:08', 132, 'F001'),
(2096, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:30', '00:00:13', 138, 'F001'),
(2097, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:33', '00:00:11', 96, 'F001'),
(2098, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:40', '00:00:07', 151, 'F001'),
(2099, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:44', '00:00:53', 67, 'F001'),
(2100, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:50', '00:00:10', 105, 'F001'),
(2101, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:12:54', '00:00:24', 75, 'F001'),
(2102, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:01', '00:00:11', 96, 'F001'),
(2103, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:20', '00:00:26', 69, 'F001'),
(2104, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:21', '00:00:20', 52, 'F001'),
(2105, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:32', '00:00:11', 96, 'F001'),
(2106, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:38', '00:00:54', 66, 'F001'),
(2107, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:41', '00:00:09', 117, 'F001'),
(2108, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:44', '00:00:24', 75, 'F001'),
(2109, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:51', '00:00:10', 105, 'F001'),
(2110, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:13:59', '00:00:08', 132, 'F001'),
(2111, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:05', '00:00:06', 176, 'F001'),
(2112, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:07', '00:00:23', 78, 'F001'),
(2113, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:11', '00:00:33', 109, 'F001'),
(2114, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:13', '00:00:08', 132, 'F001'),
(2115, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:21', '00:00:08', 132, 'F001'),
(2116, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:30', '00:00:23', 78, 'F001'),
(2117, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:35', '00:00:14', 75, 'F001'),
(2118, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:47', '00:00:12', 88, 'F001'),
(2119, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:48', '00:00:37', 97, 'F001'),
(2120, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:14:57', '00:00:27', 66, 'F001'),
(2121, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:08', '00:00:21', 50, 'F001'),
(2122, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:19', '00:00:11', 96, 'F001'),
(2123, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:24', '00:00:27', 66, 'F001'),
(2124, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:27', '00:00:39', 92, 'F001'),
(2125, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:30', '00:00:11', 96, 'F001'),
(2126, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:41', '00:00:11', 96, 'F001'),
(2127, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:51', '00:00:10', 105, 'F001'),
(2128, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:53', '00:00:29', 62, 'F001'),
(2129, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:15:58', '00:00:07', 151, 'F001'),
(2130, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:07', '00:00:40', 90, 'F001'),
(2131, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:08', '00:00:10', 105, 'F001'),
(2132, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:15', '00:00:07', 151, 'F001'),
(2133, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:17', '00:00:24', 75, 'F001'),
(2134, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:23', '00:00:08', 132, 'F001'),
(2135, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:32', '00:00:09', 117, 'F001'),
(2136, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:40', '00:00:08', 132, 'F001'),
(2137, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:43', '00:00:26', 69, 'F001'),
(2138, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:44', '00:00:37', 97, 'F001'),
(2139, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:50', '00:00:10', 105, 'F001'),
(2140, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:16:58', '00:00:08', 132, 'F001'),
(2141, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:09', '00:00:26', 69, 'F001'),
(2142, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:09', '00:00:11', 96, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(2143, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:18', '00:00:09', 117, 'F001'),
(2144, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:22', '00:00:38', 94, 'F001'),
(2145, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:25', '00:00:07', 151, 'F001'),
(2146, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:35', '00:00:10', 105, 'F001'),
(2147, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:41', '00:00:32', 56, 'F001'),
(2148, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:44', '00:00:09', 117, 'F001'),
(2149, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:17:53', '00:00:09', 117, 'F001'),
(2150, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:00', '00:00:38', 94, 'F001'),
(2151, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:03', '00:00:10', 105, 'F001'),
(2152, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:13', '00:00:10', 105, 'F001'),
(2153, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:22', '00:00:41', 43, 'F001'),
(2154, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:22', '00:00:09', 117, 'F001'),
(2155, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:31', '00:00:09', 117, 'F001'),
(2156, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:38', '00:00:07', 151, 'F001'),
(2157, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:49', '00:00:11', 96, 'F001'),
(2158, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:50', '00:00:28', 64, 'F001'),
(2159, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:56', '00:00:56', 64, 'F001'),
(2160, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:18:59', '00:00:10', 105, 'F001'),
(2161, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:19:23', '00:00:24', 44, 'F001'),
(2162, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:19:26', '00:00:36', 50, 'F001'),
(2163, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:19:32', '00:00:09', 117, 'F001'),
(2164, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:19:42', '00:00:10', 105, 'F001'),
(2165, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:19:47', '00:00:51', 70, 'F001'),
(2166, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:20:01', '00:00:35', 51, 'F001'),
(2167, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:20:44', '00:01:02', 17, 'F001'),
(2168, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:21:00', '00:00:59', 30, 'F001'),
(2169, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:21:27', '00:00:27', 66, 'F001'),
(2170, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:21:43', '00:00:59', 17, 'F001'),
(2171, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:21:44', '00:00:17', 105, 'F001'),
(2172, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:21:52', '00:00:09', 117, 'F001'),
(2173, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:03', '00:00:19', 94, 'F001'),
(2174, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:12', '00:00:20', 52, 'F001'),
(2175, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:14', '00:00:11', 163, 'F001'),
(2176, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:24', '00:00:12', 88, 'F001'),
(2177, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:32', '00:00:08', 132, 'F001'),
(2178, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:35', '00:00:21', 85, 'F001'),
(2179, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:42', '00:00:10', 105, 'F001'),
(2180, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:51', '00:00:09', 117, 'F001'),
(2181, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:59', '00:00:08', 132, 'F001'),
(2182, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:22:59', '00:00:24', 75, 'F001'),
(2183, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:09', '00:00:10', 105, 'F001'),
(2184, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:20', '00:00:11', 96, 'F001'),
(2185, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:26', '00:00:27', 66, 'F001'),
(2186, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:31', '00:00:11', 96, 'F001'),
(2187, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:39', '00:00:08', 132, 'F001'),
(2188, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:46', '00:00:20', 90, 'F001'),
(2189, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:47', '00:00:08', 132, 'F001'),
(2190, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:52', '00:04:05', 14, 'F001'),
(2191, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:23:57', '00:00:10', 105, 'F001'),
(2192, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:05', '00:00:08', 132, 'F001'),
(2193, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:14', '00:00:09', 117, 'F001'),
(2194, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:17', '00:00:31', 58, 'F001'),
(2195, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:22', '00:00:08', 132, 'F001'),
(2196, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:22', '00:00:30', 120, 'F001'),
(2197, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:33', '00:00:16', 112, 'F001'),
(2198, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:44', '00:00:22', 163, 'F001'),
(2199, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:48', '00:00:26', 40, 'F001'),
(2200, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:51', '00:00:18', 100, 'F001'),
(2201, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:58', '00:00:10', 105, 'F001'),
(2202, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:24:59', '00:00:15', 240, 'F001'),
(2203, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:07', '00:00:09', 117, 'F001'),
(2204, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:14', '00:00:07', 151, 'F001'),
(2205, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:21', '00:00:07', 151, 'F001'),
(2206, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:28', '00:00:29', 124, 'F001'),
(2207, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:30', '00:00:09', 117, 'F001'),
(2208, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:33', '00:00:42', 42, 'F001'),
(2209, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:40', '00:00:10', 105, 'F001'),
(2210, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:47', '00:00:07', 151, 'F001'),
(2211, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:54', '00:00:07', 151, 'F001'),
(2212, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:25:57', '00:00:24', 75, 'F001'),
(2213, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:26:09', '00:00:12', 150, 'F001'),
(2214, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:26:10', '00:00:16', 66, 'F001'),
(2215, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:26:27', '00:00:18', 100, 'F001'),
(2216, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:26:45', '00:00:18', 100, 'F001'),
(2217, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:26:57', '00:00:47', 22, 'F001'),
(2218, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:08', '00:00:23', 78, 'F001'),
(2219, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:09', '00:00:12', 88, 'F001'),
(2220, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:17', '00:01:49', 33, 'F001'),
(2221, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:20', '00:00:11', 96, 'F001'),
(2222, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:24', '00:00:16', 112, 'F001'),
(2223, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:29', '00:00:09', 117, 'F001'),
(2224, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:32', '00:00:15', 240, 'F001'),
(2225, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:36', '00:00:07', 151, 'F001'),
(2226, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:37', '00:00:13', 138, 'F001'),
(2227, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:46', '00:00:10', 105, 'F001'),
(2228, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:49', '00:00:17', 211, 'F001'),
(2229, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:54', '00:00:17', 105, 'F001'),
(2230, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:27:58', '00:00:12', 88, 'F001'),
(2231, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:06', '00:00:08', 132, 'F001'),
(2232, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:13', '00:00:07', 151, 'F001'),
(2233, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:14', '00:00:25', 144, 'F001'),
(2234, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:15', '00:00:21', 85, 'F001'),
(2235, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:23', '00:00:10', 105, 'F001'),
(2236, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:33', '00:00:10', 105, 'F001'),
(2237, '57-5-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:38', '00:00:23', 78, 'F001'),
(2238, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:43', '00:00:10', 105, 'F001'),
(2239, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:51', '00:00:08', 132, 'F001'),
(2240, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:28:56', '00:00:42', 85, 'F001'),
(2241, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:01', '00:00:10', 105, 'F001'),
(2242, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:10', '00:00:09', 117, 'F001'),
(2243, '74-5-01.003-62-15-0-Univerzalna', NULL, 15, 5, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(2244, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:21', '00:00:11', 96, 'F001'),
(2245, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:31', '00:00:00', 117, 'F001'),
(2246, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:31', '00:00:10', 105, 'F001'),
(2247, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:40', '00:00:09', 117, 'F001'),
(2248, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:49', '00:00:09', 117, 'F001'),
(2249, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:52', '00:00:21', 50, 'F001'),
(2250, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:29:57', '00:00:08', 132, 'F001'),
(2251, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:09', '00:00:12', 88, 'F001'),
(2252, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:14', '00:00:22', 48, 'F001'),
(2253, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:25', '00:00:16', 66, 'F001'),
(2254, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:31', '00:01:35', 37, 'F001'),
(2255, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:31', '00:00:06', 176, 'F001'),
(2256, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:32', '00:00:18', 58, 'F001'),
(2257, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:40', '00:00:09', 117, 'F001'),
(2258, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:48', '00:00:08', 132, 'F001'),
(2259, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:54', '00:00:22', 48, 'F001'),
(2260, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:56', '00:00:25', 144, 'F001'),
(2261, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:30:58', '00:00:10', 105, 'F001'),
(2262, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:05', '00:00:07', 151, 'F001'),
(2263, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:08', '00:00:14', 75, 'F001'),
(2264, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:13', '00:00:08', 132, 'F001'),
(2265, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:17', '00:00:21', 171, 'F001'),
(2266, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:23', '00:00:10', 105, 'F001'),
(2267, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:26', '00:00:18', 58, 'F001'),
(2268, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:34', '00:00:11', 96, 'F001'),
(2269, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:40', '00:00:23', 156, 'F001'),
(2270, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:43', '00:00:17', 62, 'F001'),
(2271, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:31:56', '00:00:22', 48, 'F001'),
(2272, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:02', '00:00:19', 55, 'F001'),
(2273, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:06', '00:00:10', 105, 'F001'),
(2274, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:16', '00:00:36', 100, 'F001'),
(2275, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:16', '00:00:10', 105, 'F001'),
(2276, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:21', '00:00:19', 55, 'F001'),
(2277, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:23', '00:00:07', 151, 'F001'),
(2278, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:34', '00:00:11', 96, 'F001'),
(2279, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:40', '00:00:19', 55, 'F001'),
(2280, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:41', '00:00:07', 151, 'F001'),
(2281, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:32:50', '00:00:09', 117, 'F001'),
(2282, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:00', '00:00:10', 105, 'F001'),
(2283, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:02', '00:00:22', 48, 'F001'),
(2284, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:12', '00:00:12', 88, 'F001'),
(2285, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:14', '00:00:58', 62, 'F001'),
(2286, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:22', '00:00:10', 105, 'F001'),
(2287, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:32', '00:00:18', 200, 'F001'),
(2288, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:32', '00:00:10', 105, 'F001'),
(2289, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:42', '00:00:10', 105, 'F001'),
(2290, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:45', '00:00:13', 276, 'F001'),
(2291, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:51', '00:00:09', 117, 'F001'),
(2292, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:33:56', '00:00:11', 327, 'F001'),
(2293, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:02', '00:00:11', 96, 'F001'),
(2294, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:12', '00:00:10', 105, 'F001'),
(2295, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:17', '00:00:21', 171, 'F001'),
(2296, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:23', '00:00:11', 96, 'F001'),
(2297, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:25', '00:00:08', 450, 'F001'),
(2298, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:34', '00:00:11', 96, 'F001'),
(2299, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:34:36', '00:00:11', 327, 'F001'),
(2300, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:35:03', '00:00:29', 36, 'F001'),
(2301, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:35:28', '00:00:25', 42, 'F001'),
(2302, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:35:35', '00:00:07', 151, 'F001'),
(2303, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:35:45', '00:00:10', 105, 'F001'),
(2304, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:35:53', '00:00:08', 132, 'F001'),
(2305, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:01', '00:00:08', 132, 'F001'),
(2306, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:07', '00:03:05', 5, 'F001'),
(2307, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:10', '00:01:34', 38, 'F001'),
(2308, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:10', '00:00:09', 117, 'F001'),
(2309, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:15', '00:00:08', 132, 'F001'),
(2310, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:18', '00:00:08', 132, 'F001'),
(2311, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:27', '00:00:09', 117, 'F001'),
(2312, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:33', '00:00:18', 58, 'F001'),
(2313, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:37', '00:00:10', 105, 'F001'),
(2314, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:48', '00:00:11', 96, 'F001'),
(2315, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:57', '00:00:09', 117, 'F001'),
(2316, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:36:57', '00:00:24', 44, 'F001'),
(2317, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:13', '00:01:03', 57, 'F001'),
(2318, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:14', '00:00:17', 62, 'F001'),
(2319, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:29', '00:00:32', 33, 'F001'),
(2320, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:36', '00:00:23', 156, 'F001'),
(2321, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:37', '00:00:23', 46, 'F001'),
(2322, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:39', '00:00:10', 105, 'F001'),
(2323, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:48', '00:00:09', 117, 'F001'),
(2324, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:52', '00:00:15', 70, 'F001'),
(2325, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:56', '00:00:20', 180, 'F001'),
(2326, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:37:59', '00:00:11', 96, 'F001'),
(2327, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:06', '00:00:07', 151, 'F001'),
(2328, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:08', '00:00:16', 66, 'F001'),
(2329, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:13', '00:00:07', 151, 'F001'),
(2330, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:21', '00:00:08', 132, 'F001'),
(2331, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:28', '00:00:20', 52, 'F001'),
(2332, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:30', '00:00:09', 117, 'F001'),
(2333, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:35', '00:00:39', 92, 'F001'),
(2334, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:37', '00:00:09', 117, 'F001'),
(2335, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:38', '00:00:08', 132, 'F001'),
(2336, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:48', '00:00:10', 105, 'F001'),
(2337, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:49', '00:00:12', 88, 'F001'),
(2338, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:55', '00:00:20', 180, 'F001'),
(2339, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:38:58', '00:00:10', 105, 'F001'),
(2340, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:05', '00:00:16', 66, 'F001'),
(2341, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:07', '00:00:09', 117, 'F001'),
(2342, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:22', '00:00:15', 70, 'F001'),
(2343, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:27', '00:00:22', 48, 'F001'),
(2344, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:32', '00:00:10', 105, 'F001'),
(2345, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:40', '00:00:13', 81, 'F001'),
(2346, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:41', '00:00:09', 117, 'F001'),
(2347, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:50', '00:00:09', 117, 'F001'),
(2348, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:57', '00:00:17', 62, 'F001'),
(2349, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:39:58', '00:00:08', 132, 'F001'),
(2350, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:09', '00:00:11', 96, 'F001'),
(2351, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:17', '00:00:20', 52, 'F001'),
(2352, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:17', '00:00:08', 132, 'F001'),
(2353, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:29', '00:00:12', 88, 'F001'),
(2354, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:40', '00:00:11', 96, 'F001'),
(2355, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:42', '00:00:25', 42, 'F001'),
(2356, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:44', '00:01:49', 33, 'F001'),
(2357, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:56', '00:00:14', 75, 'F001'),
(2358, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:40:59', '00:00:19', 55, 'F001'),
(2359, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:04', '00:00:20', 180, 'F001'),
(2360, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:07', '00:00:08', 132, 'F001'),
(2361, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:15', '00:00:08', 132, 'F001'),
(2362, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:15', '00:00:19', 55, 'F001'),
(2363, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:22', '00:00:18', 200, 'F001'),
(2364, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:26', '00:00:11', 96, 'F001'),
(2365, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:33', '00:00:18', 58, 'F001'),
(2366, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:36', '00:00:10', 105, 'F001'),
(2367, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:51', '00:00:18', 58, 'F001'),
(2368, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:41:58', '00:00:22', 48, 'F001'),
(2369, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:07', '00:00:16', 66, 'F001'),
(2370, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:08', '00:00:10', 105, 'F001'),
(2371, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:17', '00:00:09', 117, 'F001'),
(2372, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:20', '00:00:58', 62, 'F001'),
(2373, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:24', '00:00:17', 62, 'F001'),
(2374, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:29', '00:00:12', 88, 'F001'),
(2375, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:40', '00:00:16', 66, 'F001'),
(2376, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:42:55', '00:00:15', 70, 'F001'),
(2377, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:01', '00:00:32', 33, 'F001'),
(2378, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:11', '00:00:10', 105, 'F001'),
(2379, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:15', '00:00:20', 52, 'F001'),
(2380, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:20', '00:00:09', 117, 'F001'),
(2381, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:26', '00:01:06', 54, 'F001'),
(2382, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:38', '00:00:18', 58, 'F001'),
(2383, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:39', '00:00:24', 44, 'F001'),
(2384, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:43', '00:00:05', 211, 'F001'),
(2385, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:45', '00:00:19', 189, 'F001'),
(2386, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:50', '00:00:07', 151, 'F001'),
(2387, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:43:57', '00:00:18', 58, 'F001'),
(2388, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:00', '00:00:10', 105, 'F001'),
(2389, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:10', '00:00:10', 105, 'F001'),
(2390, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:17', '00:00:07', 151, 'F001'),
(2391, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:19', '00:00:34', 105, 'F001'),
(2392, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:26', '00:00:09', 117, 'F001'),
(2393, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:34', '00:00:08', 132, 'F001'),
(2394, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:42', '00:00:08', 132, 'F001'),
(2395, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:44:53', '00:00:11', 96, 'F001'),
(2396, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:01', '00:00:08', 132, 'F001'),
(2397, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:10', '00:00:09', 117, 'F001'),
(2398, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:18', '00:00:08', 132, 'F001'),
(2399, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:26', '00:00:08', 132, 'F001'),
(2400, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:35', '00:00:09', 117, 'F001'),
(2401, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:46', '00:00:11', 96, 'F001'),
(2402, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:50', '00:01:31', 39, 'F001'),
(2403, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:45:54', '00:00:08', 132, 'F001'),
(2404, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:46:02', '00:00:08', 132, 'F001'),
(2405, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:46:12', '00:00:22', 163, 'F001'),
(2406, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:46:41', '00:00:29', 124, 'F001'),
(2407, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:46:43', '00:02:46', 6, 'F001'),
(2408, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:46:53', '00:00:10', 105, 'F001'),
(2409, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:47:03', '00:00:22', 163, 'F001'),
(2410, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:47:25', '00:00:22', 163, 'F001'),
(2411, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:47:28', '00:00:35', 30, 'F001'),
(2412, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:47:47', '00:00:19', 55, 'F001'),
(2413, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:47:51', '00:00:26', 138, 'F001'),
(2414, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:48:06', '00:00:19', 55, 'F001'),
(2415, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:48:29', '00:00:23', 46, 'F001'),
(2416, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:48:48', '00:00:19', 55, 'F001'),
(2417, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:49:00', '00:00:12', 88, 'F001'),
(2418, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:49:17', '00:00:17', 62, 'F001'),
(2419, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:49:27', '00:01:36', 37, 'F001'),
(2420, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:49:34', '00:00:17', 62, 'F001'),
(2421, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:49:50', '00:00:16', 66, 'F001'),
(2422, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:06', '00:00:16', 66, 'F001'),
(2423, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:07', '00:00:40', 90, 'F001'),
(2424, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:33', '00:00:26', 138, 'F001'),
(2425, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:35', '00:00:29', 36, 'F001'),
(2426, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:45', '00:00:10', 105, 'F001'),
(2427, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:50:58', '00:00:13', 81, 'F001'),
(2428, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:51:12', '00:00:39', 92, 'F001'),
(2429, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:51:23', '00:00:25', 42, 'F001'),
(2430, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:51:47', '00:00:24', 44, 'F001'),
(2431, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:51:56', '00:00:44', 81, 'F001'),
(2432, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:52:25', '00:00:38', 27, 'F001'),
(2433, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:52:47', '00:00:22', 48, 'F001'),
(2434, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:11', '00:00:24', 44, 'F001'),
(2435, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:11', '00:07:09', 2, 'F001'),
(2436, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:18', '00:01:22', 43, 'F001'),
(2437, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:21', '00:00:10', 105, 'F001'),
(2438, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:28', '00:00:10', 360, 'F001'),
(2439, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:33', '00:00:12', 88, 'F001'),
(2440, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:50', '00:00:39', 27, 'F001'),
(2441, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:53:58', '00:00:08', 132, 'F001'),
(2442, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:16', '00:00:18', 58, 'F001'),
(2443, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:32', '00:01:04', 56, 'F001'),
(2444, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:35', '00:01:02', 17, 'F001'),
(2445, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:47', '00:00:12', 88, 'F001'),
(2446, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:51', '00:00:35', 30, 'F001'),
(2447, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:54:56', '00:00:09', 117, 'F001'),
(2448, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:03', '00:00:12', 88, 'F001'),
(2449, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:11', '00:00:15', 70, 'F001'),
(2450, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:22', '00:00:11', 96, 'F001'),
(2451, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:23', '00:00:20', 52, 'F001'),
(2452, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:34', '00:00:12', 88, 'F001'),
(2453, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:45', '00:00:22', 48, 'F001'),
(2454, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:47', '00:00:13', 81, 'F001'),
(2455, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:55:58', '00:00:11', 96, 'F001'),
(2456, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:06', '00:00:21', 50, 'F001'),
(2457, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:28', '00:00:30', 35, 'F001'),
(2458, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:28', '00:00:22', 48, 'F001'),
(2459, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:37', '00:00:09', 117, 'F001'),
(2460, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:43', '00:00:06', 176, 'F001'),
(2461, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:52', '00:00:09', 117, 'F001'),
(2462, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:56:53', '00:00:25', 42, 'F001'),
(2463, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:01', '00:00:09', 117, 'F001'),
(2464, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:15', '00:00:14', 75, 'F001'),
(2465, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:18', '00:00:25', 42, 'F001'),
(2466, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:19', '00:00:04', 264, 'F001'),
(2467, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:25', '00:00:06', 176, 'F001'),
(2468, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:28', '00:02:56', 20, 'F001'),
(2469, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:42', '00:00:17', 62, 'F001'),
(2470, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:45', '00:00:17', 211, 'F001'),
(2471, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:57:51', '00:00:09', 117, 'F001'),
(2472, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:03', '00:00:18', 200, 'F001'),
(2473, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:05', '00:00:14', 75, 'F001'),
(2474, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:16', '00:00:11', 96, 'F001'),
(2475, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:28', '00:00:12', 88, 'F001'),
(2476, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:38', '00:00:10', 105, 'F001'),
(2477, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:47', '00:00:09', 117, 'F001'),
(2478, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:51', '00:00:48', 75, 'F001'),
(2479, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:58:59', '00:00:12', 88, 'F001'),
(2480, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:09', '00:00:10', 105, 'F001'),
(2481, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:19', '00:00:10', 105, 'F001'),
(2482, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:28', '00:00:09', 117, 'F001'),
(2483, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:39', '00:00:11', 96, 'F001'),
(2484, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:46', '00:00:55', 65, 'F001'),
(2485, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:52', '00:02:34', 6, 'F001'),
(2486, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 13:59:57', '00:00:18', 58, 'F001'),
(2487, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:01', '00:00:15', 240, 'F001'),
(2488, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:07', '00:00:10', 105, 'F001'),
(2489, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:10', '00:00:18', 58, 'F001'),
(2490, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:31', '00:00:21', 50, 'F001'),
(2491, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:41', '00:00:34', 31, 'F001'),
(2492, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:53', '00:00:22', 48, 'F001'),
(2493, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:00:54', '00:00:13', 81, 'F001'),
(2494, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:05', '00:00:11', 96, 'F001');
INSERT INTO `working_day_session_details` (`id`, `working_day_session_id`, `working_day`, `komesa_id`, `radnik_id`, `linija_id`, `artikal_id`, `operacija_id`, `color`, `size`, `vreme_operacije`, `razlika_u_vremenu`, `procenat_ucinka`, `hint_code`) VALUES
(2495, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:13', '00:00:20', 52, 'F001'),
(2496, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:16', '00:00:11', 96, 'F001'),
(2497, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:26', '00:00:10', 105, 'F001'),
(2498, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:35', '00:00:09', 117, 'F001'),
(2499, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:36', '00:00:23', 46, 'F001'),
(2500, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:45', '00:00:10', 105, 'F001'),
(2501, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:01:54', '00:00:09', 117, 'F001'),
(2502, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:03', '00:00:09', 117, 'F001'),
(2503, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:12', '00:00:09', 117, 'F001'),
(2504, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:21', '00:00:45', 23, 'F001'),
(2505, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:21', '00:00:09', 117, 'F001'),
(2506, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:30', '00:00:09', 117, 'F001'),
(2507, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:39', '00:02:38', 22, 'F001'),
(2508, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:39', '00:00:09', 117, 'F001'),
(2509, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:40', '00:00:19', 55, 'F001'),
(2510, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:48', '00:00:09', 117, 'F001'),
(2511, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:57', '00:00:09', 117, 'F001'),
(2512, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:02:58', '00:00:18', 58, 'F001'),
(2513, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:01', '00:00:22', 163, 'F001'),
(2514, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:06', '00:00:09', 117, 'F001'),
(2515, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:14', '00:00:16', 66, 'F001'),
(2516, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:17', '00:00:11', 96, 'F001'),
(2517, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:27', '00:00:10', 105, 'F001'),
(2518, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:28', '00:00:27', 133, 'F001'),
(2519, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:31', '00:00:17', 62, 'F001'),
(2520, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:34', '00:00:07', 151, 'F001'),
(2521, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:44', '00:00:10', 105, 'F001'),
(2522, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:50', '00:00:19', 55, 'F001'),
(2523, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:52', '00:00:08', 132, 'F001'),
(2524, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:03:53', '00:00:25', 144, 'F001'),
(2525, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:03', '00:00:11', 96, 'F001'),
(2526, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:07', '00:00:17', 62, 'F001'),
(2527, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:12', '00:00:19', 189, 'F001'),
(2528, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:15', '00:00:12', 88, 'F001'),
(2529, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:24', '00:00:17', 62, 'F001'),
(2530, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:24', '00:00:09', 117, 'F001'),
(2531, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:32', '00:00:08', 132, 'F001'),
(2532, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:36', '00:00:24', 150, 'F001'),
(2533, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:04:51', '00:00:19', 55, 'F001'),
(2534, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:04', '00:00:40', 26, 'F001'),
(2535, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:13', '00:00:37', 97, 'F001'),
(2536, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:16', '00:00:12', 88, 'F001'),
(2537, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:34', '00:00:18', 58, 'F001'),
(2538, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:35', '00:00:44', 24, 'F001'),
(2539, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:45', '00:00:10', 105, 'F001'),
(2540, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:52', '00:00:18', 58, 'F001'),
(2541, '73-4-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 4, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:05:57', '00:00:12', 88, 'F001'),
(2542, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:06:01', '00:00:48', 75, 'F001'),
(2543, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:06:13', '00:00:21', 50, 'F001'),
(2544, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:06:34', '00:00:21', 50, 'F001'),
(2545, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:06:46', '00:00:12', 88, 'F001'),
(2546, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:06', '00:00:20', 52, 'F001'),
(2547, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:20', '00:00:14', 75, 'F001'),
(2548, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:25', '00:01:24', 42, 'F001'),
(2549, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:37', '00:00:17', 62, 'F001'),
(2550, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:45', '00:00:20', 180, 'F001'),
(2551, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:07:57', '00:00:20', 52, 'F001'),
(2552, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:08:07', '00:00:22', 163, 'F001'),
(2553, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:08:15', '00:00:18', 58, 'F001'),
(2554, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:08:30', '00:00:23', 156, 'F001'),
(2555, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:08:30', '00:00:15', 70, 'F001'),
(2556, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:08:47', '00:00:17', 62, 'F001'),
(2557, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:09:14', '00:00:27', 39, 'F001'),
(2558, '74-5-01.003-62-15-0-Univerzalna', '2016-12-06', 15, 5, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:09:41', '00:00:27', 39, 'F001'),
(2559, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:10:09', '00:01:39', 36, 'F001'),
(2560, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:10:29', '00:00:20', 180, 'F001'),
(2561, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:11:07', '00:00:38', 94, 'F001'),
(2562, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:11:19', '00:00:12', 300, 'F001'),
(2563, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:11:52', '00:00:33', 109, 'F001'),
(2564, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:13:37', '00:01:45', 34, 'F001'),
(2565, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:14:28', '00:00:51', 70, 'F001'),
(2566, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:14:41', '00:00:13', 276, 'F001'),
(2567, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:14:53', '00:00:12', 300, 'F001'),
(2568, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:15:12', '00:00:19', 189, 'F001'),
(2569, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:15:59', '00:00:47', 76, 'F001'),
(2570, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:16:17', '00:00:18', 200, 'F001'),
(2571, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:16:34', '00:00:17', 211, 'F001'),
(2572, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:17:20', '00:00:46', 78, 'F001'),
(2573, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:17:42', '00:00:22', 163, 'F001'),
(2574, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:19:32', '00:01:50', 32, 'F001'),
(2575, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:19:51', '00:00:19', 189, 'F001'),
(2576, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:20:13', '00:00:22', 163, 'F001'),
(2577, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:20:30', '00:00:17', 211, 'F001'),
(2578, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:20:47', '00:00:17', 211, 'F001'),
(2579, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:21:08', '00:00:21', 171, 'F001'),
(2580, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:22:24', '00:01:16', 47, 'F001'),
(2581, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:22:54', '00:00:30', 120, 'F001'),
(2582, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:23:29', '00:00:35', 102, 'F001'),
(2583, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:23:46', '00:00:17', 211, 'F001'),
(2584, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:25:14', '00:01:28', 40, 'F001'),
(2585, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:25:40', '00:00:26', 138, 'F001'),
(2586, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:26:01', '00:00:21', 171, 'F001'),
(2587, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:26:44', '00:00:43', 83, 'F001'),
(2588, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:27:48', '00:01:04', 56, 'F001'),
(2589, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:29:46', '00:01:58', 30, 'F001'),
(2590, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:30:08', '00:00:22', 163, 'F001'),
(2591, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:30:34', '00:00:26', 138, 'F001'),
(2592, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:30:45', '00:00:11', 327, 'F001'),
(2593, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:31:21', '00:00:36', 100, 'F001'),
(2594, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:32:19', '00:00:58', 62, 'F001'),
(2595, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:32:37', '00:00:18', 200, 'F001'),
(2596, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:32:52', '00:00:15', 240, 'F001'),
(2597, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:33:12', '00:00:20', 180, 'F001'),
(2598, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:33:51', '00:00:39', 92, 'F001'),
(2599, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:35:31', '00:01:40', 36, 'F001'),
(2600, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:36:07', '00:00:36', 100, 'F001'),
(2601, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:37:12', '00:01:05', 55, 'F001'),
(2602, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:38:58', '00:01:46', 33, 'F001'),
(2603, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:39:11', '00:00:13', 276, 'F001'),
(2604, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:39:24', '00:00:13', 276, 'F001'),
(2605, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:39:32', '00:00:08', 450, 'F001'),
(2606, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:40:18', '00:00:46', 78, 'F001'),
(2607, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:40:38', '00:00:20', 180, 'F001'),
(2608, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:40:56', '00:00:18', 200, 'F001'),
(2609, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:41:13', '00:00:17', 211, 'F001'),
(2610, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:41:28', '00:00:15', 240, 'F001'),
(2611, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:41:46', '00:00:18', 200, 'F001'),
(2612, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:42:30', '00:00:44', 81, 'F001'),
(2613, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:42:53', '00:00:23', 156, 'F001'),
(2614, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:43:13', '00:00:20', 180, 'F001'),
(2615, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:43:36', '00:00:23', 156, 'F001'),
(2616, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:43:58', '00:00:22', 163, 'F001'),
(2617, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:44:16', '00:00:18', 200, 'F001'),
(2618, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:44:35', '00:00:19', 189, 'F001'),
(2619, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:44:44', '00:00:09', 400, 'F001'),
(2620, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:45:04', '00:00:20', 180, 'F001'),
(2621, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:45:20', '00:00:16', 225, 'F001'),
(2622, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:45:38', '00:00:18', 200, 'F001'),
(2623, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:45:55', '00:00:17', 211, 'F001'),
(2624, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:46:16', '00:00:21', 171, 'F001'),
(2625, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:46:37', '00:00:21', 171, 'F001'),
(2626, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:47:23', '00:00:46', 78, 'F001'),
(2627, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:47:40', '00:00:17', 211, 'F001'),
(2628, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:48:27', '00:00:47', 76, 'F001'),
(2629, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:48:56', '00:00:29', 124, 'F001'),
(2630, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:49:07', '00:00:11', 327, 'F001'),
(2631, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:49:38', '00:00:31', 116, 'F001'),
(2632, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:50:00', '00:00:22', 163, 'F001'),
(2633, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:50:34', '00:00:34', 105, 'F001'),
(2634, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:50:47', '00:00:13', 276, 'F001'),
(2635, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:51:02', '00:00:15', 240, 'F001'),
(2636, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:51:13', '00:00:11', 327, 'F001'),
(2637, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:51:28', '00:00:15', 240, 'F001'),
(2638, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:52:11', '00:00:43', 83, 'F001'),
(2639, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:53:18', '00:01:07', 53, 'F001'),
(2640, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:53:37', '00:00:19', 189, 'F001'),
(2641, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:54:04', '00:00:27', 133, 'F001'),
(2642, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:54:18', '00:00:14', 257, 'F001'),
(2643, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:54:56', '00:00:38', 94, 'F001'),
(2644, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:55:10', '00:00:14', 257, 'F001'),
(2645, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:55:46', '00:00:36', 100, 'F001'),
(2646, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:57:11', '00:01:25', 42, 'F001'),
(2647, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:57:22', '00:00:11', 327, 'F001'),
(2648, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:57:44', '00:00:22', 163, 'F001'),
(2649, '70-102-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 14:59:34', '00:01:50', 32, 'F001'),
(2650, '77-3-01.007-62-15-0-Univerzalna', NULL, 15, 3, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(2651, '77-3-01.007-62-15-0-Univerzalna', '2016-12-06', 15, 3, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:12:02', '00:00:00', 100, 'F001'),
(2652, '78-102-01.012-62-15-0-Univerzalna', NULL, 15, 102, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(2653, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:15:32', '00:00:00', 142, 'F001'),
(2654, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:15:42', '00:00:10', 514, 'F001'),
(2655, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:16:34', '00:00:52', 98, 'F001'),
(2656, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:17:22', '00:00:48', 107, 'F001'),
(2657, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:17:48', '00:00:26', 197, 'F001'),
(2658, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:17:51', '00:00:03', 1714, 'F001'),
(2659, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:18:37', '00:00:46', 111, 'F001'),
(2660, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:18:56', '00:00:19', 270, 'F001'),
(2661, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:19:47', '00:00:51', 100, 'F001'),
(2662, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:20:43', '00:00:56', 91, 'F001'),
(2663, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:21:10', '00:00:27', 190, 'F001'),
(2664, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:22:02', '00:00:52', 98, 'F001'),
(2665, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:22:48', '00:00:46', 111, 'F001'),
(2666, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:23:22', '00:00:34', 151, 'F001'),
(2667, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:24:14', '00:00:52', 98, 'F001'),
(2668, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:25:59', '00:01:45', 48, 'F001'),
(2669, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:26:46', '00:00:47', 109, 'F001'),
(2670, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:27:36', '00:00:50', 102, 'F001'),
(2671, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:28:07', '00:00:31', 165, 'F001'),
(2672, '78-102-01.012-62-15-0-Univerzalna', '2016-12-06', 15, 102, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 15:38:17', '00:10:10', 8, 'F001'),
(2673, '79-101-01.004-62-15-0-Univerzalna', NULL, 15, 101, NULL, 62, 1, '0', 'Univerzalna', NULL, NULL, NULL, NULL),
(2674, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:00:45', '00:00:00', 48, 'F001'),
(2675, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:01:00', '00:00:15', 120, 'F001'),
(2676, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:01:17', '00:00:17', 105, 'F001'),
(2677, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:01:23', '00:00:06', 300, 'F001'),
(2678, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:01:29', '00:00:06', 300, 'F001'),
(2679, '79-101-01.004-62-15-0-Univerzalna', '2016-12-06', 15, 101, 2, 62, 15, '0', 'Univerzalna', '2016-12-06 19:01:34', '00:00:05', 360, 'F001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikal`
--
ALTER TABLE `artikal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1236Q1446_62`
--
ALTER TABLE `artikal_data_1236Q1446_62`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1236Q1446_62_color`
--
ALTER TABLE `artikal_data_1236Q1446_62_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1236Q1446_62_size`
--
ALTER TABLE `artikal_data_1236Q1446_62_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61`
--
ALTER TABLE `artikal_data_112233erfr_61`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61_color`
--
ALTER TABLE `artikal_data_112233erfr_61_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_112233erfr_61_size`
--
ALTER TABLE `artikal_data_112233erfr_61_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59`
--
ALTER TABLE `artikal_data_1122888_59`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59_color`
--
ALTER TABLE `artikal_data_1122888_59_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_1122888_59_size`
--
ALTER TABLE `artikal_data_1122888_59_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60`
--
ALTER TABLE `artikal_data_dfgh_60`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60_color`
--
ALTER TABLE `artikal_data_dfgh_60_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikal_data_dfgh_60_size`
--
ALTER TABLE `artikal_data_dfgh_60_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komesa`
--
ALTER TABLE `komesa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komesa_line_settings`
--
ALTER TABLE `komesa_line_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linija`
--
ALTER TABLE `linija`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_3`
--
ALTER TABLE `masina_data_3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_4`
--
ALTER TABLE `masina_data_4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_5`
--
ALTER TABLE `masina_data_5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_6`
--
ALTER TABLE `masina_data_6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_7`
--
ALTER TABLE `masina_data_7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_8`
--
ALTER TABLE `masina_data_8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_9`
--
ALTER TABLE `masina_data_9`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masina_data_10`
--
ALTER TABLE `masina_data_10`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masine`
--
ALTER TABLE `masine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operacije`
--
ALTER TABLE `operacije`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radnici_lista`
--
ALTER TABLE `radnici_lista`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_31`
--
ALTER TABLE `tablet_data_31`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_32`
--
ALTER TABLE `tablet_data_32`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_33`
--
ALTER TABLE `tablet_data_33`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_34`
--
ALTER TABLE `tablet_data_34`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_35`
--
ALTER TABLE `tablet_data_35`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_36`
--
ALTER TABLE `tablet_data_36`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_37`
--
ALTER TABLE `tablet_data_37`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_38`
--
ALTER TABLE `tablet_data_38`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_39`
--
ALTER TABLE `tablet_data_39`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_40`
--
ALTER TABLE `tablet_data_40`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_41`
--
ALTER TABLE `tablet_data_41`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_42`
--
ALTER TABLE `tablet_data_42`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_43`
--
ALTER TABLE `tablet_data_43`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_44`
--
ALTER TABLE `tablet_data_44`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_data_45`
--
ALTER TABLE `tablet_data_45`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_ip_settings`
--
ALTER TABLE `tablet_ip_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuscan_family_options`
--
ALTER TABLE `tuscan_family_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `tutorials_tbl`
--
ALTER TABLE `tutorials_tbl`
  ADD PRIMARY KEY (`tutorial_id`);

--
-- Indexes for table `working_day_session`
--
ALTER TABLE `working_day_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `working_day_session_details`
--
ALTER TABLE `working_day_session_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikal`
--
ALTER TABLE `artikal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `artikal_data_1236Q1446_62`
--
ALTER TABLE `artikal_data_1236Q1446_62`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `artikal_data_1236Q1446_62_color`
--
ALTER TABLE `artikal_data_1236Q1446_62_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_1236Q1446_62_size`
--
ALTER TABLE `artikal_data_1236Q1446_62_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61`
--
ALTER TABLE `artikal_data_112233erfr_61`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61_color`
--
ALTER TABLE `artikal_data_112233erfr_61_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_112233erfr_61_size`
--
ALTER TABLE `artikal_data_112233erfr_61_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59`
--
ALTER TABLE `artikal_data_1122888_59`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59_color`
--
ALTER TABLE `artikal_data_1122888_59_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_1122888_59_size`
--
ALTER TABLE `artikal_data_1122888_59_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60`
--
ALTER TABLE `artikal_data_dfgh_60`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60_color`
--
ALTER TABLE `artikal_data_dfgh_60_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artikal_data_dfgh_60_size`
--
ALTER TABLE `artikal_data_dfgh_60_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `komesa`
--
ALTER TABLE `komesa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `komesa_line_settings`
--
ALTER TABLE `komesa_line_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `linija`
--
ALTER TABLE `linija`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `masina_data_3`
--
ALTER TABLE `masina_data_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `masina_data_4`
--
ALTER TABLE `masina_data_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `masina_data_5`
--
ALTER TABLE `masina_data_5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `masina_data_6`
--
ALTER TABLE `masina_data_6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `masina_data_7`
--
ALTER TABLE `masina_data_7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `masina_data_8`
--
ALTER TABLE `masina_data_8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masina_data_9`
--
ALTER TABLE `masina_data_9`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `masina_data_10`
--
ALTER TABLE `masina_data_10`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masine`
--
ALTER TABLE `masine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `operacije`
--
ALTER TABLE `operacije`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `radnici_lista`
--
ALTER TABLE `radnici_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tablet_data_31`
--
ALTER TABLE `tablet_data_31`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_32`
--
ALTER TABLE `tablet_data_32`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_33`
--
ALTER TABLE `tablet_data_33`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_34`
--
ALTER TABLE `tablet_data_34`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_35`
--
ALTER TABLE `tablet_data_35`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_36`
--
ALTER TABLE `tablet_data_36`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_37`
--
ALTER TABLE `tablet_data_37`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_38`
--
ALTER TABLE `tablet_data_38`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_39`
--
ALTER TABLE `tablet_data_39`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_40`
--
ALTER TABLE `tablet_data_40`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_41`
--
ALTER TABLE `tablet_data_41`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_42`
--
ALTER TABLE `tablet_data_42`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_43`
--
ALTER TABLE `tablet_data_43`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_44`
--
ALTER TABLE `tablet_data_44`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_data_45`
--
ALTER TABLE `tablet_data_45`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tablet_ip_settings`
--
ALTER TABLE `tablet_ip_settings`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tuscan_family_options`
--
ALTER TABLE `tuscan_family_options`
  MODIFY `option_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tutorials_tbl`
--
ALTER TABLE `tutorials_tbl`
  MODIFY `tutorial_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `working_day_session`
--
ALTER TABLE `working_day_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `working_day_session_details`
--
ALTER TABLE `working_day_session_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2680;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
