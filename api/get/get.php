<?php

// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);
// 
// start session
session_start();

require_once '../../system/config/config.php';

// requires files
require_once '../../system/libs/Database.php';
require_once '../../system/libs/Url.php';
require_once '../../system/libs/Helper.php';
require_once '../../system/libs/Ip.php';

if (!Database::connect()) {
  echo json_encode(array('1','nemad'));
  die();
}

$table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

if ( isset($_GET) ) { // check if requrest is GET!!!!

  if ( isset($_GET['time']) && $_GET['time']=='last-time' ) {


      $wherewhat = array( 'working_day_session_id'=>$_SESSION['working-session']['working_day_session_id']);
      $time = Database::whereQuery($table_name, $wherewhat, 'ORDER BY id DESC', '1');
      
      echo json_encode(
        array(
          'full_time' => $time[0]['vreme_operacije'],
          'diff_time' => strtotime(Helper::DateTimeDiff( $time[0]['vreme_operacije'], date('H:i:s'))),
          'time' => strtotime($time[0]['vreme_operacije']),
          'time_seconds' => '',
          'vreme'=> strtotime($time[0]['vreme_operacije']),
          'x' => ''
        )
      );

  } elseif ( isset($_GET['options']) ) {

    switch ($_GET['options']) {
      case 'all-workers-id':
        $data = Database::fetchData('radnici_lista');
        echo json_encode($data);
        break;
      
      case 'check-status':
        $radnik_id = $_GET['radnik_id'];
        $month = $_GET['month'];
        $year = $_GET['year'];
        if ( Database::num_rows(Database::returnWhereQuery('work_stat', array('radnik_id'=>$radnik_id, 'month'=>$month, 'year'=>$year))) > 0 ) {
          $array = array(
            'status'=>true
          );
          echo json_encode($array);
        } else {
          $array = array(
            'status'=>false
          );
          echo json_encode($array);
        }
        break;
      
      default:
        # code...
        break;
    }
  } elseif ( isset($_POST['options']) && $_POST['options']=='check_wifi_status' ) {

    $array = array(
      'status'=>true
    );
    echo json_encode($array);
  }

}
