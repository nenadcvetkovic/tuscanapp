<?php


ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

// start session
session_start();
// requires files

require_once realpath(__DIR__ . '/../..').'/system/config/config.php';

require_once DIR.'/system/function.php';

$class = array('Database','Url','Helper','Ip','Table');
__autoload($class);

  // ========== INSERT PART =========== //
  // 
  // 
  // 

if ( isset($_GET['action']) && $_GET['action'] == 'insert-data' && $_POST['options'] == 'operacija-hints' ) {
  // insert data
    if ( isset($_SESSION['working-session']['radnik']) && isset($_SESSION['working-session']['radnik2']) ) {

    $o = array();

    $hint_code = $_POST['hint_code'];
    $hints = $_POST['operacija_hints'];
    $_SESSION['working-session']['operacija_hint'] = $hints;
    $_SESSION['working-session']['curr_time'] = date('H:i:s');

    $working_operation_data         = array('operacija_hint' => $hints );
    $where_update_data              = array('id' => $_POST['working-session-id']);

    $working_day_session_id = $_POST['working_day_session_id'];
    $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

    $data_query = "SELECT * FROM `$table_name` WHERE working_day_session_id='".$working_day_session_id."' ORDER BY id DESC LIMIT 1 ";
    $o['hint']['data_query'] = $data_query;
    $working_session_data =  Database::fetch( $data_query );
    $working_session_data_last_inserted_time = $working_session_data[0]['vreme_operacije'];
    $vreme_sadasnje_operacije = date("Y-m-d H:i:s");
    $current_time_time = date("H:i:s", strtotime($vreme_sadasnje_operacije));
    $razlika_u_vremenu_operacija = Helper::DateTimeDiff($working_session_data_last_inserted_time, $current_time_time);
    $o['razlika_izmedju_zadnje_operacije'] = $razlika_u_vremenu_operacija;

    if ( isset($_SESSION['working-session']['firts_hint_status']) && $_SESSION['working-session']['firts_hint_status']=='hinted') {
        $razlika_u_sekundama = Helper::returnSecondsFromTimeString($razlika_u_vremenu_operacija);

        if ( $razlika_u_sekundama > 150 ) {
          $razlika_u_sekundama = 150;
        }

    } else {

        $_SESSION['working-session']['firts_hint_status'] = 'hinted';

        // first click percent
        $time_hinted_seconds = str_replace(' ', '', $_POST['time_hint']);
        $time_seconds_miliseconds = explode('.',$time_hinted_seconds);
        $time_seconds = explode(':',$time_seconds_miliseconds[0]);
        $_SESSION['working-session']['time_hint'] = $time_hinted_seconds;
        $razlika_u_sekundama = $time_seconds[0] * 60 + $time_seconds[1];
        $_SESSION['working-session']['time_hint_in_seconds'] = $razlika_u_sekundama;
        
        if ( $razlika_u_sekundama > 150 ) {
          $razlika_u_sekundama = 150;
        }
    }

    $operacija_id = $_POST['operacija'];
    $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija_id) );

    $new_artikal_table_name = 'artikal_data_'.$_POST['name_artikal'].'_'.$_POST['artikal'];
    $o['new_artikal_table_name'] = $new_artikal_table_name;
    $o['new_artikal_table_name_query'] = Database::returnWhereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    $artikal_data = Database::whereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    $o['artikal_data_where_query'] = Database::returnWhereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    // $tempo = 3600 / $operacija_data[0]['tempo_operacije'];
    $tempo = (3600 / $artikal_data[0]['tempo_operacije']) * $artikal_data[0]['komada_po_operaciji'];
    $operacija_data_vreme_po_operaciji = $tempo;
    // $operacija_data_vreme_po_operaciji = $operacija_data[0]['operacija_vreme']; // uzeta vrednost from artikal table data

    $o['operacija_data_vreme_po_operaciji '] =$operacija_data_vreme_po_operaciji;
    $o['razlika_u_sekundama '] = $razlika_u_sekundama;
    $o['hints '] = $hints;
    if ( $razlika_u_sekundama == 0 ){
      $procenat_ucinka = 100;
    } else {
      $procenat_ucinka = floor(100*($operacija_data_vreme_po_operaciji/$razlika_u_sekundama));
    }
    $o['procenat_ucinka'] = $procenat_ucinka;

    // count workink hour success
    $hinta_per_hour_this_hours = Helper::returnCountThisHourHints($working_day_session_id);

    $o['hinta_per_hour_this_hour'] = $hinta_per_hour_this_hours;

    //
    // $o['array_one'] = array($data_query,$procenat_ucinka,$operacija_data_vreme_po_operaciji,$working_session_data_last_inserted_time,$vreme_sadasnje_operacije,$current_time_time,$razlika_u_vremenu_operacija);

    $working_operation_details_data = array(
              'working_day_session_id'  => $working_day_session_id ,
              'working_day'             => date('Y-m-d'),
              'komesa_id'               => $_POST['komesa'],
              'radnik_id'               => $_POST['radnik'],
              'linija_id'               => $_POST['linija'],
              'artikal_id'              => $_POST['artikal'],
              'operacija_id'            => $operacija_data[0]['id'],
              'color'                   => 'univerzalna',
              'size'                    => 'univerzalna',
              'vreme_operacije'         => $vreme_sadasnje_operacije ,
              'razlika_u_vremenu'       => $razlika_u_vremenu_operacija ,
              'procenat_ucinka'         => $procenat_ucinka ,
              'hint_code'               => $hint_code
            );
      if ( Database::updateData( 'working_day_session', $working_operation_data, $where_update_data )  ){

        $insert_error = 0;

        if ( $artikal_data[0]['komada_po_operaciji'] > 1 ) {
          for ($i=1; $i <= $artikal_data[0]['komada_po_operaciji']; $i++) { 
            if ( Database::insert_data( $table_name, $working_operation_details_data ) ) {
              //
            } else {
              $insert_error++;
            }
          }
        } else {
            if ( Database::insert_data( 'working_day_session_details', $working_operation_details_data ) ) {
              //
            } else {
              $insert_error++;
            }
        }
        if ( $insert_error == 0 ) {
          $o['instert_status'] = 'Data is insterted';
        } else {
          $o['insert_error'] = Database::returnInsertData( $table_name, $working_operation_details_data );
          $o['error'][] = 'Data is not insterted into '.$table_name;
          $o['error'][] = 'Errors : '.Database::return_errors();
          $o['error'][] = 'working_day_session_details input data error';
          $o['$working_operation_details_data'] = $working_operation_details_data;
        }
      } else {
        $o['error'][] = 'Data is not isterted into working_day_session ';
        $o['error'][] = 'Errors : '.mysqli_error($mysqli);
        $o['error'][] = 'Working day sesson input data error';
        $o['update_query'] = Database::returnUpdateQuery( 'working_day_session', $working_operation_data, $where_update_data);
        $o['insert_query'] = Database::returnInsertData( $table_name, $working_operation_details_data );
      }

    header('Content-Type: application/json');
    echo json_encode($o);    

  } else {

    $o = array();

    // HINT DETAILS

    $hint_code = $_POST['hint_code'];
    $hints = $_POST['operacija_hints'];
    $_SESSION['working-session']['operacija_hint'] = $hints;
    $_SESSION['working-session']['curr_time'] = date('H:i:s');

    $working_operation_data         = array('operacija_hint' => $hints );
    $where_update_data              = array('id' => $_POST['working_session_id']);

    $working_day_session_id = $_POST['working_day_session_id'];
    $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

    $data_query = "SELECT * FROM `$table_name` WHERE working_day_session_id='".$working_day_session_id."' ORDER BY id DESC LIMIT 1 ";
    $o['hint']['data_query'] = $data_query;

    $vreme_sadasnje_operacije = date("Y-m-d H:i:s");
    // $current_time_time = date("H:i:s", strtotime($vreme_sadasnje_operacije));
    $current_time_time = date("H:i:s", strtotime($_POST['vreme_operacije']));
    $o['vreme'] = $_POST['vreme_operacije'].'x'.$vreme_sadasnje_operacije.'='.$current_time_time;

    if ( Database::num_rows( $data_query ) > 0 ) {
      $working_session_data =  Database::fetch( $data_query );
      $working_session_data_last_inserted_time = $working_session_data[0]['vreme_operacije'];
    } else {
      $working_session_data_last_inserted_time = $current_time_time;
    }

    $razlika_u_vremenu_operacija = Helper::DateTimeDiff($working_session_data_last_inserted_time, $current_time_time);
    $o['razlika_izmedju_zadnje_operacije'] = $razlika_u_vremenu_operacija;

    if ( isset($_SESSION['working-session']['firts_hint_status']) && $_SESSION['working-session']['firts_hint_status']=='hinted') {
        $razlika_u_sekundama = Helper::returnSecondsFromTimeString($razlika_u_vremenu_operacija);
    } else {

        $_SESSION['working-session']['firts_hint_status'] = 'hinted';

        // first click percent
        $time_hinted_seconds = str_replace(' ', '', $_POST['time_hint']);
        $time_seconds_miliseconds = explode('.',$time_hinted_seconds);
        $time_seconds = explode(':',$time_seconds_miliseconds[0]);
        $_SESSION['working-session']['time_hint'] = $time_hinted_seconds;
        $razlika_u_sekundama = $time_seconds[0] * 60 + $time_seconds[1];
        $_SESSION['working-session']['time_hint_in_seconds'] = $razlika_u_sekundama;

    }

    $operacija_id = $_POST['operacija_id'];
    $operacija_data = Database::whereQuery('operacije', array('operacija_id'=>$operacija_id) );

    $new_artikal_table_name = 'artikal_data_'.$_POST['name_artikal'].'_'.$_POST['artikal'];
    $o['new_artikal_table_name'] = $new_artikal_table_name;
    $o['new_artikal_table_name_query'] = Database::returnWhereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    $artikal_data = Database::whereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    $o['artikal_data_where_query'] = Database::returnWhereQuery($new_artikal_table_name, array('operacija_id'=>$operacija_id) );
    $tempo = (3600 / $artikal_data[0]['tempo_operacije']) * $artikal_data[0]['komada_po_operaciji'];
    $operacija_data_vreme_po_operaciji = $tempo;

    $o['operacija_data_vreme_po_operaciji '] =$operacija_data_vreme_po_operaciji;
    $o['razlika_u_sekundama '] = $razlika_u_sekundama;
    $o['hints '] = $hints;
    if ( $razlika_u_sekundama == 0 ){
      $procenat_ucinka = 100;
    } else {
      $procenat_ucinka = floor(100*($operacija_data_vreme_po_operaciji/$razlika_u_sekundama));
    }
    if ( $procenat_ucinka >= 200 ) {
      $procenat_ucinka = 200;
    }

    $o['procenat_ucinka'] = $procenat_ucinka;

    $hinta_per_hour_this_hours = Helper::returnCountThisHourHints($working_day_session_id);

    $o['hinta_per_hour_this_hour'] = $hinta_per_hour_this_hours;


    $working_operation_details_data = array(
              'working_day_session_id'  => $working_day_session_id ,
              'working_day'             => date('Y-m-d'),
              'komesa_id'               => $_POST['komesa'],
              'radnik_id'               => $_POST['radnik'],
              'linija_id'               => $_POST['linija'],
              'artikal_id'              => $_POST['artikal'],
              'operacija_id'            => $operacija_data[0]['id'],
              'color'                   => 'univerzalna',
              'size'                    => 'univerzalna',
              'vreme_operacije'         => $vreme_sadasnje_operacije,
              'razlika_u_vremenu'       => $razlika_u_vremenu_operacija,
              'procenat_ucinka'         => $procenat_ucinka,
              'hint_code'               => $hint_code
            );

    $o['update_query'] = Database::returnUpdateData( 'working_day_session', $working_operation_data, $where_update_data );
    $o['inport_query'] = Database::returnInsertData( $table_name, $working_operation_details_data );

    $o['upquery'] = '';

      if ( Database::updateData( 'working_day_session', $working_operation_data, $where_update_data )  ){

        $insert_error = 0;

        if ( $artikal_data[0]['komada_po_operaciji'] > 1 ) {
          for ($i=1; $i <= $artikal_data[0]['komada_po_operaciji']; $i++) { 

            $o['upquery'] .= Database::returnInsertData( $table_name, $working_operation_details_data );

            if ( Database::insert_data( $table_name, $working_operation_details_data ) ) {
              //
              $o['action_status'] = true;
            } else {
              $insert_error++;
            }
          }
        } else {
            $o['upquery'] .= Database::returnInsertData( $table_name, $working_operation_details_data );
            if ( Database::insert_data( $table_name, $working_operation_details_data ) ) {
              //
            } else {
              $insert_error++;
            }
        }
        if ( $insert_error == 0 ) {
          $o['insert_status_msg'] = 'Data is insterted';
          $o['insert_status'] = true;
        } else {
          $o['insert_error'] = Database::returnInsertData( $table_name, $working_operation_details_data );
          $o['error'][] = 'Data is not insterted into '.$table_name;
          $o['error'][] = 'Errors : '.Database::return_errors();
          $o['error'][] = $table_name . ' input data error';
          $o['$working_operation_details_data'] = $working_operation_details_data;
        }
      } else {
        $o['error'][] = 'Data is not isterted into working_day_session ';
        $o['error'][] = 'Errors : '.mysqli_error($mysqli);
        $o['error'][] = 'Working day sesson input data error';
        $o['update_query'] = Database::returnUpdateQuery( 'working_day_session', $working_operation_data, $where_update_data);
        $o['insert_query'] = Database::returnInsertData( $table_name, $working_operation_details_data );
      }

    $o['insert_query_2'] = Database::returnInsertData( $table_name, $working_operation_details_data );

    header('Content-Type: application/json');
    echo json_encode($o);
    
  }

  
} elseif ( isset($_GET['action']) && $_GET['action']=='insert-wifi-data' ) {

  if ( isset($_POST['hint_code']) && $_POST['hint_code']=='NWFC' ){
    $sql = "INSERT INTO `".$table_name."`";
    $sql .= " (working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
    $sql .= " VALUES ('".$_SESSION['working-session']['working_day_session_id']."', '".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','".$_POST['hint_code']."')";
    $o['sql'] = $sql;
    if ( Database::query($sql) ) {
      $o['status'] = 'success';
    } else {
      $o['status'] = 'failed';
    }
    // return false;
  } else {
    $o['error'] = 'Error';
  }
  
  header('Content-Type: application/json');
  echo json_encode($o);
}

