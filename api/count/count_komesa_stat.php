<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once '../../system/config/config.php';
require_once '../../system/libs/Database.php';
require_once '../../system/libs/Helper.php';

$table_name = 'working_day_session_details_'.date('n').'_'.date('Y');
$today = date('Y-m-d');

if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>$today))) > 0 ) {

	if ( Database::num_rows( Database::returnWhereQuery($table_name, array('working_day'=>date('Y-m-d'))) ) > 0 ) {

		$o['status'] = 'Works';


		$sve_danasje_komese = Database::whereQuery('komesa_line_settings', array('working_day'=>$today));



		foreach ($sve_danasje_komese as $komese) {

			$komesa_info = Database::whereQuery('komesa', array('id'=>$komese['komesa_id']));
			$new_komesa_table = 'komesa_data_'.$komesa_info[0]['id'];

			if ( Database::num_rows("SHOW TABLES LIKE '".$new_komesa_table."'") == 0 ) {
	          // row doesnt exist
	          // create table 
	          $table_sql = "CREATE TABLE IF NOT EXISTS `".$new_komesa_table."` (
	            `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	            `operacija_id` int(11) DEFAULT NULL,
	            `month` varchar(2) DEFAULT NULL,
	            `year` varchar(4) DEFAULT NULL,
	            `day` varchar(4) DEFAULT NULL,
	            `komada_po_operaciji` int(3) NOT NULL DEFAULT '0'
	          ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

	          if ( Database::query($table_sql) ) {
	            $o .= '<script> Uspesno kreirana tabela za komesu '.$komese['komesa_id'].'</script>';
	          } else {
	            $o .= '<script> Nije kreirana tabela za novu komesu</script>';
	          }
	        } 

			// RETURN ALL OPERACTIONS FROM ARTIKAL IN KOMESA

			$artikal_info = Database::whereQuery('artikal', array('id'=>$komesa_info[0]['artikal_id']));

			$all_operations_from_artikal = Database::whereQuery('artikal_data_'.$artikal_info[0]['artikal_id'].'_'.$artikal_info[0]['id']);

			// Helper::pre($all_operations_from_artikal);

			foreach ($all_operations_from_artikal as $operation) {
				$operation_info = Database::whereQuery('operacije', array('operacija_id'=>$operation['operacija_id']));

				$operacija_count = Database::num_rows(Database::returnWhereQuery($table_name, array('working_day'=>date('Y-m-d'), 'komesa_id'=>$komese['komesa_id'], 'operacija_id'=>$operation_info[0]['id'], 'hint_code'=>'F001')));

				$key_value_data =  array(
					'operacija_id'=>$operation['id'],
					'year'=>date('Y'),
					'month'=>date('m'),
					'day'=>date('d')
				);

				$details_table_name = 'komesa_data_details_'.$komesa_info[0]['id'];
				$o['sql-problem'] = "SELECT * FROM `".$details_table_name."` WHERE 'operacija_id'='".$operation_info[0]['id']."' AND 'date'='".date('Y-m-d')."' AND 'status'='1'";

				if ( Database::num_rows(Database::returnWhereQuery($new_komesa_table, $key_value_data)) > 0 ) {
					$key_value_data['komada_po_operaciji'] = $operacija_count;
					$o['operacija_count'] = $operacija_count;
					$o['msg']= 'Ima ';
					if ( Database::updateData($new_komesa_table, $key_value_data) ){
						$o['insert_data_msg'] = 'Updatovani podaci';


			            $new_komesa_details_table = "CREATE TABLE IF NOT EXISTS `".$details_table_name."` (
			              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			              `operacija_id` int(11) DEFAULT NULL,
			              `ukupno_komada` int(11) NOT NULL DEFAULT '0',
			              `date` DATE DEFAULT NULL,
			              `status` BOOLEAN NOT NULL DEFAULT FALSE
			            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			            if ( Database::query($new_komesa_details_table) ) {
			            } else {
			              echo '<scritp>alert("Problem sa kreiranjem tabele za detalje komese!");</script>';
			            }
			            
						$o['problem']['check-sql-1'] = "SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`='".date('Y-m-d')."' AND `status`='1'";
						$o['problem']['count'] = Database::num_rows("SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`='".date('Y-m-d')."' AND `status`='1'");

						if ( Database::num_rows("SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`='".date('Y-m-d')."' AND `status`='1'") == 0 ) {

							if ( Database::insert_data($details_table_name, array('operacija_id'=>$operation_info[0]['id'], 'ukupno_komada'=>$operacija_count, 'date'=>date('Y-m-d'), 'status'=>'1')) ) {

							}	else {
								$o['problem']['insert-problem-sql'] = Database::returnInsertData($details_table_name, array('operacija_id'=>$operation_info[0]['id'], 'ukupno_komada'=>$operacija_count, 'date'=>date('Y-m-d'), 'status'=>'1'));
							}

						} elseif ( Database::num_rows("SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`<>'".date('Y-m-d')."' AND `status`='1'") > 0 ) {
							
							$komada_po_operaciji = Database::whereQuery($details_table_name, array('operacija_id'=>$operation_info[0]['id']));

							if ( Database::updateData($details_table_name, array( 'ukupno_komada'=>$operacija_count+$komada_po_operaciji[0]['ukupno_komada'], 'date'=>date('Y-m-d'), 'status'=>'1'), array('operacija_id'=>$operation_info[0]['id'])) ) {

							}	else {
								$o['problem']['insert-problem-sql'] = Database::returnupdateQuery($details_table_name, array( 'ukupno_komada'=>$operacija_count+$komada_po_operaciji[0]['ukupno_komada'], 'date'=>date('Y-m-d'), 'status'=>'1'), array('operacija_id'=>$operation_info[0]['id']));
							}


						}
 
					} else {
						$o['insert_data_msg'] = 'Nisu updatovani podaci';
						$o['error']['update_sql'] = Database::returnUpdateQuery($new_komesa_table, $key_value_data);
					}
				} else {
					$key_value_data['komada_po_operaciji'] = $operacija_count;
					$o['operacija_count'] = $operacija_count;
					$o['msg']= 'Nema ';

					if ( Database::insert_data($new_komesa_table, $key_value_data) ){
						$o['insert_data_msg'] = 'Ubacceni podaci';
						$o['problem']['check-sql-2'] = "SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`='".date('Y-m-d')."' AND `status`='1'";


			            $new_komesa_details_table = "CREATE TABLE IF NOT EXISTS `".$details_table_name."` (
			              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			              `operacija_id` int(11) DEFAULT NULL,
			              `ukupno_komada` int(11) NOT NULL DEFAULT '0',
			              `date` DATE DEFAULT NULL,
			              `status` BOOLEAN NOT NULL DEFAULT FALSE
			            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			            if ( Database::query($new_komesa_details_table) ) {
			            } else {
			              echo '<scritp>alert("Problem sa kreiranjem tabele za detalje komese!");</script>';
			            }

						if ( Database::num_rows("SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`='".date('Y-m-d')."' AND `status`='1'") == 0 ) {

							if ( Database::insert_data($details_table_name, array('operacija_id'=>$operation_info[0]['id'], 'ukupno_komada'=>$operacija_count, 'date'=>date('Y-m-d'), 'status'=>'1')) ) {

							}	else {
								$o['problem']['insert-problem-sql'] = Database::returnInsertData($details_table_name, array('operacija_id'=>$operation_info[0]['id'], 'ukupno_komada'=>$operacija_count, 'date'=>date('Y-m-d'), 'status'=>'1'));
							}

						} elseif ( Database::num_rows("SELECT * FROM `".$details_table_name."` WHERE `operacija_id`='".$operation_info[0]['id']."' AND `date`<>'".date('Y-m-d')."' AND `status`='1'") > 0 ) {
							
							$komada_po_operaciji = Database::whereQuery($details_table_name, array('operacija_id'=>$operation_info[0]['id']));

							if ( Database::updateData($details_table_name, array( 'ukupno_komada'=>$operacija_count+$komada_po_operaciji[0]['ukupno_komada'], 'date'=>date('Y-m-d'), 'status'=>'1'), array('operacija_id'=>$operation_info[0]['id'])) ) {

							}	else {
								$o['problem']['insert-problem-sql'] = Database::returnupdateQuery($details_table_name, array( 'ukupno_komada'=>$operacija_count+$komada_po_operaciji[0]['ukupno_komada'], 'date'=>date('Y-m-d'), 'status'=>'1'), array('operacija_id'=>$operation_info[0]['id']));
							}


						}

					} else {
						$o['insert_data_msg'] = 'Nisu ubaceni podaci';
						$o['error']['insert_sql'] = Database::returnInsertData($new_komesa_table, $key_value_data);
					}
				}
			}

			
		}

	} else {
		$o['status'] = 'Dont works';
	}

	// Helper::pre($o);

	header('Content-Type: application/json');
	echo json_encode($o);

}
