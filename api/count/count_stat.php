<?php 

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
// 
// start session
session_start();

require_once '../../system/config/config.php';

// requires files
require_once '../../system/libs/Database.php';
require_once '../../system/libs/Url.php';
require_once '../../system/libs/Helper.php';
require_once '../../system/libs/Ip.php';

if (!Database::connect()) {
  echo json_encode(array('1','nemad'));
  die();
}

// check if row exist for updating
if ( Database::num_rows("SHOW TABLES LIKE 'work_stat'") == 0 ) {
  // row doesnt exist
  // create table 
  $table_sql = "CREATE TABLE IF NOT EXISTS `work_stat` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `radnik_id` int(11) DEFAULT NULL,
    `month` varchar(2) DEFAULT NULL,
    `year` varchar(4) DEFAULT NULL,
    `work_stat` json DEFAULT NULL,
    `day1` int(3) DEFAULT '0',
    `day2` int(3) DEFAULT '0',
    `day3` int(3) NOT NULL DEFAULT '0',
    `day4` int(3) NOT NULL DEFAULT '0',
    `day5` int(3) DEFAULT '0',
    `day6` int(3) NOT NULL DEFAULT '0',
    `day7` int(3) NOT NULL DEFAULT '0',
    `day8` int(3) NOT NULL DEFAULT '0',
    `day9` int(3) NOT NULL DEFAULT '0',
    `day10` int(3) NOT NULL DEFAULT '0',
    `day11` int(3) NOT NULL DEFAULT '0',
    `day12` int(3) NOT NULL DEFAULT '0',
    `day13` int(3) NOT NULL DEFAULT '0',
    `day14` int(3) NOT NULL DEFAULT '0',
    `day15` int(3) NOT NULL DEFAULT '0',
    `day16` int(3) NOT NULL DEFAULT '0',
    `day17` int(3) NOT NULL DEFAULT '0',
    `day18` int(3) NOT NULL DEFAULT '0',
    `day19` int(3) NOT NULL DEFAULT '0',
    `day20` int(3) NOT NULL DEFAULT '0',
    `day21` int(3) NOT NULL DEFAULT '0',
    `day22` int(3) NOT NULL DEFAULT '0',
    `day23` int(3) NOT NULL DEFAULT '0',
    `day24` int(3) NOT NULL DEFAULT '0',
    `day25` int(3) NOT NULL DEFAULT '0',
    `day26` int(3) NOT NULL DEFAULT '0',
    `day27` int(3) NOT NULL DEFAULT '0',
    `day28` int(3) NOT NULL DEFAULT '0',
    `day29` int(3) DEFAULT '0',
    `day30` int(3) NOT NULL DEFAULT '0',
    `day31` int(3) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

  Database::query($table_sql);
}

$radnici_lista = Database::fetchData('radnici_lista');

foreach ($radnici_lista as $radnik) {
 
  $radnik_id = $radnik['radnik_id'];


  if ( Database::num_rows(Database::returnWhereQuery('working_day_session_details_'.date('n').'_'.date('Y'), array('radnik_id'=>$radnik_id, 'hint_code'=>'F001', 'working_day'=>date('Y-m-d')))) > 0 ) { // AKO IMA DNEVNIH AKTIVNOSTI


    $procenat = 0;

    $data = Database::whereQuery('working_day_session_details_'.date('n').'_'.date('Y'), array('radnik_id'=>$radnik_id, 'hint_code'=>'F001', 'working_day'=>date('Y-m-d')));
    $count_num = Database::num_rows(Database::returnWhereQuery('working_day_session_details_'.date('n').'_'.date('Y'), array('radnik_id'=>$radnik_id, 'hint_code'=>'F001', 'working_day'=>date('Y-m-d'))));

    foreach ($data as $p ) {
      $procenat += $p['procenat_ucinka'];
    }

    $dnevni_procenat = floor($procenat / $count_num);

    $table_name = 'work_stat';
    if ( Database::num_rows(Database::returnWhereQuery('work_stat', array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y')))) > 0 ) {


      $key_value_data = array('day'.date('d') => $dnevni_procenat);
      $where_array = array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y'));

      if ( Database::updateData($table_name, $key_value_data, $where_array) ) {
        $array['status'] = true;
      } else {
        Helper::pre(Database::returnUpdateQuery($table_name, $key_value_data, $where_array));
        $array['status'] = false;
      }

    //   echo json_encode($array);
    } else {
      Helper::pre(Database::returnWhereQuery('work_stat', array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y'))));

      $key_value_data = array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y'),'day'.date('d') => $dnevni_procenat);


      if ( Database::insert_data($table_name, $key_value_data) ) {
        $array['status'] = true;
      } else {
        Helper::pre(Database::returnInsertData($table_name, $key_value_data));
        $array['status'] = false;
      }

      echo json_encode($array);
    }


  } else { // AKO NEMA DNEVNIH AKTIVNOSTI
    Helper::pre(Database::returnWhereQuery('work_stat', array('radnik_id'=>$radnik_id, 'month'=>date('m'), 'year'=>date('Y'))));
    if ( Database::num_rows(Database::returnWhereQuery('work_stat', array('radnik_id'=>$radnik_id, 'month'=>date('m'), 'year'=>date('Y')))) > 0 ) {

      $key_value_data = array('day'.date('d') => '0');
      $where_array = array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y'));

      if ( Database::updateData('work_stat', $key_value_data, $where_array) ) {
        $array['status'] = true;
      } else {
        Helper::pre(Database::returnUpdateQuery('work_stat', $key_value_data, $where_array));
        $array['status'] = false;
      }

    } else {
      $key_value_data = array('radnik_id'=>$radnik_id , 'month'=>date('m'), 'year'=>date('Y'), 'day'.date('d')=>'0');

      if ( Database::insert_data('work_stat', $key_value_data) ) {
        $array['status'] = true;
      } else {
        Helper::pre(Database::returnInsertData('work_stat', $key_value_data));
        $array['status'] = false;
      }
    }


  }


}