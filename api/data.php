<?php
// start session
session_start();

// requires files
require_once '../system/function.php';
require_once '../system/config/config.php';

$class = array('Database','Url','Helper','Ip','Table');
__autoload($class);

// Helper::pre($_SESSION);

if (!Database::connect()) {
  echo json_encode(array('1','Nema konekcije sa bazom podataka.<br/> Molimo kontaktirajte administratora'));
}

if ( !isset($_SESSION['data']) ) { $_SESSION['data'] = 'data'; }

$new_table = 'working_day_session_details_'.date('n').'_'.date('Y');

if ( isset($_GET['action']) && $_GET['action']=='logout_user' ) {

  require_once 'session/session.php';

  $o['session'] = $session;

  $o = array(
    'status' => 'Success',
    'error_msg' => 'User logouted'
  );


  header('Content-Type: application/json');
  echo json_encode($o);

} elseif ( isset($_GET['action']) && $_GET['action']=='login-data-new' ) {

  if ( !file_exists('../require_files/data_login_part.php') ) {
    // Url::header_status(BASEURL.urlencode('404.php?message=File Not Found'));
    echo json_encode(array(
      'status' => 'Error',
      'error_msg' => 'Error loading file'
    ));
  } else {
    // echo json_encode(array(
    //   'status' => 'ok',
    //   'post'=>$_POST
    // ));
    require_once '../require_files/data_login_part.php';
  }

} elseif ( isset($_GET['action']) && $_GET['action']=='login-data' ) {
  if ( $_POST['name_radnik'] == '' || $_POST['sifra']=='' ) {
    Url::header_status('index.php');
  }
  $radnik_name = $_POST['name_radnik'];
  $sifra = $_POST['sifra'];
  $query = "SELECT * FROM radnici_lista WHERE radnik_name='".$radnik_name."' AND sifra='".$sifra."'";
  $radnik_result = Database::query($query);
  if ( $radnik_result->num_rows > 0 ) {

    $working_day_data = array(
      'work_day'  => date("Y-m-d"),
      'radnik_id' => $_POST['radnik'],
      'login_time'=> date("Y-m-d H:i:s"),
      // 'logout_time' => ,
      // 'pausa_1_start_time' => ,
      // 'pausa_1_end_time' => ,
      // 'pausa_2_start_time' => ,
      // 'pausa_2_end_time' => ,
      'linija' => $_POST['linija'],
      // 'radni_broj' => ,
      // 'artikal_id' => ,
      // 'operacija_id' =>
    );
    if ( $last_id = Database::insert_data( 'working_day_session', $working_day_data ) ) {
        $_SESSION['working-session']['working-session-id'] = $last_id;
        // $query = "";
        $_SESSION['working-session']['action'] = 'start';
        $_SESSION['working-session']['first-login-time'] = date("Y-m-d H:i:s");
        $_SESSION['working-session']['first-login-timestamp'] = time();
        $_SESSION['working-session']['login-data'] = 'user-loged';
        $_SESSION['working-session']['linija'] = $_POST['linija'];
        $_SESSION['working-session']['name_linija'] = $_POST['name_linija'];
        $_SESSION['working-session']['radnik'] = $_POST['radnik'];
        $_SESSION['working-session']['name_radnik'] = $_POST['name_radnik'];
        $_SESSION['working-session']['sifra'] = $_POST['sifra'];
        echo json_encode($_POST);
    } else {
        $_SESSION['error'] = 'Error inserting data to database';
        $error = array('error'=>'Error inserting data to database');
        echo json_encode($error);
     }
  } else {
    $_SESSION['error']['login-error-message'] = 'Pogresna kombinacija ime/sifra.';

    echo json_encode(array('status'=>'error','error-msg'=>'Pogresna kombinacija ime/sifra'));
  }

  //
} elseif ( isset($_GET['action']) && $_GET['action']=='job-data' ) { // add job data to session and update working day db

  // echo json_encode($_POST);
  require_once '../require_files/job_part.php';

} elseif ( isset($_GET['action']) && $_GET['action']=='return-data' && $_POST['options'] == 'all_day_data' ) {

  if ( isset($_POST['options_all_summ']) && $_POST['options_all_summ']==true ){

    if ( isset($_POST['working_day_session_id']) && $_POST['working_day_session_id'] !='' && 
        isset($_POST['operacija_id']) && $_POST['operacija_id'] !='' && 
        isset($_POST['radnik_id']) && $_POST['radnik_id'] !='' ) {

        $working_day_session_id = $_POST['working_day_session_id'];
        $operacija_id           = $_POST['operacija_id'];
        $radnik_id              = $_POST['radnik_id'];
      
        $index = 1;
        $efikasnost = 0;
        $table_name = 'working_day_session_details_'.date('n').'_'.date('Y');

        $sva_dnevna_efikasnost = Database::whereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001'));
        $count_all_day_data = Database::num_rows(Database::returnWhereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001')));
        // $o['sva_dnevna_efikasnost'] = $sva_dnevna_efikasnost;

        foreach ($sva_dnevna_efikasnost as $item) {
          $efikasnost += $item['procenat_ucinka'];
          $index++;
        }
        
        $o['dnevna_efikasnost'] = ceil($efikasnost / $count_all_day_data);

        $o['all_day_uspesnost_query'] = Database::returnWhereQuery($table_name, array('radnik_id'=>$radnik_id,'working_day'=>date('Y-m-d'),'hint_code'=>'F001'));
        $o['all_day_uspesnost_count'] = $count_all_day_data;

      
    }

  } elseif ( !isset($_POST['options_all_summ']) || $_POST['options_all_summ']==false ) {

  }

  header('Content-Type: application/json');
  echo json_encode($o);

}elseif ( isset($_GET['action']) && $_GET['action']=='return-data' && $_POST['options'] == 'linija') {
  $o = '';
  $o .= '<option selected disabled value="default">Izaberite liniju</option>';
  $list_result = Database::fetchData('linija');
  foreach ($list_result as $item ) {
    # code...
    $o .= '<option name="'.$item['linija_name'].'" value="'.$item['linija_id'].'">'.$item['linija_name'].'</option>';
  }
  echo $o;
//
} elseif ( isset($_GET['action']) && $_GET['action']=='return-data' && $_POST['options'] == 'radnice' ) {
  $o = '';
  $o .= '<option selected disabled value="default">Izaberite radnika</option>';
  $radnici_result = Database::whereQuery( 'radnici_lista', array('linija'=>$_POST['linija_id']) );
  foreach ($radnici_result as $item ) {
    # code...
    $o .= '<option name="'.$item['radnik_name'].'" value="'.$item['radnik_id'].'">'.$item['radnik_name'].'</option>';
  }
  echo $o;

} elseif ( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'operacija' ) {
  $o = '';
  $o .= '<option selected disabled value="default">Izaberite operaciju</option>';

  if ( isset($_POST['artikal_id']) ) {
    $artikal_data = Database::whereQuery('artikal',array('id'=>$_POST['artikal_id']));
    $artikal_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$artikal_data[0]['id'];
    $operacije_list = Database::fetchData($artikal_table);

    $tablet_data = Database::whereQuery('tablet_ip_settings', array('converted_tablet_ip'=>Ip::ReturnConvertedIp()));

    $masina_table = 'masina_data_'.$tablet_data[0]['masina'];
    $operacije = Database::fetchData($masina_table);

    foreach ($operacije_list as $item) {
      foreach ($operacije as $op) {
        if ($op['operacija_id'] == $item['operacija_id']) {
          $o .= '<option name="'.$item['id'].'" value="'.$item['operacija_id'].'"> '.$item['opis_operacije'].' | Operacija ID:'.$item['operacija_id'].' | Tempo Operacije: '.$item['tempo_operacije'].'</option>';
        }
      }
    }
    if ( $o === '<option selected disabled value="default">Izaberite operaciju</option>' ) {
      $o = '<option selected disabled value="default">Nema operacija</option>';
    }

  } else {
    $o .= '<option selected disabled value="default">Izaberite operaciju</option>';
    $operacije_list = Database::fetchData('operacije');
    foreach ($operacije_list as $item) {
      $o .= '<option name="'.$item['id'].'" value="'.$item['operacija_id'].'"> '.$item['opis_operacije'].' | Operacija ID:'.$item['operacija_id'].' | Tempo Operacije: '.$item['tempo_operacije'].'</option>';
    }
  }

  echo $o;

} elseif ( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'komesa' ) {
  if ( isset($_POST['artikal_id']) ) {
    $o = '';
    $artikal_id = $_POST['artikal_id'];
    if ( Database::num_rows(Database::returnWhereQuery('komesa', array('artikal_id'=>$artikal_id))) > 0 ) {
      $komesa_list = Database::whereQuery('komesa', array('artikal_id'=>$artikal_id));
      // Helper::pre($komesa_list);
      $o .= '<option selected disabled value="default">Izaberite komesu</option>';
      foreach ($komesa_list as $komesa) {
        $komesa_data = Database::whereQuery('komesa',array('id'=>$komesa['id']));
        // Helper::pre(Database::returnWhereQuery('komesa',array('komesa_id'=>$komesa['komesa_id'])));
        // Helper::pre($komesa_data);

        // use only komesa where working date is today
        if ( Database::num_rows( Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d'),'komesa_id'=>$komesa_data[0]['id'])) ) > 0 ) {
          echo '<option value="'.$komesa['id'].'"> [ Ime komese '.$komesa_data[0]['komesa_name']. ' ] [ ID '.$komesa_data[0]['id'] . '[ Komesa ID '.$komesa_data[0]['komesa_id'] . ' ] </option>';
        }
        // $o .= '<option value="'.$komesa['komesa_id'].'"> [ Ime komese '.$komesa_data[0]['komesa_name']. ' ] [ Komesa ID '.$komesa_data[0]['komesa_id'] . ' ] </option>';
      }

      echo $o;
    } else {
      echo '0';
    }
  } else {
    $linija_id = isset($_POST['linija']) ? $_POST['linija'] : '';

    $table_name = isset($_POST['table_name']) ? $_POST['table_name'] : 'komesa_line_settings';
    $where = isset($_POST['where']) ? $_POST['where'] : '';

    // return all komesa from db where komesa has artikal with this opreaciju
    if ( $komesa_array = Database::whereQuery($table_name,$where) ) {
      if ( $komesa_array != 0 ) {
        echo json_encode($komesa_array);
      } else {
        echo 0;
      }
    } else {
      echo 0;
    }  }


} elseif( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'artikal' ) {
  $komesa_id = $_POST['komesa_id'];
  $o = '';
  $list = Database::whereQuery('komesa',array('id'=>$komesa_id));
  if ( $list != 0 ) {
    foreach ($list as $item) {
      $artikal_data = Database::whereQuery('artikal', array('id'=>$item['artikal_id']));
      $_SESSION['artikal_id'] = Database::returnWhereQuery('artikal', array('id'=>$item['artikal_id']));
      $o .= '<option name="'.$artikal_data[0]['artikal_id'].'" value="'.$artikal_data[0]['id'].'" selected="selected">'.$artikal_data[0]['artikal_id'].'</option>';
    }
  } else {
    $o .= '<option name="nula">Nema podataka trenutno</option>';
    $o .= '<option name="nula">'.Database::returnWhereQuery('komesa',array('id'=>$komesa_id)).'</option>';
  }
  echo $o;

} elseif( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'full_artikal' ) {
  $id = $_POST['artikal_id'];
  $artikal_data = Database::whereQuery('artikal',array('id'=>$id));
  $new_table = 'artikal_data_'.$artikal_data[0]['artikal_id'].'_'.$id;
  // echo $artikal_id;
  $o = '';
  // $o .= $new_table.'_color';

  $all_colors = $all_colors = Database::fetchData('colors');
  $o .= '<div class="color_options col-xs-5 wrap-top btn-groupp hidden" data-toggle="buttons">';
    $o .= '<label class="col-xs-3 btn active" style="border: thin solid #000;" active>';
    $o .= '<input type="radio" name="color_options" id="0" autocomplete="on"';
    $o .= ' value="0" selected="selected">';
    $o .= '<span class="glyphicon glyphicon-ok"></span>';
    $o .= '<h5 class="glyphicon glyphicon-remove"></h5>';
    $o .= '</label>';

  // foreach ($all_colors as $color) {
  //   if ( Database::num_rows(Database::returnWhereQuery($new_table.'_color', array('color_id'=>$color['id'],'color_status'=>'1'))) == 1 ) {
  //     $o .= '<label class="col-xs-3 btn" style="background-color:#'.$color['color_value'].'">';
  //     $o .= '<input type="radio" name="color_options" id="'.$color['color_name'].'" autocomplete="on"';
  //     $o .= ' value="'.$color['id'].'">';
  //     $o .= '<span class="glyphicon glyphicon-ok"></span>';
  //     $o .= '</label>';
  //   }
  // }
  $o .= '</div>';

  $all_sizes = Database::fetchData('sizes');
  $o .= '<div class="size_options col-xs-5 wrap-top btn-groupp hidden" data-toggle="buttons">';
    $o .= '<label class="col-xs-4 btn active" style="background-color:#eee" active>';
    $o .= '<input type="radio" name="size_options" id="0" autocomplete="on"';
    $o .= ' value="0" selected="selected">';
    $o .= '<span class="glyphicon glyphicon-ok"></span>';
    $o .= '<h5 class="glyphicon glyphicon-remove"></h5>';
    $o .= '</label>';
  // foreach ($all_sizes as $size) {
  //   // id, size_name, size_value, size_date
  //   if ( Database::num_rows(Database::returnWhereQuery($new_table.'_size', array('size_id'=>$size['id'],'size_status'=>'1'))) == 1 ) {
  //     $o .= '<label class="col-xs-3 btn" style="background-color:#eee">';
  //     $o .= '<input type="radio" name="size_options" id="'.$size['size_value'].'" autocomplete="on"';
  //     $o .= ' value="'.$size['size_value'].'">';
  //     $o .= '<span class="glyphicon glyphicon-ok"></span>';
  //     $o .= '<h5>'.$size['size_value'].'</h5>';
  //     $o .= '</label>';
  //   }
  // }
  $o .= '</div>';

  $o .= '<div class="clear"></div>';
  echo $o;

} elseif( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'zadatak' ) {
  
  if ( Database::num_rows(Database::returnWhereQuery('komesa_line_settings', array('working_day'=>date('Y-m-d')) )) > 0 ) { //Povera da li ima zadataka za danasji dan
    $o['status'] = 'true';
  } else {
    $o['status'] = 'false';
  }

  header('Content-Type: application/json');
  echo json_encode($o);
} elseif( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'get_options') {
  
  if ( !empty($_POST['option_name']) && $_POST['option_name']!= '' && !isset($_POST['masina_id']) && !isset($_POST['radnik_id']) ) {
    $opton_value = get_options($_POST['option_name']);
    $o['option_value'] = $opton_value;
  } elseif ( !empty($_POST['option_name']) && $_POST['option_name']!= '' && isset($_POST['masina_id']) && $_POST['masina_id']!='' ) {
    $masina_data = Database::whereQuery('masine_options', array('masina_id'=>$_POST['masina_id']));
    $o['btn_disable_time'] = $masina_data[0]['btn_disable_time'] == '' ? 100 : $masina_data[0]['btn_disable_time'];
  } elseif ( !empty($_POST['option_name']) && $_POST['option_name']!= '' && isset($_POST['radnik_id']) && $_POST['radnik_id']!='' ) {
    $radnik_data = Database::whereQuery('radnici_lista', array('radnik_id'=>$_POST['radnik_id']));
    $o['btn_disable_time'] = $radnik_data[0]['btn_disable_time'] == '' ? 100 : $radnik_data[0]['btn_disable_time'];
  } else {
  }

  header('Content-Type: application/json');
  echo json_encode($o);

  // GET OPTIONS
} elseif ( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_GET['options'] == 'get_options' ) {
  if ( !empty($_GET['option_name']) && $_GET['option_name']!= '' ) {
    $opton_value = get_options($_GET['option_name']);
  }
  $o['option_status'] = 'Success';
  $o['option_value']= $opton_value;

  header('Content-Type: application/json');
  echo json_encode($o);
} elseif ( isset($_GET['action']) && $_GET['action'] == 'insert-data' && $_POST['options'] == 'operacija-hints' ) {
  // insert data
  require_once 'hint_data.php';
  
} elseif ( isset($_GET['action']) && $_GET['action']=='insert-greska-data' ) {

  if ( isset($_POST['hint_code']) && $_POST['hint_code']=='G002' ){
    $sql = "INSERT INTO `".$new_table."`";
    $sql .= " (working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
    $sql .= " VALUES ('".$_SESSION['working-session']['working_day_session_id']."', '".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','".$_POST['hint_code']."')";
    $o['sql'] = $sql;
    if ( Database::query($sql) ) {
      $o['status'] = 'success';
    } else {
      $o['status'] = 'failed';
    }
    // return false;
  } elseif ( isset($_POST['options']) && $_POST['options']=='count_greska' ) {
    $sql = "INSERT INTO `".$new_table."`";
    $sql .= " (working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
    $sql .= " VALUES ('".$_SESSION['working-session']['working_day_session_id']."', '".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','".$_POST['hint_code']."')";

    $o['sql'] = $sql;
    if ( Database::query($sql) ) {
      $o['status'] = 'success';
    } else {
      $o['status'] = 'failed';
    }

  } else {
    $o['error'] = 'Error';
  }
  header('Content-Type: application/json');
  echo json_encode($o);

} elseif ( isset($_GET['action']) && $_GET['action']=='insert-pausa-data' ) {
  $o['dfd'] = 'Hello';
  if ( isset($_POST['hint_code']) && $_POST['hint_code']=='P002' ){
    $sql = "INSERT INTO `".$new_table."`";
    $sql .= " (working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
    $sql .= " VALUES ('".$_SESSION['working-session']['working_day_session_id']."', '".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','".$_POST['hint_code']."')";

    $o['rgdfg'] = 'Hello';
    $o['sql'] =  $sql;

    $_SESSION['dfsdf'] = $sql;

      $update_sql = "UPDATE `working_day_session`";
      $update_sql .= " SET pausa_1_end_time='".date('Y-m-d H:i:s')."' ";
      $update_sql .= " WHERE working_day_session_id = '".$_SESSION['working-session']['working_day_session_id']."' AND radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";

      $o['status'] = $update_sql;
      $o['update_sql'] = $update_sql;


    if ( Database::query($sql) ) {
      // update working_day_session
      $update_sql = "UPDATE `working_day_session`";
      $update_sql .= " SET pausa_1_end_time='".date('Y-m-d H:i:s')."' ";
      $update_sql .= " WHERE working_day_session_id = '".$_SESSION['working-session']['working_day_session_id']."' AND radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";

      $_SESSION['sfsdf'] = $update_sql;

      if ( Database::query($update_sql) ){
        $o['update_status'] = 'Updatevana tabela working_day_session je supelo';
      } else {
        $o['update_status'] = 'Updatevana tabela working_day_session nije uspelo';
      }
      $o['status'] = 'Pausa se zavrsila';
    } else {
      $o['status'] = 'Pausa se nije zavrsila';
    }
  } elseif ( isset($_POST['hint_code']) && $_POST['hint_code']=='P004' ){
      $sql = "INSERT INTO `".$new_table."`";
      $sql .= " (working_day_session_id,working_day,radnik_id,vreme_operacije,hint_code )";
      $sql .= " VALUES ('".$_SESSION['working-session']['working_day_session_id']."', '".date('Y-m-d')."','".$_SESSION['working-session']['radnik']."','".date('Y-m-d H:i:s')."','".$_POST['hint_code']."')";

      $o['rgdfg'] = 'Hello';

      $_SESSION['dfsdf'] = $sql;

        $update_sql = "UPDATE `working_day_session`";
        $update_sql .= " SET pausa_2_end_time='".date('Y-m-d H:i:s')."' ";
        $update_sql .= " WHERE working_day_session_id = '".$_SESSION['working-session']['working_day_session_id']."' AND radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";

        $o['status'] = $update_sql;


      if ( Database::query($sql) ) {
        // update working_day_session
        $update_sql = "UPDATE `working_day_session`";
        $update_sql .= " SET pausa_2_end_time='".date('Y-m-d H:i:s')."' ";
        $update_sql .= " WHERE working_day_session_id = '".$_SESSION['working-session']['working_day_session_id']."' AND radnik_id='".$_SESSION['working-session']['radnik']."' AND work_day='".date('Y-m-d')."'";

        $_SESSION['sfsdf'] = $update_sql;

        if ( Database::query($update_sql) ){
          $o['update_status'] = 'Updatevana tabela working_day_session je supelo';
        } else {
          $o['update_status'] = 'Updatevana tabela working_day_session nije uspelo';
        }
        $o['status'] = 'Pausa se zavrsila';
      } else {
        $o['status'] = 'Pausa se nije zavrsila';
      }
    }


  header('Content-Type: application/json');
  echo json_encode($o);

} elseif ( isset($_GET['action']) && $_GET['action']=='new-get-data' ) {
  header('Content-Type: application/json');
  echo json_encode($_GET);

} elseif ( isset($_GET['action']) && $_GET['action']=='get_siteurl' ) {
  $o = get_options('siteurl');
  header('Content-Type: application/json');
  echo json_encode($o);

} elseif( isset($_GET['action']) && $_GET['action']=='return_data_query') {
  // if ( isset($_POST['query']) ) {
  //   $o['status'] = 'sucess';
  //   $o['status_text'] = 'Ima query';
  //   $o['query'] = $_POST['query'];
  //   if ( Database::num_rows($o['query']) > 0 ) {
  //     $results = Database::query($o['query']);
  //     $o['status_results'] = 'success';
  //     $o['results'] = $results;
  //     $o['json_results'] = json_encode($results);
  //   } else {
  //     $o['status_results'] = 'false';
  //   }
  // } else {
  //   $o['status'] = 'failed';
  //   $o['status_text'] = 'Nema query';
  // }
 
 if ( Database::num_rows($_POST['query']) == 0 ) {
  $o['status'] = 'failed';
 } else {
   $res = Database::query($_POST['query']);

   foreach ($res as $key => $value) {
     $o[$key] = $value;
   }  
 }



  header('Content-Type: application/json');
  echo json_encode($o);


} elseif ( isset($_GET['action']) && $_GET['action'] == 'return-data' && $_POST['options'] == 'check_login_status' ) {

    $radnik_id = $_POST['radnik_id'];

    $sql = "SELECT * FROM `working_day_session` WHERE radnik_id='".$radnik_id."' AND work_day='".date('Y-m-d')."' AND logout_time IS NULL ";
    $o['sql'] = $sql;
    if ( Database::num_rows($sql) > 0 ) {
      $o['login_status_msg'] = 'Logged';
      $o['login_status'] = 'true';
    } else {
      $o['login_status_msg'] = 'Not logged';
      $o['login_status'] = 'false';
    }
    $o['radnik_id'] = $radnik_id;

  header('Content-Type: application/json');
  echo json_encode($o);


} elseif ( isset($_GET['action']) && $_GET['action']=='check_user_noactivity' ) {
  $max_neaktivnost = get_options('max_neaktivnost');

  $o['noActivityTime'] = $max_neaktivnost;

  $track_data = Database::whereQuery( 'user_tracking_log_file', array('radnik_id'=>$_POST['radnik_id'], 'working_session_id'=>$_POST['working_session_id'], 'working_day'=>date('Y-m-d')), 'ORDER BY `id` DESC LIMIT 1' );

  $timeStart  = date('Y-m-d H:i:s');
  $timeEnd    = $track_data[0]['vreme_zadnje_posete'];

  $razlika = Helper::DateTimeDiff($timeStart, $timeEnd);
  $razlika_array = explode(':', $razlika);

  if ( ($razlika_array[0]*3600 + $razlika_array[1]*60 + $razlika_array[2]) >= ($max_neaktivnost*60) ) {
    $o['no_activity_status_msg'] = 'Razlika je ili vece ili jednako';
    $o['no_activity_status'] = 'true';
    $o['vrednost1'] = $razlika_array[0]*3600 + $razlika_array[1]*60 + $razlika_array[2];
    $o['vrednost2'] = $max_neaktivnost*60;
    $o['no_activity_status_time'] = Helper::DateTimeDiff($timeStart, $timeEnd);
  } else {
    $o['no_activity_status_msg'] = 'Vrednost je manja od vrednsoti neaktivnsoti';
    $o['no_activity_status'] = 'false';
    $o['start'] = $timeStart;
    $o['end'] = $timeEnd;
    $o['sql'] = Database::returnWhereQuery( 'user_tracking_log_file', array('radnik_id'=>$_POST['radnik_id'], 'working_session_id'=>$_POST['working_session_id'], 'working_day'=>date('Y-m-d')), 'ORDER BY `id` DESC LIMIT 1' );
    $o['no_activity_status_time'] = Helper::DateTimeDiff($timeStart, $timeEnd);
    $o['vrednost1'] = $razlika_array[0]*3600 + $razlika_array[1]*60 + $razlika_array[2];
    $o['vrednost2'] = $max_neaktivnost*60;
  }
 
  header('Content-Type: application/json');
  echo json_encode($o);
}elseif ( isset($_GET['action']) && $_GET['action']=='user_activity' ) {

  $radnik_id              = $_POST['radnik_id'];
  $working_session_id     = $_POST['working_session_id'];
  $vreme_zadnje_posete    = date('Y-m-d H:i:s');
  $workign_day            = date('Y-m-d');

  $key_value_data = array(
    'radnik_id'               =>$radnik_id,
    'working_session_id'      =>$working_session_id,
    'vreme_zadnje_posete'     =>$vreme_zadnje_posete,
    'working_day'             =>$workign_day
  );

  $o = $key_value_data;

  // EXAMPLE: Database::insert_data($table_name, $key_value_data, $where_array='');
  if ( Database::insert_data('user_tracking_log_file', $key_value_data) ) {
    $o['insert_status'] = 'true';
  } else {
    $o['insert_status'] = 'false';
    $o['sql'] = Database::returnInsertData('user_tracking_log_file', $key_value_data);
  }

  header('Content-Type: application/json');
  echo json_encode($o);

} elseif ( isset($_GET['action']) && $_GET['action']=='logout_session' ) {

  $radnik_id = $_POST['radnik_id'];
  $working_session_id = $_POST['working_session_id'];

    $query = "UPDATE working_day_session SET logout_time = '".date('Y-m-d H:i:s')."' WHERE id = '".$working_session_id."' AND `radnik_id` ='".$radnik_id."' AND `work_day`= '".date('Y-m-d')."'";
    
    $o['post'] = $_POST;

    if ( $result = Database::query( $query ) ) {
      $o['logout_session_status'] = 'success';
    } else {
      $o['logout_session_status'] = 'failed';
      $o['query'] = $query;
    }

  header('Content-Type: application/json');
  echo json_encode($o);


} else {
  echo '<h1> Helllooooo </h1>';
}

?>
