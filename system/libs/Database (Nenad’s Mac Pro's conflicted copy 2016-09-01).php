<?php
/**
* Format datetime for inserting into databas = date("Y-m-d H:i:s")
*/
class Database{


// $mysqli = new mysqli("localhost", "my_user", "my_password", "world");

// /* check connection */
// if ($mysqli->connect_errno) {
//     printf("Connect failed: %s\n", $mysqli->connect_error);
//     exit();
// }

// $query = "SELECT Name, CountryCode FROM City ORDER by ID LIMIT 3";
// $result = $mysqli->query($query);

// /* numeric array */
// $row = $result->fetch_array(MYSQLI_NUM);
// printf ("%s (%s)\n", $row[0], $row[1]);

// /* associative array */
// $row = $result->fetch_array(MYSQLI_ASSOC);
// printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);

// /* associative and numeric array */
// $row = $result->fetch_array(MYSQLI_BOTH);
// printf ("%s (%s)\n", $row[0], $row["CountryCode"]);

// /* free result set */
// $result->free();

    private $mysqli;

    public static function connect(){
      date_default_timezone_set('Europe/Belgrade');
        try{
            return new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public static function query($query){
        $mysqli = Database::connect();
        $mysqli->query("SET NAMES utf8");
        return $mysqli->query($query);
    }

    public static function fetch($query) {
      $mysqli = Database::connect();
      $mysqli->query("SET NAMES utf8");
      $res = $mysqli->query($query);
      while($row = $res->fetch_assoc()){
          $list[]=$row;
      }
      return $list;
    }

    public static function whereQuery($table_name, $where_array='', $other='', $limit=''){
      $query = "SELECT * FROM `".$table_name."` ";
      $query .= "WHERE ";
      if ( $where_array != '' ) {
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key."='".$value."'";
          } else {
            $query .= "AND ".$key."='".$value."'";
          }
        }
      }
      if ( $other != '' ) { $query .= " ".$other; }
      if ( $limit != '' ) { $query .= " LIMIT ".$limit; }

      $_SESSION['test2'] = $query;
      $res = Database::query($query);
      if ( $res->num_rows > 0 ) {
        while($row = $res->fetch_assoc()){
          $list[]=$row;
        }
        return $list;
      } else {
        return 0;
      }
    }

    public static function fetchData($table_name){
        $query = "SELECT * FROM `".$table_name."`";
        $res = Database::query($query);
        while($row = $res->fetch_assoc()){
            $list[]=$row;
        }
        return $list;
    }

    public static function insert_data($table_name, $key_value_data, $where_array='') {
      $mysqli = Database::connect();
      $query = "INSERT INTO ".$table_name."(";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= "$key"; // where
        } else {
          $query .= "$key,"; // where
        }
      }
      $query .= " ) VALUES (";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= "'".$value."'"; // where
        } else {
          $query .= "'".$value."',"; // where
        }
      }
      $query .= ")";
      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }


      $_SESSION['data_query'] = $query;

      // return $query;

      if ( $result = $mysqli->query($query) ) {
        $last_id =  $mysqli->insert_id;
        return $last_id;
      } else {
        return false;
      }


    }

    public static function return_last_iserted_id() {
      $mysqli = Database::connect();
      return $mysqli->insert_id;
    }

    public static function updateData($table_name, $key_value_data, $where_array='') {
      $mysqli = Database::connect();
      $query = "UPDATE ".$table_name." SET ";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= $key."='".$value."'"; // where
        } else {
          $query .= $key."='".$value."',"; // where
        }
      }

      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }
      $_SESSION['data_query'] = $query;

      if ( $result = $mysqli->query($query) ) {
        $_SESSION['success'] = 'success';
        return true;
      } else {
        $_SESSION['success'] = 'Error';
        return false;
      }
    }

    public static function num_rows($query){
        $mysqli = Database::connect();
        $mysqli->query("SET NAMES utf8");
        $res = $mysqli->query($query);
        return $res->num_rows;
    }

    public static function insertData(){
        $mysqli = Database::connect();
        $stmt = $mysqli->prepare("INSERT INTO movies(filmName,filmDescription,filmImage,filmPrice,filmReview) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('sssdi', $_POST['filmName'],
        $_POST['filmDescription'],
        $_POST['filmImage'],
        $_POST['filmPrice'],
        $_POST['filmReview']);
        $stmt->execute();
        $newId = $stmt->insert_id;
        $stmt->close();
    }

    public static function update_data(){
        $stmt = $mysqli->prepare("UPDATE movies SET filmName = ?,
           filmDescription = ?,
           filmImage = ?,
           filmPrice = ?,
           filmReview = ?
           WHERE filmID = ?");
        $stmt->bind_param('sssdii',
           $_POST['filmName'],
           $_POST['filmDescription'],
           $_POST['filmImage'],
           $_POST['filmPrice'],
           $_POST['filmReview'],
           $_POST['filmID']);
        $stmt->execute();
        $stmt->close();
    }

    public static function deleteData(){
        $stmt = $mysqli->prepare("DELETE FROM movies WHERE filmID = ?");
        $stmt->bind_param('i', $_POST['filmID']);
        $stmt->execute();
        $stmt->close();
    }


    public static function return_errors() {
      $mysqli = Database::connect();
      return  $mysqli->error;
      // print_r($mysqli->error);
    }

    public static function returnQuery($query){
      return $query;
    }

    public static function returnWhereQuery($table_name, $where_array, $limit=1){
      $query = "SELECT * FROM `".$table_name."` ";
      $query .= "WHERE ";
      foreach ($where_array as $key => $value) {
        if (key($where_array) == $key ) {
          // We are at the first element
          $query .= $key."='".$value."'";
        } else {
          $query .= "AND ".$key."='".$value."'";
        }
      }
      $query .= " LIMIT ".$limit;
      return $query;
    }

    public static function returnUpdateQuery($table_name, $key_value_data, $where_array='') {
      $query = "UPDATE ".$table_name." SET ";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= $key."='".$value."'"; // where
        } else {
          $query .= $key."='".$value."',"; // where
        }
      }

      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }
      return $query;
    }
    public static function returnInsertData($table_name, $key_value_data, $where_array='') {
      $mysqli = Database::connect();
      $query = "INSERT INTO ".$table_name."(";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= "$key"; // where
        } else {
          $query .= "$key,"; // where
        }
      }
      $query .= " ) VALUES (";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= "'".$value."'"; // where
        } else {
          $query .= "'".$value."',"; // where
        }
      }
      // $query = substr_replace($query, "", -1);
      $query .= ")";
      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }
      // return $query;
      return $query;
    }

    static public function backUpDb() {
      date_default_timezone_set('Europe/Belgrade');
      $user=DB_USER;
      $pass=DB_PASS;
      $host=DB_HOST;
      $dbname= DB_NAME;

      $cmd='c:\wamp64\bin\mysql\mysql5.7.9\bin\mysqldump --user='.$user.' --password='.$pass .' --host='.$host.' '.$dbname.' > db/backup/'.date('"Y_m_d_H_i_s"').'_db_backup.sql';
      //var_dump($cmd);exit;
      exec($cmd, $output, $return);
      if ($return != 0) { //0 is ok
          die('Error: ' . implode("\r\n", $output));
      }

      echo "dump complete";
    }

}

// Database::connect(DB_HOST, DB_USER, DB_PASS);

$mysqli = new Database();
