<?php
/**
* Format datetime for inserting into databas = date("Y-m-d H:i:s")
*/
class Database{


// $mysqli = new mysqli("localhost", "my_user", "my_password", "world");

// /* check connection */
// if ($mysqli->connect_errno) {
//     printf("Connect failed: %s\n", $mysqli->connect_error);
//     exit();
// }

// $query = "SELECT Name, CountryCode FROM City ORDER by ID LIMIT 3";
// $result = $mysqli->query($query);

// /* numeric array */
// $row = $result->fetch_array(MYSQLI_NUM);
// printf ("%s (%s)\n", $row[0], $row[1]);

// /* associative array */
// $row = $result->fetch_array(MYSQLI_ASSOC);
// printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);

// /* associative and numeric array */
// $row = $result->fetch_array(MYSQLI_BOTH);
// printf ("%s (%s)\n", $row[0], $row["CountryCode"]);

// /* free result set */
// $result->free();

    private $mysqli;

    public static function connect(){
      date_default_timezone_set('Europe/Belgrade');
        try{
            return new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public static function query($query){
        $mysqli = Database::connect();
        $mysqli->query("SET NAMES utf8");
        return $mysqli->query($query);
    }

    // public static function fetch($query) {
    //   $mysqli = Database::connect();
    //   $mysqli->query("SET NAMES utf8");
    //   if ( $res = $mysqli->query($query) ) {
    //     while($row = $res->fetch_assoc()){
    //       $list[]=$row;
    //     }
    //     return $list;
    //   } else {
    //     return 0;
    //   }
    // }

    public static function fetch($query) {

      if ( $res = Database::query($query) ) {
        while($row = $res->fetch_assoc()){
          $list[]=$row;
        }
        return $list;
      } else {
        return 0;
      }
    }

    public static function whereQuery($table_name, $where_array='', $other='', $limit=''){
      $query = "SELECT * FROM `".$table_name."` ";
      if ( $where_array != '' && !empty($where_array) ) {
        $query .= "WHERE ";
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key."='".$value."'";
          } else {
            $query .= "AND ".$key."='".$value."'";
          }
        }
      }
      if ( $other != '' ) { $query .= " ".$other; }
      if ( $limit != '' ) { $query .= " LIMIT ".$limit; }

      $_SESSION['test2'] = $query;
      $res = Database::query($query);
      if ( $res->num_rows > 0 ) {
        while($row = $res->fetch_assoc()){
          $list[]=$row;
        }
        return $list;
      } else {
        return 0;
      }
    }


    public static function fetchData($table_name){
        $query = "SELECT * FROM `".$table_name."`";
        if ( $res = Database::query($query) ) {
          while($row = $res->fetch_assoc()){
            $list[]=$row;
          }
          return $list;
        } else {
          return 0;
        }
    }

    public static function fetchPagData($table_name,$start,$item_per_page){
        $query = "SELECT * FROM `".$table_name."` LIMIT ".$start.",".$item_per_page;
        if ( $res = Database::query($query) ) {
          while($row = $res->fetch_assoc()){
            $list[]=$row;
          }
          return $list;
        } else {
          return 0;
        }
    }

    public static function returnFetchData($table_name){
        $query = "SELECT * FROM `".$table_name."`";
        return $query;
    }

    public static function insert_data($table_name, $key_value_data, $where_array='') {

      if ( ( isset($table_name) && $table_name != '' ) && ( isset($key_value_data) && $key_value_data != '' ) ) {
        $mysqli = Database::connect();
        $query = "INSERT INTO ".$table_name."";

        if ( isset($key_value_data[0]) && is_array($key_value_data[0]) ) {
          // Helper::pre('Multy : '.Database::insertMultyStringQuery($key_value_data));
          $query .= Database::insertMultyStringQuery($key_value_data);
          // die('Array je');
        } else {
          // Helper::pre('Single : '.Database::insertSingleStringQuery($key_value_data));
          $query .= Database::insertSingleStringQuery($key_value_data);
          // die('Nije array');
        }


        if ( $where_array != '' ) {
          $query .= ' WHERE ';
          foreach ($where_array as $key => $value) {
            if (key($where_array) == $key ) {
              // We are at the first element
              $query .= $key." ='".$value."'";
            } else {
              $query .= " AND ".$key." ='".$value."'";
            }
          }
        }
        // Helper::pre($query);
        // die('radiiii');

        $_SESSION['data_query'] = $query;

        // return $query;

        if ( $result = $mysqli->query($query) ) {
          $last_id =  $mysqli->insert_id;
          return $last_id;
        } else {
          return false;
        }

      } else {
        return false;
      }

    }

    private static function insertMultyStringQuery($key_value_data) {
      $query = '(';
      foreach ($key_value_data[0] as $key => $value) {

        reset($key_value_data[0]);
        $first_key = key($key_value_data[0]);
        next($key_value_data[0]);

        if ( $first_key == $key ) {
          $query .= '';
        } else {
          $query .= "'".$key."',"; // where
        }
      };

      $query = rtrim($query, ',');

      $query .= " ) VALUES ";

      foreach ($key_value_data as $key => $value) {

        reset($key_value_data);
        $current_key = key($key_value_data);
        next($key_value_data);

        $query .= ' ( ';
        $new_for_value = $value;

        foreach ($new_for_value as $key => $value) {

          if ( key($new_for_value) == $key ) {
            $query .= '';
          } else {
            $query .= "'".$value."',"; // where
          }
        }
        $query = rtrim($query, ',');
        $query .= "),";

      }
      $query = rtrim($query, ',');

      return $query;
    }

    private static function insertSingleStringQuery($key_value_data) {
      $query = '(';
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)=="'".$value."'" ) {
          $query .= "$key"; // where
        } else {
          $query .= "$key,"; // where
        }
      }
      $query = rtrim($query, ',');
      $query .= " ) VALUES (";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( $value == '' || $value == NULL )  { $value = 'NULL'; }
        if ( end($key_value_data)=="'".$value."'" ) {
          $query .= "'".$value."'"; // where
        } else {
          $query .= "'".$value."',"; // where
        }
      }
      $query = rtrim($query, ',');
      $query .= ")";

      return $query;
    }

    public static function return_last_iserted_id() {
      $mysqli = Database::connect();
      return $mysqli->insert_id;
    }

    public static function updateData($table_name, $key_value_data, $where_array='') {
      $mysqli = Database::connect();
      $query = "UPDATE ".$table_name." SET ";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)=="'".$value."'" ) {
          $query .= $key."='".$value."'"; // where
        } else {
          $query .= $key."='".$value."',"; // where
        }
      }
      $query = rtrim($query, ',');

      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }
      $_SESSION['data_query'] = $query;

      if ( $result = $mysqli->query($query) ) {
        $_SESSION['success'] = 'success';
        return true;
      } else {
        $_SESSION['success'] = 'Error';
        return false;
      }
      // return $query;
    }

    public static function num_rows($query){
        $mysqli = Database::connect();
        $mysqli->query("SET NAMES utf8");
        if ( $res = $mysqli->query($query) ){
          return $res->num_rows;
        } else {
          return 0;
        }
    }

    public static function insertData(){
        $mysqli = Database::connect();
        $stmt = $mysqli->prepare("INSERT INTO movies(filmName,filmDescription,filmImage,filmPrice,filmReview) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('sssdi', $_POST['filmName'],
        $_POST['filmDescription'],
        $_POST['filmImage'],
        $_POST['filmPrice'],
        $_POST['filmReview']);
        $stmt->execute();
        $newId = $stmt->insert_id;
        $stmt->close();
    }

    public static function update_data(){
        $stmt = $mysqli->prepare("UPDATE movies SET filmName = ?,
           filmDescription = ?,
           filmImage = ?,
           filmPrice = ?,
           filmReview = ?
           WHERE filmID = ?");
        $stmt->bind_param('sssdii',
           $_POST['filmName'],
           $_POST['filmDescription'],
           $_POST['filmImage'],
           $_POST['filmPrice'],
           $_POST['filmReview'],
           $_POST['filmID']);
        $stmt->execute();
        $stmt->close();
    }

    public static function deleteDataExtra(){
        $stmt = $mysqli->prepare("DELETE FROM movies WHERE filmID = ?");
        $stmt->bind_param('i', $_POST['filmID']);
        $stmt->execute();
        $stmt->close();
    }

    public static function deleteData($table_name, $where_array='', $other='', $limit=''){
      $query = "DELETE FROM `".$table_name."` ";
      if ( $where_array != '' ) {
        $query .= "WHERE ";
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key."='".$value."'";
          } else {
            $query .= "AND ".$key."='".$value."'";
          }
        }
      }
      if ( $other != '' ) { $query .= " ".$other; }
      if ( $limit != '' ) { $query .= " LIMIT ".$limit; }

      if ( Database::query($query) ) {
        return 1;
      } else {
        return 0;
      }
    }

    public static function returnDeleteData($table_name, $where_array='', $other='', $limit=''){
      $query = "DELETE FROM `".$table_name."` ";
      if ( $where_array != '' ) {
        $query .= "WHERE ";
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key."='".$value."'";
          } else {
            $query .= "AND ".$key."='".$value."'";
          }
        }
      }
      if ( $other != '' ) { $query .= " ".$other; }
      if ( $limit != '' ) { $query .= " LIMIT ".$limit; }

      return $query;
    }


    public static function return_errors() {
      $mysqli = Database::connect();
      return  $mysqli->error;
      // print_r($mysqli->error);
    }

    public static function returnQuery($query){
      return $query;
    }

    public static function returnWhereQuery($table_name, $where_array, $other='', $limit=''){
      $query = "SELECT * FROM `".$table_name."` ";
      if ( $where_array != '' && !empty($where_array) ) {
        $query .= "WHERE ";
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key."='".$value."'";
          } else {
            $query .= " AND ".$key."='".$value."'";
          }
        }
      }
      if ( $other != '' ) { $query .= " ".$other; }
      if ( $limit != '' ) { $query .= " LIMIT ".$limit; }
      return $query;
    }

    public static function returnUpdateData($table_name, $key_value_data, $where_array='') {
      $query = "UPDATE `".$table_name."` SET ";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)==$value ) {
          $query .= $key."='".$value."'"; // where
        } else {
          $query .= $key."='".$value."',"; // where
        }
      }

      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }
      return $query;
    }
    public static function returnInsertData($table_name, $key_value_data, $where_array='') {
      $mysqli = Database::connect();
      $query = "INSERT INTO ".$table_name."(";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)=="'".$value."'" ) {
          $query .= "$key"; // where
        } else {
          $query .= "$key,"; // where
        }
      }
      $query = rtrim($query, ',');
      $query .= " ) VALUES (";
      foreach ($key_value_data as $key => $value) {
        # code...
        if ( end($key_value_data)=="'".$value."'" ) {
          $query .= "'".$value."'"; // where
        } else {
          $query .= "'".$value."',"; // where
        }
      }
      $query = rtrim($query, ',');
      $query .= ")";
      if ( $where_array != '' ) {
        $query .= ' WHERE ';
        foreach ($where_array as $key => $value) {
          if (key($where_array) == $key ) {
            // We are at the first element
            $query .= $key." ='".$value."'";
          } else {
            $query .= " AND ".$key." ='".$value."'";
          }
        }
      }


      $_SESSION['data_query'] = $query;

      // return $query;

      return $query;

    }

    static public function backUpDb() {
      date_default_timezone_set('Europe/Belgrade');
      $user=DB_USER;
      $pass=DB_PASS;
      $host=DB_HOST;
      $dbname= DB_NAME;

      $cmd='C:/wamp64/bin/mysql/mysql5.7.11/bin/mysqldump --user='.$user.' --password='.$pass .' --host='.$host.' '.$dbname.' > db/backup/'.date('"Y_m_d_H"').'_db_backup.sql';
      //var_dump($cmd);exit;
      if ( file_exists('db/backup/'.date('"Y_m_d_H"').'_db_backup.sql') ) {
        return false;
      } else {
        exec($cmd, $output, $return);
        if ($return != 0) { //0 is ok
          // die('Error: ' . implode("\r\n", $output));
          return false;
        }

        return true;
      }
    }

}

// Database::connect(DB_HOST, DB_USER, DB_PASS);

$mysqli = new Database();
