<?php

/**
 *
 */
class Helper {


  /*
  * return difference from two times
  */
  public static function DateTimeDiff($timeStart, $timeEnd) {

    # 2. Next convert the string to a date variable
    $dteStart = new DateTime($timeStart);
    $dteEnd   = new DateTime($timeEnd);

    # 3. Calculate the difference
    $dteDiff  = $dteStart->diff($dteEnd);

    # 4. Format the output
    return $dteDiff->format("%H:%I:%S");
  }


  public static function returnSecondsFromTimeString($timeString, $fullDate=false) {
    if ( $fullDate == true ) {

    } else {
      $time_parts = explode(':', $timeString);

      $seconds = $time_parts[0] * 3600 + $time_parts[1] *60 + $time_parts[2];
    }
    return $seconds;
  }

  public static function hoursMinutesSeconds($timeSeconds, $format=false) {
    if ( $format == true ) {

    } else {
      $hours = floor($timeSeconds / 3600);
      $mins = floor($timeSeconds / 60 % 60);
      $secs = floor($timeSeconds % 60);
      if ( $hours < 10 ) {
        $hours = '0'.$hours;
      }
      if ( $mins < 10 ) {
        $mins = '0'.$mins;
      }
      if ( $secs < 10 ) {
        $secs = '0'.$secs;
      }

      $return = $hours.':'.$mins.':'.$secs;
    }
    return $return;
  }

  public static function returnCountThisHourHints($working_day_session_id) {
    $now_datatime = date("Y-m-d H").':00:00';
    $modify_datetime = date("Y-m-d H",strtotime('+1 hour')).':00:00';
    $satni_ucinak_query = "SELECT id FROM `working_day_session_details` WHERE working_day_session_id ='".$working_day_session_id."' AND vreme_operacije > '".$now_datatime."' AND vreme_operacije < '".$modify_datetime."' AND hint_code='F001'";
    $hinta_per_hour_this_hours = Database::num_rows($satni_ucinak_query);
    return $hinta_per_hour_this_hours;
  }

  public static function pre($what){
    echo '<pre>';
    print_r($what);
    echo '</pre>';
  }

  public static function js($js_input){
    echo '<script type="text/javascript">'.$js_input.'</script>';
  }

  public static function progressBar($percent) {
    $o = '';

    $o .= '<div class="progress">';
    $o .= ' <div class="progress-bar" role="progressbar" aria-valuenow="'.$percent.'"';
    $o .= ' aria-valuemin="0" aria-valuemax="100" style="width:'.$percent.'%">';
    $o .= '   <span class="sr-only">'.$percent.'% Complete</span>';
    $o .= ' </div>';
    $o .= '</div>';
    return $o;
  }

  public static function returnIcon($icon){
    return '<span class="glyphicon glyphicon-'.$icon.'" aria-hidden="true"></span>';
  }


  public static function custom_script($msg) {
    return '<script type="text/javascript" charset="utf-8" async defer> '.$msg.' </script>';
  }

}
