<?php

/**
 *
 */
class Table {

  public static function returnListDataTable( $table_head='', $table_body='', $url_array='' ) {

    // This is if data is returned from database with "$komesa_list = Database::fetchData('komesa')"
    if ( ($table_head != '') && ($table_body != '') ) {
      $o = '';
      $o .= '<div class="panel">';
      $o .= '<div class="table-responsive">';
      $o .= '<table class="table table-stripedd  table-hover">';
      if ( $table_head != '' ) {

        $o .= '<thead>';
        $o .=   '<tr>';
        foreach ($table_head as $item ) {
          $o .= '<th>'.$item.'</th>';
        }
        $o .=   '</tr>';
        $o .= '</thead>';
      }

      if ($table_body != '' ) {

        $o .= '<tbody>';
        foreach ($table_body as $item ) {
          $o .= '<tr class="table_row">';
          foreach ($item as $key => $value) {
            $o .= '<td';
            if ( $key == 'id' ) {
              $o .= ' class="row_id"';
            }
            $o .= '>'.$item[$key].'</td>';
          }
          if ( $url_array != '' )  {
            $o .= '<td>';
            foreach ($url_array as $key => $value) {
              if (strpos($key, 'Edit') !== false) {
                $o .= '<a title="'.$key.'" href="'.$value.'&id='.$item['id'].'" class="btn btn-success wrap-right">';
                $o .= '<i class="fa fa-pencil-square-o"></i>';
                $o .= '</a>';
              } elseif (strpos($key, 'Details') !== false) {
                $o .= '<a title="'.$key.'" href="'.$value.'&id='.$item['id'].'" class="btn btn-info wrap-right">';
                $o .= '<i class="fa fa-list"></i>';
                $o .= '</a>';
              } elseif (strpos($key, 'Delete') !== false) {
                $o .= '<a title="'.$key.'" href="'.$value.'&id='.$item['id'].'" class="btn btn-danger wrap-right">';
                $o .= '<i class="fa fa-trash"></i>';
                $o .= '</a>';
              }

            }
            $o .= '</td>';
          }
          $o .= '</tr>';
        }
        $o .= '</tbody>';
      }
      $o .= '</table>';
      $o .= '</div>';

      $o .= '</div>';

      return $o;
    } else {
      return '<div class="alert alert-danger">No table data inserted</div>';
    }
  }

  public static function returnTableHead( $table_head ){

  }

  public static function returnListDataTableBody(){

  }

}
