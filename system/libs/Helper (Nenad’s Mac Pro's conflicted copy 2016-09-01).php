<?php

/**
 *
 */
class Helper {


  /*
  * return difference from two times
  */
  public static function DateTimeDiff($timeStart, $timeEnd) {

    # 2. Next convert the string to a date variable
    $dteStart = new DateTime($timeStart);
    $dteEnd   = new DateTime($timeEnd);

    # 3. Calculate the difference
    $dteDiff  = $dteStart->diff($dteEnd);

    # 4. Format the output
    return $dteDiff->format("%H:%I:%S");
  }


  public static function returnSecondsFromTimeString($timeString, $fullDate=false) {
    if ( $fullDate == true ) {

    } else {
      $time_parts = explode(':', $timeString);

      $seconds = $time_parts[0] * 3600 + $time_parts[1] *60 + $time_parts[2];
    }
    return $seconds;
  }

  public static function returnCountThisHourHints() {
    $now_datatime = date("Y-m-d H").':00:00';
    $modify_datetime = date("Y-m-d H",strtotime('+1 hour')).':00:00';
    $satni_ucinak_query = "SELECT id FROM `working_day_session_details` WHERE vreme_operacije > '".$now_datatime."' AND vreme_operacije < '".$modify_datetime."'";
    $hinta_per_hour_this_hours = Database::num_rows($satni_ucinak_query);
    return $hinta_per_hour_this_hours;
  }


}
