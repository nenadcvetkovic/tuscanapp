<?php

/**
 *
 */
class Ip  {

  static public function getUserRealIpAdress() {
      $ipaddress = '';
      if (getenv('HTTP_CLIENT_IP')) {
          $ipaddress = getenv('HTTP_CLIENT_IP');
      } else if(getenv('HTTP_X_FORWARDED_FOR')) {
          $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
      } else if(getenv('HTTP_X_FORWARDED')) {
          $ipaddress = getenv('HTTP_X_FORWARDED');
      } else if(getenv('HTTP_FORWARDED_FOR')) {
          $ipaddress = getenv('HTTP_FORWARDED_FOR');
      } else if(getenv('HTTP_FORWARDED')) {
         $ipaddress = getenv('HTTP_FORWARDED');
      } else if(getenv('REMOTE_ADDR')) {
          $ipaddress = getenv('REMOTE_ADDR');
      } else {
          $ipaddress = 'UNKNOWN';
      }
      if ($ipaddress == '::1') {
        return '127.0.0.1';
      } else {
        return  $ipaddress;
      }
  }

  // convert real ip adress to ip for checking
  static public function Dot2LongIP ($IPaddr) {
   if ($IPaddr == "") {
     return 0;
   } else {
     $ips = explode(".", $IPaddr);
     return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
   }
  }
}
