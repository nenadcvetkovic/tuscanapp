<?php

/**
*
*/
class Url{

	static public function header_status($location){
		if(!headers_sent()){
			header('location: '.$location);
		}else{
			?>
			<script type="text/javascript">window.location.replace("<?php echo $location; ?>");</script>
			<?php
		}
	}

	static public function returnCurrentPageUrl(){
		return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
}
