<?php

/**
*
*/
class Pagination{

	static public function displayPagination($count, $limit, $prev, $next, $ppage=null, $link=''){
		$o = '';
		$new_link = explode('?', $link);

		// create $_GET returning loink
		if ( isset($_GET) ) {
			$url = '';
			$get_var = $_GET;
			foreach ($get_var as $key => $value) {
				if ( $key == 'ppage') {

				} else {
					if (key($get_var) == $key ) {
						// We are at the first element
						$url .= '?'.$key.'='.$value;
					} else {
						$url .= '&'.$key.'='.$value;
					}
				}
			}
			$url .= '&ppage=';
		}

		$o .= '<div class="breadcrumb col-xs-12">';

		$o .= '<div class="col-xs-9">';
		$o .= '  <ul class="pagination text-center">';
		$o .= '    <li>';
		$o .= '      <a href="';
		if($ppage!=null && $ppage>1) { $o .= $new_link[0].$url.($ppage-1); }else{ $o .= '#';}
		$o .= '" aria-label="Previous">';
		$o .= '        <span aria-hidden="true">&laquo; Predhodna</span>';
		$o .= '      </a>';
		$o .= '    </li>';

		$max = ceil($count/$limit);
		for ($i=1; $i <= $max; $i++) {
			if ( $i == $ppage-2 || $i == $ppage-1 || $i == $ppage || $i == $ppage+2 || $i == $ppage+1 ) {
				$o .= '<li';
				if(isset($_GET['ppage']) && $_GET['ppage']==$i){ $o .= ' class="active"'; }
				$o .= '>';
				$o .= '<a href="'.$new_link[0].$url.$i.'">'.$i.'</a>';
				$o .= '</li>';
			}
		}
		$o .= '    <li>';
		$o .= '      <a href="';
		if($ppage!=null && $ppage<$max) { $o .= $new_link[0].$url.($ppage+1); }else{ $o .= '#';}
		$o .= '" aria-label="Next">';
		$o .= '        <span aria-hidden="true">Sledeca &raquo;</span>';
		$o .= '      </a>';
		$o .= '    </li>';
		$o .= '  </ul>';

		$o .= '</div>';

		// $o .= '<div class="col-xs-3 wrap wrap-top">';
		//
		// $o .= '<div class="dropdown wrap-top">';
		// $o .= '  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
		// isset($ppage) ? $o .= ' Page No '.$ppage : $o .= 'Izaberite stranicu';
		// $o .= '    <span class="caret"></span>';
		// $o .= '  </button>';
		// $o .= '  <ul class="pull-right dropdown-menu" aria-labelledby="dropdownMenu1" style="height:200px; overflow:scroll;">';
		// for ($i=1; $i <= $max; $i++) {
		// 	// if ( $i == $ppage-2 || $i == $ppage-1 || $i == $ppage || $i == $ppage+2 || $i == $ppage+1 ) {
		// 	$o .= '<li';
		// 	if(isset($_GET['ppage']) && $_GET['ppage']==$i){ $o .= ' class="active"'; }
		// 	$o .= '>';
		// 	$o .= '<a href="'.$new_link[0].$url.$i.'">'.$i.'</a>';
		// 	$o .= '</li>';
		// 	// }
		// }
		// // $o .= '    <li><a href="#">Action</a></li>';
		// // $o .= '    <li><a href="#">Another action</a></li>';
		// // $o .= '    <li><a href="#">Something else here</a></li>';
		// // $o .= '    <li role="separator" class="divider"></li>';
		// // $o .= '    <li><a href="#">Separated link</a></li>';
		// $o .= '  </ul>';
		// $o .= '</div>';
		//
		// $o .= '</div>';

		// selected
		// $o .= '<select>';
		// for ($i=1; $i <= $max; $i++) {
		// 	// if ( $i == $ppage-2 || $i == $ppage-1 || $i == $ppage || $i == $ppage+2 || $i == $ppage+1 ) {
		// 	$o .= '<option';
		// 	if(isset($_GET['ppage']) && $_GET['ppage']==$i){ $o .= ' selected="selected"'; }
		// 	$o .= '>';
		// 	$o .= '<a href="'.$new_link[0].$url.$i.'">'.$i.'</a>';
		// 	$o .= '</option>';
		// 	// }
		// }
		// $o .= '</select>';
		//

		$o .= '</div>';

		$o .= '<div class="clear"></div>';

		return $o;

	}


}
