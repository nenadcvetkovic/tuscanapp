<?php

/**
 *
 */
class Helper {


  /*
  * return filterbox
  */
  public static function filter_box() {

    // filter bulk_edit_custom_box
    <div class="filter-container alert-info">

      <form action="<?php echo ADMINURL; ?>" method="get">
        <input name="page" value="analiza" hidden="hidden"/>
        <div class="container">
          <div class="row">
            <!-- <span class=""> Izaberi datum </span></br> -->
            <div class="col-xs-12">
              <div class="filter-box">
                <input class="col-xs-12 col-sm-3 col-md-3 col-lg-2" type="text" name="date" id="datepicker-1" value="<?php echo (isset($_GET['date']) ? $_GET['date'] : '' ); ?>" placeholder="Izaberite datum">
                <!-- Vreme OD -->
                <!-- <span  class="">Vreme od</span> -->
                <select class="col-xs-6 col-sm-3 col-md-3 col-lg-2"  id="start_time" name="start_time">
                  <option value="0"<?php if ( isset($_GET['start_time']) && $_GET['start_time'] == 7 ) echo 'selected="selected"'; ?>>Vreme OD</option>
                  <option value="00">00</option>
                  <?php
                  for ($i=1; $i < 24; $i++) {
                    echo '<option class="" value="'.$i.'"';
                    if ( isset($_GET['start_time']) && $_GET['start_time'] == $i ) echo 'selected="selected"';
                    echo '>'.$i.'</option>';
                  }
                  ?>
                </select>
                <!-- Vreme DO -->
                <!-- <span  class="">Vreme do</span> -->
                <select class="col-xs-6 col-sm-3 col-md col-lg-2" name="end_time">
                  <option value="0"<?php if ( isset($_GET['end_time']) && $_GET['end_time'] == 15 ) echo 'selected="selected"'; ?>>Vreme DO</option>
                  <option value="00">00</option>
                  <?php
                  for ($i=1; $i < 24; $i++) {
                    echo '<option class="" value="'.$i.'"';
                    if ( isset($_GET['end_time']) && $_GET['end_time'] == $i ) echo 'selected="selected"';
                    echo '>'.$i.'</option>';
                  }
                  ?>
                </select>
              </div>
              <?php
              if ( true )  { // if isset $_GET['komesa']
                echo '<select name="komesa" class="select-komesa col-xs-9 col-sm-6 col-md-3 col-lg-2">';
                if( isset($_GET['komesa']) && $_GET['komesa'] != '0' ) {
                  echo '<option ';
                  echo 'selected="selected"';
                  echo ' value="'.$_GET['komesa'].'">'.$_GET['komesa'].'</option>';
                } else {
                  echo '<option name="komesa"> Izaberite datum </option>';
                }
                echo '</select>';
              }
              ?>
              <div class="input-style-btn wrap clear komesa-filter-btn btn btn-primary col-xs-3 col-sm-3 col-md-3 col-lg-2">Change komesa</div>

              <!-- Radnici -->
              <?php $radnici = Database::fetchData('radnici_lista'); ?>
              <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="radnik">
                <option value="0"<?php if ( isset($_GET['radnik']) && $_GET['radnik'] == 0 ) echo 'selected="selected"'; ?>>Izaberi radnika</option>
                <?php
                foreach ($radnici as $radnik) {
                  echo '<option value="'.$radnik['id'].'"';
                  if ( isset($_GET['radnik']) && $_GET['radnik'] == $radnik['id'] ) {
                    echo 'selected="selected"';
                  }
                  echo '>ID:'.$radnik['id'].' | '.$radnik['radnik_name'].'</option>';
                }
                ?>
              </select>

              <!-- Linija -->
              <?php $sve_linije = Database::fetchData('linija'); ?>
              <select class="col-xs-12 col-sm-6 col-md-3 col-lg-2" name="linija">
                <option value="0"<?php if ( isset($_GET['linija']) && $_GET['linija'] == 0 ) echo 'selected="selected"'; ?>>Izaberi liniju</option>
                <?php
                foreach ($sve_linije as $linija) {
                  echo '<option value="'.$linija['id'].'"';
                  if ( isset($_GET['linija']) && $_GET['linija'] == $linija['id'] ) {
                    echo 'selected="selected"';
                  }
                  echo '">ID:'.$linija['id'].' | '.$linija['linija_name'].'</option>';
                }
                ?>
              </select>
              <div class="clear"></div>

              <hr class="delimiter">

              <input class="btn btn-info wrap" type="submit" name="select" value="Display Data" />
              <!-- <input class="btn btn-warning wrap" type="reset" name="reset" value="Reset Filter" /> -->
              <a class="btn btn-warning wrap" href="<?php echo ADMINURL; ?>?page=analiza">Reset Filter</a>
            </div>
          </div>
        </div>
      </form>

    </div> <!-- filter-box -->
    <div class="filter-btn-visible btn btn-success wrap">Show filter options</div>
    <?php

      // end filter box


  }


}
