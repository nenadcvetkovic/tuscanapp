<?php


/**
 *
 */
class Messages {

  public static function infoErrorMsg()
  {
    $o = '';
    if ( isset($_SESSION['errors']['error_page_msg']) && $_SESSION['errors']['error_page_msg'] != '' ) {
      $error_page_msg = $_SESSION['errors']['error_page_msg'];
      $_SESSION['errors']['error_page_msg'] = '';
      unset($_SESSION['errors']['error_page_msg']);
      $o .= '<div class="alert alert-danger">';
      $o .= $error_page_msg;
      $o .= '</div>';
      return $o;
      // unset($session['error_page_msg']);
    } elseif ( isset($_SESSION['errors']['info_page_msg']) && $_SESSION['errors']['info_page_msg'] != '' ) {
      $info_page_msg = $_SESSION['errors']['info_page_msg'];
      $_SESSION['errors']['info_page_msg'] = '';
      unset($_SESSION['errors']['info_page_msg']);
      $o .= '<div class="alert alert-info">';
      $o .= $info_page_msg;
      $o .= '</div>';
      return $o;
      // unset($_SESSION['info_page_msg']);
    }
  }
}
