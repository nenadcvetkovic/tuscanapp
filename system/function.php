<?php

// we've writen this code where we need
function __autoload($classname) {
  if ( is_array($classname) ) {
    foreach ($classname as $class) {
      if ( file_exists("./system/libs/". $class .".php") ) {
        include_once("./system/libs/". $class .".php");
      } elseif (file_exists("../system/libs/". $class .".php")) {
        include_once("../system/libs/". $class .".php");
      } elseif (file_exists("../system/libs/". $class .".php")) {
        include_once("../../system/libs/". $class .".php");
      } elseif (file_exists(DIR."/system/libs/". $class .".php")) {
        include_once(DIR."/system/libs/". $class .".php");
      } 

    }
  } else {
    if ( file_exists("./system/libs/". $classname .".php") ) {
      include_once("./system/libs/". $classname .".php");
    } elseif (file_exists("../system/libs/". $classname .".php")) {
      include_once("../system/libs/". $classname .".php");
    } elseif (file_exists("../system/libs/". $classname .".php")) {
      include_once("../../system/libs/". $classname .".php");
    } elseif (file_exists(DIR."/system/libs/". $classname .".php")) {
      include_once(DIR."/system/libs/". $classname .".php");
    } 
    // $filename = "./". $classname .".php";
    // include_once($filename);
  }
}

function get_options($option_name){
  if ( Database::num_rows(Database::returnWhereQuery('tuscan_family_options', array('option_name'=>$option_name))) > 0 ) {
    $opt = Database::whereQuery('tuscan_family_options', array('option_name'=>$option_name));
    $option_value = $opt[0]['option_value'];
  } else {
    $option_value = 0;
  }

  return $option_value;
}


    // $today = date("j-F-Y");                          // 10-March-2001
    // $today = date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
    // $today = date("m.d.y");                         // 03.10.01
    // $today = date("j, n, Y");                       // 10, 3, 2001
    // $today = date("Ymd");                           // 20010310
    // $today = date('h-i-s, j-m-y, it is w Day');     // 05-16-18, 10-03-01, 1631 1618 6 Satpm01
    // $today = date('\i\t \i\s \t\h\e jS \d\a\y.');   // it is the 10th day.
    // $today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001
    // $today = date('H:m:s \m \i\s\ \m\o\n\t\h');     // 17:03:18 m is month
    // $today = date("H:i:s");                         // 17:16:18


function display_date($date, $delimiter){
  $timestamp = strtotime($date);
  $newDate = date('d'.$delimiter.'m'.$delimiter.'Y', $timestamp);
  return $newDate;
}


function get_days_for_this_month ( $month, $year ) {
  return cal_days_in_month(CAL_GREGORIAN, $month, $year);
}

function work_day_for_month ( $year, $month, $day = false ) {

  if ( $day == false ) {
    $max_date = get_days_for_this_month($month, $year);
  } else {
    $max_date = $day;
  }

  $list = array();
  for ( $start=1; $start<=$max_date; $start++ ) {

    $monthday = explode(',', date('t,N', strtotime($year.'-'.$month.'-'.$start)));
    if ( $monthday[1] == '6' || $monthday[1]=='7' ) {

    } else {
      array_push($list, $start);  
    }
  }

  return $list;

}

function return_class_eff($eff){
  switch( $eff ) {
    case ( $eff > 90 ):
      return 'green';
      break;
    case ( $eff < 90 ) && ( $eff > 80 ):
      return 'yellow';
      break;
    case ( $eff < 80 ) && ( $eff > 70 ):
      return 'orange';
      break;
    case ( $eff < 70 ) && ( $eff >= 0 ):
      return 'red';
      break;
    default:
      return 'default';
    break;
  }

}
