<?php
define('TIMEZONE', 'Europe/Belgrade');
date_default_timezone_set('Europe/Belgrade');
// define constants for aplication

// define('DB_HOST','localhost');
// define('DB_USER','root');
// define('DB_PASS','root');
// define('DB_NAME','tuskan_family');

 define('DB_HOST','localhost');
 define('DB_USER','root');
 define('DB_PASS','mysql');
 define('DB_NAME','tuscan_family_2017');


// $url = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
// define('BASEURL', $url.'');
// define root path for aplication

// $siteurl = get_options('siteurl');
// define('BASEURL', $siteurl);
// define('BASEURL', 'http://192.168.1.9/tuscan/');
// 
// define('BASEURL', 'http://localhost/tuscan/');
define('BASEURL', 'http://192.168.1.200/tuscan/');
define('BASEIP', '127.0.0.10');

// ddefine admin Url
define('ADMINURL', BASEURL.'admin/');

// error pages
define('ADMIN_ERROR_PAGE', ADMINURL.'?page=404');

// ddefine js Url
define('JSURL', BASEURL.'template/js/');
define('ADMINJSURL', ADMINURL.'template/js/');

// ddefine js Url
define('CSSURL', BASEURL.'template/css/');
define('ADMINCSSURL', ADMINURL.'template/css/');

define('DIR', realpath(__DIR__ . '/../..'));

